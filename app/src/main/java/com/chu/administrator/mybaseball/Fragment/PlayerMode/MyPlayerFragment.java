package com.chu.administrator.mybaseball.Fragment.PlayerMode;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chu.administrator.mybaseball.Fragment.BaseFragment;
import com.chu.administrator.mybaseball.Fragment.TeamDetailFragment;
import com.chu.administrator.mybaseball.Model.ProposalItem;
import com.chu.administrator.mybaseball.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-07-16.
 */

public class MyPlayerFragment extends BaseFragment{

    Button mailBtn;
    ArrayList<ProposalItem> proposalItemArrayList;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = false;
        return inflater.inflate(R.layout.my_player_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        proposalItemArrayList = new ArrayList<>();

        mailBtn = (Button) view.findViewById(R.id.mailBtn);
        mailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupMailWindow();
            }
        });
        mailBtn.setEnabled(false);
        getProposalItems();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initFragment(){
        mailBtn.setEnabled(true);
        setMailNew();
    }

    private void setMailNew(){
        mailBtn.setText("Nothing");
        for(ProposalItem proposalItem : proposalItemArrayList){
            Log.e("j13 ",""+proposalItem.toString());
            if (proposalItem.isNew()){
                mailBtn.setText("NEW");
            }
        }
    }

    private void getProposalItems(){
        bInstance.showProgressBar();

        bInstance.getDb().collection("proposal").document(bInstance.getUID()).collection("hire").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                String deviceId;
                String message;
                String teamName;
                String teamUid;
                boolean isNew;
                for (DocumentSnapshot document : documentSnapshots) {
                    if (document.exists()) {
                        deviceId = document.get("deviceId").toString();
                        message = document.get("message").toString();
                        teamName = document.get("teamName").toString();
                        teamUid = document.get("teamUid").toString();
                        isNew = document.get("isNew").toString().equals("true");
                        proposalItemArrayList.add(new ProposalItem(deviceId,message,teamName,teamUid,isNew));
                    }
                }
                bInstance.hideProgressBar();
                initFragment();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
            }
        });
    }

    Dialog dialog;
    private void popupMailWindow(){
        dialog = new Dialog(getContext(), R.style.generalDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.mail_window);

        RecyclerView mailRV;
        RecyclerView.Adapter mailAdapter;
        Button cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
        mailRV = (RecyclerView) dialog.findViewById(R.id.mailRV);
        mailAdapter = new MailAdapter(proposalItemArrayList);
        mailRV.setAdapter(mailAdapter);
        mailRV.setLayoutManager(new GridLayoutManager(getActivity(),1));

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                setMailNew();
            }
        });
    }

    private void updateOldMessage(){
        for(final ProposalItem proposalItem : proposalItemArrayList){
            if (proposalItem.isNew()){
                bInstance.showProgressBar();
                bInstance.getDb().collection("proposal").document(bInstance.getUID()).collection("hire").document(proposalItem.getTeamUid()).update("isNew",false).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e("j13 ","success uploading read");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("j13 ","fail uploading read");
                    }
                });
                bInstance.hideProgressBar();
            }
        }
    }

    private void updateSingleMessageRead(String uid){
        bInstance.showProgressBar();
        bInstance.getDb().collection("proposal").document(bInstance.getUID()).collection("hire").document(uid).update("isNew",false).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.e("j13 ","success uploading single read");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("j13 ","fail uploading single read");
                }
            });
            bInstance.hideProgressBar();
    }



    public class MailAdapter extends RecyclerView.Adapter<MailAdapter.CustomViewHolder> {
        private ArrayList<ProposalItem> mItems;

        public MailAdapter(ArrayList<ProposalItem> mItems) {
            this.mItems = mItems;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mail_item,null);
            CustomViewHolder holder = new CustomViewHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            holder.messageTV.setText(mItems.get(position).getMessage());
            holder.teamNameTV.setText(mItems.get(position).getTeamName());
            if (mItems.get(position).isNew()){
                holder.newIV.setBackgroundResource(R.drawable.closed_mail);
            }else {
                holder.newIV.setBackgroundResource(R.drawable.open_mail);
            }
            holder.messageTV.getLayoutParams().height = 150;
            holder.newIV.getLayoutParams().height = 150;
            holder.teamNameTV.getLayoutParams().height = 150;
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public TextView messageTV,teamNameTV;
            public ImageView newIV;

            public CustomViewHolder(View view) {
                super(view);
                messageTV = (TextView) view.findViewById(R.id.messageTV);
                teamNameTV = (TextView) view.findViewById(R.id.teamNameTV);
                newIV = (ImageView) view.findViewById(R.id.newIV);

                messageTV.setOnClickListener(this);
                teamNameTV.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.messageTV || view.getId() == R.id.teamNameTV){

                    String teamUid = proposalItemArrayList.get(getLayoutPosition()).getTeamUid();
                    updateSingleMessageRead(teamUid);
                    dialog.dismiss();
                    proposalItemArrayList.get(getLayoutPosition()).setNew(false);
                    bInstance.addFragment(new TeamDetailFragment(teamUid,true));
                }
            }
        }
    }
}
