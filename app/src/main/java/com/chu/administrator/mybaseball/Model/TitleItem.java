package com.chu.administrator.mybaseball.Model;

/**
 * Created by Administrator on 2018-07-14.
 */

public class TitleItem {
    String season;
    int title;

    public TitleItem(String season, int title) {
        this.season = season;
        this.title = title;
    }

    public String getSeason() {
        return season;
    }

    public int getTitle() {
        return title;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public void setTitle(int title) {
        this.title = title;
    }
    public String getAllTitle(){
        return season+" " + GlobalValue.getTitleString(title);
    }
}
