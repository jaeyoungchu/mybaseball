package com.chu.administrator.mybaseball.Fragment.ManagerMode;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chu.administrator.mybaseball.Adapter.AwardAdapter;
import com.chu.administrator.mybaseball.BaseCoreActivity;
import com.chu.administrator.mybaseball.Fragment.BaseFragment;
import com.chu.administrator.mybaseball.Helper.HashMapManager;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.BatterMatchRecord;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.Manager;
import com.chu.administrator.mybaseball.Model.ManagerUserModel;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.PithcerMatchRecord;
import com.chu.administrator.mybaseball.Model.Team;
import com.chu.administrator.mybaseball.Model.TitleItem;
import com.chu.administrator.mybaseball.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018-07-27.
 */

public class MyTeamFragment extends BaseFragment implements View.OnClickListener{

    private ManagerUserModel teamModel;
    private boolean ischanged,isBatterOrder;
    Button batterBtn,pitcherBtn,batterOrderBtn,pitcherOrderBtn;
    LinearLayout positionVS;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_team_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        batterBtn = (Button) view.findViewById(R.id.batterBtn);
        pitcherBtn = (Button) view.findViewById(R.id.pitcherBtn);
        batterOrderBtn = (Button) view.findViewById(R.id.batterOrderBtn);
        pitcherOrderBtn = (Button) view.findViewById(R.id.pitcherOrderBtn);
        positionVS = (LinearLayout) view.findViewById(R.id.positionVS);
        ischanged = false;
        batterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawBatterView();
                batterBtn.setEnabled(false);
                batterOrderBtn.setEnabled(true);
                pitcherOrderBtn.setEnabled(true);
                pitcherBtn.setEnabled(true);
            }
        });

        pitcherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawPitcherView();
                batterBtn.setEnabled(true);
                batterOrderBtn.setEnabled(true);
                pitcherOrderBtn.setEnabled(true);
                pitcherBtn.setEnabled(false);
            }
        });
        batterOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titlePageNum=0;
                drawBatterOrderView();
                batterBtn.setEnabled(true);
                batterOrderBtn.setEnabled(false);
                pitcherOrderBtn.setEnabled(true);
                pitcherBtn.setEnabled(true);
            }
        });
        pitcherOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titlePageNum=0;
                drawPitcherOrderView();
                batterBtn.setEnabled(true);
                batterOrderBtn.setEnabled(true);
                pitcherOrderBtn.setEnabled(false);
                pitcherBtn.setEnabled(true);
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }


    private ImageView arrowLeftBtn,arrowRightBtn;
    private TextView bTitleTV0,bTitleTV1,bTitleTV2,bTitleTV3,bTitleTV4,bTitleTV5,bTitleTV6,bTitleTV7,bTitleTV8;
    private int titlePageNum=0;
    private RecyclerView batterOrderRV;
    private TitleBAdapter titleBatterAdapter;
    private TitlePAdapter titlePitcherAdapter;

    private void drawBatterOrderView(){
        isBatterOrder =true;
        positionVS.removeAllViews();
        View batterOrderView = getLayoutInflater().inflate(R.layout.batter_order_frame, null);
        positionVS.addView(batterOrderView);

        bTitleTV0 = (TextView) batterOrderView.findViewById(R.id.bTitleTV0);
        bTitleTV1 = (TextView) batterOrderView.findViewById(R.id.bTitleTV1);
        bTitleTV2 = (TextView) batterOrderView.findViewById(R.id.bTitleTV2);
        bTitleTV3 = (TextView) batterOrderView.findViewById(R.id.bTitleTV3);
        bTitleTV4 = (TextView) batterOrderView.findViewById(R.id.bTitleTV4);
        bTitleTV5 = (TextView) batterOrderView.findViewById(R.id.bTitleTV5);
        bTitleTV6 = (TextView) batterOrderView.findViewById(R.id.bTitleTV6);
        bTitleTV7 = (TextView) batterOrderView.findViewById(R.id.bTitleTV7);
        bTitleTV8 = (TextView) batterOrderView.findViewById(R.id.bTitleTV8);

        batterOrderRV = (RecyclerView) batterOrderView.findViewById(R.id.batterOrderRV);

        arrowLeftBtn = (ImageView) batterOrderView.findViewById(R.id.arrowLeftBtn);
        arrowRightBtn = (ImageView) batterOrderView.findViewById(R.id.arrowRightBtn);

        drawOrderTitle();

        titleBatterAdapter = new TitleBAdapter();
        batterOrderRV.setAdapter(titleBatterAdapter);
        batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));



        arrowLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                increaseTitlePageNum();
                titleBatterAdapter = new TitleBAdapter();
                batterOrderRV.setAdapter(titleBatterAdapter);
                batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
            }
        });
        arrowRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decreaseTitlePageNum();
                titleBatterAdapter = new TitleBAdapter();
                batterOrderRV.setAdapter(titleBatterAdapter);
                batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
            }
        });
    }

    private void drawOrderTitle(){
        if (isBatterOrder){
            if (titlePageNum == 0){
                bTitleTV2.setText(bInstance.getContext().getResources().getString(R.string.avg));
                bTitleTV3.setText(bInstance.getContext().getResources().getString(R.string.obp));
                bTitleTV4.setText(bInstance.getContext().getResources().getString(R.string.slg));
                bTitleTV5.setText("OPS");
                bTitleTV6.setText(bInstance.getContext().getResources().getString(R.string.hr));
                bTitleTV7.setText(bInstance.getContext().getResources().getString(R.string.RBI));
                bTitleTV8.setText(bInstance.getContext().getResources().getString(R.string.RUN));
            }else if (titlePageNum ==1){
                bTitleTV2.setText(bInstance.getContext().getResources().getString(R.string.sb));
                bTitleTV3.setText(bInstance.getContext().getResources().getString(R.string.sbRate));
                bTitleTV4.setText(bInstance.getContext().getResources().getString(R.string.fielding));
                bTitleTV5.setText(bInstance.getContext().getResources().getString(R.string.fieldingRate));
                bTitleTV6.setText("");
                bTitleTV7.setText("");
                bTitleTV8.setText("");
            }else if (titlePageNum ==2){
                bTitleTV2.setText(bInstance.getContext().getResources().getString(R.string.con));
                bTitleTV3.setText(bInstance.getContext().getResources().getString(R.string.power));
                bTitleTV4.setText(bInstance.getContext().getResources().getString(R.string.eye));
                bTitleTV5.setText(bInstance.getContext().getResources().getString(R.string.clutch));
                bTitleTV6.setText(bInstance.getContext().getResources().getString(R.string.spd));
                bTitleTV7.setText(bInstance.getContext().getResources().getString(R.string.def));
                bTitleTV8.setText("");
            }
        }else {
            if (titlePageNum == 0) {
                bTitleTV2.setText(bInstance.getContext().getResources().getString(R.string.INNING));
                bTitleTV3.setText(bInstance.getContext().getResources().getString(R.string.GAME));
                bTitleTV4.setText(bInstance.getContext().getResources().getString(R.string.ERA));
                bTitleTV5.setText(bInstance.getContext().getResources().getString(R.string.W));
                bTitleTV6.setText(bInstance.getContext().getResources().getString(R.string.L));
                bTitleTV7.setText(bInstance.getContext().getResources().getString(R.string.HOLD));
                bTitleTV8.setText(bInstance.getContext().getResources().getString(R.string.SAVE));
            } else if (titlePageNum == 1) {
                bTitleTV2.setText("WHIP");
                bTitleTV3.setText("SO/9");
                bTitleTV4.setText("BB/9");
                bTitleTV5.setText("HR/9");
                bTitleTV6.setText(bInstance.getContext().getResources().getString(R.string.OAVG));
                bTitleTV7.setText(bInstance.getContext().getResources().getString(R.string.OSLG));
                bTitleTV8.setText("");
            } else if (titlePageNum == 2) {
                bTitleTV2.setText(bInstance.getContext().getResources().getString(R.string.MOV));
                bTitleTV3.setText(bInstance.getContext().getResources().getString(R.string.LOC));
                bTitleTV4.setText(bInstance.getContext().getResources().getString(R.string.STF));
                bTitleTV5.setText(bInstance.getContext().getResources().getString(R.string.MEN));
                bTitleTV6.setText(bInstance.getContext().getResources().getString(R.string.VEL));
                bTitleTV7.setText(bInstance.getContext().getResources().getString(R.string.HP));
                bTitleTV8.setText("");
            }
        }
    }



    private void increaseTitlePageNum(){
        if (titlePageNum > 1){
            titlePageNum = 0;
        }else {
            titlePageNum++;
        }
    }

    private void decreaseTitlePageNum(){
        if (titlePageNum == 0){
            titlePageNum = 2;
        }else {
            titlePageNum--;
        }
    }

    private void drawPitcherOrderView(){
        isBatterOrder =false;
        positionVS.removeAllViews();
        View batterOrderView = getLayoutInflater().inflate(R.layout.batter_order_frame, null);
        positionVS.addView(batterOrderView);

        bTitleTV0 = (TextView) batterOrderView.findViewById(R.id.bTitleTV0);
        bTitleTV1 = (TextView) batterOrderView.findViewById(R.id.bTitleTV1);
        bTitleTV2 = (TextView) batterOrderView.findViewById(R.id.bTitleTV2);
        bTitleTV3 = (TextView) batterOrderView.findViewById(R.id.bTitleTV3);
        bTitleTV4 = (TextView) batterOrderView.findViewById(R.id.bTitleTV4);
        bTitleTV5 = (TextView) batterOrderView.findViewById(R.id.bTitleTV5);
        bTitleTV6 = (TextView) batterOrderView.findViewById(R.id.bTitleTV6);
        bTitleTV7 = (TextView) batterOrderView.findViewById(R.id.bTitleTV7);
        bTitleTV8 = (TextView) batterOrderView.findViewById(R.id.bTitleTV8);

        batterOrderRV = (RecyclerView) batterOrderView.findViewById(R.id.batterOrderRV);

        arrowLeftBtn = (ImageView) batterOrderView.findViewById(R.id.arrowLeftBtn);
        arrowRightBtn = (ImageView) batterOrderView.findViewById(R.id.arrowRightBtn);

        drawOrderTitle();

        titlePitcherAdapter = new TitlePAdapter();
        batterOrderRV.setAdapter(titlePitcherAdapter);
        batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));

        arrowLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                increaseTitlePageNum();
                titlePitcherAdapter = new TitlePAdapter();
                batterOrderRV.setAdapter(titlePitcherAdapter);
                batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
            }
        });
        arrowRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decreaseTitlePageNum();
                titlePitcherAdapter = new TitlePAdapter();
                batterOrderRV.setAdapter(titlePitcherAdapter);
                batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
            }
        });
    }


    FrameLayout mLO;
    FrameLayout cLO,b1LO,b2LO,ssLO,b3LO,lfLO,cfLO,rfLO,dhLO;

    private void drawBatterView(){

        positionVS.removeAllViews();
        View batterView = getLayoutInflater().inflate(R.layout.batter_position_frame, null);
        positionVS.addView(batterView);

        batterBtn.setEnabled(false);
        cLO = (FrameLayout) batterView.findViewById(R.id.cFrameLO);
        b1LO = (FrameLayout) batterView.findViewById(R.id.b1FrameLO);
        b2LO = (FrameLayout) batterView.findViewById(R.id.b2FrameLO);
        ssLO = (FrameLayout) batterView.findViewById(R.id.ssFrameLO);
        b3LO = (FrameLayout) batterView.findViewById(R.id.b3FrameLO);
        lfLO = (FrameLayout) batterView.findViewById(R.id.lfFrameLO);
        cfLO = (FrameLayout) batterView.findViewById(R.id.cfFrameLO);
        rfLO = (FrameLayout) batterView.findViewById(R.id.rfFrameLO);
        dhLO = (FrameLayout) batterView.findViewById(R.id.dhFrameLO);
        mLO = (FrameLayout) batterView.findViewById(R.id.mFrameLO);

        cLO.setOnClickListener(this);
        b1LO.setOnClickListener(this);
        b2LO.setOnClickListener(this);
        ssLO.setOnClickListener(this);
        b3LO.setOnClickListener(this);
        lfLO.setOnClickListener(this);
        cfLO.setOnClickListener(this);
        rfLO.setOnClickListener(this);
        dhLO.setOnClickListener(this);
        mLO.setOnClickListener(this);

        drawManager(teamModel.getManager(),mLO);
        drawBatterInfo1();
    }

    private void drawBatterInfo1(){
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.catcher),cLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.firstBase),b1LO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.secondBase),b2LO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.thirdBase),b3LO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.shortStop),ssLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.leftField),lfLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.centerField),cfLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.rightField),rfLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.DH),dhLO);
    }

    private void drawBatterInfo2(Batter batter, FrameLayout frameLayout){
        TextView statTV = (TextView) frameLayout.findViewById(R.id.statTV);
        TextView nameTV = (TextView) frameLayout.findViewById(R.id.nameTV);
        LinearLayout bgLO = (LinearLayout) frameLayout.findViewById(R.id.bgLO);
        LinearLayout pictureLO = (LinearLayout) frameLayout.findViewById(R.id.pictureLO);

        bInstance.setTextViewWithColor(statTV,batter.getAverageStat());
        nameTV.setText(batter.getBatterName());

    }

    private void drawManager(Manager manager, FrameLayout frameLayout){
        TextView mEnchantTV = (TextView) frameLayout.findViewById(R.id.statTV);
        mEnchantTV.setText("+"+manager.getEnchant());
        LinearLayout pictureLO = (LinearLayout) frameLayout.findViewById(R.id.pictureLO);
        pictureLO.setBackgroundResource(R.drawable.face_ex);
        TextView nameTV = (TextView) frameLayout.findViewById(R.id.nameTV);
        nameTV.setText("Manager");
    }

    FrameLayout sp1LO,sp2LO,sp3LO,sp4LO,sp5LO,rp1LO,rp2LO,rp3LO,rp4LO;
    private void drawPitcherView(){
        positionVS.removeAllViews();
        View pitcherView = getLayoutInflater().inflate(R.layout.pitcher_position_frame, null);
        positionVS.addView(pitcherView);

        batterBtn.setEnabled(false);

        sp1LO = (FrameLayout) pitcherView.findViewById(R.id.sp1FrameLO);
        sp2LO = (FrameLayout) pitcherView.findViewById(R.id.sp2FrameLO);
        sp3LO = (FrameLayout) pitcherView.findViewById(R.id.sp3FrameLO);
        sp4LO = (FrameLayout) pitcherView.findViewById(R.id.sp4FrameLO);
        sp5LO = (FrameLayout) pitcherView.findViewById(R.id.sp5FrameLO);
        rp1LO = (FrameLayout) pitcherView.findViewById(R.id.rp1FrameLO);
        rp2LO = (FrameLayout) pitcherView.findViewById(R.id.rp2FrameLO);
        rp3LO = (FrameLayout) pitcherView.findViewById(R.id.rp3FrameLO);
        rp4LO = (FrameLayout) pitcherView.findViewById(R.id.rp4FrameLO);
        mLO = (FrameLayout) pitcherView.findViewById(R.id.mFrameLO);

        sp1LO.setOnClickListener(this);
        sp2LO.setOnClickListener(this);
        sp3LO.setOnClickListener(this);
        sp4LO.setOnClickListener(this);
        sp5LO.setOnClickListener(this);
        rp1LO.setOnClickListener(this);
        rp2LO.setOnClickListener(this);
        rp3LO.setOnClickListener(this);
        rp4LO.setOnClickListener(this);
        mLO.setOnClickListener(this);

        drawManager(teamModel.getManager(),mLO);
        drawPitcherInfo1();
    }

    private void drawPitcherInfo1(){
        drawPitcherInfo2(teamModel.getPitcherByPosition(0),sp1LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(1),sp2LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(2),sp3LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(3),sp4LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(4),sp5LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(5),rp1LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(6),rp2LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(7),rp3LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(8),rp4LO);
    }

    private void drawPitcherInfo2(Pitcher pitcher, FrameLayout frameLayout){
        TextView statTV = (TextView) frameLayout.findViewById(R.id.statTV);
        TextView nameTV = (TextView) frameLayout.findViewById(R.id.nameTV);
        LinearLayout bgLO = (LinearLayout) frameLayout.findViewById(R.id.bgLO);
        LinearLayout pictureLO = (LinearLayout) frameLayout.findViewById(R.id.pictureLO);

        bInstance.setTextViewWithColor(statTV,pitcher.getAverageStat());
        nameTV.setText(pitcher.getPitcherName());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.catcher));
                break;
            case R.id.b1FrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.firstBase));
                break;
            case R.id.b2FrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.secondBase));
                break;
            case R.id.ssFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.shortStop));
                break;
            case R.id.b3FrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.thirdBase));
                break;
            case R.id.lfFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.leftField));
                break;
            case R.id.cfFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.centerField));
                break;
            case R.id.rfFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.rightField));
                break;
            case R.id.dhFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.DH));
                break;
            case R.id.sp1FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(0));
                break;
            case R.id.sp2FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(1));
                break;
            case R.id.sp3FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(2));
                break;
            case R.id.sp4FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(3));
                break;
            case R.id.sp5FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(4));
                break;
            case R.id.rp1FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(5));
                break;
            case R.id.rp2FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(6));
                break;
            case R.id.rp3FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(7));
                break;
            case R.id.rp4FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(8));
                break;
            case R.id.mFrameLO:
                popupManagerWindow(teamModel.getManager());
                break;
        }

    }
    private void popupManagerWindow(Manager manager){
        Log.e("j13 ",""+manager.getEnchant());


    }

    private void popupPitcherWindow(Pitcher pitcher){

        Dialog playerDialog = new Dialog(getContext(), R.style.playerDialog);
        playerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        playerDialog.setContentView(R.layout.pitcher_window);
        BaseCoreActivity.getInstance().setDetailWindowSize(playerDialog,(float) 1,(float)0.92);

        Window window = playerDialog.getWindow();

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);

        initPitcherDialogUI(playerDialog,pitcher);

        playerDialog.show();
    }

    private void initPitcherDialogUI(Dialog dialog, final Pitcher pitcher){
        Button titleBtn = (Button) dialog.findViewById(R.id.BDTitleTV);
        int awardCount = pitcher.getTitleItemArrayList().size();
        if (awardCount>1){
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIMES));
            titleBtn.setEnabled(true);
        }else {
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIME));
            titleBtn.setEnabled(false);
        }
        if (awardCount==1){
            titleBtn.setEnabled(true);
        }
        titleBtn.setEnabled(true);

        titleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupAwardWindow(pitcher);
            }
        });

        TextView batterDialogTotalStatTV = (TextView) dialog.findViewById(R.id.batterDialogTotalStatTV);
        bInstance.setTextViewWithColor(batterDialogTotalStatTV,pitcher.getAverageStat());

        TextView batterDialogPositionTV = (TextView) dialog.findViewById(R.id.batterDialogPositionTV);
        batterDialogPositionTV.setText(GlobalValue.getPitcherPositionString(pitcher.getPositionInt()));

        TextView batterDialogTypeTV = (TextView) dialog.findViewById(R.id.batterDialogTypeTV);
        batterDialogTypeTV.setText(GlobalValue.getPlayerTypeString(pitcher.getPitcherType()));

        LinearLayout batterDialogPlayerLevelIV = (LinearLayout) dialog.findViewById(R.id.batterDialogPlayerLevelIV);
        batterDialogPlayerLevelIV.setBackgroundResource(pitcher.getPlayerBackgroundResource());

        TextView playerLevelTV = (TextView) dialog.findViewById(R.id.playerLevelTV);
        playerLevelTV.setText(pitcher.getPlayerLevel()+"");

        TextView batterDialogNameTV = (TextView) dialog.findViewById(R.id.batterDialogNameTV);
        batterDialogNameTV.setText(pitcher.getPitcherName());

        TextView batterDialogSkill1TV = (TextView) dialog.findViewById(R.id.batterDialogSkill1TV);
        TextView batterDialogSkill1DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill1DecTV);
        if (pitcher.getPitcherSkill1() != null){
            batterDialogSkill1TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill1().getSkillLevel()));
            batterDialogSkill1TV.setText(GlobalValue.getSkillName(pitcher.getPitcherSkill1().getSkillType()));
            batterDialogSkill1DecTV.setText(pitcher.getPitcherSkill1().getSkillDescription());
        }else {
            batterDialogSkill1TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill1TV.setText("");
            batterDialogSkill1DecTV.setText("");
        }

        TextView batterDialogSkill2TV = (TextView) dialog.findViewById(R.id.batterDialogSkill2TV);
        TextView batterDialogSkill2DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill2DecTV);
        if (pitcher.getPitcherSkill2() != null){
            batterDialogSkill2TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill2().getSkillLevel()));
            batterDialogSkill2TV.setText(GlobalValue.getSkillName(pitcher.getPitcherSkill2().getSkillType()));
            batterDialogSkill2DecTV.setText(pitcher.getPitcherSkill2().getSkillDescription());
        }else {
            batterDialogSkill2TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill2TV.setText("");
            batterDialogSkill2DecTV.setText("");
        }


        TextView BDcon = (TextView) dialog.findViewById(R.id.BDcon);
        bInstance.setTextViewWithColor(BDcon,pitcher.getMovementWithItem());

        TextView BDPowerTV = (TextView) dialog.findViewById(R.id.BDPowerTV);
        bInstance.setTextViewWithColor(BDPowerTV,pitcher.getLocationWithItem());

        TextView BDEyeTV = (TextView) dialog.findViewById(R.id.BDEyeTV);
        bInstance.setTextViewWithColor(BDEyeTV,pitcher.getStuffWithItem());

        TextView BDClutchTV = (TextView) dialog.findViewById(R.id.BDClutchTV);
        bInstance.setTextViewWithColor(BDClutchTV,pitcher.getMentalWithItem());

        TextView BDspd = (TextView) dialog.findViewById(R.id.BDspd);
        bInstance.setTextViewWithColor(BDspd,pitcher.getVelocityWithItem());

        TextView BDdef = (TextView) dialog.findViewById(R.id.BDdef);
        bInstance.setTextViewWithColor(BDdef,pitcher.getHealthWithItem());

        PithcerMatchRecord m;
        m = pitcher.seasonRecord;

        TextView BDavg = (TextView) dialog.findViewById(R.id.BDavg);
        BDavg.setText(m.game+"");
        TextView BDobp = (TextView) dialog.findViewById(R.id.BDobp);
        BDobp.setText(m.getInning()+"");
        TextView BDslg = (TextView) dialog.findViewById(R.id.BDslg);
        BDslg.setText(m.getERA()+"");
        TextView BDops = (TextView) dialog.findViewById(R.id.BDops);
        BDops.setText(m.getWhip()+"");

        TextView BDgame = (TextView) dialog.findViewById(R.id.BDgame);
        BDgame.setText(m.win+"");
        TextView BDhr = (TextView) dialog.findViewById(R.id.BDhr);
        BDhr.setText(m.lose+"");
        TextView BDbb = (TextView) dialog.findViewById(R.id.BDbb);
        BDbb.setText(m.save+"");
        TextView BDso = (TextView) dialog.findViewById(R.id.BDso);
        BDso.setText(m.hold+"");

        TextView BDrbi = (TextView) dialog.findViewById(R.id.BDrbi);
        BDrbi.setText(m.so+"");
        TextView BDrun = (TextView) dialog.findViewById(R.id.BDrun);
        BDrun.setText(m.bb+"");
        TextView BDsb = (TextView) dialog.findViewById(R.id.BDsb);
        BDsb.setText(m.hit+"");
        TextView BDcs = (TextView) dialog.findViewById(R.id.BDcs);
        BDcs.setText(m.hr+"");

        TextView BDferr = (TextView) dialog.findViewById(R.id.BDferr);
        BDferr.setText(m.r+"");
        TextView BDftot = (TextView) dialog.findViewById(R.id.BDftot);
        BDftot.setText(m.er+"");
        TextView BDfrate = (TextView) dialog.findViewById(R.id.BDfrate);
        BDfrate.setText(m.getOavg()+"");
        TextView BDab = (TextView) dialog.findViewById(R.id.BDab);
        BDab.setText(m.getOslg()+"");

        TextView BDpa = (TextView) dialog.findViewById(R.id.BDpa);
        BDpa.setText(m.getSo9()+"");
        TextView BDh = (TextView) dialog.findViewById(R.id.BDh);
        BDh.setText(m.getBb9()+"");
        TextView BD2b = (TextView) dialog.findViewById(R.id.BD2b);
        BD2b.setText(m.getHr9()+"");

        final LinearLayout pitchingGuageBarLO = (LinearLayout) dialog.findViewById(R.id.pitchingGuageBarLO);

        if (pitcher.getMaxPitchOrder() == 0){
            pitchingGuageBarLO.setBackgroundResource(R.drawable.less1);
        }else if (pitcher.getMaxPitchOrder() == 1){
            pitchingGuageBarLO.setBackgroundResource(R.drawable.auto2);
        }else {
            pitchingGuageBarLO.setBackgroundResource(R.drawable.more3);
        }
        Button buntLessBtn = (Button) dialog.findViewById(R.id.buntLessBtn);
        buntLessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pitchingGuageBarLO.setBackgroundResource(R.drawable.less1);
                pitcher.setMaxPitchOrder(0);
                setPitcher(pitcher);
            }
        });
        Button buntAutoBtn = (Button) dialog.findViewById(R.id.buntAutoBtn);
        buntAutoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pitchingGuageBarLO.setBackgroundResource(R.drawable.auto2);
                pitcher.setMaxPitchOrder(1);
                setPitcher(pitcher);
            }
        });
        Button buntMoreBtn = (Button) dialog.findViewById(R.id.buntMoreBtn);
        buntMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pitchingGuageBarLO.setBackgroundResource(R.drawable.more3);
                pitcher.setMaxPitchOrder(2);
                setPitcher(pitcher);
            }
        });

    }
    private void popupBatterWindow(Batter batter){

        Dialog playerDialog = new Dialog(getContext(), R.style.playerDialog);
        playerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        playerDialog.setContentView(R.layout.batter_window);
        BaseCoreActivity.getInstance().setDetailWindowSize(playerDialog,(float) 1,(float)0.92);

        Window window = playerDialog.getWindow();

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);

        initBatterDialogUI(playerDialog,batter);


        playerDialog.show();
    }

    private void popupAwardWindow(Object object){

        Dialog playerDialog = new Dialog(getContext(), R.style.playerDialog);
        playerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        playerDialog.setContentView(R.layout.title_popup);

        Window window = playerDialog.getWindow();

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.RIGHT;
        window.setAttributes(params);
        bInstance.setDetailWindowSize(playerDialog,0.3f,0.6f);
        RecyclerView titleRV = (RecyclerView) playerDialog.findViewById(R.id.titleRV);

        RecyclerView.Adapter titleAdapter;

        if (object instanceof Batter){
            ArrayList<TitleItem> items = new ArrayList<TitleItem>();
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
//            titleAdapter = new AwardAdapter(((Batter) object).getTitleItemArrayList());

            titleAdapter = new AwardAdapter(items);
        }else {
            ArrayList<TitleItem> items = new ArrayList<TitleItem>();
            items.add(new TitleItem("2017-4",21));
//            titleAdapter = new AwardAdapter(((Pitcher) object).getTitleItemArrayList());

            titleAdapter = new AwardAdapter(items);
        }
        titleRV.setAdapter(titleAdapter);

//        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),1);
//        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
//
//        titleRV.setLayoutManager(layoutManager);
        titleRV.setLayoutManager(new GridLayoutManager(getActivity(),1));

        playerDialog.show();
    }
    private void initBatterDialogUI(final Dialog dialog, final Batter batter){

        Button titleBtn = (Button) dialog.findViewById(R.id.BDTitleTV);
        int awardCount = batter.getTitleItemArrayList().size();
        if (awardCount>1){
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIMES));
            titleBtn.setEnabled(true);
        }else {
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIME));
            titleBtn.setEnabled(false);
        }
        if (awardCount==1){
            titleBtn.setEnabled(true);
        }
        titleBtn.setEnabled(true);//j13

        titleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupAwardWindow(batter);
            }
        });

        TextView batterDialogTotalStatTV = (TextView) dialog.findViewById(R.id.batterDialogTotalStatTV);
        bInstance.setTextViewWithColor(batterDialogTotalStatTV,batter.getAverageStat());

        TextView batterDialogPositionTV = (TextView) dialog.findViewById(R.id.batterDialogPositionTV);
        batterDialogPositionTV.setText(GlobalValue.getPositionString(batter.getDefensePosition()));

        TextView batterDialogTypeTV = (TextView) dialog.findViewById(R.id.batterDialogTypeTV);
        batterDialogTypeTV.setText(GlobalValue.getPlayerTypeString(batter.getBatterType()));

        LinearLayout batterDialogPlayerLevelIV = (LinearLayout) dialog.findViewById(R.id.batterDialogPlayerLevelIV);
        batterDialogPlayerLevelIV.setBackgroundResource(batter.getPlayerBackgroundResource());

        TextView playerLevelTV = (TextView) dialog.findViewById(R.id.playerLevelTV);
        playerLevelTV.setText(batter.getPlayerLevel()+"");

        TextView batterDialogNameTV = (TextView) dialog.findViewById(R.id.batterDialogNameTV);
        batterDialogNameTV.setText(batter.getBatterName());

        TextView batterDialogSkill1TV = (TextView) dialog.findViewById(R.id.batterDialogSkill1TV);
        TextView batterDialogSkill1DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill1DecTV);
        if (batter.getBatterSkill1() != null){
            batterDialogSkill1TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill1().getSkillLevel()));
            batterDialogSkill1TV.setText(GlobalValue.getSkillName(batter.getBatterSkill1().getSkillType()));
            batterDialogSkill1DecTV.setText(batter.getBatterSkill1().getSkillDescription());
        }else {
            batterDialogSkill1TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill1TV.setText("");
            batterDialogSkill1DecTV.setText("");
        }

        TextView batterDialogSkill2TV = (TextView) dialog.findViewById(R.id.batterDialogSkill2TV);
        TextView batterDialogSkill2DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill2DecTV);
        if (batter.getBatterSkill2() != null){
            batterDialogSkill2TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill2().getSkillLevel()));
            batterDialogSkill2TV.setText(GlobalValue.getSkillName(batter.getBatterSkill2().getSkillType()));
            batterDialogSkill2DecTV.setText(batter.getBatterSkill2().getSkillDescription());
        }else {
            batterDialogSkill2TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill2TV.setText("");
            batterDialogSkill2DecTV.setText("");
        }


        TextView BDcon = (TextView) dialog.findViewById(R.id.BDcon);
        bInstance.setTextViewWithColor(BDcon,batter.getContactWithItem());

        TextView BDPowerTV = (TextView) dialog.findViewById(R.id.BDPowerTV);
        bInstance.setTextViewWithColor(BDPowerTV,batter.getPowerWithItem());

        TextView BDEyeTV = (TextView) dialog.findViewById(R.id.BDEyeTV);
        bInstance.setTextViewWithColor(BDEyeTV,batter.getEyeWithItem());

        TextView BDClutchTV = (TextView) dialog.findViewById(R.id.BDClutchTV);
        bInstance.setTextViewWithColor(BDClutchTV,batter.getClutchWithItem());

        TextView BDspd = (TextView) dialog.findViewById(R.id.BDspd);
        bInstance.setTextViewWithColor(BDspd,batter.getSpeedWithItem());

        TextView BDdef = (TextView) dialog.findViewById(R.id.BDdef);
        bInstance.setTextViewWithColor(BDdef,batter.getDefenceInPositionWithItem());

        BatterMatchRecord m;

        m = batter.seasonRecord;


        TextView BDavg = (TextView) dialog.findViewById(R.id.BDavg);
        BDavg.setText(m.getAvg()+"");
        TextView BDobp = (TextView) dialog.findViewById(R.id.BDobp);
        BDobp.setText(m.getObp()+"");
        TextView BDslg = (TextView) dialog.findViewById(R.id.BDslg);
        BDslg.setText(m.getSlg()+"");
        TextView BDops = (TextView) dialog.findViewById(R.id.BDops);
        BDops.setText(m.getOps()+"");

        TextView BDgame = (TextView) dialog.findViewById(R.id.BDgame);
        BDgame.setText(m.game+"");
        TextView BDhr = (TextView) dialog.findViewById(R.id.BDhr);
        BDhr.setText(m.homer+"");
        TextView BDbb = (TextView) dialog.findViewById(R.id.BDbb);
        BDbb.setText(m.baseOnball+"");
        TextView BDso = (TextView) dialog.findViewById(R.id.BDso);
        BDso.setText(m.strikeOut+"");

        TextView BDrbi = (TextView) dialog.findViewById(R.id.BDrbi);
        BDrbi.setText(m.rbi+"");
        TextView BDrun = (TextView) dialog.findViewById(R.id.BDrun);
        BDrun.setText(m.run+"");
        TextView BDsb = (TextView) dialog.findViewById(R.id.BDsb);
        BDsb.setText(m.sb+"");
        TextView BDcs = (TextView) dialog.findViewById(R.id.BDcs);
        BDcs.setText(m.cs+"");

        TextView BDferr = (TextView) dialog.findViewById(R.id.BDferr);
        BDferr.setText(m.error+"");
        TextView BDftot = (TextView) dialog.findViewById(R.id.BDftot);
        BDftot.setText((m.error+m.successDefense)+"");
        TextView BDfrate = (TextView) dialog.findViewById(R.id.BDfrate);
        BDfrate.setText(m.getErrorRate()+"");
        TextView BDab = (TextView) dialog.findViewById(R.id.BDab);
        BDab.setText(m.ab+"");

        TextView BDpa = (TextView) dialog.findViewById(R.id.BDpa);
        BDpa.setText(m.pa+"");
        TextView BDh = (TextView) dialog.findViewById(R.id.BDh);
        BDh.setText(m.hit+"");
        TextView BD2b = (TextView) dialog.findViewById(R.id.BD2b);
        BD2b.setText(m.doubleHit+"");
        TextView BD3b = (TextView) dialog.findViewById(R.id.BD3b);
        BD3b.setText(m.tripleHit+"");

        final LinearLayout buntGuageBarLO = (LinearLayout) dialog.findViewById(R.id.buntGuageBarLO);

        if (batter.getBuntOrder() == 0){
            buntGuageBarLO.setBackgroundResource(R.drawable.less1);
        }else if (batter.getBuntOrder() == 1){
            buntGuageBarLO.setBackgroundResource(R.drawable.auto2);
        }else {
            buntGuageBarLO.setBackgroundResource(R.drawable.more3);
        }
        Button buntLessBtn = (Button) dialog.findViewById(R.id.buntLessBtn);
        buntLessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buntGuageBarLO.setBackgroundResource(R.drawable.less1);
//                realm.beginTransaction();
                batter.setBuntOrder(0);
                setBatter(batter);
//                realm.commitTransaction();
            }
        });
        Button buntAutoBtn = (Button) dialog.findViewById(R.id.buntAutoBtn);
        buntAutoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buntGuageBarLO.setBackgroundResource(R.drawable.auto2);
//                realm.beginTransaction();
                batter.setBuntOrder(1);
                setBatter(batter);
//                realm.commitTransaction();
            }
        });
        Button buntMoreBtn = (Button) dialog.findViewById(R.id.buntMoreBtn);
        buntMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buntGuageBarLO.setBackgroundResource(R.drawable.more3);
//                realm.beginTransaction();
                batter.setBuntOrder(2);
                setBatter(batter);
//                realm.commitTransaction();
            }
        });

        final LinearLayout stealBGLO = (LinearLayout) dialog.findViewById(R.id.stealBGLO);

        if (batter.getStealOrder() == 0){
            stealBGLO.setBackgroundResource(R.drawable.less1);
        }else if (batter.getStealOrder() == 1){
            stealBGLO.setBackgroundResource(R.drawable.auto2);
        }else {
            stealBGLO.setBackgroundResource(R.drawable.more3);
        }
        Button stealLessBtn = (Button) dialog.findViewById(R.id.stealLessBtn);
        stealLessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stealBGLO.setBackgroundResource(R.drawable.less1);

                batter.setStealOrder(0);
                setBatter(batter);
            }
        });
        final Button stealAutoBtn = (Button) dialog.findViewById(R.id.stealAutoBtn);
        stealAutoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stealBGLO.setBackgroundResource(R.drawable.auto2);
                batter.setStealOrder(1);
                setBatter(batter);
            }
        });
        Button stealMoreBtn = (Button) dialog.findViewById(R.id.stealMoreBtn);
        stealMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stealBGLO.setBackgroundResource(R.drawable.more3);
                batter.setStealOrder(2);
                setBatter(batter);
            }
        });

        drawPositionLV(dialog,batter);
    }

    private void setBatter(Batter batter){
        ischanged =true;
        teamModel.setBatter(batter);

    }

    private void setPitcher(Pitcher pitcher){
        ischanged =true;
        teamModel.setPitcher(pitcher);

    }

    private void drawPositionLV(Dialog dialog,Batter batter){
        ImageView cIV,b1IV,b2IV,b3IV,ssIV,lfIV,cfIV,rfIV;
        cIV = (ImageView) dialog.findViewById(R.id.cIV);
        b1IV = (ImageView) dialog.findViewById(R.id.b1IV);
        b2IV = (ImageView) dialog.findViewById(R.id.b2IV);
        b3IV = (ImageView) dialog.findViewById(R.id.b3IV);
        ssIV = (ImageView) dialog.findViewById(R.id.ssIV);
        lfIV = (ImageView) dialog.findViewById(R.id.lfIV);
        cfIV = (ImageView) dialog.findViewById(R.id.cfIV);
        rfIV = (ImageView) dialog.findViewById(R.id.rfIV);

        ArrayList<Batter.DefencePosition> defencePositionArrayList = batter.getPositionLVList();
        for (Batter.DefencePosition defencePosition : defencePositionArrayList){
            switch (defencePosition.getPositionInt()){
                case GlobalValue.catcher:
                    cIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.firstBase:
                    b1IV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.secondBase:
                    b2IV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.thirdBase:
                    b3IV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.shortStop:
                    ssIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.leftField:
                    lfIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.centerField:
                    cfIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.rightField:
                    rfIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
            }
        }

    }

    @Override
    public void onResume() {
        downloadTeamModel();
        super.onResume();
    }

    @Override
    public void onPause() {
        if (ischanged){
            uploadTeamModel();
        }
        super.onPause();
    }
    private void uploadTeamModel(){
        HashMapManager hManager = bInstance.gethManager();

        DocumentReference docRef = bInstance.getDb().collection("userModel").document(bInstance.getUID());
        ArrayList<Batter> batterSquad = teamModel.getTeam().getBatterSquad();
        if (batterSquad == null){
            return;
        }

        ArrayList<Pitcher> pitcherSquad = teamModel.getTeam().getPitcherSquad();

        if (pitcherSquad == null){
            return;
        }

        Team team = teamModel.getTeam();


        Map<String,Object> teamModelMap = new HashMap<>();
        teamModelMap = bInstance.gethManager().createManagerModeModelMap(teamModelMap,teamModel);

        docRef.set(teamModelMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("MyTeamFrag ","data uploaded.");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("MyTeamFrag ","data upload fail.");
            }
        });
    }

    private void downloadTeamModel(){
        bInstance.showProgressBar();
        enableUI(false);
        DocumentReference docRef = bInstance.getDb().collection("userModel").document(bInstance.getUID());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
                        int mode = Integer.parseInt(document.get("mode").toString());

                        teamModel = bInstance.getManagerModelFromServer(document);

                        if (mode == GlobalValue.PLAYERMODE){
                            bInstance.hideProgressBar();
                            enableUI(true);
                            bInstance.popupGeneralWindow("Invalid Mode Type..");
                            return;
                        }
                        bInstance.hideProgressBar();
                        enableUI(true);

                        drawUI();

                    } else {
                        bInstance.popupGeneralWindow("Not found Team..");
                        bInstance.hideProgressBar();
                        enableUI(true);
                    }
                } else {
                    Log.d("TeamDetail", "get failed with ", task.getException());
                    bInstance.popupGeneralWindow("Server Error..");
                    bInstance.hideProgressBar();
                    enableUI(true);
                }
            }
        });
    }

    private ArrayList<Batter> batters,batterSubList;
    private ArrayList<Pitcher> pitchers,pitcherSubList;
    private boolean isAnimaiting =false;
    private int firstIndex,secondIndex;

    private void handleClick(int index,View view){

        batterSubList = teamModel.getSubBatterSquad();
        batters = teamModel.getTeam().getBatterSquad();
//
        if (isAnimaiting){
            isAnimaiting = false;
//            setBlinkSlow(view,View.INVISIBLE);

            secondIndex = index;

            if (firstIndex <9 && secondIndex <9){  // main main

                Batter firstIndexBatter = batters.get(firstIndex);

                int firstBattingOrder = firstIndexBatter.getBattingOrder();

                batters.get(firstIndex).setBattingOrder(batters.get(secondIndex).getBattingOrder());
                batters.get(secondIndex).setBattingOrder(firstBattingOrder);
//
                batters.set(firstIndex,batters.get(secondIndex));
                batters.set(secondIndex,firstIndexBatter);

            }else if (firstIndex >8 && secondIndex >8){ // sub sub
                firstIndex -= 9;
                secondIndex -= 9;

                Batter firstIndexBatter = batterSubList.get(firstIndex);

                batterSubList.set(firstIndex,batterSubList.get(secondIndex));
                batterSubList.set(secondIndex,firstIndexBatter);
            }else if (firstIndex <9 && secondIndex >8){ // main sub

                secondIndex -= 9; //sub

                Batter firstIndexBatter = batters.get(firstIndex);  //main
                Batter secondIndexBatter = batterSubList.get(secondIndex); //sub

                secondIndexBatter.setDefensePosition(firstIndexBatter.getDefensePosition());  //sub
                firstIndexBatter.setDefensePosition(firstIndexBatter.getPrimaryPosition());  //main

                batters.set(firstIndex,secondIndexBatter);
                batterSubList.set(secondIndex,firstIndexBatter);
                if (bInstance.isAIToken(firstIndexBatter.getCardToken())){
                    batterSubList.remove(secondIndex);
                    batterSubList.trimToSize();
                }


            }else if (firstIndex >8 && secondIndex <9) { // sub main

                firstIndex -= 9;  //sub

                Batter firstIndexBatter = batterSubList.get(firstIndex);  //sub
                Batter secondIndexBatter = batters.get(secondIndex); //main

                firstIndexBatter.setDefensePosition(secondIndexBatter.getDefensePosition());  //sub
                secondIndexBatter.setDefensePosition(secondIndexBatter.getPrimaryPosition());  //main

                batters.set(secondIndex, firstIndexBatter);
                batterSubList.set(firstIndex, secondIndexBatter);
                if (bInstance.isAIToken(secondIndexBatter.getCardToken())){
                    batterSubList.remove(firstIndex);
                    batterSubList.trimToSize();
                }
                // 3 2    sub main

            }

//            BaseCoreActivity.getInstance().setAppBar();
            teamModel.setSubBatterSquad(batterSubList);
            teamModel.getTeam().setBatterSquad(batters);

//            titleBatterAdapter.notifyDataSetChanged();

            titleBatterAdapter = new TitleBAdapter();
            batterOrderRV.setAdapter(titleBatterAdapter);
            batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
            ischanged=true;
        }else {
            firstIndex = index;
            bInstance.setBlink1000(view,View.VISIBLE);
            isAnimaiting = true;
        }
    }

    private void handleClickP(int index,View view){

        pitcherSubList = teamModel.getSubPitcherSquad();
        pitchers = teamModel.getTeam().getPitcherSquad();
//
        if (isAnimaiting){
            isAnimaiting = false;
//            setBlinkSlow(view,View.INVISIBLE);

            secondIndex = index;

            if (firstIndex <9 && secondIndex <9){  // main main

                Pitcher firstIndexPitcher = pitchers.get(firstIndex);

                int firstPitcherPosition = firstIndexPitcher.getPositionInt();

                pitchers.get(firstIndex).setPositionInt(pitchers.get(secondIndex).getPositionInt());
                pitchers.get(secondIndex).setPositionInt(firstPitcherPosition);

                pitchers.set(firstIndex,pitchers.get(secondIndex));
                pitchers.set(secondIndex,firstIndexPitcher);

            }else if (firstIndex >8 && secondIndex >8){ // sub sub
                firstIndex -= 9;
                secondIndex -= 9;

                Pitcher firstIndexPitcher = pitcherSubList.get(firstIndex);

                pitcherSubList.set(firstIndex,pitcherSubList.get(secondIndex));
                pitcherSubList.set(secondIndex,firstIndexPitcher);

            }else if (firstIndex <9 && secondIndex >8){ // main sub

                secondIndex -= 9; //sub

                Pitcher firstIndexPitcher = pitchers.get(firstIndex);
                Pitcher secondIndexPitcher = pitcherSubList.get(secondIndex);

                secondIndexPitcher.setPositionInt(firstIndexPitcher.getPositionInt());
                firstIndexPitcher.setPositionInt(-1);

                pitchers.set(firstIndex,secondIndexPitcher);
                pitcherSubList.set(secondIndex,firstIndexPitcher);

                if (bInstance.isAIToken(firstIndexPitcher.getCardToken())){
                    pitcherSubList.remove(secondIndex);
                    pitcherSubList.trimToSize();
                }

            }else if (firstIndex >8 && secondIndex <9) { // sub main

                firstIndex -= 9;  //sub

                Pitcher firstIndexPitcher = pitcherSubList.get(firstIndex);
                Pitcher secondIndexPitcher = pitchers.get(secondIndex);

                firstIndexPitcher.setPositionInt(secondIndexPitcher.getPositionInt());
                secondIndexPitcher.setPositionInt(-1);

                pitchers.set(secondIndex,firstIndexPitcher);
                pitcherSubList.set(firstIndex,secondIndexPitcher);

                if (bInstance.isAIToken(secondIndexPitcher.getCardToken())){
                    pitcherSubList.remove(firstIndex);
                    pitcherSubList.trimToSize();
                }

            }

//            BaseCoreActivity.getInstance().setAppBar();
//            for (Pitcher p : pitchers){
//                Log.e("j13 ",""+p.getPitcherName());
//            }
//            Log.e("j13 ","==============");
//            for (Pitcher p : pitcherSubList){
//                Log.e("j13 ",""+p.getPitcherName());
//            }



            teamModel.setSubPitcherSquad(pitcherSubList);
            teamModel.getTeam().setPitcherSquad(pitchers);

//            titleBatterAdapter.notifyDataSetChanged();

            titlePitcherAdapter = new TitlePAdapter();
            batterOrderRV.setAdapter(titlePitcherAdapter);
            batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
            ischanged=true;
        }else {
            firstIndex = index;
            bInstance.setBlink1000(view,View.VISIBLE);
            isAnimaiting = true;
        }
    }
    private void handlePutOut(int index){
        batterSubList = teamModel.getSubBatterSquad();
        batters = teamModel.getTeam().getBatterSquad();

        Batter batter = batters.get(index);  //main

        batters.set(index,bInstance.createBatter(batter.getDefensePosition()));
        batterSubList.add(0,batter);


        teamModel.setSubBatterSquad(batterSubList);
        teamModel.getTeam().setBatterSquad(batters);

        titleBatterAdapter = new TitleBAdapter();
        batterOrderRV.setAdapter(titleBatterAdapter);
        batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
        ischanged=true;
    }

    private void handlePutOutP(int index){
        pitcherSubList = teamModel.getSubPitcherSquad();
        pitchers = teamModel.getTeam().getPitcherSquad();

        Pitcher pitcher = pitchers.get(index);  //main

        pitchers.set(index,bInstance.createPitcher(pitcher.getPositionInt()));
        pitcherSubList.add(0,pitcher);

        teamModel.setSubPitcherSquad(pitcherSubList);
        teamModel.getTeam().setPitcherSquad(pitchers);

        titlePitcherAdapter = new TitlePAdapter();
        batterOrderRV.setAdapter(titlePitcherAdapter);
        batterOrderRV.setLayoutManager(new GridLayoutManager(getActivity(),1));


//        ischanged=true;
    }
    private void drawUI(){
        drawBatterView();
    }

    private void enableUI(boolean flag){
        pitcherBtn.setEnabled(flag);
        batterBtn.setEnabled(flag);
        batterOrderBtn.setEnabled(flag);
        pitcherOrderBtn.setEnabled(flag);
    }


    public class TitleBAdapter extends RecyclerView.Adapter<TitleBAdapter.CustomViewHolder> {
        private ArrayList<Batter> mItems;

        public TitleBAdapter() {
            mItems = new ArrayList<>();
            mItems.addAll(teamModel.getTeam().getBatterSquad());
            mItems.addAll(teamModel.getSubBatterSquad());
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.batter_order_item,null);
            CustomViewHolder holder = new CustomViewHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            holder.itemBaseLO.getLayoutParams().height = 100;
            bInstance.setTextViewWithColor(holder.bItem0,mItems.get(position).getAverageStat());
            holder.bItem1.setText(mItems.get(position).getBatterName());
            if (position <9){
                holder.bItem2.setText(GlobalValue.getPositionString(mItems.get(position).getDefensePosition()));
            }else {
                holder.bItem2.setText(GlobalValue.getPositionString(mItems.get(position).getPrimaryPosition()));
            }
            if (bInstance.isAIToken(mItems.get(position).getCardToken())){
                holder.itemBaseLO.setBackgroundResource(R.drawable.ai_order_bg);
                holder.orderBtn.setVisibility(View.INVISIBLE);
            }else {
                if (position <9){
                    holder.orderBtn.setVisibility(View.VISIBLE);
                }else {
                    holder.orderBtn.setVisibility(View.INVISIBLE);
                }
                holder.itemBaseLO.setBackgroundResource(mItems.get(position).getPlayerBackgroundResource());

            }

            if (titlePageNum == 0){
                holder.bItem3.setText(mItems.get(position).getSeasonRecord().getAvg()+"");
                holder.bItem4.setText(mItems.get(position).getSeasonRecord().getObp()+"");
                holder.bItem5.setText(mItems.get(position).getSeasonRecord().getSlg()+"");
                holder.bItem6.setText(mItems.get(position).getSeasonRecord().getOps()+"");
                holder.bItem7.setText(mItems.get(position).getSeasonRecord().getHrPerGame()+"");
                holder.bItem8.setText(mItems.get(position).getSeasonRecord().getRbiPerGame()+"");
                holder.bItem9.setText(mItems.get(position).getSeasonRecord().getRunPerGame()+"");
            }else if (titlePageNum ==1){
                holder.bItem3.setText(mItems.get(position).getSeasonRecord().sb+"");
                holder.bItem4.setText(mItems.get(position).getSeasonRecord().getSbPerGame()+"");
                holder.bItem5.setText(mItems.get(position).getSeasonRecord().successDefense+"");
                holder.bItem6.setText(mItems.get(position).getSeasonRecord().getErrorRate()+"");
                holder.bItem7.setText("");
                holder.bItem8.setText("");
                holder.bItem9.setText("");
            }else if (titlePageNum ==2){
                bInstance.setTextViewWithColor(holder.bItem3,mItems.get(position).getContactWithItem());
                bInstance.setTextViewWithColor(holder.bItem4,mItems.get(position).getPowerWithItem());
                bInstance.setTextViewWithColor(holder.bItem5,mItems.get(position).getEyeWithItem());
                bInstance.setTextViewWithColor(holder.bItem6,mItems.get(position).getClutchWithItem());
                bInstance.setTextViewWithColor(holder.bItem7,mItems.get(position).getSpeedWithItem());
                bInstance.setTextViewWithColor(holder.bItem8,mItems.get(position).getDefenceInPositionWithItem());
                holder.bItem9.setText("");
            }
            drawOrderTitle();

//            bInstance.getContext().getResources().getString(R.string.)

        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public LinearLayout itemBaseLO;
            public TextView bItem0,bItem1,bItem2,bItem3,bItem4,bItem5,bItem6,bItem7,bItem8,bItem9;
            public Button orderBtn;

            public CustomViewHolder(View view) {
                super(view);
                itemBaseLO = (LinearLayout) view.findViewById(R.id.itemBaseLO);

                bItem0 = (TextView) view.findViewById(R.id.bItem0);
                bItem1 = (TextView) view.findViewById(R.id.bItem1);
                bItem2 = (TextView) view.findViewById(R.id.bItem2);
                bItem3 = (TextView) view.findViewById(R.id.bItem3);
                bItem4 = (TextView) view.findViewById(R.id.bItem4);
                bItem5 = (TextView) view.findViewById(R.id.bItem5);
                bItem6 = (TextView) view.findViewById(R.id.bItem6);
                bItem7 = (TextView) view.findViewById(R.id.bItem7);
                bItem8 = (TextView) view.findViewById(R.id.bItem8);
                bItem9 = (TextView) view.findViewById(R.id.bItem9);

                orderBtn = (Button)  view.findViewById(R.id.orderBtn);
                orderBtn.setOnClickListener(this);
                itemBaseLO.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.orderBtn){
                    handlePutOut(getLayoutPosition());
                }else if (view.getId() == R.id.itemBaseLO){
                    handleClick(getLayoutPosition(),view);
                }
            }
        }
    }

    public class TitlePAdapter extends RecyclerView.Adapter<TitlePAdapter.CustomViewHolder> {
        private ArrayList<Pitcher> mItems;

        public TitlePAdapter() {
            mItems = new ArrayList<>();
            mItems.addAll(teamModel.getTeam().getPitcherSquad());
            mItems.addAll(teamModel.getSubPitcherSquad());
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.batter_order_item,null);
            CustomViewHolder holder = new CustomViewHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            holder.itemBaseLO.getLayoutParams().height = 100;
            bInstance.setTextViewWithColor(holder.bItem0,mItems.get(position).getAverageStat());
            holder.bItem1.setText(mItems.get(position).getPitcherName());
            if (position <9){
                holder.bItem2.setText(GlobalValue.getPitcherPositionString(mItems.get(position).getPositionInt()));
            }else {
                holder.bItem2.setText("");
            }
            if (bInstance.isAIToken(mItems.get(position).getCardToken())){
                holder.itemBaseLO.setBackgroundResource(R.drawable.ai_order_bg);
                holder.orderBtn.setVisibility(View.INVISIBLE);
            }else {
                if (position <9){
                    holder.orderBtn.setVisibility(View.VISIBLE);
                }else {
                    holder.orderBtn.setVisibility(View.INVISIBLE);
                }
                holder.itemBaseLO.setBackgroundResource(mItems.get(position).getPlayerBackgroundResource());

            }

            if (titlePageNum == 0){
                holder.bItem3.setText(mItems.get(position).getSeasonRecord().getInning()+"");
                holder.bItem4.setText(mItems.get(position).getSeasonRecord().game+"");
                holder.bItem5.setText(mItems.get(position).getSeasonRecord().getERA()+"");
                holder.bItem6.setText(mItems.get(position).getSeasonRecord().win+"");
                holder.bItem7.setText(mItems.get(position).getSeasonRecord().lose+"");
                holder.bItem8.setText(mItems.get(position).getSeasonRecord().hold+"");
                holder.bItem9.setText(mItems.get(position).getSeasonRecord().save+"");
            }else if (titlePageNum ==1){
                holder.bItem3.setText(mItems.get(position).getSeasonRecord().getWhip()+"");
                holder.bItem4.setText(mItems.get(position).getSeasonRecord().getSo9()+"");
                holder.bItem5.setText(mItems.get(position).getSeasonRecord().getBb9()+"");
                holder.bItem6.setText(mItems.get(position).getSeasonRecord().getHr9()+"");
                holder.bItem7.setText(mItems.get(position).getSeasonRecord().getOavg()+"");
                holder.bItem8.setText(mItems.get(position).getSeasonRecord().getOslg()+"");
                holder.bItem9.setText("");
            }else if (titlePageNum ==2){
                bInstance.setTextViewWithColor(holder.bItem3,mItems.get(position).getMovementWithItem());
                bInstance.setTextViewWithColor(holder.bItem4,mItems.get(position).getLocationWithItem());
                bInstance.setTextViewWithColor(holder.bItem5,mItems.get(position).getStuffWithItem());
                bInstance.setTextViewWithColor(holder.bItem6,mItems.get(position).getMentalWithItem());
                bInstance.setTextViewWithColor(holder.bItem7,mItems.get(position).getVelocityWithItem());
                bInstance.setTextViewWithColor(holder.bItem8,mItems.get(position).getHealthWithItem());
                holder.bItem9.setText("");
            }
            drawOrderTitle();

//            bInstance.getContext().getResources().getString(R.string.)

        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public LinearLayout itemBaseLO;
            public TextView bItem0,bItem1,bItem2,bItem3,bItem4,bItem5,bItem6,bItem7,bItem8,bItem9;
            public Button orderBtn;

            public CustomViewHolder(View view) {
                super(view);
                itemBaseLO = (LinearLayout) view.findViewById(R.id.itemBaseLO);

                bItem0 = (TextView) view.findViewById(R.id.bItem0);
                bItem1 = (TextView) view.findViewById(R.id.bItem1);
                bItem2 = (TextView) view.findViewById(R.id.bItem2);
                bItem3 = (TextView) view.findViewById(R.id.bItem3);
                bItem4 = (TextView) view.findViewById(R.id.bItem4);
                bItem5 = (TextView) view.findViewById(R.id.bItem5);
                bItem6 = (TextView) view.findViewById(R.id.bItem6);
                bItem7 = (TextView) view.findViewById(R.id.bItem7);
                bItem8 = (TextView) view.findViewById(R.id.bItem8);
                bItem9 = (TextView) view.findViewById(R.id.bItem9);

                orderBtn = (Button)  view.findViewById(R.id.orderBtn);
                orderBtn.setOnClickListener(this);
                itemBaseLO.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.orderBtn){
                    handlePutOutP(getLayoutPosition());
                }else if (view.getId() == R.id.itemBaseLO){
                    handleClickP(getLayoutPosition(),view);
                }
            }
        }
    }

}
