package com.chu.administrator.mybaseball;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Administrator on 2018-06-12.
 */

public class AplicationClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Context.getFilesDir()에 "default.realm"란 이름의 Realm 파일이 위치한다
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
    }
}