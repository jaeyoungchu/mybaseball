package com.chu.administrator.mybaseball.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chu.administrator.mybaseball.Adapter.AwardAdapter;
import com.chu.administrator.mybaseball.BaseCoreActivity;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.BatterMatchRecord;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.Manager;
import com.chu.administrator.mybaseball.Model.ManagerUserModel;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.PithcerMatchRecord;
import com.chu.administrator.mybaseball.Model.TitleItem;
import com.chu.administrator.mybaseball.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Administrator on 2018-07-16.
 */

public class TeamDetailFragment extends BaseFragment implements View.OnClickListener{

    String teamUid;
    boolean isFromMail = false;
    ManagerUserModel teamModel;
    Button batterBtn,pitcherBtn,joinBtn;
    LinearLayout positionVS;

    @SuppressLint("ValidFragment")
    public TeamDetailFragment(String teamUid, boolean isFromMail) {
        this.teamUid = teamUid;
        this.isFromMail = isFromMail;
    }

    public TeamDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.team_detail_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        batterBtn = (Button) view.findViewById(R.id.batterBtn);
        pitcherBtn = (Button) view.findViewById(R.id.pitcherBtn);
        joinBtn = (Button) view.findViewById(R.id.joinBtn);
        positionVS = (LinearLayout) view.findViewById(R.id.positionVS);

        batterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawBatterView();
                batterBtn.setEnabled(false);
                pitcherBtn.setEnabled(true);
            }
        });

        pitcherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               drawPitcherView();
               batterBtn.setEnabled(true);
               pitcherBtn.setEnabled(false);
            }
        });

        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableUI(false);
                joinTeam1();
            }
        });

        if (!isFromMail){
            joinBtn.setVisibility(View.INVISIBLE);
        }


        super.onViewCreated(view, savedInstanceState);
    }

    FrameLayout mLO;
    FrameLayout cLO,b1LO,b2LO,ssLO,b3LO,lfLO,cfLO,rfLO,dhLO;

    private void drawBatterView(){

        positionVS.removeAllViews();
        View batterView = getLayoutInflater().inflate(R.layout.batter_position_frame, null);
        positionVS.addView(batterView);

        batterBtn.setEnabled(false);
        cLO = (FrameLayout) batterView.findViewById(R.id.cFrameLO);
        b1LO = (FrameLayout) batterView.findViewById(R.id.b1FrameLO);
        b2LO = (FrameLayout) batterView.findViewById(R.id.b2FrameLO);
        ssLO = (FrameLayout) batterView.findViewById(R.id.ssFrameLO);
        b3LO = (FrameLayout) batterView.findViewById(R.id.b3FrameLO);
        lfLO = (FrameLayout) batterView.findViewById(R.id.lfFrameLO);
        cfLO = (FrameLayout) batterView.findViewById(R.id.cfFrameLO);
        rfLO = (FrameLayout) batterView.findViewById(R.id.rfFrameLO);
        dhLO = (FrameLayout) batterView.findViewById(R.id.dhFrameLO);
        mLO = (FrameLayout) batterView.findViewById(R.id.mFrameLO);

        cLO.setOnClickListener(this);
        b1LO.setOnClickListener(this);
        b2LO.setOnClickListener(this);
        ssLO.setOnClickListener(this);
        b3LO.setOnClickListener(this);
        lfLO.setOnClickListener(this);
        cfLO.setOnClickListener(this);
        rfLO.setOnClickListener(this);
        dhLO.setOnClickListener(this);
        mLO.setOnClickListener(this);

        drawManager(teamModel.getManager(),mLO);
        drawBatterInfo1();
    }

    private void drawBatterInfo1(){
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.catcher),cLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.firstBase),b1LO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.secondBase),b2LO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.thirdBase),b3LO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.shortStop),ssLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.leftField),lfLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.centerField),cfLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.rightField),rfLO);
        drawBatterInfo2(teamModel.getBatterByPosition(GlobalValue.DH),dhLO);
    }

    private void drawBatterInfo2(Batter batter,FrameLayout frameLayout){
        TextView statTV = (TextView) frameLayout.findViewById(R.id.statTV);
        TextView nameTV = (TextView) frameLayout.findViewById(R.id.nameTV);
        LinearLayout bgLO = (LinearLayout) frameLayout.findViewById(R.id.bgLO);
        LinearLayout pictureLO = (LinearLayout) frameLayout.findViewById(R.id.pictureLO);

        bInstance.setTextViewWithColor(statTV,batter.getAverageStat());
        nameTV.setText(batter.getBatterName());

    }


    FrameLayout sp1LO,sp2LO,sp3LO,sp4LO,sp5LO,rp1LO,rp2LO,rp3LO,rp4LO;
    private void drawPitcherView(){
        positionVS.removeAllViews();
        View pitcherView = getLayoutInflater().inflate(R.layout.pitcher_position_frame, null);
        positionVS.addView(pitcherView);

        batterBtn.setEnabled(false);

        sp1LO = (FrameLayout) pitcherView.findViewById(R.id.sp1FrameLO);
        sp2LO = (FrameLayout) pitcherView.findViewById(R.id.sp2FrameLO);
        sp3LO = (FrameLayout) pitcherView.findViewById(R.id.sp3FrameLO);
        sp4LO = (FrameLayout) pitcherView.findViewById(R.id.sp4FrameLO);
        sp5LO = (FrameLayout) pitcherView.findViewById(R.id.sp5FrameLO);
        rp1LO = (FrameLayout) pitcherView.findViewById(R.id.rp1FrameLO);
        rp2LO = (FrameLayout) pitcherView.findViewById(R.id.rp2FrameLO);
        rp3LO = (FrameLayout) pitcherView.findViewById(R.id.rp3FrameLO);
        rp4LO = (FrameLayout) pitcherView.findViewById(R.id.rp4FrameLO);
        mLO = (FrameLayout) pitcherView.findViewById(R.id.mFrameLO);

        sp1LO.setOnClickListener(this);
        sp2LO.setOnClickListener(this);
        sp3LO.setOnClickListener(this);
        sp4LO.setOnClickListener(this);
        sp5LO.setOnClickListener(this);
        rp1LO.setOnClickListener(this);
        rp2LO.setOnClickListener(this);
        rp3LO.setOnClickListener(this);
        rp4LO.setOnClickListener(this);
        mLO.setOnClickListener(this);

        drawManager(teamModel.getManager(),mLO);
        drawPitcherInfo1();
    }
    private void drawManager(Manager manager,FrameLayout frameLayout){
        TextView mEnchantTV = (TextView) frameLayout.findViewById(R.id.statTV);
        mEnchantTV.setText("+"+manager.getEnchant());
        LinearLayout pictureLO = (LinearLayout) frameLayout.findViewById(R.id.pictureLO);
        pictureLO.setBackgroundResource(R.drawable.face_ex);
        TextView nameTV = (TextView) frameLayout.findViewById(R.id.nameTV);
        nameTV.setText("Manager");
    }

    private void drawPitcherInfo1(){
        drawPitcherInfo2(teamModel.getPitcherByPosition(0),sp1LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(1),sp2LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(2),sp3LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(3),sp4LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(4),sp5LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(5),rp1LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(6),rp2LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(7),rp3LO);
        drawPitcherInfo2(teamModel.getPitcherByPosition(8),rp4LO);
    }


    private void drawPitcherInfo2(Pitcher pitcher,FrameLayout frameLayout){
        TextView statTV = (TextView) frameLayout.findViewById(R.id.statTV);
        TextView nameTV = (TextView) frameLayout.findViewById(R.id.nameTV);
        LinearLayout bgLO = (LinearLayout) frameLayout.findViewById(R.id.bgLO);
        LinearLayout pictureLO = (LinearLayout) frameLayout.findViewById(R.id.pictureLO);

        bInstance.setTextViewWithColor(statTV,pitcher.getAverageStat());
        nameTV.setText(pitcher.getPitcherName());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.catcher));
                break;
            case R.id.b1FrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.firstBase));
                break;
            case R.id.b2FrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.secondBase));
                break;
            case R.id.ssFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.shortStop));
                break;
            case R.id.b3FrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.thirdBase));
                break;
            case R.id.lfFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.leftField));
                break;
            case R.id.cfFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.centerField));
                break;
            case R.id.rfFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.rightField));
                break;
            case R.id.dhFrameLO:
                popupBatterWindow(teamModel.getBatterByPosition(GlobalValue.DH));
                break;
            case R.id.sp1FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(0));
                break;
            case R.id.sp2FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(1));
                break;
            case R.id.sp3FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(2));
                break;
            case R.id.sp4FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(3));
                break;
            case R.id.sp5FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(4));
                break;
            case R.id.rp1FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(5));
                break;
            case R.id.rp2FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(6));
                break;
            case R.id.rp3FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(7));
                break;
            case R.id.rp4FrameLO:
                popupPitcherWindow(teamModel.getPitcherByPosition(8));
                break;
            case R.id.mFrameLO:
                popupManagerWindow(teamModel.getManager());
                break;
        }

    }

    private void popupManagerWindow(Manager manager){
        Log.e("j13 ",""+manager.getEnchant());


    }

    private void popupPitcherWindow(Pitcher pitcher){

        Dialog playerDialog = new Dialog(getContext(), R.style.playerDialog);
        playerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        playerDialog.setContentView(R.layout.pitcher_window_public);
        BaseCoreActivity.getInstance().setDetailWindowSize(playerDialog,(float) 1,(float)0.92);

        Window window = playerDialog.getWindow();

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);

        initPitcherDialogUI(playerDialog,pitcher);

        playerDialog.show();
    }

    private void initPitcherDialogUI(Dialog dialog, final Pitcher pitcher){
        Button titleBtn = (Button) dialog.findViewById(R.id.BDTitleTV);
        int awardCount = pitcher.getTitleItemArrayList().size();
        if (awardCount>1){
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIMES));
            titleBtn.setEnabled(true);
        }else {
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIME));
            titleBtn.setEnabled(false);
        }
        if (awardCount==1){
            titleBtn.setEnabled(true);
        }
        titleBtn.setEnabled(true);

        titleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupAwardWindow(pitcher);
            }
        });

        TextView batterDialogTotalStatTV = (TextView) dialog.findViewById(R.id.batterDialogTotalStatTV);
        bInstance.setTextViewWithColor(batterDialogTotalStatTV,pitcher.getAverageStat());

        TextView batterDialogPositionTV = (TextView) dialog.findViewById(R.id.batterDialogPositionTV);
        batterDialogPositionTV.setText(GlobalValue.getPitcherPositionString(pitcher.getPositionInt()));

        TextView batterDialogTypeTV = (TextView) dialog.findViewById(R.id.batterDialogTypeTV);
        batterDialogTypeTV.setText(GlobalValue.getPlayerTypeString(pitcher.getPitcherType()));

        LinearLayout batterDialogPlayerLevelIV = (LinearLayout) dialog.findViewById(R.id.batterDialogPlayerLevelIV);
        batterDialogPlayerLevelIV.setBackgroundResource(pitcher.getPlayerBackgroundResource());

        TextView playerLevelTV = (TextView) dialog.findViewById(R.id.playerLevelTV);
        playerLevelTV.setText(pitcher.getPlayerLevel()+"");

        TextView batterDialogNameTV = (TextView) dialog.findViewById(R.id.batterDialogNameTV);
        batterDialogNameTV.setText(pitcher.getPitcherName());

        TextView batterDialogSkill1TV = (TextView) dialog.findViewById(R.id.batterDialogSkill1TV);
        TextView batterDialogSkill1DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill1DecTV);
        if (pitcher.getPitcherSkill1() != null){
            batterDialogSkill1TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill1().getSkillLevel()));
            batterDialogSkill1TV.setText(GlobalValue.getSkillName(pitcher.getPitcherSkill1().getSkillType()));
            batterDialogSkill1DecTV.setText(pitcher.getPitcherSkill1().getSkillDescription());
        }else {
            batterDialogSkill1TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill1TV.setText("");
            batterDialogSkill1DecTV.setText("");
        }

        TextView batterDialogSkill2TV = (TextView) dialog.findViewById(R.id.batterDialogSkill2TV);
        TextView batterDialogSkill2DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill2DecTV);
        if (pitcher.getPitcherSkill2() != null){
            batterDialogSkill2TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill2().getSkillLevel()));
            batterDialogSkill2TV.setText(GlobalValue.getSkillName(pitcher.getPitcherSkill2().getSkillType()));
            batterDialogSkill2DecTV.setText(pitcher.getPitcherSkill2().getSkillDescription());
        }else {
            batterDialogSkill2TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill2TV.setText("");
            batterDialogSkill2DecTV.setText("");
        }


        TextView BDcon = (TextView) dialog.findViewById(R.id.BDcon);
        bInstance.setTextViewWithColor(BDcon,pitcher.getMovementWithItem());

        TextView BDPowerTV = (TextView) dialog.findViewById(R.id.BDPowerTV);
        bInstance.setTextViewWithColor(BDPowerTV,pitcher.getLocationWithItem());

        TextView BDEyeTV = (TextView) dialog.findViewById(R.id.BDEyeTV);
        bInstance.setTextViewWithColor(BDEyeTV,pitcher.getStuffWithItem());

        TextView BDClutchTV = (TextView) dialog.findViewById(R.id.BDClutchTV);
        bInstance.setTextViewWithColor(BDClutchTV,pitcher.getMentalWithItem());

        TextView BDspd = (TextView) dialog.findViewById(R.id.BDspd);
        bInstance.setTextViewWithColor(BDspd,pitcher.getVelocityWithItem());

        TextView BDdef = (TextView) dialog.findViewById(R.id.BDdef);
        bInstance.setTextViewWithColor(BDdef,pitcher.getHealthWithItem());

        PithcerMatchRecord m;
        m = pitcher.seasonRecord;

        TextView BDavg = (TextView) dialog.findViewById(R.id.BDavg);
        BDavg.setText(m.game+"");
        TextView BDobp = (TextView) dialog.findViewById(R.id.BDobp);
        BDobp.setText(m.getInning()+"");
        TextView BDslg = (TextView) dialog.findViewById(R.id.BDslg);
        BDslg.setText(m.getERA()+"");
        TextView BDops = (TextView) dialog.findViewById(R.id.BDops);
        BDops.setText(m.getWhip()+"");

        TextView BDgame = (TextView) dialog.findViewById(R.id.BDgame);
        BDgame.setText(m.win+"");
        TextView BDhr = (TextView) dialog.findViewById(R.id.BDhr);
        BDhr.setText(m.lose+"");
        TextView BDbb = (TextView) dialog.findViewById(R.id.BDbb);
        BDbb.setText(m.save+"");
        TextView BDso = (TextView) dialog.findViewById(R.id.BDso);
        BDso.setText(m.hold+"");

        TextView BDrbi = (TextView) dialog.findViewById(R.id.BDrbi);
        BDrbi.setText(m.so+"");
        TextView BDrun = (TextView) dialog.findViewById(R.id.BDrun);
        BDrun.setText(m.bb+"");
        TextView BDsb = (TextView) dialog.findViewById(R.id.BDsb);
        BDsb.setText(m.hit+"");
        TextView BDcs = (TextView) dialog.findViewById(R.id.BDcs);
        BDcs.setText(m.hr+"");

        TextView BDferr = (TextView) dialog.findViewById(R.id.BDferr);
        BDferr.setText(m.r+"");
        TextView BDftot = (TextView) dialog.findViewById(R.id.BDftot);
        BDftot.setText(m.er+"");
        TextView BDfrate = (TextView) dialog.findViewById(R.id.BDfrate);
        BDfrate.setText(m.getOavg()+"");
        TextView BDab = (TextView) dialog.findViewById(R.id.BDab);
        BDab.setText(m.getOslg()+"");

        TextView BDpa = (TextView) dialog.findViewById(R.id.BDpa);
        BDpa.setText(m.getSo9()+"");
        TextView BDh = (TextView) dialog.findViewById(R.id.BDh);
        BDh.setText(m.getBb9()+"");
        TextView BD2b = (TextView) dialog.findViewById(R.id.BD2b);
        BD2b.setText(m.getHr9()+"");

    }
    private void popupBatterWindow(Batter batter){

        Dialog playerDialog = new Dialog(getContext(), R.style.playerDialog);
        playerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        playerDialog.setContentView(R.layout.batter_window_public);
        BaseCoreActivity.getInstance().setDetailWindowSize(playerDialog,(float) 1,(float)0.92);

        Window window = playerDialog.getWindow();

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);

        initBatterDialogUI(playerDialog,batter);


        playerDialog.show();
    }

    private void popupAwardWindow(Object object){

        Dialog playerDialog = new Dialog(getContext(), R.style.playerDialog);
        playerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        playerDialog.setContentView(R.layout.title_popup);

        Window window = playerDialog.getWindow();

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.RIGHT;
        window.setAttributes(params);
        bInstance.setDetailWindowSize(playerDialog,0.3f,0.6f);
        RecyclerView titleRV = (RecyclerView) playerDialog.findViewById(R.id.titleRV);

        RecyclerView.Adapter titleAdapter;

        if (object instanceof Batter){
            ArrayList<TitleItem> items = new ArrayList<TitleItem>();
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
            items.add(new TitleItem("2017-4",0));
//            titleAdapter = new AwardAdapter(((Batter) object).getTitleItemArrayList());

            titleAdapter = new AwardAdapter(items);
        }else {
            ArrayList<TitleItem> items = new ArrayList<TitleItem>();
            items.add(new TitleItem("2017-4",21));
//            titleAdapter = new AwardAdapter(((Pitcher) object).getTitleItemArrayList());

            titleAdapter = new AwardAdapter(items);
        }
        titleRV.setAdapter(titleAdapter);

//        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),1);
//        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
//
//        titleRV.setLayoutManager(layoutManager);
        titleRV.setLayoutManager(new GridLayoutManager(getActivity(),1));

        playerDialog.show();
    }
    private void initBatterDialogUI(final Dialog dialog, final Batter batter){

        Button titleBtn = (Button) dialog.findViewById(R.id.BDTitleTV);
        int awardCount = batter.getTitleItemArrayList().size();
        if (awardCount>1){
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIMES));
            titleBtn.setEnabled(true);
        }else {
            titleBtn.setText(getResources().getString(R.string.AWARD)+"  "+ awardCount + " "+getResources().getString(R.string.TIME));
            titleBtn.setEnabled(false);
        }
        if (awardCount==1){
            titleBtn.setEnabled(true);
        }
        titleBtn.setEnabled(true);//j13

        titleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupAwardWindow(batter);
            }
        });

        TextView batterDialogTotalStatTV = (TextView) dialog.findViewById(R.id.batterDialogTotalStatTV);
        bInstance.setTextViewWithColor(batterDialogTotalStatTV,batter.getAverageStat());

        TextView batterDialogPositionTV = (TextView) dialog.findViewById(R.id.batterDialogPositionTV);
        batterDialogPositionTV.setText(GlobalValue.getPositionString(batter.getDefensePosition()));

        TextView batterDialogTypeTV = (TextView) dialog.findViewById(R.id.batterDialogTypeTV);
        batterDialogTypeTV.setText(GlobalValue.getPlayerTypeString(batter.getBatterType()));

        LinearLayout batterDialogPlayerLevelIV = (LinearLayout) dialog.findViewById(R.id.batterDialogPlayerLevelIV);
        batterDialogPlayerLevelIV.setBackgroundResource(batter.getPlayerBackgroundResource());

        TextView playerLevelTV = (TextView) dialog.findViewById(R.id.playerLevelTV);
        playerLevelTV.setText(batter.getPlayerLevel()+"");

        TextView batterDialogNameTV = (TextView) dialog.findViewById(R.id.batterDialogNameTV);
        batterDialogNameTV.setText(batter.getBatterName());

        TextView batterDialogSkill1TV = (TextView) dialog.findViewById(R.id.batterDialogSkill1TV);
        TextView batterDialogSkill1DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill1DecTV);
        if (batter.getBatterSkill1() != null){
            batterDialogSkill1TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill1().getSkillLevel()));
            batterDialogSkill1TV.setText(GlobalValue.getSkillName(batter.getBatterSkill1().getSkillType()));
            batterDialogSkill1DecTV.setText(batter.getBatterSkill1().getSkillDescription());
        }else {
            batterDialogSkill1TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill1TV.setText("");
            batterDialogSkill1DecTV.setText("");
        }

        TextView batterDialogSkill2TV = (TextView) dialog.findViewById(R.id.batterDialogSkill2TV);
        TextView batterDialogSkill2DecTV = (TextView) dialog.findViewById(R.id.batterDialogSkill2DecTV);
        if (batter.getBatterSkill2() != null){
            batterDialogSkill2TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill2().getSkillLevel()));
            batterDialogSkill2TV.setText(GlobalValue.getSkillName(batter.getBatterSkill2().getSkillType()));
            batterDialogSkill2DecTV.setText(batter.getBatterSkill2().getSkillDescription());
        }else {
            batterDialogSkill2TV.setBackgroundResource(R.drawable.skilllevel0);
            batterDialogSkill2TV.setText("");
            batterDialogSkill2DecTV.setText("");
        }


        TextView BDcon = (TextView) dialog.findViewById(R.id.BDcon);
        bInstance.setTextViewWithColor(BDcon,batter.getContactWithItem());

        TextView BDPowerTV = (TextView) dialog.findViewById(R.id.BDPowerTV);
        bInstance.setTextViewWithColor(BDPowerTV,batter.getPowerWithItem());

        TextView BDEyeTV = (TextView) dialog.findViewById(R.id.BDEyeTV);
        bInstance.setTextViewWithColor(BDEyeTV,batter.getEyeWithItem());

        TextView BDClutchTV = (TextView) dialog.findViewById(R.id.BDClutchTV);
        bInstance.setTextViewWithColor(BDClutchTV,batter.getClutchWithItem());

        TextView BDspd = (TextView) dialog.findViewById(R.id.BDspd);
        bInstance.setTextViewWithColor(BDspd,batter.getSpeedWithItem());

        TextView BDdef = (TextView) dialog.findViewById(R.id.BDdef);
        bInstance.setTextViewWithColor(BDdef,batter.getDefenceInPositionWithItem());

        BatterMatchRecord m;

        m = batter.seasonRecord;


        TextView BDavg = (TextView) dialog.findViewById(R.id.BDavg);
        BDavg.setText(m.getAvg()+"");
        TextView BDobp = (TextView) dialog.findViewById(R.id.BDobp);
        BDobp.setText(m.getObp()+"");
        TextView BDslg = (TextView) dialog.findViewById(R.id.BDslg);
        BDslg.setText(m.getSlg()+"");
        TextView BDops = (TextView) dialog.findViewById(R.id.BDops);
        BDops.setText(m.getOps()+"");

        TextView BDgame = (TextView) dialog.findViewById(R.id.BDgame);
        BDgame.setText(m.game+"");
        TextView BDhr = (TextView) dialog.findViewById(R.id.BDhr);
        BDhr.setText(m.homer+"");
        TextView BDbb = (TextView) dialog.findViewById(R.id.BDbb);
        BDbb.setText(m.baseOnball+"");
        TextView BDso = (TextView) dialog.findViewById(R.id.BDso);
        BDso.setText(m.strikeOut+"");

        TextView BDrbi = (TextView) dialog.findViewById(R.id.BDrbi);
        BDrbi.setText(m.rbi+"");
        TextView BDrun = (TextView) dialog.findViewById(R.id.BDrun);
        BDrun.setText(m.run+"");
        TextView BDsb = (TextView) dialog.findViewById(R.id.BDsb);
        BDsb.setText(m.sb+"");
        TextView BDcs = (TextView) dialog.findViewById(R.id.BDcs);
        BDcs.setText(m.cs+"");

        TextView BDferr = (TextView) dialog.findViewById(R.id.BDferr);
        BDferr.setText(m.error+"");
        TextView BDftot = (TextView) dialog.findViewById(R.id.BDftot);
        BDftot.setText((m.error+m.successDefense)+"");
        TextView BDfrate = (TextView) dialog.findViewById(R.id.BDfrate);
        BDfrate.setText(m.getErrorRate()+"");
        TextView BDab = (TextView) dialog.findViewById(R.id.BDab);
        BDab.setText(m.ab+"");

        TextView BDpa = (TextView) dialog.findViewById(R.id.BDpa);
        BDpa.setText(m.pa+"");
        TextView BDh = (TextView) dialog.findViewById(R.id.BDh);
        BDh.setText(m.hit+"");
        TextView BD2b = (TextView) dialog.findViewById(R.id.BD2b);
        BD2b.setText(m.doubleHit+"");
        TextView BD3b = (TextView) dialog.findViewById(R.id.BD3b);
        BD3b.setText(m.tripleHit+"");

        drawPositionLV(dialog,batter);
    }

    private void drawPositionLV(Dialog dialog,Batter batter){
        ImageView cIV,b1IV,b2IV,b3IV,ssIV,lfIV,cfIV,rfIV;
        cIV = (ImageView) dialog.findViewById(R.id.cIV);
        b1IV = (ImageView) dialog.findViewById(R.id.b1IV);
        b2IV = (ImageView) dialog.findViewById(R.id.b2IV);
        b3IV = (ImageView) dialog.findViewById(R.id.b3IV);
        ssIV = (ImageView) dialog.findViewById(R.id.ssIV);
        lfIV = (ImageView) dialog.findViewById(R.id.lfIV);
        cfIV = (ImageView) dialog.findViewById(R.id.cfIV);
        rfIV = (ImageView) dialog.findViewById(R.id.rfIV);

        ArrayList<Batter.DefencePosition> defencePositionArrayList = batter.getPositionLVList();
        for (Batter.DefencePosition defencePosition : defencePositionArrayList){
            switch (defencePosition.getPositionInt()){
                case GlobalValue.catcher:
                    cIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.firstBase:
                    b1IV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.secondBase:
                    b2IV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.thirdBase:
                    b3IV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.shortStop:
                    ssIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.leftField:
                    lfIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.centerField:
                    cfIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
                case GlobalValue.rightField:
                    rfIV.setBackgroundResource(defencePosition.getPositionLvImage());
                    break;
            }
        }

    }


    private void downloadTeamModel(){
        isFromMail = false;
        bInstance.showProgressBar();
        enableUI(false);
        DocumentReference docRef = bInstance.getDb().collection("userModel").document(teamUid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
                        int mode = Integer.parseInt(document.get("mode").toString());

                        teamModel = bInstance.getManagerModelFromServer(document);

                        if (mode == GlobalValue.PLAYERMODE){
                            bInstance.hideProgressBar();
                            enableUI(true);
                            bInstance.popupGeneralWindow("Invalid Mode Type..");
                           return;
                        }
                        Log.e("j13 ",""+teamModel.getTeamName());
                        bInstance.hideProgressBar();
                        enableUI(true);

                        drawUI();

                    } else {
                        bInstance.popupGeneralWindow("Not found Team..");
                        bInstance.hideProgressBar();
                        enableUI(true);
                    }
                } else {
                    Log.d("TeamDetail", "get failed with ", task.getException());
                    bInstance.popupGeneralWindow("Server Error..");
                    bInstance.hideProgressBar();
                    enableUI(true);
                }
            }
        });
    }

    private void joinTeam1(){

        bInstance.showProgressBar();
        DocumentReference docRef = bInstance.getDb().collection("userModel").document(teamUid);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
//                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        int mode = Integer.parseInt(document.get("mode").toString());
                        teamModel = bInstance.getManagerModelFromServer(document);
                        Log.e("j13 ","sizzzze "+ teamModel.getSubPitcherSquad().size());
                        if (mode == GlobalValue.PLAYERMODE){
                            bInstance.hideProgressBar();
                            enableUI(true);
                            bInstance.popupGeneralWindow("Invalid Mode Type..");
                            return;
                        }

                        joinTeam2();

                    } else {
                        bInstance.popupGeneralWindow("Not found Team..");
                        bInstance.hideProgressBar();
                        enableUI(true);
                    }
                } else {
                    Log.d("TeamDetail", "get failed with ", task.getException());
                    bInstance.popupGeneralWindow("Server Error..");
                    bInstance.hideProgressBar();
                    enableUI(true);
                }
            }
        });
    }

    private void joinTeam2(){
        DocumentReference docRef = bInstance.getDb().collection("userModel").document(teamUid);
        ArrayList<Batter> subBatters = teamModel.getSubBatterSquad();
        if (subBatters == null){
            subBatters = new ArrayList<>();
        }

        ArrayList<Pitcher> subPitchers = teamModel.getSubPitcherSquad();
        Log.e("j13 ","size " + subPitchers.size());
        if (subPitchers == null){
            subPitchers = new ArrayList<>();
        }

        subBatters.add(bInstance.getmPlayerUserModel().getMainBatter());
        subPitchers.add(bInstance.getmPlayerUserModel().getMainPitcher());

        Map<String,Object> subBattersMap = bInstance.gethManager().getSubBattersMap(subBatters);
        Map<String,Object> subPitchersMap = bInstance.gethManager().getSubPitchersMap(subPitchers);

        docRef.update(
                "subBatters",subBattersMap,
                "subPitchers",subPitchersMap
        ).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                bInstance.hideProgressBar();
                enableUI(true);
                bInstance.popupGeneralWindow("You join the team!");
                joinBtn.setVisibility(View.INVISIBLE);
                deleteMailBox();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
                bInstance.popupGeneralWindow("Server connection error..");
                enableUI(true);
            }
        });
    }
    private void deleteMailBox(){
        bInstance.getDb().collection("market").document(bInstance.getUID()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("TeamDetail ","deleted market");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("TeamDetail ","failed to delete market db");
            }
        });

        final CollectionReference docRef = bInstance.getDb().collection("proposal").document(bInstance.getUID()).collection("hire");

        docRef.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                for (DocumentSnapshot document : documentSnapshots){
                    if (document.exists()){
                        docRef.document(document.get("teamUid").toString()).delete();
                        Log.e("TeamDetail ","deleted proposal ");
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("TeamDetail ","failed to delete proposal");
            }
        });

    }

    private void drawUI(){
        drawBatterView();
    }

    @Override
    public void onResume() {
        downloadTeamModel();

        super.onResume();
    }

    private void enableUI(boolean flag){
        joinBtn.setEnabled(flag);
        batterBtn.setEnabled(flag);
        pitcherBtn.setEnabled(flag);
    }
}
