package com.chu.administrator.mybaseball.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chu.administrator.mybaseball.Adapter.AwardAdapter;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.ManagerUserModel;
import com.chu.administrator.mybaseball.Model.MarketRVItem;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018-07-12.
 */

public class PlayerDetailFragment extends BaseFragment {

    private MarketRVItem marketRVItem;
    private RecyclerView pitcherTitleRV,batterTitleRV;
    private RecyclerView.Adapter pitcherTitleAdapter,batterTitleAdapter;
    private TextView pTotalStatTV,pLevelTV,pNameTV,p1TV,p2TV,p3TV,p4TV,p5TV,p6TV,pSkill1TV,pSkill2TV,bTotalStatTV,bLevelTV,bNameTV,b1TV,b2TV,b3TV,b4TV,b5TV,b6TV,bSkill1TV,bSkill2TV;
    private ImageView pItem1IV,pItem2IV,pItem3IV,bItem1IV,bItem2IV,bItem3IV;
    private Button proposalBtn;
    public PlayerDetailFragment() {
    }

    @SuppressLint("ValidFragment")
    public PlayerDetailFragment(MarketRVItem marketRVItem) {
        this.marketRVItem = marketRVItem;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = true;
        return inflater.inflate(R.layout.player_detail_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        pitcherTitleRV = (RecyclerView) view.findViewById(R.id.pitcherTitleRV);
        batterTitleRV = (RecyclerView) view.findViewById(R.id.batterTitleRV);

        pitcherTitleAdapter = new AwardAdapter(marketRVItem.getPitcher().getTitleItemArrayList());
        pitcherTitleRV.setAdapter(pitcherTitleAdapter);
        pitcherTitleRV.setLayoutManager(new GridLayoutManager(getActivity(),1));

        batterTitleAdapter = new AwardAdapter(marketRVItem.getBatter().getTitleItemArrayList());
        batterTitleRV.setAdapter(batterTitleAdapter);
        batterTitleRV.setLayoutManager(new GridLayoutManager(getActivity(),1));
        initFragment(view);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bInstance.isManagerMode()){
            proposalBtn.setVisibility(View.VISIBLE);
        }else {
            proposalBtn.setVisibility(View.INVISIBLE);
        }
    }

    private void initFragment(View view){
        pTotalStatTV = (TextView) view.findViewById(R.id.pTotalStatTV);
        pLevelTV = (TextView) view.findViewById(R.id.pLevelTV);
        pNameTV = (TextView) view.findViewById(R.id.pNameTV);
        p1TV = (TextView) view.findViewById(R.id.p1TV);
        p2TV = (TextView) view.findViewById(R.id.p2TV);
        p3TV = (TextView) view.findViewById(R.id.p3TV);
        p4TV = (TextView) view.findViewById(R.id.p4TV);
        p5TV = (TextView) view.findViewById(R.id.p5TV);
        p6TV = (TextView) view.findViewById(R.id.p6TV);
        pSkill1TV = (TextView) view.findViewById(R.id.pSkill1TV);
        pSkill2TV = (TextView) view.findViewById(R.id.pSkill2TV);

        bTotalStatTV = (TextView) view.findViewById(R.id.bTotalStatTV);
        bLevelTV = (TextView) view.findViewById(R.id.bLevelTV);
        bNameTV = (TextView) view.findViewById(R.id.bNameTV);
        b1TV = (TextView) view.findViewById(R.id.b1TV);
        b2TV = (TextView) view.findViewById(R.id.b2TV);
        b3TV = (TextView) view.findViewById(R.id.b3TV);
        b4TV = (TextView) view.findViewById(R.id.b4TV);
        b5TV = (TextView) view.findViewById(R.id.b5TV);
        b6TV = (TextView) view.findViewById(R.id.b6TV);
        bSkill1TV = (TextView) view.findViewById(R.id.bSkill1TV);
        bSkill2TV = (TextView) view.findViewById(R.id.bSkill2TV);

        pItem1IV = (ImageView) view.findViewById(R.id.pItem1IV);
        pItem2IV = (ImageView) view.findViewById(R.id.pItem2IV);
        pItem3IV = (ImageView) view.findViewById(R.id.pItem3IV);

        bItem1IV = (ImageView) view.findViewById(R.id.bItem1IV);
        bItem2IV = (ImageView) view.findViewById(R.id.bItem2IV);
        bItem3IV = (ImageView) view.findViewById(R.id.bItem3IV);

        proposalBtn = (Button) view.findViewById(R.id.proposalBtn);


        proposalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupProposalWindow();
            }
        });
        assignUI();
    }


    private void assignUI(){
        Pitcher pitcher = marketRVItem.getPitcher();
        Batter batter = marketRVItem.getBatter();

        pTotalStatTV.setText(pitcher.getAverageStat()+"");
        pLevelTV.setText(pitcher.getPlayerLevel()+"");
        pNameTV.setText(pitcher.getPitcherName()+"");
        p1TV.setText(pitcher.getMovementWithItem()+"");
        p2TV.setText(pitcher.getLocationWithItem()+"");
        p3TV.setText(pitcher.getStuffWithItem()+"");
        p4TV.setText(pitcher.getVelocityWithItem()+"");
        p5TV.setText(pitcher.getMentalWithItem()+"");
        p6TV.setText(pitcher.getHealthWithItem()+"");
        if (pitcher.getPitcherSkill1() != null){
            pSkill1TV.setText(pitcher.getPitcherSkill1().getSkillName());
            pSkill1TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill1().getSkillLevel()));
        }
        if (pitcher.getPitcherSkill2() != null){
            pSkill2TV.setText(pitcher.getPitcherSkill2().getSkillName());
            pSkill2TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill2().getSkillLevel()));
        }
        pItem1IV.setBackgroundResource(R.drawable.glove1);
        pItem2IV.setBackgroundResource(R.drawable.glove1);
        pItem3IV.setBackgroundResource(R.drawable.glove1);

        bTotalStatTV.setText(batter.getAverageStat()+"");
        bLevelTV.setText(batter.getPlayerLevel()+"");
        bNameTV.setText(batter.getBatterName()+"");
        b1TV.setText(batter.getContactWithItem()+"");
        b2TV.setText(batter.getPowerWithItem()+"");
        b3TV.setText(batter.getEyeWithItem()+"");
        b4TV.setText(batter.getClutchWithItem()+"");
        b5TV.setText(batter.getSpeedWithItem()+"");
        b6TV.setText(batter.getDefenseWithItem()+"");
        if (batter.getBatterSkill1() != null){
            bSkill1TV.setText(batter.getBatterSkill1().getSkillName());
            bSkill1TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill1().getSkillLevel()));
        }
        if (batter.getBatterSkill2() != null){
            bSkill2TV.setText(batter.getBatterSkill2().getSkillName());
            bSkill2TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill2().getSkillLevel()));
        }

        bItem1IV.setBackgroundResource(R.drawable.bat1);
        bItem2IV.setBackgroundResource(R.drawable.bat1);
        bItem3IV.setBackgroundResource(R.drawable.bat1);

    }
    private void popupProposalWindow(){
        final Dialog dialog = new Dialog(getContext(), R.style.generalDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.proposal_window);

        Button confirmBtn = (Button) dialog.findViewById(R.id.confirmBtn);
        Button cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
        final EditText messageET = (EditText) dialog.findViewById(R.id.messageET);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(messageET.getText().toString())){
                    messageET.setError("please input welcome message..");
                }else {
                    uploadProposal(messageET.getText().toString());
                }
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void uploadProposal(String message){
        if (!bInstance.isManagerMode()){
            bInstance.popupGeneralWindow("Invalid access..");
            return;
        }

        bInstance.showProgressBar();
        Map<String, Object> proposalInfo = new HashMap<>();
        ManagerUserModel managerUserModel = bInstance.getmManagerUserModel();

        proposalInfo.put("isNew", true);
        proposalInfo.put("teamUid", managerUserModel.getUid());
        proposalInfo.put("message", message);
        proposalInfo.put("teamName", managerUserModel.getTeamName());
        proposalInfo.put("deviceId", bInstance.getDeviceID());
        proposalInfo.put("timestamp", FieldValue.serverTimestamp());

        DocumentReference proposalRef = bInstance.getDb().collection("proposal").document(marketRVItem.getUid()).collection("hire").document(managerUserModel.getUid());


        proposalRef.set(proposalInfo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                bInstance.hideProgressBar();
                bInstance.popupGeneralWindow("You have proposed to player.");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
                bInstance.popupGeneralWindow("Error in Sign up..");
            }
        });

    }
}
