package com.chu.administrator.mybaseball.Model;

/**
 * Created by Administrator on 2018-06-26.
 */

public class Manager {

    private int enchant;
    private int contact,power,eye,clutch,speed,defense,movement,location,stuff,velocity,mental,health;
    private String deviceId,uID,cardToken;

    public Manager(String deviceId, String uID, String cardToken) {
        this.enchant = 1;
        this.deviceId = deviceId;
        this.uID = uID;
        this.cardToken = cardToken;
    }

    public Manager(int contact, int power, int eye, int clutch, int speed, int defense, int movement, int location, int stuff, int velocity, int mental, int health, String deviceId, String uID, String cardToken) {
        this.enchant = 1;
        this.contact = contact;
        this.power = power;
        this.eye = eye;
        this.clutch = clutch;
        this.speed = speed;
        this.defense = defense;
        this.movement = movement;
        this.location = location;
        this.stuff = stuff;
        this.velocity = velocity;
        this.mental = mental;
        this.health = health;
        this.deviceId = deviceId;
        this.uID = uID;
        this.cardToken = cardToken;
    }

    public int getEnchant() {
        return enchant;
    }

    public int getContactEnchanted() {

        return contact +enchant-1;
    }

    public int getPowerEnchanted() {
        return power*enchant;
    }

    public int getEyeEnchanted() {
        return eye*enchant;
    }

    public int getClutchEnchanted() {
        return clutch*enchant;
    }

    public int getSpeedEnchanted() {
        return speed*enchant;
    }

    public int getDefenseEnchanted() {
        return defense*enchant;
    }

    public int getMovementEnchanted() {
        return movement*enchant;
    }

    public int getLocationEnchanted() {
        return location*enchant;
    }

    public int getStuffEnchanted() {
        return stuff*enchant;
    }

    public int getVelocityEnchanted() {
        return velocity*enchant;
    }

    public int getMentalEnchanted() {
        return mental*enchant;
    }

    public int getHealthEnchanted() {
        return health*enchant;
    }

    public int getContact() {
        return contact;
    }

    public int getPower() {
        return power;
    }

    public int getEye() {
        return eye;
    }

    public int getClutch() {
        return clutch;
    }

    public int getSpeed() {
        return speed;
    }

    public int getDefense() {
        return defense;
    }

    public int getMovement() {
        return movement;
    }

    public int getLocation() {
        return location;
    }

    public int getStuff() {
        return stuff;
    }

    public int getVelocity() {
        return velocity;
    }

    public int getMental() {
        return mental;
    }

    public int getHealth() {
        return health;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getuID() {
        return uID;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setEnchant(int enchant) {
        this.enchant = enchant;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setEye(int eye) {
        this.eye = eye;
    }

    public void setClutch(int clutch) {
        this.clutch = clutch;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public void setStuff(int stuff) {
        this.stuff = stuff;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    public void setMental(int mental) {
        this.mental = mental;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "enchant=" + enchant +
                ", contact=" + contact +
                ", power=" + power +
                ", eye=" + eye +
                ", clutch=" + clutch +
                ", speed=" + speed +
                ", defense=" + defense +
                ", movement=" + movement +
                ", location=" + location +
                ", stuff=" + stuff +
                ", velocity=" + velocity +
                ", mental=" + mental +
                ", health=" + health +
                ", deviceId='" + deviceId + '\'' +
                ", uID='" + uID + '\'' +
                ", cardToken='" + cardToken + '\'' +
                ", getContactEnchanted=" + getContactEnchanted() +
                ", getPowerEnchanted=" + getPowerEnchanted() +
                ", getEyeEnchanted=" + getEyeEnchanted() +
                ", getClutchEnchanted=" + getClutchEnchanted() +
                ", getSpeedEnchanted=" + getSpeedEnchanted() +
                ", getDefenseEnchanted=" + getDefenseEnchanted() +
                ", getMovementEnchanted=" + getMovementEnchanted() +
                ", getLocationEnchanted=" + getLocationEnchanted() +
                ", getStuffEnchanted=" + getStuffEnchanted() +
                ", getVelocityEnchanted=" + getVelocityEnchanted() +
                ", getMentalEnchanted=" + getMentalEnchanted() +
                ", getHealthEnchanted=" + getHealthEnchanted() +

                '}';
    }
}
