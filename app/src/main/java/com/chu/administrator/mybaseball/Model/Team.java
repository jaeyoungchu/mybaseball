package com.chu.administrator.mybaseball.Model;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-06-28.
 */

public class Team {
    private String teamName;
    private int pvpWinNum,pvpLoseNum,pvpPoint;
    private ArrayList<Batter> batterSquad;
    private ArrayList<Pitcher> pitcherSquad;

    public Team(){
        this.teamName = "";
        this.pvpPoint = 1000;
        this.pvpWinNum = 0;
        this.pvpLoseNum = 0;
        this.batterSquad = new ArrayList<Batter>();
        this.pitcherSquad = new ArrayList<Pitcher>();
    }

    public Team(String teamName, ArrayList<Batter> batterSquad, ArrayList<Pitcher> pitcherSquad) {
        this.pvpPoint = 1000;
        this.pvpWinNum = 0;
        this.pvpLoseNum = 0;
        this.batterSquad = sortInBatOrder(batterSquad);
        this.pitcherSquad = sortInPitcherOrder(pitcherSquad);
        this.teamName = teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setBatterSquad(ArrayList<Batter> batterSquad) {
        this.batterSquad = batterSquad;
    }

    public void setPitcherSquad(ArrayList<Pitcher> pitcherSquad) {
        this.pitcherSquad = pitcherSquad;
    }


    private ArrayList<Batter> sortInBatOrder(ArrayList<Batter> batterSquad){
        ArrayList<Batter> orderedBatters = new ArrayList<>();
        for (int i = 0; i < batterSquad.size() ; i++){
            orderedBatters.add(batterSquad.get(i).getBattingOrder(), batterSquad.get(i));
        }
        return orderedBatters;
    }

    private ArrayList<Pitcher> sortInPitcherOrder(ArrayList<Pitcher> pitcherSquad){
        ArrayList<Pitcher> orderedPitcers = new ArrayList<>();
        for (int i = 0; i < pitcherSquad.size() ; i++){
            orderedPitcers.add(pitcherSquad.get(i).getPositionInt(), pitcherSquad.get(i));
        }
        return orderedPitcers;
    }


    public void resetTeamRecord(){

        pvpWinNum = 0;
        pvpLoseNum = 0;
        pvpPoint = 1000;
        for (Batter batter : batterSquad){
            batter.getSeasonRecord().resetRecord();
        }
        for (Pitcher pitcher : pitcherSquad){
            pitcher.getSeasonRecord().resetRecord();
        }
    }

    public int getPvpPoint() {
        return pvpPoint;
    }

    public void setPvpPoint(int pvpPoint) {
        this.pvpPoint = pvpPoint;
    }



    public int getPvpWinNum() {
        return pvpWinNum;
    }

    public int getPvpLoseNum() {
        return pvpLoseNum;
    }

    public void setPvpWinNum(int pvpWinNum) {
        this.pvpWinNum = pvpWinNum;
    }

    public void setPvpLoseNum(int pvpLoseNum) {
        this.pvpLoseNum = pvpLoseNum;
    }


    public ArrayList<Batter> getBatterSquad(){
        ArrayList<Batter> returnList = new ArrayList<Batter>();
        for (int i = 0; i < batterSquad.size(); i++){
            returnList.add(i, batterSquad.get(i));
        }
        return returnList;
    }

    public ArrayList<Pitcher> getPitcherSquad(){
        ArrayList<Pitcher> returnList = new ArrayList<Pitcher>();
        for (int i = 0; i < pitcherSquad.size(); i++){
            returnList.add(i, pitcherSquad.get(i));
        }
        return returnList;
    }

    public String getTeamAvg(){
        float avg = 0;
        for (int i = 0; i < batterSquad.size() ; i++){
            avg += batterSquad.get(i).getSeasonRecord().getAvgFloat();
        }
        avg = avg/ batterSquad.size();
        avg = avg * 1000;
        String str = String.format("%.0f",avg);
        if (avg == 0){
            return "0.000";
        }
        return "0."+str;
    }

    public String getTeamObp(){

        float avg,bbRate,obp;

        String returnStr;
        int hit = 0, ab = 0, baseOnball = 0,pa = 0;

        for (int i = 0; i < batterSquad.size() ; i++){
            BatterMatchRecord record = batterSquad.get(i).getSeasonRecord();
            if (record.pa !=0){
                hit += record.hit;
                ab += record.ab;
                baseOnball += record.baseOnball;
                pa += record.pa;
            }
        }
        if (pa != 0){
            avg = (float)hit/(float)ab;

            bbRate = (float)baseOnball/(float)pa;
            obp = avg+bbRate;
            returnStr = String.format("%.3f",obp);
            if (obp >= 1){
                return returnStr;
            }else {
                return returnStr.substring(1);
            }
        }else {
            return ".000";
        }
    }

    public String getTeamOps(){
        float ops = 0;
        for (int i = 0; i < batterSquad.size() ; i++){
            ops += Float.parseFloat(batterSquad.get(i).getSeasonRecord().getOps());
        }
        ops = ops/ batterSquad.size();

        if (ops == 0){
            return "0.000";
        }

        String str = String.format("%.3f",ops);
        return str;
    }
    public int getTeamHomerun(){
        int hr = 0;
        for (int i = 0; i < batterSquad.size() ; i++){
            hr += batterSquad.get(i).getSeasonRecord().homer;
        }
        return hr;
    }

    public int getTeamSB(){
        int sb = 0;
        for (int i = 0; i < batterSquad.size() ; i++){
            sb += batterSquad.get(i).getSeasonRecord().sb;
        }
        return sb;
    }

    public String getTeamFielding(){

        float fielding = 0;
        int err = 0;
        int suc = 0;
        for (int i = 0; i < batterSquad.size() ; i++){
            err += batterSquad.get(i).getSeasonRecord().error;
            suc += batterSquad.get(i).getSeasonRecord().successDefense;
        }
        if ((err+suc) == 0){
            return "1.000";
        }else {
            fielding = (float) suc / (float)(err+suc);
            fielding = fielding*1000;
            String str = String.format("%.0f",fielding);
            return "0."+str;
        }
    }

    public String getTeamEra(){
        float era = 0f;
        int er=0;
        int outcount = 0;
        for (int i = 0; i < pitcherSquad.size(); i++){

            er += pitcherSquad.get(i).getSeasonRecord().er;
            outcount += pitcherSquad.get(i).getSeasonRecord().outcount;
        }
        era = (float) er*9/(float)(outcount/3);
        String str = String.format("%.2f",era);
        if (outcount == 0){
            return "0.00";
        }else {
            return str;
        }
    }

    public String getTeamWhip(){

        int bb = 0;
        int hit = 0;
        int outcount = 0;

        for (int i = 0; i < pitcherSquad.size(); i++){
            bb += pitcherSquad.get(i).getSeasonRecord().bb;
            hit += pitcherSquad.get(i).getSeasonRecord().hit;
            outcount += pitcherSquad.get(i).getSeasonRecord().outcount;
        }

        float whip = (float) (bb + hit)/(float) (outcount/3);
        String str = String.format("%.3f",whip);
        if (outcount == 0){
            return "0.000";
        }else {
            return str;
        }
    }

    public int getTeamSO(){
        int so = 0;
        for (int i = 0; i < pitcherSquad.size(); i++){
            so += pitcherSquad.get(i).getSeasonRecord().so;
        }
        return so;
    }
    public int getTeamRun(){
        int run = 0;
        for (int i = 0; i < batterSquad.size(); i++){
            run += batterSquad.get(i).getSeasonRecord().run;
        }
        return run;
    }

    public String getTeamName() {
        return teamName;
    }

    public Batter getBatterByName(String name){
        Batter returnBatter = null;
        for (Batter batter : batterSquad){
            if (batter.getBatterName().equals(name)){
                returnBatter = batter;
                break;
            }
        }
        return returnBatter;
    }

    public Pitcher getPitcherByName(String name){
        Pitcher returnPitcher = null;
        for (Pitcher pitcher : pitcherSquad){
            if (pitcher.getPitcherName().equals(name)){
                returnPitcher = pitcher;
                break;
            }
        }
        return returnPitcher;
    }

    @Override
    public String toString() {
        String returnStr = "Team{" +
                "teamName='" + teamName + '\'' +
                ", winNum=" + pvpWinNum +
                ", loseNum=" + pvpLoseNum +
                ", statPoint=" + pvpPoint +
                '}';

        for (Batter batter : batterSquad){
            returnStr += '\n' + batter.toString();
        }

        for (Pitcher pitcher : pitcherSquad){
            returnStr += '\n' + pitcher.toString();
        }

        return returnStr;
    }
}