package com.chu.administrator.mybaseball.Helper;

import android.util.Log;

import com.chu.administrator.mybaseball.BaseCoreActivity;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.BatterMatchRecord;
import com.chu.administrator.mybaseball.Model.BatterSkill;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.Item;
import com.chu.administrator.mybaseball.Model.Manager;
import com.chu.administrator.mybaseball.Model.ManagerUserModel;
import com.chu.administrator.mybaseball.Model.MarketRVItem;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.PitcherSkill;
import com.chu.administrator.mybaseball.Model.PithcerMatchRecord;
import com.chu.administrator.mybaseball.Model.PlayerUserModel;
import com.chu.administrator.mybaseball.Model.Team;
import com.chu.administrator.mybaseball.Model.TitleItem;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018-06-22.
 */

public class HashMapManager {

    public Map<String,Object> createMarketModelMap(Map<String,Object> marketInfo, MarketRVItem marketRVItem){

        Map<String, Object> batterInfo = new HashMap<>();
        marketInfo.put("batter",createBatterMap(batterInfo,marketRVItem.getBatter()));

        Map<String, Object> pitcherInfo = new HashMap<>();
        marketInfo.put("pitcher",createPitcherMap(pitcherInfo,marketRVItem.getPitcher()));

        marketInfo.put("totalStat",marketRVItem.getTotalStat());
        marketInfo.put("uid",marketRVItem.getUid());
        return marketInfo;
    }

    public MarketRVItem getMarketRVItem(DocumentSnapshot marketInfo){

        int totalStat = Integer.parseInt(marketInfo.get("totalStat").toString());
        String uid = marketInfo.get("uid").toString();

        Map<String,Object> batterInfo = (Map<String,Object>)marketInfo.get("batter");
        Batter batter = getBatterFromMap(batterInfo);

        Map<String,Object> pitcherInfo = (Map<String,Object>)marketInfo.get("pitcher");
        Pitcher pitcher = getPitcherFromMap(pitcherInfo);

        return new MarketRVItem(batter,pitcher,totalStat,uid);
    }

    public Map<String,Object> createManagerModeModelMap(Map<String,Object> modeInfo, ManagerUserModel managerUserModel) {
        modeInfo.put("deviceId", BaseCoreActivity.getInstance().getDeviceID());
        modeInfo.put("uid", managerUserModel.getUid());
        modeInfo.put("teamName", managerUserModel.getTeamName());
        modeInfo.put("cash", managerUserModel.getCash());
        modeInfo.put("gp", managerUserModel.getGp());
        modeInfo.put("mode", managerUserModel.getModeType());
        modeInfo.put("version", GlobalValue.version+"");
        modeInfo.put("timestamp", FieldValue.serverTimestamp());

        Map<String, Object> managerInfo = new HashMap<>();
        Manager manager = managerUserModel.getManager();
        modeInfo.put("manager",createManagerMap(managerInfo,manager));


        Team team = managerUserModel.getTeam();

        modeInfo.put("team",createTeam(team));

        Map<String, Object> batterInfo;

        ArrayList<Batter> subBatterList = managerUserModel.getSubBatterSquad();
        Map<String, Object> subBattersInfo = new HashMap<>();
        if (subBatterList.size() >0){
            for (int i = 0 ; i < subBatterList.size() ; i++){
                batterInfo = new HashMap<>();
                subBattersInfo.put("batter"+i,createBatterMap(batterInfo,subBatterList.get(i)));
            }
            modeInfo.put("subBatters",subBattersInfo);
        }

        Map<String, Object> pitcherInfo;
        ArrayList<Pitcher> subPitcherList = managerUserModel.getSubPitcherSquad();
        Map<String, Object> subPitchersInfo = new HashMap<>();
        if (subPitcherList.size() >0){
            for (int i = 0 ; i < subPitcherList.size() ; i++){
                pitcherInfo = new HashMap<>();
                subPitchersInfo.put("pitcher"+i,createPitcherMap(pitcherInfo,subPitcherList.get(i)));
            }
            modeInfo.put("subPitchers",subPitchersInfo);
        }
        return modeInfo;
    }

    public Map<String, Object> getSubBattersMap(ArrayList<Batter> subBatterList){
        Map<String, Object> batterInfo;
        Map<String, Object> subBattersInfo = new HashMap<>();
        if (subBatterList.size() >0){
            for (int i = 0 ; i < subBatterList.size() ; i++){
                batterInfo = new HashMap<>();
                subBattersInfo.put("batter"+i,createBatterMap(batterInfo,subBatterList.get(i)));
            }
        }
        return subBattersInfo;
    }

    public Map<String, Object> getSubPitchersMap(ArrayList<Pitcher> subPitcherList){
        Map<String, Object> pitcherInfo;
        Map<String, Object> subPitchersInfo = new HashMap<>();
        if (subPitcherList.size() >0){
            for (int i = 0 ; i < subPitcherList.size() ; i++){
                pitcherInfo = new HashMap<>();
                subPitchersInfo.put("pitcher"+i,createPitcherMap(pitcherInfo,subPitcherList.get(i)));
            }
        }
        return subPitchersInfo;
    }

    public Map<String, Object> createTeam(Team team){
        Map<String, Object> teamInfo = new HashMap<>();

        teamInfo.put("teamName",team.getTeamName());
        teamInfo.put("pvpWinNum",team.getPvpWinNum());
        teamInfo.put("pvpLoseNum",team.getPvpLoseNum());
        teamInfo.put("pvpPoint",team.getPvpPoint());


        ArrayList<Batter> batterSquad = team.getBatterSquad();
        Map<String, Object> batterSquadsInfo = new HashMap<>();
        Map<String, Object> batterInfo;

        for (int i = 0 ; i < batterSquad.size() ; i++){
            batterInfo = new HashMap<>();
            batterSquadsInfo.put("batter"+i,createBatterMap(batterInfo,batterSquad.get(i)));
        }

        teamInfo.put("batterSquad",batterSquadsInfo);


        ArrayList<Pitcher> pitcherSquad = team.getPitcherSquad();
        Map<String, Object> pitcherSquadsInfo = new HashMap<>();
        Map<String, Object> pitcherInfo;

        for (int i = 0 ; i < pitcherSquad.size() ; i++){
            pitcherInfo = new HashMap<>();
            pitcherSquadsInfo.put("pitcher"+i,createPitcherMap(pitcherInfo,pitcherSquad.get(i)));
        }
        teamInfo.put("pitcherSquad",pitcherSquadsInfo);

        return teamInfo;
    }


    public ManagerUserModel getManagerModelFromDocument(DocumentSnapshot document,ManagerUserModel managerUserModel){

        String teamName = document.get("teamName").toString();
        managerUserModel.setTeamName(teamName);



        Map<String,Object> subBatterListInfo = (Map<String,Object>)document.get("subBatters");
        Map<String,Object> subBatterInfo =new HashMap<>();
        ArrayList<Batter> subBatterSquad = new ArrayList<>();

        Batter subBatter;


        for (int i = 0 ; i < 20 ; i++){
            if (subBatterListInfo == null ||subBatterListInfo.get("batter"+i) == null ){
                break;
            }
            subBatterInfo = (Map<String,Object>)subBatterListInfo.get("batter"+i);
            subBatter = getBatterFromMap(subBatterInfo);
            subBatterSquad.add(i,subBatter);
        }

        Map<String,Object> subPitcherListInfo = (Map<String,Object>)document.get("subPitchers");
        Map<String,Object> subPitcherInfo =new HashMap<>();
        ArrayList<Pitcher> subPitcherSquad = new ArrayList<>();

        Pitcher subPitcher;

        for (int i = 0 ; i < 20 ; i++){
            if (subPitcherListInfo == null || subPitcherListInfo.get("pitcher"+i) == null){
                break;
            }
            subPitcherInfo = (Map<String,Object>)subPitcherListInfo.get("pitcher"+i);
            subPitcher = getPitcherFromMap(subPitcherInfo);
            subPitcherSquad.add(i,subPitcher);
        }

        Map<String,Object> managerInfo = (Map<String,Object>)document.get("manager");

        Manager manager = getManagerFromMap(managerInfo);


        Map<String,Object> teamInfo = (Map<String,Object>)document.get("team");
        Team team = getTeamFromMap(teamInfo);

        managerUserModel.setTeam(team);
        managerUserModel.setSubBatterSquad(subBatterSquad);
        managerUserModel.setSubPitcherSquad(subPitcherSquad);
        managerUserModel.setManager(manager);

        return managerUserModel;
    }

    public Team getTeamFromMap(Map<String,Object> teamInfo){

        String teamName = teamInfo.get("teamName").toString();
        int pvpWinNum = Integer.parseInt(teamInfo.get("pvpWinNum").toString());
        int pvpLoseNum = Integer.parseInt(teamInfo.get("pvpLoseNum").toString());
        int pvpPoint = Integer.parseInt(teamInfo.get("pvpPoint").toString());

        Team team = new Team();
        team.setTeamName(teamName);
        team.setPvpWinNum(pvpWinNum);
        team.setPvpLoseNum(pvpLoseNum);
        team.setPvpPoint(pvpPoint);

        Map<String,Object> batterListInfo = (Map<String,Object>)teamInfo.get("batterSquad");
        Map<String,Object> batterInfo =new HashMap<>();
        ArrayList<Batter> batterSquad = new ArrayList<>();

        Batter batter;

        for (int i = 0 ; i < 9 ; i++){
            batterInfo = (Map<String,Object>)batterListInfo.get("batter"+i);
            batter = getBatterFromMap(batterInfo);
            batterSquad.add(i,batter);
        }

        Map<String,Object> pitcherListInfo = (Map<String,Object>)teamInfo.get("pitcherSquad");
        Map<String,Object> pitcherInfo =new HashMap<>();
        ArrayList<Pitcher> pitcherSquad = new ArrayList<>();

        Pitcher pitcher;

        for (int i = 0 ; i < 9 ; i++){
            pitcherInfo = (Map<String,Object>)pitcherListInfo.get("pitcher"+i);
            pitcher = getPitcherFromMap(pitcherInfo);
            pitcherSquad.add(i,pitcher);
        }

        team.setBatterSquad(batterSquad);
        team.setPitcherSquad(pitcherSquad);

        return team;

    }

    public Manager getManagerFromMap(Map<String,Object> managerInfo){

        Manager manager;

        int enchant = Integer.parseInt(managerInfo.get("enchant").toString());
        int contact = Integer.parseInt(managerInfo.get("contact").toString());
        int power = Integer.parseInt(managerInfo.get("power").toString());
        int eye = Integer.parseInt(managerInfo.get("eye").toString());
        int clutch = Integer.parseInt(managerInfo.get("clutch").toString());
        int speed = Integer.parseInt(managerInfo.get("speed").toString());
        int defense = Integer.parseInt(managerInfo.get("defense").toString());
        int movement = Integer.parseInt(managerInfo.get("movement").toString());
        int location = Integer.parseInt(managerInfo.get("location").toString());
        int stuff = Integer.parseInt(managerInfo.get("stuff").toString());
        int velocity = Integer.parseInt(managerInfo.get("velocity").toString());
        int mental = Integer.parseInt(managerInfo.get("mental").toString());
        int health = Integer.parseInt(managerInfo.get("health").toString());
        String deviceId = managerInfo.get("deviceId").toString();
        String uID = managerInfo.get("uID").toString();
        String cardToken = managerInfo.get("cardToken").toString();

        manager = new Manager(contact,power,eye,clutch,speed,defense,movement,location,stuff,velocity,mental,health,deviceId,uID,cardToken);

        return manager;
    }

    public Pitcher getPitcherFromMap(Map<String,Object> mainPitcherInfo){
        Pitcher pitcher;

        String teamName = mainPitcherInfo.get("teamName").toString();
        String name = mainPitcherInfo.get("name").toString();
        String uid = mainPitcherInfo.get("uid").toString();
        String cardToken = mainPitcherInfo.get("cardToken").toString();

        int positionInt = Integer.parseInt(mainPitcherInfo.get("positionInt").toString());
        int playerExp = Integer.parseInt(mainPitcherInfo.get("playerExp").toString());
        int playerLevel = Integer.parseInt(mainPitcherInfo.get("playerLevel").toString());
        int type = Integer.parseInt(mainPitcherInfo.get("type").toString());
        int maxPitchOrder = Integer.parseInt(mainPitcherInfo.get("MaxPitchOrder").toString());

        int movement = Integer.parseInt(mainPitcherInfo.get("movement").toString());
        int location = Integer.parseInt(mainPitcherInfo.get("location").toString());
        int stuff = Integer.parseInt(mainPitcherInfo.get("stuff").toString());
        int velocity = Integer.parseInt(mainPitcherInfo.get("velocity").toString());
        int mental = Integer.parseInt(mainPitcherInfo.get("mental").toString());
        int health = Integer.parseInt(mainPitcherInfo.get("health").toString());

        pitcher = new Pitcher(uid,cardToken,name,movement,location,velocity,stuff,mental,health,type);
        pitcher.setPositionInt(positionInt);
        pitcher.setTeamName(teamName);
        pitcher.setMaxPitchOrder(maxPitchOrder);
        pitcher.setPlayerExp(playerExp);
        pitcher.setPlayerLevel(playerLevel);

        Map<String,Object> recordInfo;

        recordInfo = (Map<String,Object>)mainPitcherInfo.get("matchRecord");
        PithcerMatchRecord match = getPitcherMatchRecordFromMap(recordInfo);
        pitcher.setMatchRecord(match);

        recordInfo = (Map<String,Object>)mainPitcherInfo.get("seasonRecord");
        PithcerMatchRecord season = getPitcherMatchRecordFromMap(recordInfo);
        pitcher.setMatchRecord(season);

        if (mainPitcherInfo.get("cap") != null){
            Map<String,Object> itemInfo = (Map<String,Object>)mainPitcherInfo.get("cap");
            Item cap = getItemFromMap(itemInfo);
            pitcher.setCap(cap);
        }
        if (mainPitcherInfo.get("uniform") != null){
            Map<String,Object> itemInfo = (Map<String,Object>)mainPitcherInfo.get("uniform");
            Item uniform = getItemFromMap(itemInfo);
            pitcher.setUniform(uniform);
        }

        if (mainPitcherInfo.get("shoes") != null){
            Map<String,Object> itemInfo = (Map<String,Object>)mainPitcherInfo.get("shoes");
            Item shoes = getItemFromMap(itemInfo);
            pitcher.setShoes(shoes);
        }

        if (mainPitcherInfo.get("titles") != null){
            Map<String,Object> titlesInfo = (Map<String,Object>)mainPitcherInfo.get("titles");

            ArrayList<TitleItem> titleItemArrayList = getTitleList(titlesInfo);
            pitcher.setTitleItemArrayList(titleItemArrayList);
        }

        if (mainPitcherInfo.get("pitcherSkill1") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainPitcherInfo.get("pitcherSkill1");
            PitcherSkill skill = getPitcherSkillFromMap(skillInfo);
            pitcher.setPitcherSkill1(skill);
        }
        if (mainPitcherInfo.get("pitcherSkill2") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainPitcherInfo.get("pitcherSkill2");
            PitcherSkill skill = getPitcherSkillFromMap(skillInfo);
            pitcher.setPitcherSkill2(skill);
        }
        if (mainPitcherInfo.get("pitcherSkill3") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainPitcherInfo.get("pitcherSkill3");
            PitcherSkill skill = getPitcherSkillFromMap(skillInfo);
            pitcher.setPitcherSkill3(skill);
        }
        if (mainPitcherInfo.get("pitcherSkill4") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainPitcherInfo.get("pitcherSkill4");
            PitcherSkill skill = getPitcherSkillFromMap(skillInfo);
            pitcher.setPitcherSkill4(skill);
        }

        return pitcher;
    }

    public Map<String,Object> createPlayerModeModelMap(Map<String,Object> modeInfo, PlayerUserModel playerUserModel) {

        modeInfo.put("uid", playerUserModel.getUid());
        modeInfo.put("mode", playerUserModel.getModeType());
        modeInfo.put("cash", playerUserModel.getCash());
        modeInfo.put("gp", playerUserModel.getGp());
        modeInfo.put("historyCap",playerUserModel.getHistoryCap());
        modeInfo.put("version", GlobalValue.version+"");
        modeInfo.put("deviceId", BaseCoreActivity.getInstance().getDeviceID());
        modeInfo.put("timestamp", FieldValue.serverTimestamp());
        modeInfo.put("teamUid", playerUserModel.getTeamUid());

//        Map<String, Object> mainBatterInfo = new1 HashMap<>();
//        Batter mainBatter = playerUserModel.getMainBatter();
//        modeInfo.put("mainBatter", createBatterMap(mainBatterInfo,mainBatter));
//
//
//        Map<String, Object> mainPitcherInfo = new1 HashMap<>();
//        Pitcher mainPitcher = playerUserModel.getMainPitcher();
//        modeInfo.put("mainPitcher",createPitcherMap(mainPitcherInfo,mainPitcher));


        Map<String,Object> subItemsInfo = new HashMap<>();
        ArrayList<Item> subItems = playerUserModel.getSubItems();
        Map<String,Object> itemsInfo;
        for (int i = 0 ; i< subItems.size() ; i++){
            itemsInfo = new HashMap<>();
            subItemsInfo.put("item"+i, createItemMap(itemsInfo,subItems.get(i)));
        }
        modeInfo.put("subItem",subItemsInfo);


        Map<String,Object> subBattersInfo = new HashMap<>();
        ArrayList<Batter> subBatters = playerUserModel.getSubBatters();
        subBatters.add(0,playerUserModel.getMainBatter());
        Map<String,Object> subBatterInfo;
        for (int i = 0; i < subBatters.size() ; i++){
            subBatterInfo = new HashMap<>();
            subBattersInfo.put("batter" +i , createBatterMap(subBatterInfo,subBatters.get(i)));
        }
        modeInfo.put("batters",subBattersInfo);


        Map<String,Object> subPitchersInfo = new HashMap<>();
        ArrayList<Pitcher> subPitchers = playerUserModel.getSubPitchers();
        subPitchers.add(0,playerUserModel.getMainPitcher());
        Map<String,Object> subPitcherInfo;
        for (int i = 0; i < subPitchers.size() ; i++){
            subPitcherInfo = new HashMap<>();
            subPitchersInfo.put("pitcher" +i , createPitcherMap(subPitcherInfo,subPitchers.get(i)));
        }
        modeInfo.put("pitchers",subPitchersInfo);

        return modeInfo;
    }

    public Map<String,Object> createManagerMap(Map<String,Object> managerInfo, Manager manager){

        managerInfo.put("enchant",manager.getEnchant());
        managerInfo.put("contact",manager.getContact());
        managerInfo.put("power",manager.getPower());
        managerInfo.put("eye",manager.getEye());
        managerInfo.put("clutch",manager.getClutch());
        managerInfo.put("speed",manager.getSpeed());
        managerInfo.put("defense",manager.getDefense());
        managerInfo.put("movement",manager.getMovement());
        managerInfo.put("location",manager.getLocation());
        managerInfo.put("stuff",manager.getStuff());
        managerInfo.put("velocity",manager.getVelocity());
        managerInfo.put("mental",manager.getMental());
        managerInfo.put("health",manager.getHealth());
        managerInfo.put("deviceId",manager.getDeviceId());
        managerInfo.put("uID",manager.getuID());
        managerInfo.put("cardToken",manager.getCardToken());

        return managerInfo;
    }

    public Map<String,Object> createPitcherMap(Map<String,Object> pitcherInfo, Pitcher pitcher){

        pitcherInfo.put("teamName",pitcher.getPitcherType());
        pitcherInfo.put("name",pitcher.getPitcherName());
        pitcherInfo.put("uid",pitcher.getuID());
        pitcherInfo.put("cardToken",pitcher.getCardToken());
        pitcherInfo.put("positionInt", pitcher.getPositionInt());
        pitcherInfo.put("playerExp",pitcher.getPlayerExp());
        pitcherInfo.put("playerLevel",pitcher.getPlayerLevel());
        pitcherInfo.put("type",pitcher.getPitcherType());
        pitcherInfo.put("position",pitcher.getPositionString());
        pitcherInfo.put("MaxPitchOrder",pitcher.getMaxPitchOrder());

        pitcherInfo.put("movement",pitcher.movement);
        pitcherInfo.put("location",pitcher.location);
        pitcherInfo.put("stuff",pitcher.stuff);
        pitcherInfo.put("velocity",pitcher.velocity);
        pitcherInfo.put("mental",pitcher.mental);
        pitcherInfo.put("health",pitcher.health);

        Map<String, Object> pitcherRecordInfo;

        pitcherRecordInfo = new HashMap<>();
        pitcherInfo.put("matchRecord", createPitcherRecordMap(pitcherRecordInfo,pitcher.getMatchRecord()));

        pitcherRecordInfo = new HashMap<>();
        pitcherInfo.put("seasonRecord", createPitcherRecordMap(pitcherRecordInfo,pitcher.getSeasonRecord()));


        Map<String, Object> pitcherSkillInfo;
        if (pitcher.getPitcherSkill1() != null){
            pitcherSkillInfo = new HashMap<>();
            pitcherInfo.put("pitcherSkill1", createPitcherSkillMap(pitcherSkillInfo,pitcher.getPitcherSkill1()));
        }
        if (pitcher.getPitcherSkill2() != null){
            pitcherSkillInfo = new HashMap<>();
            pitcherInfo.put("pitcherSkill2", createPitcherSkillMap(pitcherSkillInfo,pitcher.getPitcherSkill2()));
        }
        if (pitcher.getPitcherSkill3() != null){
            pitcherSkillInfo = new HashMap<>();
            pitcherInfo.put("pitcherSkill3", createPitcherSkillMap(pitcherSkillInfo,pitcher.getPitcherSkill3()));
        }
        if (pitcher.getPitcherSkill4() != null){
            pitcherSkillInfo = new HashMap<>();
            pitcherInfo.put("pitcherSkill4", createPitcherSkillMap(pitcherSkillInfo,pitcher.getPitcherSkill4()));
        }

        if (pitcher.getTitleItemArrayList() != null){
            Map<String, Object> titlesInfo;
            titlesInfo = new HashMap<>();
            Map<String, Object> titleInfo;
            for(int i = 0 ; i < pitcher.getTitleItemArrayList().size(); i++){
                titleInfo = new HashMap<>();
                titlesInfo.put("title"+i, createTitleMap(titleInfo,pitcher.getTitleItemArrayList().get(i)));
            }
            pitcherInfo.put("titles", titlesInfo);
        }

        Map<String, Object> pitcherItemInfo;
        if (pitcher.getCap() != null){
            pitcherItemInfo = new HashMap<>();
            pitcherInfo.put("cap", createItemMap(pitcherItemInfo,pitcher.getCap()));
        }
        if (pitcher.getUniform() != null){
            pitcherItemInfo = new HashMap<>();
            pitcherInfo.put("uniform", createItemMap(pitcherItemInfo,pitcher.getUniform()));
        }
        if (pitcher.getShoes() != null){
            pitcherItemInfo = new HashMap<>();
            pitcherInfo.put("shoes", createItemMap(pitcherItemInfo,pitcher.getShoes()));
        }
        return pitcherInfo;
    }




    public Map<String,Object> createBatterMap(Map<String,Object> batterInfo, Batter batter){

        batterInfo.put("name",batter.getBatterName());
        batterInfo.put("teamName",batter.getTeamName());
        batterInfo.put("uid",batter.getuID());
        batterInfo.put("cardToken",batter.getCardToken());
        batterInfo.put("primaryPosition",batter.getPrimaryPosition());
        batterInfo.put("defensePosition",batter.getDefensePosition());
        batterInfo.put("type",batter.getBatterType());
        batterInfo.put("stealOrder",batter.getStealOrder());
        batterInfo.put("buntOrder",batter.getBuntOrder());
        batterInfo.put("battingOrder",batter.getBattingOrder());
        batterInfo.put("playerExp",batter.getPlayerExp());
        batterInfo.put("playerLevel",batter.getPlayerLevel());

        batterInfo.put("contact",batter.contact);
        batterInfo.put("power",batter.power);
        batterInfo.put("eye",batter.eye);
        batterInfo.put("clutch",batter.clutch);
        batterInfo.put("speed",batter.speed);
        batterInfo.put("defence",batter.defense);

        Map<String, Object> batterRecordInfo;

        batterRecordInfo = new HashMap<>();
        batterInfo.put("matchRecord", createBatterRecordMap(batterRecordInfo,batter.getMatchRecord()));

        batterRecordInfo = new HashMap<>();
        batterInfo.put("seasonRecord", createBatterRecordMap(batterRecordInfo,batter.getSeasonRecord()));


        Map<String, Object> batterSkillInfo;
        if (batter.getBatterSkill1() != null){
            batterSkillInfo = new HashMap<>();
            batterInfo.put("batterSkill1", createBatterSkillMap(batterSkillInfo,batter.getBatterSkill1()));

        }
        if (batter.getBatterSkill2() != null){
            batterSkillInfo = new HashMap<>();
            batterInfo.put("batterSkill2", createBatterSkillMap(batterSkillInfo,batter.getBatterSkill2()));
        }
        if (batter.getBatterSkill3() != null){
            batterSkillInfo = new HashMap<>();
            batterInfo.put("batterSkill3", createBatterSkillMap(batterSkillInfo,batter.getBatterSkill3()));
        }
        if (batter.getBatterSkill4() != null){
            batterSkillInfo = new HashMap<>();
            batterInfo.put("batterSkill4", createBatterSkillMap(batterSkillInfo,batter.getBatterSkill4()));
        }

        if (batter.getTitleItemArrayList() != null){
            Map<String, Object> titlesInfo;
            titlesInfo = new HashMap<>();
            Map<String, Object> titleInfo;
            for(int i = 0 ; i < batter.getTitleItemArrayList().size(); i++){
                titleInfo = new HashMap<>();
                titlesInfo.put("title"+i, createTitleMap(titleInfo,batter.getTitleItemArrayList().get(i)));
            }
            batterInfo.put("titles", titlesInfo);
        }

        Map<String, Object> batterItemInfo;
        if (batter.getBat() != null){
            batterItemInfo = new HashMap<>();
            batterInfo.put("bat", createItemMap(batterItemInfo,batter.getBat()));
        }
        if (batter.getGlasses() != null){
            batterItemInfo = new HashMap<>();
            batterInfo.put("glasses", createItemMap(batterItemInfo,batter.getGlasses()));
        }
        if (batter.getGlove() != null){
            batterItemInfo = new HashMap<>();
            batterInfo.put("glove", createItemMap(batterItemInfo,batter.getGlove()));
        }

        ArrayList<Batter.DefencePosition> positionList =  batter.getPositionLVList();
        Map<String, Object> positionInfo = new HashMap<>();
        Map<String, Object> positionDetail = new HashMap<>();

        for (Batter.DefencePosition position : positionList){
            positionInfo.put("positionInt", position.getPositionInt());
            positionInfo.put("positionExt", position.getPositionExp());
            positionDetail.put(""+position.getPositionInt(),positionInfo);
            positionInfo = new HashMap<>();
        }
        batterInfo.put("positionList",positionDetail);
        return batterInfo;
    }

    public Map<String,Object> createPitcherRecordMap(Map<String,Object> recordInfo, PithcerMatchRecord record){

        recordInfo.put("game",record.game);
        recordInfo.put("outcount",record.outcount);
        recordInfo.put("er",record.er);
        recordInfo.put("r",record.r);
        recordInfo.put("so",record.so);
        recordInfo.put("hit",record.hit);
        recordInfo.put("bb",record.bb);
        recordInfo.put("hr",record.hr);
        recordInfo.put("outNum",record.outNum);
        recordInfo.put("sf",record.sf);
        recordInfo.put("single",record.single);
        recordInfo.put("doublehit",record.doublehit);
        recordInfo.put("tripple",record.tripple);
        recordInfo.put("win",record.win);
        recordInfo.put("lose",record.lose);
        recordInfo.put("hold",record.hold);
        recordInfo.put("save",record.save);
        recordInfo.put("rispOutcount",record.rispOutcount);
        recordInfo.put("rispHit",record.rispHit);


        recordInfo.put("era",record.getERA());
        recordInfo.put("winPerGame",record.getWinPerGame());
        recordInfo.put("so9",record.getSo9());
        recordInfo.put("inningPerGame",record.getInningPerGame());

        return recordInfo;
    }

    public PithcerMatchRecord getPitcherMatchRecordFromMap(Map<String,Object> recordInfo){

        int game = Integer.parseInt(recordInfo.get("game").toString());
        int outcount = Integer.parseInt(recordInfo.get("outcount").toString());
        int er = Integer.parseInt(recordInfo.get("er").toString());
        int r = Integer.parseInt(recordInfo.get("r").toString());
        int so = Integer.parseInt(recordInfo.get("so").toString());
        int hit = Integer.parseInt(recordInfo.get("hit").toString());
        int bb = Integer.parseInt(recordInfo.get("bb").toString());
        int hr = Integer.parseInt(recordInfo.get("hr").toString());
        int outNum = Integer.parseInt(recordInfo.get("outNum").toString());
        int sf = Integer.parseInt(recordInfo.get("sf").toString());
        int single = Integer.parseInt(recordInfo.get("single").toString());
        int doublehit = Integer.parseInt(recordInfo.get("doublehit").toString());
        int tripple = Integer.parseInt(recordInfo.get("tripple").toString());
        int win = Integer.parseInt(recordInfo.get("win").toString());
        int lose = Integer.parseInt(recordInfo.get("lose").toString());
        int hold = Integer.parseInt(recordInfo.get("hold").toString());
        int save = Integer.parseInt(recordInfo.get("save").toString());
        int rispOutcount = Integer.parseInt(recordInfo.get("rispOutcount").toString());
        int rispHit = Integer.parseInt(recordInfo.get("rispHit").toString());

        return new PithcerMatchRecord(game,outcount,er,r,so,hit,bb,hr,outNum,sf,single,doublehit,tripple,win,lose,hold,save,rispOutcount,rispHit);
    }

    public BatterMatchRecord getBatterMatchRecordFromMap(Map<String,Object> recordInfo){

        int game = Integer.parseInt(recordInfo.get("game").toString());
        int pa = Integer.parseInt(recordInfo.get("pa").toString());
        int ab = Integer.parseInt(recordInfo.get("ab").toString());
        int strikeOut = Integer.parseInt(recordInfo.get("strikeOut").toString());
        int baseOnball = Integer.parseInt(recordInfo.get("baseOnball").toString());
        int hit = Integer.parseInt(recordInfo.get("hit").toString());
        int singleHit = Integer.parseInt(recordInfo.get("singleHit").toString());
        int doubleHit = Integer.parseInt(recordInfo.get("doubleHit").toString());
        int tripleHit = Integer.parseInt(recordInfo.get("tripleHit").toString());
        int homer = Integer.parseInt(recordInfo.get("homer").toString());
        int sb = Integer.parseInt(recordInfo.get("sb").toString());
        int cs = Integer.parseInt(recordInfo.get("cs").toString());
        int error = Integer.parseInt(recordInfo.get("error").toString());
        int successDefense = Integer.parseInt(recordInfo.get("successDefense").toString());
        int rbi = Integer.parseInt(recordInfo.get("rbi").toString());
        int run = Integer.parseInt(recordInfo.get("run").toString());
        int gotSB = Integer.parseInt(recordInfo.get("gotSB").toString());
        int missSB = Integer.parseInt(recordInfo.get("missSB").toString());
        int rispHit = Integer.parseInt(recordInfo.get("rispHit").toString());
        int rispAb = Integer.parseInt(recordInfo.get("rispAb").toString());


        return new BatterMatchRecord(game,pa,ab,strikeOut,baseOnball,hit,singleHit,doubleHit,tripleHit,homer,sb,cs,error,successDefense,rbi,run,gotSB,missSB,rispHit,rispAb);
    }

    public Map<String,Object> createBatterRecordMap(Map<String,Object> recordInfo, BatterMatchRecord record){

        recordInfo.put("game",record.game);
        recordInfo.put("pa",record.pa);
        recordInfo.put("ab",record.ab);
        recordInfo.put("strikeOut",record.strikeOut);
        recordInfo.put("baseOnball",record.baseOnball);
        recordInfo.put("hit",record.hit);
        recordInfo.put("singleHit",record.singleHit);
        recordInfo.put("doubleHit",record.doubleHit);
        recordInfo.put("tripleHit",record.tripleHit);
        recordInfo.put("homer",record.homer);
        recordInfo.put("sb",record.sb);
        recordInfo.put("cs",record.cs);
        recordInfo.put("error",record.error);
        recordInfo.put("successDefense",record.successDefense);
        recordInfo.put("rbi",record.rbi);
        recordInfo.put("run",record.run);
        recordInfo.put("gotSB",record.gotSB);
        recordInfo.put("missSB",record.missSB);
        recordInfo.put("rispHit",record.rispHit);
        recordInfo.put("rispAb",record.rispAb);

        recordInfo.put("avg",record.getAvg());
        recordInfo.put("obp",record.getObp());
        recordInfo.put("slg",record.getSlg());
        recordInfo.put("ops",record.getOps());
        recordInfo.put("hrPerGame",record.getHrPerGame());
        recordInfo.put("rbiPerGame",record.getRbiPerGame());
        recordInfo.put("runPerGame",record.getRunPerGame());
        recordInfo.put("sbPerGame",record.getSbPerGame());
        recordInfo.put("fieldingSuccess",record.getErrorRate());

        return recordInfo;
    }

    public Batter getBatterFromMap(Map<String,Object> mainBatterInfo){

        Batter batter;

        String name = mainBatterInfo.get("name").toString();
        String teamName = mainBatterInfo.get("teamName").toString();
        String uid = mainBatterInfo.get("uid").toString();
        String cardToken = mainBatterInfo.get("cardToken").toString();

        int primaryPosition = Integer.parseInt(mainBatterInfo.get("primaryPosition").toString());
        int defensePosition = Integer.parseInt(mainBatterInfo.get("defensePosition").toString());
        int type = Integer.parseInt(mainBatterInfo.get("type").toString());
        int stealOrder = Integer.parseInt(mainBatterInfo.get("stealOrder").toString());
        int buntOrder = Integer.parseInt(mainBatterInfo.get("buntOrder").toString());
        int battingOrder = Integer.parseInt(mainBatterInfo.get("battingOrder").toString());
        int playerExp = Integer.parseInt(mainBatterInfo.get("playerExp").toString());
        int playerLevel = Integer.parseInt(mainBatterInfo.get("playerLevel").toString());

        int contact = Integer.parseInt(mainBatterInfo.get("contact").toString());
        int power = Integer.parseInt(mainBatterInfo.get("power").toString());
        int eye = Integer.parseInt(mainBatterInfo.get("eye").toString());
        int clutch = Integer.parseInt(mainBatterInfo.get("clutch").toString());
        int speed = Integer.parseInt(mainBatterInfo.get("speed").toString());
        int defence = Integer.parseInt(mainBatterInfo.get("defence").toString());

        batter = new Batter(primaryPosition,type,playerExp,playerLevel,null,name,uid,cardToken,contact,power,eye,clutch,speed,defence,null,null,null,null,null,null,null);
        batter.setBattingOrder(battingOrder);
        batter.setDefensePosition(defensePosition);
        batter.setTeamName(teamName);
        batter.setStealOrder(stealOrder);
        batter.setBuntOrder(buntOrder);


        Map<String,Object> recordInfo;

        recordInfo = (Map<String,Object>)mainBatterInfo.get("matchRecord");
        BatterMatchRecord match = getBatterMatchRecordFromMap(recordInfo);
        batter.setMatchRecord(match);

        recordInfo = (Map<String,Object>)mainBatterInfo.get("seasonRecord");
        BatterMatchRecord season = getBatterMatchRecordFromMap(recordInfo);
        batter.setMatchRecord(season);

        if (mainBatterInfo.get("bat") != null){
            Map<String,Object> itemInfo = (Map<String,Object>)mainBatterInfo.get("bat");
            Item bat = getItemFromMap(itemInfo);
            batter.setBat(bat);
        }
        if (mainBatterInfo.get("glasses") != null){
            Map<String,Object> itemInfo = (Map<String,Object>)mainBatterInfo.get("glasses");
            Item bat = getItemFromMap(itemInfo);
            batter.setBat(bat);
        }
        if (mainBatterInfo.get("glove") != null){
            Map<String,Object> itemInfo = (Map<String,Object>)mainBatterInfo.get("glove");
            Item bat = getItemFromMap(itemInfo);
            batter.setBat(bat);
        }

        if (mainBatterInfo.get("batterSkill1") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainBatterInfo.get("batterSkill1");
            BatterSkill skill = getBatterSkillFromMap(skillInfo);
            batter.setBatterSkill1(skill);
        }

        if (mainBatterInfo.get("batterSkill2") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainBatterInfo.get("batterSkill2");
            BatterSkill skill = getBatterSkillFromMap(skillInfo);
            batter.setBatterSkill2(skill);
        }
        if (mainBatterInfo.get("batterSkill3") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainBatterInfo.get("batterSkill3");
            BatterSkill skill = getBatterSkillFromMap(skillInfo);
            batter.setBatterSkill3(skill);
        }
        if (mainBatterInfo.get("batterSkill4") != null){
            Map<String,Object> skillInfo = (Map<String,Object>)mainBatterInfo.get("batterSkill4");
            BatterSkill skill = getBatterSkillFromMap(skillInfo);
            batter.setBatterSkill4(skill);
        }

        if (mainBatterInfo.get("titles") != null){
            Map<String,Object> titlesInfo = (Map<String,Object>)mainBatterInfo.get("titles");

            ArrayList<TitleItem> titleItemArrayList = getTitleList(titlesInfo);
            batter.setTitleItemArrayList(titleItemArrayList);
        }



        Map<String,Object> positionListInfo = (Map<String,Object>)mainBatterInfo.get("positionList");
        Map<String,Object> positionList=new HashMap<>();
        ArrayList<Batter.DefencePosition> positionArrayList = new ArrayList<>();
        for (int i = 0 ; i < 8 ; i++){
            positionList = (Map<String,Object>)positionListInfo.get(""+i);

            int positionExt = Integer.parseInt(positionList.get("positionExt").toString());

            positionArrayList.add(i,new Batter().new DefencePosition(i,positionExt));
        }

        batter.setPositionLVList(positionArrayList);

//        Log.e("j13",""+batter.toString());

        return batter;

    }
    public ArrayList<TitleItem> getTitleList(Map<String,Object> titlesInfo){
        ArrayList<TitleItem> titleList = new ArrayList<>();
        Map<String,Object> titleInfo;
        for (int i = 0; i < 200 ; i++){
            if (titlesInfo.get("title"+i) != null){
                titleInfo = (Map<String,Object>)titlesInfo.get("title"+i);
                titleList.add(i,getTitleItemFromMap(titleInfo));
            }else {
                break;
            }
        }
        return titleList;
    }

    public PitcherSkill getPitcherSkillFromMap(Map<String,Object> skillInfo){

        int skillType = Integer.parseInt(skillInfo.get("skillType").toString());
        int skillLevel = Integer.parseInt(skillInfo.get("skillLevel").toString());

        return new PitcherSkill(skillType,skillLevel);

    }

    public BatterSkill getBatterSkillFromMap(Map<String,Object> skillInfo){

        int skillType = Integer.parseInt(skillInfo.get("skillType").toString());
        int skillLevel = Integer.parseInt(skillInfo.get("skillLevel").toString());

        return new BatterSkill(skillType,skillLevel);

    }

    public Item getItemFromMap(Map<String,Object> itemInfo){

        int equipTypeInt = Integer.parseInt(itemInfo.get("equipTypeInt").toString());
        int enchant = Integer.parseInt(itemInfo.get("enchant").toString());

        int contact = Integer.parseInt(itemInfo.get("contact").toString());
        int power = Integer.parseInt(itemInfo.get("power").toString());
        int eye = Integer.parseInt(itemInfo.get("eye").toString());
        int clutch = Integer.parseInt(itemInfo.get("clutch").toString());
        int speed = Integer.parseInt(itemInfo.get("speed").toString());
        int defence = Integer.parseInt(itemInfo.get("defence").toString());
        int movement = Integer.parseInt(itemInfo.get("movement").toString());
        int location = Integer.parseInt(itemInfo.get("location").toString());
        int velocity = Integer.parseInt(itemInfo.get("velocity").toString());
        int stuff = Integer.parseInt(itemInfo.get("stuff").toString());
        int mental = Integer.parseInt(itemInfo.get("mental").toString());
        int health = Integer.parseInt(itemInfo.get("health").toString());

        if (equipTypeInt >= GlobalValue.typeBat && equipTypeInt <= GlobalValue.typeGlove){
            return new Item(equipTypeInt,contact,power,eye,clutch,speed,defence);
        }else {
            return new Item(equipTypeInt,movement,location,velocity,stuff,mental,health);
        }

    }

    public ArrayList<Batter> getBatterList(Map<String,Object> subBattersInfo){
        ArrayList<Batter> batterList = new ArrayList<>();
        Map<String,Object> batterInfo;
        for (int i = 0; i < 10 ; i++){
            if (subBattersInfo.get("batter"+i) != null){
                batterInfo = (Map<String,Object>)subBattersInfo.get("batter"+i);
                batterList.add(i,getBatterFromMap(batterInfo));
            }else {
                break;
            }
        }
        return batterList;
    }

    public ArrayList<Pitcher> getPitcherList(Map<String,Object> subPitchersInfo){
        ArrayList<Pitcher> pitcherList = new ArrayList<>();
        Map<String,Object> pitcherInfo;
        for (int i = 0; i < 10 ; i++){
            if (subPitchersInfo.get("pitcher"+i) != null){
                pitcherInfo = (Map<String,Object>)subPitchersInfo.get("pitcher"+i);
                pitcherList.add(i,getPitcherFromMap(pitcherInfo));
            }else {
                break;
            }
        }
        return pitcherList;
    }

    public ArrayList<Item> getItemList(Map<String,Object> subItemsInfo){
        ArrayList<Item> itemList = new ArrayList<>();
        Map<String,Object> itemInfo;
        for (int i = 0; i < 20 ; i++){
            if (subItemsInfo.get("item"+i) != null){
                itemInfo = (Map<String,Object>)subItemsInfo.get("item"+i);
                itemList.add(i,getItemFromMap(itemInfo));
            }else {
                break;
            }
        }
        return itemList;
    }




    public Map<String,Object> createItemMap(Map<String,Object> infoMap, Item item) {

        infoMap.put("equipTypeInt",item.getEquipTypeInt());
        infoMap.put("enchant",item.getEnchant());

        infoMap.put("contact",item.contact);
        infoMap.put("power",item.power);
        infoMap.put("eye",item.eye);
        infoMap.put("clutch",item.clutch);
        infoMap.put("speed",item.speed);
        infoMap.put("defence",item.defence);
        infoMap.put("movement",item.movement);
        infoMap.put("location",item.location);
        infoMap.put("velocity",item.velocity);
        infoMap.put("stuff",item.stuff);
        infoMap.put("mental",item.mental);
        infoMap.put("health",item.health);

        return infoMap;
    }

    public Map<String,Object> createBatterSkillMap(Map<String,Object> infoMap, BatterSkill batterSkill) {

        infoMap.put("skillType",batterSkill.getSkillType());
        infoMap.put("skillLevel",batterSkill.getSkillLevel());

        return infoMap;
    }

    public Map<String,Object> createPitcherSkillMap(Map<String,Object> infoMap, PitcherSkill pitcherSkill) {

        infoMap.put("skillType",pitcherSkill.getSkillType());
        infoMap.put("skillLevel",pitcherSkill.getSkillLevel());

        return infoMap;
    }
    public Map<String,Object> createTitleMap(Map<String,Object> infoMap, TitleItem titleItem) {

        infoMap.put("season",titleItem.getSeason());
        infoMap.put("title",titleItem.getTitle());

        return infoMap;
    }

    public TitleItem getTitleItemFromMap(Map<String,Object> titleInfo){

        String season = titleInfo.get("season").toString();
        int title = Integer.parseInt(titleInfo.get("title").toString());

        return new TitleItem(season,title);

    }
}


























