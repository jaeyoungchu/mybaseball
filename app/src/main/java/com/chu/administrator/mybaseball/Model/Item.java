package com.chu.administrator.mybaseball.Model;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by Administrator on 2018-06-17.
 */

public class Item{

    private int equipTypeInt;
    private int maxEnchant = 10;
    private int enchant;
    public int contact,power,eye,clutch,speed,defence;
    public int movement,location,velocity,stuff,mental,health;

    public Item() {
    }

    public Item(int equipTypeInt, int contactMov, int powerStf, int eyeLoc, int clutch, int speedVel, int defenceHel) {
        this.equipTypeInt = equipTypeInt;
        this.enchant = 1;
        if(equipTypeInt == GlobalValue.typeBat || equipTypeInt == GlobalValue.typeGlasses || equipTypeInt == GlobalValue.typeGlove){
            this.contact = contactMov;
            this.power = powerStf;
            this.eye = eyeLoc;
            this.clutch = clutch;
            this.speed = speedVel;
            this.defence = defenceHel;
            this.movement = 0;
            this.location = 0;
            this.stuff = 0;
            this.mental = 0;
            this.velocity = 0;
            this.health = 0;
        }else {
            this.contact = 0;
            this.power = 0;
            this.clutch = 0;
            this.eye = 0;
            this.speed = 0;
            this.defence = 0;
            this.movement = contactMov;
            this.location = eyeLoc;
            this.stuff = powerStf;
            this.mental = clutch;
            this.velocity = speedVel;
            this.health = defenceHel;
        }
    }

    public ArrayList<Attr> getAttrArrayList(){
        ArrayList<Attr> attrs = new ArrayList<>();
        if (equipTypeInt == GlobalValue.typeBat || equipTypeInt == GlobalValue.typeGlasses || equipTypeInt == GlobalValue.typeGlove){
            if (contact != 0){
                attrs.add(new Attr("CON",getContact()));
            }
            if (eye != 0){
                attrs.add(new Attr("EYE",getEye()));
            }
            if (power != 0){
                attrs.add(new Attr("POW",getPower()));
            }
            if (clutch != 0){
                attrs.add(new Attr("CLU",getClutch()));
            }
            if (speed != 0){
                attrs.add(new Attr("SPD",getSpeed()));
            }
            if (defence != 0){
                attrs.add(new Attr("DEF",getDefence()));
            }
            return attrs;
        }else{
            if (movement != 0){
                attrs.add(new Attr("MOV",getMovement()));
            }
            if (location != 0){
                attrs.add(new Attr("LOC",getLocation()));
            }
            if (stuff != 0){
                attrs.add(new Attr("STF",getStuff()));
            }
            if (mental != 0){
                attrs.add(new Attr("MEN",getMental()));
            }
            if (velocity != 0){
                attrs.add(new Attr("VEL",getVelocity()));
            }
            if (health != 0){
                attrs.add(new Attr("HP",getHealth()));
            }
            return attrs;
        }
    }

    public int getEquipTypeInt() {
        return equipTypeInt;
    }

    public int getMaxEnchant() {
        return maxEnchant;
    }

    public void setEquipTypeInt(int equipTypeInt) {
        this.equipTypeInt = equipTypeInt;
    }

    public int getEnchant() {
        return enchant;
    }

    public int getContact() {
        return contact*enchant;
    }

    public int getPower() {
        return power*enchant;
    }

    public int getEye() {
        return eye*enchant;
    }

    public int getClutch() {
        return clutch*enchant;
    }

    public int getSpeed() {
        return speed*enchant;
    }

    public int getDefence() {
        return defence*enchant;
    }

    public int getMovement() {
        return movement*enchant;
    }

    public int getLocation() {
        return location*enchant;
    }

    public int getVelocity() {
        return velocity*enchant;
    }

    public int getStuff() {
        return stuff*enchant;
    }

    public int getMental() {
        return mental*enchant;
    }

    public int getHealth() {
        return health*enchant;
    }

    public void setEnchant(int enchant) {
        this.enchant = enchant;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setEye(int eye) {
        this.eye = eye;
    }

    public void setClutch(int clutch) {
        this.clutch = clutch;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    public void setStuff(int stuff) {
        this.stuff = stuff;
    }

    public void setMental(int mental) {
        this.mental = mental;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public class Attr {
        String attrName;
        int attrNum;

        public Attr(String attrName, int attrNum) {
            this.attrName = attrName;
            this.attrNum = attrNum;
        }

        public String getAttrName() {
            return attrName;
        }

        public int getAttrNum() {
            return attrNum;
        }
    }

    @Override
    public String toString() {
        return "Item{" +
                ", equipTypeStr='" + equipTypeInt + '\'' +
                ", maxEnchant=" + maxEnchant +
                ", enchant=" + enchant +
                ", contact=" + contact +
                ", power=" + power +
                ", clutch=" + clutch +
                ", eye=" + eye +
                ", speed=" + speed +
                ", defence=" + defence +
                ", movement=" + movement +
                ", location=" + location +
                ", velocity=" + velocity +
                ", stuff=" + stuff +
                ", mental=" + mental +
                ", health=" + health +
                '}';
    }
}
