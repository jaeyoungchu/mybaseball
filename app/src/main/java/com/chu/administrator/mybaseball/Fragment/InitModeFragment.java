package com.chu.administrator.mybaseball.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.chu.administrator.mybaseball.Fragment.ManagerMode.InitManagerFragment;
import com.chu.administrator.mybaseball.Fragment.PlayerMode.InitPlayerFragment;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.PlayerUserModel;
import com.chu.administrator.mybaseball.R;

/**
 * Created by Administrator on 2018-06-14.
 */

public class InitModeFragment extends BaseFragment {

    Button playerModeBtn,managerModeBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = true;
        return inflater.inflate(R.layout.initmode_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        playerModeBtn = (Button) view.findViewById(R.id.playerModeBtn);
        managerModeBtn = (Button) view.findViewById(R.id.managerModeBtn);

        playerModeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlayerUserModel pModel = new PlayerUserModel(bInstance.getDeviceID(),bInstance.getUID(), GlobalValue.PLAYERMODE);
                bInstance.setmPlayerUserModel(pModel);
                bInstance.addAsRootFragment(new InitPlayerFragment());
            }
        });

        managerModeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                bInstance.signOutGoogle();
//                bInstance.addAsRootFragment(new1 LoginFragment());
                bInstance.addAsRootFragment(new InitManagerFragment());
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }
}
