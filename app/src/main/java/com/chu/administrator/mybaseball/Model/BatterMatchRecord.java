package com.chu.administrator.mybaseball.Model;

/**
 * Created by Administrator on 2018-06-17.
 */

public class BatterMatchRecord {
    public int game,pa,ab,strikeOut,baseOnball,hit,singleHit,doubleHit,tripleHit,homer,sb,cs,error,successDefense,rbi,run,gotSB,missSB,rispHit,rispAb;
    public String dutyPitcher;

    public BatterMatchRecord(int game, int pa, int ab, int strikeOut, int baseOnball, int hit, int singleHit, int doubleHit, int tripleHit, int homer, int sb, int cs, int error, int successDefense, int rbi, int run, int gotSB, int missSB,int rispHit,int rispAb) {
        this.game = game;
        this.pa = pa;
        this.ab = ab;
        this.strikeOut = strikeOut;
        this.baseOnball = baseOnball;
        this.hit = hit;
        this.singleHit = singleHit;
        this.doubleHit = doubleHit;
        this.tripleHit = tripleHit;
        this.homer = homer;
        this.sb = sb;
        this.cs = cs;
        this.error = error;
        this.successDefense = successDefense;
        this.rbi = rbi;
        this.run = run;
        this.gotSB = gotSB;
        this.missSB = missSB;
        this.rispHit = rispHit;
        this.rispAb = rispAb;
    }

    public BatterMatchRecord() {
        this.game = 0;
        this.pa = 0;
        this.ab = 0;
        this.strikeOut = 0;
        this.baseOnball = 0;
        this.hit = 0;
        this.singleHit = 0;
        this.doubleHit = 0;
        this.tripleHit = 0;
        this.homer = 0;
        this.sb = 0;
        this.cs = 0;
        this.error = 0;
        this.rbi = 0;
        this.run = 0;
        this.successDefense = 0;
        this.gotSB = 0;
        this.missSB = 0;
        this.rispHit = 0;
        this.rispAb = 0;
    }
    public void resetRecord(){
        this.game = 0;
        this.pa = 0;
        this.ab = 0;
        this.strikeOut = 0;
        this.baseOnball = 0;
        this.hit = 0;
        this.singleHit = 0;
        this.doubleHit = 0;
        this.tripleHit = 0;
        this.homer = 0;
        this.sb = 0;
        this.cs = 0;
        this.error = 0;
        this.rbi = 0;
        this.run = 0;
        this.successDefense = 0;
        this.gotSB = 0;
        this.missSB = 0;
        this.rispHit = 0;
        this.rispAb = 0;
    }

    public void saveMatchRecord(BatterMatchRecord matchRecord) {
        this.pa += matchRecord.pa;
        this.ab += matchRecord.ab;
        this.strikeOut += matchRecord.strikeOut;
        this.baseOnball += matchRecord.baseOnball;
        this.hit += matchRecord.hit;
        this.singleHit += matchRecord.singleHit;
        this.doubleHit += matchRecord.doubleHit;
        this.tripleHit += matchRecord.tripleHit;
        this.homer += matchRecord.homer;
        this.sb += matchRecord.sb;
        this.cs += matchRecord.cs;
        this.error += matchRecord.error;
        this.successDefense += matchRecord.successDefense;
        this.rbi += matchRecord.rbi;
        this.run += matchRecord.run;
        this.gotSB +=matchRecord.gotSB;
        this.missSB += matchRecord.missSB;
        this.rispHit += matchRecord.rispHit;
        this.rispAb += matchRecord.rispAb;
    }
    public String getErrorRate(){
        if (error+successDefense == 0){
            return "1.000";
        }else if (error == 0){
            return "1.000";
        }else {

            return String.format("%.3f",(float)(successDefense)/(float)(error+successDefense));
        }

    }
    public String getRisp(){
        float avg;
        if (game != 0 ){
            avg = (float)rispHit/(float)rispAb;
            String str = String.format("%.3f",avg);
            if (avg == 0){
                str = "0.000";
            }
            return str;
        }else {
            return "0.000";
        }
    }

    public String getAvg(){
        float avg;
        if (game != 0 ){
            avg = (float)hit/(float)ab;
//            avg = avg * 1000;
            String str = String.format("%.3f",avg);
            if (avg == 0){
                str = "0.000";
            }
            return str;
        }else {
            return "0.000";
        }
    }

    public String getHrPerGame(){
        float avg;
        if (game != 0 ){
            avg = (float)homer/(float)game;
            String str = String.format("%.2f",avg);
            if (avg == 0){
                str = "0.00";
            }
            return str;
        }else {
            return "0.00";
        }
    }

    public String getSbPerGame(){
        float avg;
        if (game != 0 ){
            avg = (float)sb/(float)game;
            String str = String.format("%.2f",avg);
            if (avg == 0){
                str = "0.00";
            }
            return str;
        }else {
            return "0.00";
        }
    }

    public String getRbiPerGame(){
        float avg;
        if (game != 0 ){
            avg = (float)rbi/(float)game;
            String str = String.format("%.2f",avg);
            if (avg == 0){
                str = "0.00";
            }
            return str;
        }else {
            return "0.00";
        }
    }

    public String getRunPerGame(){
        float avg;
        if (game != 0 ){
            avg = (float)run/(float)game;
            String str = String.format("%.2f",avg);
            if (avg == 0){
                str = "0.00";
            }
            return str;
        }else {
            return "0.00";
        }
    }



    public float getRAVG(){
        float avg;
        if (pa != 0 ){
            avg = (float)hit/(float)ab;
//            avg = avg * 1000;

            return avg;
        }else {
            return 0;
        }
    }
    public float getAvgFloat(){
        float avg;
        if (pa != 0){
            avg = (float)hit/(float)ab;
            return avg;
        }else {
            return 0f;
        }
    }
    public String getObp(){
        float avg,bbRate,obp;
        String returnStr;
        if (pa != 0){
            avg = (float)hit/(float)ab;

            bbRate = (float)baseOnball/(float)pa;
            obp = avg+bbRate;
//            obp = obp * 1000;
            returnStr = String.format("%.3f",obp);
            return returnStr;
        }else {
            return "0.000";
        }
    }
    public float getRObp(){
        float avg,bbRate,obp;

        if (pa != 0){
            avg = (float)hit/(float)ab;

            bbRate = (float)baseOnball/(float)pa;
            obp = avg+bbRate;

            return obp;
        }else {
            return 0;
        }
    }
    public String getSlg(){
        float slg;

        if (pa != 0){
            slg = (float)(singleHit+doubleHit*2+tripleHit*3+homer*4)/(float)(ab);
            String returnStr = "";
            returnStr = String.format("%.3f",slg);
            return returnStr;
        }else {
            return "0.000";
        }
    }

    public float getRSlg(){
        float slg;

        if (pa != 0){
            slg = (float)(singleHit+doubleHit*2+tripleHit*3+homer*4)/(float)(ab);
            return slg;
        }else {
            return 0;
        }
    }
    public String getOps(){

        if (pa !=0){
            float avg = (float)hit/(float)ab;
            float bbRate = (float)baseOnball/(float)pa;
            float obp = avg+bbRate;
            float slg = (float)(singleHit+doubleHit*2+tripleHit*3+homer*4)/(float)(ab);
            float ops = obp+slg;
            String returnStr = String.format("%.3f",ops);
            return returnStr;
        }else {
            return "0.000";
        }
    }
    public float getROps(){

        if (pa !=0){
            float avg = (float)hit/(float)ab;
            float bbRate = (float)baseOnball/(float)pa;
            float obp = avg+bbRate;
            float slg = (float)(singleHit+doubleHit*2+tripleHit*3+homer*4)/(float)(ab);
            float ops = obp+slg;

            return ops;

        }else {
            return 0;
        }
    }
    public int getGame() {
        return game;
    }

    public void setGame(int game) {
        this.game = game;
    }

    @Override
    public String toString() {
        return "BatterMatchRecord{\t" +
                "\tpa=" + pa +
                ", ab=" + ab +
                ", avg=" + getAvg() +
                ", obp=" + getObp() +
                ", slg=" + getSlg() +
                ", ops=" + getOps() +
                ", strikeOut=" + strikeOut +
                ", baseOnball=" + baseOnball +
                ", hit=" + hit +
                ", singleHit=" + singleHit +
                ", doubleHit=" + doubleHit +
                ", trippleHit=" + tripleHit +
                ", homer=" + homer +
                ", sb=" + sb +
                ", cs=" + cs +
                ", success def =" + successDefense +
                ", error=" + error +
                ", rbi=" + rbi +
                ", run=" + run +
                ", gotSB=" + gotSB +
                ", missSB=" + missSB +
                ", rispHit=" + rispHit +
                ", rispAb=" + rispAb +
                ", RISP=" + getRisp() +
                '}';
    }
}