package com.chu.administrator.mybaseball;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chu.administrator.mybaseball.Fragment.LoginFragment;
import com.chu.administrator.mybaseball.Helper.HashMapManager;
import com.chu.administrator.mybaseball.Helper.MatchLogic;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.Item;
import com.chu.administrator.mybaseball.Model.Manager;
import com.chu.administrator.mybaseball.Model.ManagerUserModel;
import com.chu.administrator.mybaseball.Model.MatchDetail;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.PlayerUserModel;
import com.chu.administrator.mybaseball.Model.UserInfo;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import io.realm.Realm;

public class BaseCoreActivity extends AppCompatActivity {

    private final int RC_SIGN_IN = 11, RC_LINK = 12;
    private final String TAG = "Main Activity";
    LinearLayout appBar;
    ConstraintLayout baseLayout;


    Random randomInt;
    private FragmentManager fm;
    private FragmentTransaction fragmentTransaction;
    private static BaseCoreActivity activityInstance;
    private Context context;
    private List<Fragment> fragmentStack;
    private ProgressBar mainProgressBar;
    private PlayerUserModel mPlayerUserModel;
    private ManagerUserModel mManagerUserModel;
    private ArrayList<MatchDetail> matchDetails;

    // local Manager
    private HashMapManager hManager;
    // firebase
    private GoogleSignInOptions gso;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private GoogleApiClient mGoogleApiClient;
    //Realm
    private Realm realm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_core);
        activityInstance = this;
        realm = Realm.getDefaultInstance();
        db = FirebaseFirestore.getInstance();
        hManager = new HashMapManager();
        fragmentStack = new ArrayList<>();

        appBar = (LinearLayout) findViewById(R.id.appBarLO);
        baseLayout = (ConstraintLayout) findViewById(R.id.baseLO);
        mainProgressBar = (ProgressBar) findViewById(R.id.mainProgressBar);

        // Configure Google Sign In
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();

        fm = getSupportFragmentManager();

        if (fm.findFragmentById(R.id.baseLO) == null){
            fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.add(R.id.baseLO, new LoginFragment());
            fragmentTransaction.commit();
        }
    }

    public ArrayList<MatchDetail> getMatchDetails() {
        return matchDetails;
    }

    public void setMatchDetails(ArrayList<MatchDetail> matchDetails) {
        this.matchDetails = matchDetails;
    }

    public HashMapManager gethManager() {
        if (hManager == null){
            hManager = new HashMapManager();
        }
        return hManager;
    }


    public FirebaseFirestore getDb() {
        if (db == null){
            return FirebaseFirestore.getInstance();
        }else {
            return db;
        }
    }

    public Realm getRealm(){
        if (realm == null){
            realm = Realm.getDefaultInstance();
        }
        return realm;
    }

    public void saveUserId(String userId){
        realm.beginTransaction();
        UserInfo result = realm.where(UserInfo.class).findFirst();
        if (result == null){
            // write token
            UserInfo userInfo = realm.createObject(UserInfo.class);
            userInfo.setUserId(userId);
        }else {
            // update token
            result.setUserId(userId);
        }
        realm.commitTransaction();
    }
    public String getUserIdFromRealm(){
        realm.beginTransaction();
        UserInfo result = realm.where(UserInfo.class).findFirst();
        String uid="";
        if (result != null){
            uid= result.getUserId();
        }
        realm.commitTransaction();
        return uid;
    }

    public void saveModeToRealm(String mode){
        realm.beginTransaction();
        UserInfo result = realm.where(UserInfo.class).findFirst();
        if (result == null){
            // write token
            UserInfo userInfo = realm.createObject(UserInfo.class);
            userInfo.setMode(mode);
        }else {
            // update token
            result.setMode(mode);
        }
        realm.commitTransaction();
    }
    public String getModeFromRealm(){
        realm.beginTransaction();
        UserInfo result = realm.where(UserInfo.class).findFirst();
        String mode="";
        if (result != null){
            mode= result.getMode();
        }
        realm.commitTransaction();
        return mode;
    }

    public boolean isManagerMode(){
        if (getModeFromRealm().equals("manager")){
            return true;
        }else {
            return false;
        }
    }

    public ManagerUserModel getmManagerUserModel() {
        if (mManagerUserModel == null){
            downloadUserModel();
            popupGeneralWindow("Try again after few seconds..");
            return null;
        }
        return mManagerUserModel;
    }

    public void setmManagerUserModel(ManagerUserModel mManagerUserModel) {
        this.mManagerUserModel = mManagerUserModel;
    }

    public PlayerUserModel getmPlayerUserModel() {
        if (mPlayerUserModel == null){
            downloadUserModel();
            popupGeneralWindow("Try again after few seconds..");
            return null;
        }
        return mPlayerUserModel;
    }

    public void setmPlayerUserModel(PlayerUserModel mPlayerUserModel) {
        this.mPlayerUserModel = mPlayerUserModel;
    }



    public void signInGoogle() {
        showProgressBar();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void linkGoogle() {
        showProgressBar();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_LINK);
    }



    public void syncGoogleAcct(GoogleSignInAccount acct){
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "linkWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                        } else {
                            Log.w(TAG, "linkWithCredential:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }

    public void signInGuest(){
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            signInResult(true,user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            signInResult(true,null);
                        }

                        // ...
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                signInResult(false,null);
                hideProgressBar();
                // ...
            }
        }else if (requestCode == RC_LINK) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                syncGoogleAcct(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                signInResult(false,null);
                hideProgressBar();
                // ...
            }
        }

    }

    public void signOutGoogle(){

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            addAsRootFragment(new LoginFragment());
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Log.d(TAG, user.getEmail() +" / " + user.getPhoneNumber() + " / " + user.getDisplayName()+ " / " + user.getUid());
                            signInResult(true,user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            signInResult(false,null);
                        }
                        hideProgressBar();
                        // ...
                    }
                });
    }

    public void signInResult(boolean isOk,FirebaseUser user){

        if (!fm.findFragmentById(R.id.baseLO).getClass().getSimpleName().equals("LoginFragment")){
            return;
        }
        Fragment fragment = fm.findFragmentById(R.id.baseLO);
        ((LoginFragment) fragment).signInResult(isOk,user);

//        if (ft instanceof Fragment1) {
//            ((Fragment1) ft).fragMethod(scanResult.toString());
//        } else if(ft instanceof Fragment2) {
//            ((Fragment2) ft).fragMethod(scanResult.toString());
//        }
    }

    public void setUserModelFromServer(DocumentSnapshot document){
        hManager = gethManager();
        int mode = Integer.parseInt(document.get("mode").toString());
        String uid = document.get("uid").toString();
        String deviceId =  document.get("deviceId").toString();
        int cash = Integer.parseInt(document.get("cash").toString());
        int gp =  Integer.parseInt(document.get("gp").toString());

        if (mode == GlobalValue.PLAYERMODE){
            PlayerUserModel playerUserModel = new PlayerUserModel(deviceId,uid,mode);
            String teamUid =  document.get("teamUid").toString();
            int historyCap =  Integer.parseInt(document.get("historyCap").toString());

            ArrayList<Item> subItemList= new ArrayList<>();
            if (document.get("subItem") != null){
                Map<String,Object> subItemsInfo = (Map<String,Object>)document.get("subItem");
                subItemList = hManager.getItemList(subItemsInfo);
            }

            ArrayList<Batter> batterList = new ArrayList<>();
            if (document.get("batters") != null){
                Map<String,Object> subBattersInfo = (Map<String,Object>)document.get("batters");
                batterList = hManager.getBatterList(subBattersInfo);
            }

            ArrayList<Pitcher> pitcherList = new ArrayList<>();
            if (document.get("pitchers") != null){
                Map<String,Object> subPitchersInfo = (Map<String,Object>)document.get("pitchers");
                pitcherList = hManager.getPitcherList(subPitchersInfo);
            }

            Batter mainBatter = batterList.get(0);
            batterList.remove(0);
            batterList.trimToSize();

            Pitcher mainPitcher = pitcherList.get(0);
            pitcherList.remove(0);
            pitcherList.trimToSize();

            playerUserModel.setCash(cash);
            playerUserModel.setGp(gp);
            playerUserModel.setTeamUid(teamUid);
            playerUserModel.setHistoryCap(historyCap);
            playerUserModel.setMainBatter(mainBatter);
            playerUserModel.setMainPitcher(mainPitcher);
            playerUserModel.setSubItems(subItemList);
            playerUserModel.setSubBatters(batterList);
            playerUserModel.setSubPitchers(pitcherList);

            setmPlayerUserModel(playerUserModel);

        }else if (mode == GlobalValue.MANAGERMODE){
            ManagerUserModel managerUserModel = new ManagerUserModel(uid,deviceId,cash,gp,mode);

            managerUserModel = hManager.getManagerModelFromDocument(document,managerUserModel);

            setmManagerUserModel(managerUserModel);

        }

    }

    public ManagerUserModel getManagerModelFromServer(DocumentSnapshot document){
        hManager = gethManager();
        int mode = Integer.parseInt(document.get("mode").toString());
        String uid = document.get("uid").toString();
        String deviceId =  document.get("deviceId").toString();
        int cash = Integer.parseInt(document.get("cash").toString());
        int gp =  Integer.parseInt(document.get("gp").toString());

        ManagerUserModel managerUserModel = new ManagerUserModel(uid,deviceId,cash,gp,mode);
        managerUserModel = hManager.getManagerModelFromDocument(document,managerUserModel);
        return managerUserModel;
    }


    public void downloadUserModel(){
        showProgressBar();

        DocumentReference docRef = getDb().collection("userModel").document(getUID());

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                DocumentSnapshot document = documentSnapshot;

                if (document != null && document.exists()) {
                    setUserModelFromServer(document);
                    hideProgressBar();
                } else {
                    popupGeneralWindow("Server connection error..");
                    hideProgressBar();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                popupGeneralWindow("Server connection error..");
                hideProgressBar();
            }
        });
    }

    public void setBlink1000(View view, int willVisibile){
        if (getContext() == null){
            return;
        }
        view.setVisibility(willVisibile);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink);
        anim.setDuration(1000);
        if (view.getVisibility() == View.VISIBLE){
            view.startAnimation(anim);
        }else {
            view.clearAnimation();
        }
    }

    public void setTextViewWithColor(TextView textView, int stat){

        if (stat < 60){
            textView.setText(stat+"");
            textView.setTextColor(getResources().getColor(R.color.below70));
        }else if (stat < 70){
            textView.setText(stat+"");
            textView.setTextColor(getResources().getColor(R.color.color7089));
        }else if (stat < 80){
            textView.setText(stat+"");
            textView.setTextColor(getResources().getColor(R.color.color9099));
        }else if (stat < 90){
            textView.setText(stat+"");
            textView.setTextColor(getResources().getColor(R.color.color0009));
        }else if (stat < 100){
            textView.setText(stat+"");
            textView.setTextColor(getResources().getColor(R.color.color1019));
        } else if (stat < 110){
            textView.setText(stat+"");
            textView.setTextColor(getResources().getColor(R.color.over20));
        }else{
            textView.setText(stat+"");
            textView.setTextColor(getResources().getColor(R.color.over30));
        }
    }

    public String getUID(){
        if (mAuth==null){
            mAuth = FirebaseAuth.getInstance();
        }
        return mAuth.getUid();
    }

    public String getDeviceID(){
        String androidId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return androidId;
    }

    TimeZone seoul = TimeZone.getTimeZone ("Asia/Seoul");



    public String getLocalDate(){
        int year = Calendar.getInstance(seoul).get(Calendar.YEAR);
        int month = Calendar.getInstance(seoul).get(Calendar.MONTH)+1;
        int day = Calendar.getInstance(seoul).get(Calendar.DAY_OF_MONTH);
        return year+"-"+month+"-"+day;
    }

    public String getLocalMillisecond(){
        return Calendar.getInstance(seoul).get(Calendar.MILLISECOND)+"";
    }

    public String getLocalTimeStamp(){
        int year = Calendar.getInstance(seoul).get(Calendar.YEAR);
        int month = Calendar.getInstance(seoul).get(Calendar.MONTH)+1;
        int day = Calendar.getInstance(seoul).get(Calendar.DAY_OF_MONTH);
        int hour = Calendar.getInstance(seoul).get(Calendar.HOUR_OF_DAY);
        int min = Calendar.getInstance(seoul).get(Calendar.MINUTE);
        int second = Calendar.getInstance(seoul).get(Calendar.SECOND);
        if (second <10){
            return year+"-"+month+"-"+day+"-"+hour+"-"+min+"-0"+second;
        }else {
            return year+"-"+month+"-"+day+"-"+hour+"-"+min+"-"+second;
        }
    }

    public boolean isLengthOk(String input){
        if (input.length() < 8 && input.length() != 0){
            return true;
        }else {
            return false;
        }
    }
    public void showProgressBar(){
        mainProgressBar.bringToFront();
        mainProgressBar.setVisibility(View.VISIBLE);
    }

    public String createPlayerToken(){
        String uniqueID = UUID.randomUUID().toString();

        return BaseCoreActivity.getInstance().getDeviceID().substring(0,5)+"/"+BaseCoreActivity.getInstance().getLocalMillisecond()+"/"+uniqueID;
    }

    public boolean isAIToken(String token){
        if (token.substring(0,2).equals("ai")){
            return true;
        }else {
            return false;
        }
    }

    public Pitcher createPitcher(int position){


        Pitcher pitcher = new Pitcher();
        String teamName = getmManagerUserModel().getTeamName();

        if (position <5){
            pitcher = new Pitcher(getUID(),"ai"+createPlayerToken(),"SP"+(position+1),50,50,50,50,50,70,GlobalValue.STARTER_PITCHER);
            pitcher.setPositionInt(position);
            pitcher.setTeamName(teamName);
        }else if (position == 8){
            pitcher = new Pitcher(getUID(),"ai"+createPlayerToken(),"clSub",55,50,50,50,50,40,GlobalValue.RELIEF_PITCHER);
            pitcher.setPositionInt(position);
            pitcher.setTeamName(teamName);
        }else {
            pitcher = new Pitcher(getUID(),"ai"+createPlayerToken(),"RP"+(position-4),55,50,50,50,50,40,GlobalValue.RELIEF_PITCHER);
            pitcher.setPositionInt(position);
            pitcher.setTeamName(teamName);
        }

        return pitcher;
    }

    public Batter createBatter(int position){
        Batter batter = new Batter();
        String teamName = getmManagerUserModel().getTeamName();

        switch (position){
            case GlobalValue.catcher:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"C",GlobalValue.catcher,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.catcher);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.firstBase:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"1B",GlobalValue.firstBase,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.firstBase);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.secondBase:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"2B",GlobalValue.secondBase,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.secondBase);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.thirdBase:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"3B",GlobalValue.thirdBase,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.thirdBase);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.shortStop:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"SS",GlobalValue.shortStop,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.shortStop);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.leftField:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"LF",GlobalValue.leftField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.leftField);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.centerField:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"CF",GlobalValue.centerField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.centerField);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.rightField:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"RF",GlobalValue.rightField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.rightField);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.DH:
                batter = new Batter(getUID(),"ai"+createPlayerToken(),"DH",GlobalValue.rightField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
                batter.setDefensePosition(GlobalValue.DH);
                batter.setTeamName(teamName);
                break;
        }

        return batter;
    }

    public Pitcher createPitcher(String name,int type){


        Pitcher pitcher = new Pitcher();
        String teamName = "none";

        int mov=50,loc=50,vel=50,stf=50,men=50,hp=50;

        switch (type){
            case GlobalValue.MOVMENT_PITCHER:
                mov = 60;
                break;
            case GlobalValue.LOCATION_PITCHER:
                loc = 60;
                break;
            case GlobalValue.STUFF_PITCHER:
                stf = 60;
                break;
            case GlobalValue.MENTAL_PITCHER:
                men = 60;
                break;
            case GlobalValue.STARTER_PITCHER:
                hp = 70;
                break;
            case GlobalValue.GENERAL_PITCHER:
                mov = 53;
                loc = 53;
                vel = 53;
                stf = 53;
                men = 53;
                hp = 44;
                break;
            case GlobalValue.RELIEF_PITCHER:
                mov = 56;
                loc = 56;
                vel = 56;
                stf = 56;
                men = 56;
                hp = 20;
                break;
        }

        pitcher = new Pitcher(getUID(),createPlayerToken(),name,mov,loc,vel,stf,men,hp,type);
        pitcher.setTeamName(teamName);

        return pitcher;
    }

    public Batter createBatter(String name,int position,int type){
        Batter batter = new Batter();
        String teamName = "none";
        int con=50,pow=50,eye=50,clu=50,spd=50,def=50;

        switch (type){
            case GlobalValue.CONTACT_BATTER:
                con = 60;
                break;
            case GlobalValue.POWER_BATTER:
                pow = 60;
                break;
            case GlobalValue.EYE_BATTER:
                eye = 60;
                break;
            case GlobalValue.DEFENCE_BATTER:
                clu = 60;
                break;
            case GlobalValue.CLUTCH_BATTER:
                spd = 60;
                break;
            case GlobalValue.SPEED_BATTER:
                def = 60;
                break;
            case GlobalValue.GENERAL_BATTER:
                con = 52;
                pow = 52;
                eye = 52;
                clu = 52;
                spd = 52;
                def = 52;
                break;
        }


        switch (position){
            case GlobalValue.catcher:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.catcher,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.catcher);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.firstBase:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.firstBase,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.firstBase);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.secondBase:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.secondBase,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.secondBase);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.thirdBase:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.thirdBase,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.thirdBase);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.shortStop:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.shortStop,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.shortStop);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.leftField:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.leftField,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.leftField);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.centerField:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.centerField,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.centerField);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.rightField:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.rightField,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.rightField);
                batter.setTeamName(teamName);
                break;
            case GlobalValue.DH:
                batter = new Batter(getUID(),createPlayerToken(),name,GlobalValue.rightField,con,pow,eye,clu,spd,def,type);
                batter.setDefensePosition(GlobalValue.DH);
                batter.setTeamName(teamName);
                break;
        }

        return batter;
    }


    public void hideProgressBar(){
        mainProgressBar.setVisibility(View.GONE);
    }

    public static BaseCoreActivity getInstance(){
        if (activityInstance == null){
            activityInstance = new BaseCoreActivity();
        }
        return activityInstance;
    }
    public Context getContext() {
        if (context == null){
            context = getApplicationContext();
        }
        return context;
    }

    public void hideAppBar(boolean isHiding){
        if (isHiding){
            appBar.setVisibility(View.GONE);
        }else {
            appBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (fragmentStack.size() >1){
            backFragment(fragmentStack.get(fragmentStack.size()-2));
            return;
        }else {
            return;
        }
//            super.onBackPressed();
    }
    public void removeAllFragmentStack(){
        fragmentStack.clear();
//        fragmentStack.add(getSupportFragmentManager().findFragmentById(R.id.coreBaseLayout));
    }
    public void addFragment(Fragment fragment){
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.baseLO,fragment);
        fragmentTransaction.commitAllowingStateLoss();
        fragmentStack.add(fragment);
    }
    public void addAsRootFragment(Fragment fragment){
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.baseLO,fragment);
        fragmentTransaction.commitAllowingStateLoss();
        removeAllFragmentStack();
        fragmentStack.add(fragment);
    }
    public void backFragment(Fragment fragment){
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.baseLO,fragment);
        fragmentTransaction.commitAllowingStateLoss();
        fragmentStack.remove(fragmentStack.size()-1);
    }

    public void popupGeneralWindow(String message){
        final Dialog dialog = new Dialog(this, R.style.generalDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.general_warring_dialog);

        Button confirmBtn = (Button) dialog.findViewById(R.id.confirmBtn);
        TextView errorTitleTV = (TextView) dialog.findViewById(R.id.errorTitleTV);
        errorTitleTV.setText(message);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    public void setDetailWindowSize(Dialog dialog,float w, float h){
        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
        } else {
            size.x = display.getWidth();
            size.y = display.getHeight();
        }
        int width = size.x;
        int height = size.y;
        dialog.getWindow().setLayout((int)Math.round(width*w),(int)Math.round(height*h));

    }
    public Manager createRandomManager(){
        randomInt = new Random();
        ArrayList<Integer> integerArrayList = new ArrayList<>();

        for (int i = 0 ; i < 12 ; i++){     //init array
            integerArrayList.add(i,0);
        }


        for (int i = 0 ; i <3 ; i++){       //input value in random array
            int attrIndex = randomInt.nextInt(12); //0,1,2,3....,11
            integerArrayList.set(attrIndex,integerArrayList.get(attrIndex)+1);
        }

        Manager manager = new Manager(integerArrayList.get(0),integerArrayList.get(1),integerArrayList.get(2),integerArrayList.get(3),integerArrayList.get(4),integerArrayList.get(5),
                integerArrayList.get(6),integerArrayList.get(7),integerArrayList.get(8),integerArrayList.get(9),integerArrayList.get(10),integerArrayList.get(11)
                ,getDeviceID(),getUID(), createPlayerToken());

        return manager;
    }

    public Manager changeManagerStat(Manager manager){
        randomInt = new Random();
        ArrayList<Integer> integerArrayList = new ArrayList<>();

        for (int i = 0 ; i < 12 ; i++){     //init array
            integerArrayList.add(i,0);
        }


        for (int i = 0 ; i <3 ; i++){       //input value in random array
            int attrIndex = randomInt.nextInt(12); //0,1,2,3....,11
            integerArrayList.set(attrIndex,integerArrayList.get(attrIndex)+1);
        }
        manager.setContact(integerArrayList.get(0));
        manager.setPower(integerArrayList.get(1));
        manager.setEye(integerArrayList.get(2));
        manager.setClutch(integerArrayList.get(3));
        manager.setSpeed(integerArrayList.get(4));
        manager.setDefense(integerArrayList.get(5));
        manager.setMovement(integerArrayList.get(6));
        manager.setLocation(integerArrayList.get(7));
        manager.setStuff(integerArrayList.get(8));
        manager.setVelocity(integerArrayList.get(9));
        manager.setMental(integerArrayList.get(10));
        manager.setHealth(integerArrayList.get(11));

        Log.e("j13 ",""+manager.toString());

        return manager;
    }



    @Override
    protected void onDestroy() {
        realm.close();
//        if (bp != null) {
//            bp.release();
//        }
        super.onDestroy();
    }
}






























