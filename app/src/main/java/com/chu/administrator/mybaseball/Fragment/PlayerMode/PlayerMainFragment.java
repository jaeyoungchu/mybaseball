package com.chu.administrator.mybaseball.Fragment.PlayerMode;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.chu.administrator.mybaseball.Fragment.BaseFragment;
import com.chu.administrator.mybaseball.Fragment.MarketFragment;
import com.chu.administrator.mybaseball.R;

/**
 * Created by Administrator on 2018-06-14.
 */

public class PlayerMainFragment extends BaseFragment {

    Button syncGoogleBtn,signOutBtn,marketBtn,playerDetailBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = false;

        return inflater.inflate(R.layout.main_player_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        signOutBtn = (Button) view.findViewById(R.id.signOutBtn);
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bInstance.signOutGoogle();
            }
        });

        syncGoogleBtn = (Button) view.findViewById(R.id.syncGoogleBtn);
        syncGoogleBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                bInstance.linkGoogle();
            }
        });

        marketBtn = (Button) view.findViewById(R.id.marketBtn);
        marketBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bInstance.addFragment(new MarketFragment());
            }
        });

        playerDetailBtn = (Button) view.findViewById(R.id.playerDetailBtn);
        playerDetailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bInstance.addFragment(new MyPlayerFragment());
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        bInstance.saveModeToRealm("player");
        super.onResume();
    }
}
