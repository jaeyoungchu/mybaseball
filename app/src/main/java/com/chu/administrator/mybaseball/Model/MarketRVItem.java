package com.chu.administrator.mybaseball.Model;

/**
 * Created by Administrator on 2018-07-11.
 */

public class MarketRVItem {

    private Batter batter;
    private Pitcher pitcher;
    private int totalStat;
    private String uid;

    public MarketRVItem(Batter batter, Pitcher pitcher, int totalStat,String uid) {
        this.batter = batter;
        this.pitcher = pitcher;
        this.totalStat = totalStat;
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Batter getBatter() {
        return batter;
    }

    public Pitcher getPitcher() {
        return pitcher;
    }

    public int getTotalStat() {
        return totalStat;
    }

    public void setBatter(Batter batter) {
        this.batter = batter;
    }

    public void setPitcher(Pitcher pitcher) {
        this.pitcher = pitcher;
    }

    public void setTotalStat(int totalStat) {
        this.totalStat = totalStat;
    }
}
