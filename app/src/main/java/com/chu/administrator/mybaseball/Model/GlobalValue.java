package com.chu.administrator.mybaseball.Model;

import android.content.Context;

import com.chu.administrator.mybaseball.BaseCoreActivity;
import com.chu.administrator.mybaseball.R;

import java.util.UUID;

/**
 * Created by Administrator on 2018-06-13.
 */

public class GlobalValue {

    public static int version=1;
    public static final int CONTACT_BATTER = 0,POWER_BATTER=1,EYE_BATTER=2,DEFENCE_BATTER=3,CLUTCH_BATTER=4,SPEED_BATTER=5,GENERAL_BATTER=6;
    public static final int MOVMENT_PITCHER = 10,LOCATION_PITCHER=11,STUFF_PITCHER=12,MENTAL_PITCHER=13,STARTER_PITCHER=14,GENERAL_PITCHER=15,RELIEF_PITCHER=16;
    public static final int PLAYERMODE = 0,MANAGERMODE=1;
    public static final int B_GOLIATH=0,B_DAVID=1,RBI_GUY=2,CLEANUP_MEMBER=3,GUESS_HITTER=4,CUTTING_MACHINE=5,PULL_HITTER=6,SPRAY_HITTER=7,PISTOL=8,THIEF=9,DEFENCE_MASTER=10;
    public static final int PERFECTIONIST=21,P_GOLIATH=22,P_DAVID=23,PINPOINT_MASTER=24,WARRIOR=25,BBMASTER=26,FIRE_BALLER=27,HEAVY_STUFF=28,FINAL_BOSS=29,HOLDING_MAN=30;
    public static final int typeBat = 0,typeGlasses =1, typeGlove =2, typeCap = 3, typeUniform=4, typeShoes=5;


    public static final int catcher=0,firstBase=1,secondBase=2,thirdBase=3,shortStop=4,leftField=5,centerField=6,rightField=7,DH=8,pitcherInDefense = 10;


    public static String[] positionList= {"C","1B","2B","3B","SS","LF","CF","RF","DH"};
    public static final int[] positionMinFieldingRate= {80,88,80,86,80,88,86,88,91,93};
    public static final int[] positionAvgFieldingRate= {96,96,95,96,95,93,95,96,95,96};
    public static final int catcherFieldingRate=5,firstBaseFieldingRate=6,secondBaseFieldingRate=24,thirdBaseFieldingRate=13,shortStopFieldingRate=24,leftFieldFieldingRate=8,centerFieldFieldingRate=12,rightFieldFieldingRate=8;

    public static final int minCon = 49,maxCon = 110, minPow = 49,maxPow = 110,minEye=49,maxEye=110,minSpd=49,maxSpd=100,minDef=49,maxDef=110;

    public static final int minMov = 49,maxMov=110,minLoc=49,maxLoc=110,minStf=49,maxStf=110,minVel=80,maxVel=101,minHRResistance=49,maxHRResistance=110,minGapResistance=49,maxGapResistance=110;
    public static final int avgLeagueSpd = 80;

    public static final float minSORate=13.76f, avgSORate =21.0f,maxSORate=32.34f;
    public static final float minBBRate=3.0f, avgBBRate =5.0f,maxBBRate=16.0f;
    public static final float minHITRate=15.8f, avgHITRate =23.0f,maxHITRate=31.28f;
    public static final float minHRRate=3.9f, avgHRRate =6.0f,maxHRRate=15.0f;

    //    public static final float minGapRate=8.4f, avgGapRate =21.0f,maxGapRate=37.4f;
    public static final float minGapRate=4.2f, avgGapRate =10.5f,maxGapRate=18.7f;
    public static final float minThirdBaseRate = 0.0f, avgThirdBaseRate =2.0f, maxThirdBaseRate = 7.0f;
    public static final float minSBTryRate = 5.0f, avgSBTryRate = 10.0f, maxSBTryRate = 30.0f;
    public static final float minSBSuccessRate = 35.0f, avgSBSuccessRate = 70.0f, maxSBSuccessRate = 93.0f;


    public static float batterGravity = 1.0f;
    public static float clutchGravity = 0.26f;
    public static final float pitcherGravity = 1.0f;
    public static final float defenseGravity = 1.0f;

    public static final float batterGravityForContact=batterGravity,batterGravityForHRPower=batterGravity,batterGravityForGapPower=batterGravity,batterGravityForEye=batterGravity,batterGravityForSpeed=batterGravity
            ,batterGravityForHit=batterGravity;

    public static final float pitcherGravityForMovement=pitcherGravity,pitcherGravityForLocation=pitcherGravity,pitcherGravityForStuff=pitcherGravity
            ,pitcherGravityForVelocity=pitcherGravity,pitcherGravityForHRResistance=pitcherGravity,pitcherGravityForGapResistance=pitcherGravity;

    public static String getPitcherPositionString(int positionInt){
        String val = "";
        if (positionInt == 0){
            val = "SP1";
        }else if (positionInt == 1){
            val = "SP2";
        }else if (positionInt == 2){
            val = "SP3";
        }else if (positionInt == 3){
            val = "SP4";
        }else if (positionInt == 4){
            val = "SP5";
        }else if (positionInt == 5){
            val = "RP1";
        }else if (positionInt == 6){
            val = "RP2";
        }else if (positionInt == 7){
            val = "RP3";
        }else if (positionInt == 8){
            val = "CL";
        }
        return val;
    }

    public static String getPlayerTypeString(int typeInt){
        Context context = BaseCoreActivity.getInstance().getContext();
        String val = "";
        if (typeInt == 0){
            val = context.getResources().getString(R.string.CONTACT_BATTER);
        }else if (typeInt == 1){
            val = context.getResources().getString(R.string.POWER_BATTER);
        }else if (typeInt == 2){
            val = context.getResources().getString(R.string.EYE_BATTER);
        }else if (typeInt == 3){
            val = context.getResources().getString(R.string.DEFENCE_BATTER);
        }else if (typeInt == 4){
            val = context.getResources().getString(R.string.CLUTCH_BATTER);
        }else if (typeInt == 5){
            val = context.getResources().getString(R.string.SPEED_BATTER);
        }else if (typeInt == 6){
            val = context.getResources().getString(R.string.GENERAL_BATTER);
        }else if (typeInt == 10){
            val = context.getResources().getString(R.string.MOVMENT_PITCHER);
        }else if (typeInt == 11){
            val = context.getResources().getString(R.string.LOCATION_PITCHER);
        }else if (typeInt == 12){
            val = context.getResources().getString(R.string.STUFF_PITCHER);
        }else if (typeInt == 13){
            val = context.getResources().getString(R.string.MENTAL_PITCHER);
        }else if (typeInt == 14){
            val = context.getResources().getString(R.string.STARTER_PITCHER);
        }else if (typeInt == 15){
            val = context.getResources().getString(R.string.GENERAL_PITCHER);
        }else if (typeInt == 16){
            val = context.getResources().getString(R.string.RELIEF_PITCHER);
        }
        return val;
    }


    public static String getTitleString(int titleInt){
        Context context = BaseCoreActivity.getInstance().getContext();
        String val = "";
        if (titleInt == 0){
            val = context.getResources().getString(R.string.avgTitle);
        }else if (titleInt == 1){
            val = context.getResources().getString(R.string.obpTitle);
        }else if (titleInt == 2){
            val = context.getResources().getString(R.string.HRTitle);
        }else if (titleInt == 3){
            val = context.getResources().getString(R.string.SBTitle);
        }else if (titleInt == 4){
            val = context.getResources().getString(R.string.RBITitle);
        }else if (titleInt == 5){
            val = context.getResources().getString(R.string.RUNTitle);
        }else if (titleInt == 6){
            val = context.getResources().getString(R.string.opsTitle);
        }else if (titleInt == 7){
            val = context.getResources().getString(R.string.slgTitle);
        }else if (titleInt == 20){
            val = context.getResources().getString(R.string.SP_ERATitle);
        }else if (titleInt == 21){
            val = context.getResources().getString(R.string.SP_WINRATETitle);
        }else if (titleInt == 22){
            val = context.getResources().getString(R.string.SP_SOTitle);
        }else if (titleInt == 23){
            val = context.getResources().getString(R.string.SP_INNINGTitle);
        }else if (titleInt == 24){
            val = context.getResources().getString(R.string.RP_ERATitle);
        }else if (titleInt == 25){
            val = context.getResources().getString(R.string.RP_SOTitle);
        }
        return val;
    }

    public static String getPositionString(int positionInt){
        String val = "";
        if (positionInt == 0){
            val = "C";
        }else if (positionInt == 1){
            val = "1B";
        }else if (positionInt == 2){
            val = "2B";
        }else if (positionInt == 3){
            val = "3B";
        }else if (positionInt == 4){
            val = "SS";
        }else if (positionInt == 5){
            val = "LF";
        }else if (positionInt == 6){
            val = "CF";
        }else if (positionInt == 7){
            val = "RF";
        }else if (positionInt == 8){
            val = "DH";
        }
        return val;
    }
    public static int getPositionInt(String position){
        int positionInt=-1;

        if (position.equals("C")){
            positionInt = 0;
        }else if (position.equals("1B")){
            positionInt = 1;
        }else if (position.equals("2B")){
            positionInt = 2;
        }else if (position.equals("3B")){
            positionInt = 3;
        }else if (position.equals("SS")){
            positionInt = 4;
        }else if (position.equals("LF")){
            positionInt = 5;
        }else if (position.equals("CF")){
            positionInt = 6;
        }else if (position.equals("RF")){
            positionInt = 7;
        }

        return positionInt;
    }


    public static String getSkillName(int type){
        Context context = BaseCoreActivity.getInstance().getContext();

        String val = "";
        switch (type){
            case 0: val = context.getResources().getString(R.string.goliath);
                break;
            case 1: val = context.getResources().getString(R.string.david);
                break;
            case 2: val = context.getResources().getString(R.string.rbi_guy);
                break;
            case 3: val = context.getResources().getString(R.string.cleanup);
                break;
            case 4: val = context.getResources().getString(R.string.guess_hitter);
                break;
            case 5: val = context.getResources().getString(R.string.ball_cutter);
                break;
            case 6: val = context.getResources().getString(R.string.pull_hitter);
                break;
            case 7: val = context.getResources().getString(R.string.spray_hitter);
                break;
            case 8: val = context.getResources().getString(R.string.pistol);
                break;
            case 9: val = context.getResources().getString(R.string.thief);
                break;
            case 10: val = context.getResources().getString(R.string.defence_master);
                break;


            case 21: val = context.getResources().getString(R.string.perfectionist);
                break;
            case 22: val = context.getResources().getString(R.string.goliath_p);
                break;
            case 23: val = context.getResources().getString(R.string.david_p);
                break;
            case 24: val = context.getResources().getString(R.string.pinpoint_master);
                break;
            case 25: val = context.getResources().getString(R.string.warrior);
                break;
            case 26: val = context.getResources().getString(R.string.bb_master);
                break;
            case 27: val = context.getResources().getString(R.string.fire_baller);
                break;
            case 28: val = context.getResources().getString(R.string.heavy_stuff);
                break;
            case 29: val = context.getResources().getString(R.string.final_boss);
                break;
            case 30: val = context.getResources().getString(R.string.holding_man);
                break;
        }
        return val;
    }


    public static String getSkillDescription(int type){
        Context context = BaseCoreActivity.getInstance().getContext();

        String val = "";
        switch (type){
            case 0: val = context.getResources().getString(R.string.goliath_detail);
                break;
            case 1: val = context.getResources().getString(R.string.david_detail);
                break;
            case 2: val = context.getResources().getString(R.string.rbi_guy_detail);
                break;
            case 3: val = context.getResources().getString(R.string.cleanup_detail);
                break;
            case 4: val = context.getResources().getString(R.string.guess_hitter_detail);
                break;
            case 5: val = context.getResources().getString(R.string.ball_cutter_detail);
                break;
            case 6: val = context.getResources().getString(R.string.pull_hitter_detail);
                break;
            case 7: val = context.getResources().getString(R.string.spray_hitter_detail);
                break;
            case 8: val = context.getResources().getString(R.string.pistol_detail);
                break;
            case 9: val = context.getResources().getString(R.string.thief_detail);
                break;
            case 10: val = context.getResources().getString(R.string.defence_master_detail);
                break;


            case 21: val = context.getResources().getString(R.string.perfectionist_detail);
                break;
            case 22: val = context.getResources().getString(R.string.goliath_p_detail);
                break;
            case 23: val = context.getResources().getString(R.string.david_p_detail);
                break;
            case 24: val = context.getResources().getString(R.string.pinpoint_master_detail);
                break;
            case 25: val = context.getResources().getString(R.string.warrior_detail);
                break;
            case 26: val = context.getResources().getString(R.string.bb_master_detail);
                break;
            case 27: val = context.getResources().getString(R.string.fire_baller_detail);
                break;
            case 28: val = context.getResources().getString(R.string.heavy_stuff_detail);
                break;
            case 29: val = context.getResources().getString(R.string.final_boss_detail);
                break;
            case 30: val = context.getResources().getString(R.string.holding_man_detail);
                break;
        }
        return val;
    }

}
