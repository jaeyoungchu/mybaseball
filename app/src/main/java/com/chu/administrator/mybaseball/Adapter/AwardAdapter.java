package com.chu.administrator.mybaseball.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chu.administrator.mybaseball.Model.TitleItem;
import com.chu.administrator.mybaseball.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-07-14.
 */


public class AwardAdapter extends RecyclerView.Adapter<AwardAdapter.CustomViewHolder> {
    private ArrayList<TitleItem> mItems;

    public AwardAdapter(ArrayList<TitleItem> mItems) {
        this.mItems = mItems;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_item,null);
        AwardAdapter.CustomViewHolder holder = new AwardAdapter.CustomViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.titleTV.setText(mItems.get(position).getAllTitle());
        holder.titleTV.getLayoutParams().height = 100;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder{
        public TextView titleTV;

        public CustomViewHolder(View view) {
            super(view);
            titleTV = (TextView) view.findViewById(R.id.titleTV);
        }
    }
}