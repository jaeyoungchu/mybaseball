package com.chu.administrator.mybaseball.Helper;

import android.util.Log;

import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.BatterSkill;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.MatchDetail;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.PitcherSkill;
import com.chu.administrator.mybaseball.Model.Team;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Administrator on 2018-06-28.
 */

public class MatchLogic {
    private Team homeTeam, awayTeam;
    private Team attackingTeam,deffendingTeam;
    private Batter batter;
    private Pitcher pitcher,homeStartPitcher,awayStartPitcher;
    private ArrayList<Batter> homeBatterRoster, awayBatterRoaster;
    private ArrayList<Pitcher> homePitcherRoaster, awayPitcherRoaster,homeUsedPicherList,awayUsedPicherList;
    private BattingLogic battingLogic;
    private int resultOfBatting;
    private Batter firstBasePlayer,secondBasePlayer,thirdBasePlayer;
    private Batter defenseBatter;
    private boolean isHomeTeamAttackInning,isRbiSituation;
    private int winPoint = 2;
    private int losePoint = 1;
    private String resultOfPAStr;
    private ArrayList<MatchDetail> matchDetails;
    private MatchDetail matchDetail;
    private boolean isShowingBatterSkilll1,isShowingBatterSkilll2,isShowingPitcherSkill1,isShowingPitcherSkill2;
    private int startingPicherIndex;
    private int inning;
    private ArrayList<Integer> homeTeamInningScore, awayTeamInningScore;
    private int outCount;
    private boolean isFirstBon,isSecondBon,isThirdBon;
    private int homeTeamScore, homeTeamHit, homeTeamHR,homeTeamBB, homeTeamError;
    private int awayTeamScore, awayTeamHit, awayTeamHR,awayTeamBB, awayTeamError;
    private int homeBatterOrder, homePitcherOrder;
    private int awayBatterOrder, awayPitcherOrder;
    private Random randomInt;
    private boolean isTwoOutError;
    private boolean isBeginningInning;

    public MatchLogic(Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public ArrayList<MatchDetail> playGame(){
        initScore();
        playMatch();
        setLastPitcherDif();
        setHomePitcherWinLose();
        setAwayPitcherWinLose();
        saveMatchRecordToSeason();
        Log.e("j13 ",""+matchDetails.size());
        return matchDetails;
    }

    private Team getAttackingTeam(){
        if (isHomeTeamAttackInning){
            return homeTeam;
        }else {
            return awayTeam;
        }
    }
    private Team getDeffendingTeam(){
        if (isBeginningInning){
            return awayTeam;
        }else {
            return homeTeam;
        }
    }

    private void initScore(){
        randomInt = new Random();
        startingPicherIndex = getStartingPitcherIndex();

        isBeginningInning = true;
        isShowingBatterSkilll1 = false;
        isShowingBatterSkilll2 = false;
        isShowingPitcherSkill1 = false;
        isShowingPitcherSkill2 = false;
        defenseBatter = null;
        resultOfPAStr="";
        matchDetails = new ArrayList<MatchDetail>();
        homeTeamInningScore = new ArrayList<Integer>();
        awayTeamInningScore = new ArrayList<Integer>();
        homeBatterRoster = new ArrayList<Batter>();
        homePitcherRoaster = new ArrayList<Pitcher>();
        awayBatterRoaster = new ArrayList<Batter>();
        awayPitcherRoaster = new ArrayList<Pitcher>();

        homeBatterRoster = homeTeam.getBatterSquad();
        homePitcherRoaster = homeTeam.getPitcherSquad();
        awayBatterRoaster = awayTeam.getBatterSquad();
        awayPitcherRoaster = awayTeam.getPitcherSquad();

        homeUsedPicherList = new ArrayList<Pitcher>();
        awayUsedPicherList = new ArrayList<Pitcher>();
        initMatchRecord();
        homeStartPitcher = getStartPitcherFromOrder(false);
        homeStartPitcher.matchRecord.game++;
        homeUsedPicherList.add(0,homeStartPitcher);

        awayStartPitcher = getStartPitcherFromOrder(true);
        awayStartPitcher.matchRecord.game++;
        awayUsedPicherList.add(0,awayStartPitcher);

        isTwoOutError = false;

        inning = 1;
        isFirstBon=false;
        isSecondBon=false;
        isThirdBon=false;
        homeTeamScore = 0;
        homeTeamHit = 0;
        homeTeamHR = 0;
        homeTeamError = 0;
        awayTeamScore = 0;
        awayTeamHit = 0;
        awayTeamHR = 0;
        awayTeamError = 0;
        homeBatterOrder = 1;
        awayBatterOrder = 1;
        homeTeamBB =0;
        awayTeamBB =0;
        homePitcherOrder = startingPicherIndex;
        awayPitcherOrder = startingPicherIndex;
        isHomeTeamAttackInning = true;
    }



    private int getStartingPitcherIndex(){
        int randomIndex = getRandom100Integer();
        int val = 0;
        // 24,22,20,18,16
        if (randomIndex <= 24){
            val =0;
        }else if (randomIndex <= 46){
            val =1;
        }else if (randomIndex <= 66){
            val =2;
        }else if (randomIndex <= 84){
            val =3;
        }else if (randomIndex <= 100){
            val =4;
        }
        return val;
    }


    private void playMatch(){
        while (inning < 10 || ((inning>9)&&(homeTeamScore == awayTeamScore))  ) {
            playFirstHalfInning();
            if (inning > 8 && awayTeamScore > homeTeamScore){
                break;
            }
//            Log.d("finish half","============================================================");
            clearSituation();
            playLastHalfInning();
//            Log.d("finish inning","============================================================");
            clearSituation();
            inning+=1;
        }
    }
    private void clearSituation(){
        isFirstBon =false;
        isSecondBon = false;
        isThirdBon = false;
        firstBasePlayer = null;
        secondBasePlayer = null;
        thirdBasePlayer = null;
        defenseBatter = null;
        outCount = 0;
        isTwoOutError = false;
    }

    private void playFirstHalfInning(){
        isRbiSituation = false;
        isHomeTeamAttackInning = true;
        attackingTeam = getAttackingTeam();
        deffendingTeam = getDeffendingTeam();
        int maxOutCount =getRPMaxOutCounter();
        homeTeamInningScore.add(inning-1,0);

        if (inning > 8 && !isAlreadyUsedInAway(awayPitcherRoaster.get(8))) {
            if ((awayTeamScore - homeTeamScore) < 4 && (awayTeamScore - homeTeamScore) > 0) {
                awayUsedPicherList.get(awayUsedPicherList.size()-1).matchRecord.endScoreDif = getScoreDif();
                pitcher = (Pitcher) awayPitcherRoaster.get(8);
                pitcher.matchRecord.game++;
                awayUsedPicherList.add(awayUsedPicherList.size(),pitcher);
            }
        }

        while (outCount < 3) {
            isShowingBatterSkilll1 = false;
            isShowingBatterSkilll2 = false;
            isShowingPitcherSkill1 = false;
            isShowingPitcherSkill2 = false;
            batter = getBatterFromOrder(attackingTeam.equals(homeTeam));

            if (awayUsedPicherList.size() == 1){
                pitcher = awayStartPitcher;
                if (pitcher.getMaxPitchCount() < pitcher.pitchCount || (pitcher.getPositionString().equals("SP") && pitcher.getMaxPitchOrder() == 0 && inning == 7) ){
                    pitcher = getPitcherFromOrder(isHomeTeamAttackInning);
                }
            }else {
                pitcher = awayUsedPicherList.get(awayUsedPicherList.size()-1);
                if (pitcher.matchRecord.outcount > maxOutCount){
                    pitcher = getPitcherFromOrder(isHomeTeamAttackInning);
                }
            }
            if (isBeginningInning){
                matchDetail = new MatchDetail(homeTeamInningScore,awayTeamInningScore,homeStartPitcher,awayStartPitcher,null,inning,outCount,isHomeTeamAttackInning,homeTeamScore,homeTeamHit,homeTeamHR,homeTeamBB,homeTeamError,awayTeamScore,awayTeamHit,awayTeamHR,awayTeamBB,awayTeamError,isFirstBon,isSecondBon,isThirdBon,firstBasePlayer,secondBasePlayer,thirdBasePlayer,batter,pitcher,"beginInning",homeTeam,awayTeam,isShowingBatterSkilll1,isShowingBatterSkilll2,isShowingPitcherSkill1,isShowingPitcherSkill2);
                matchDetails.add(matchDetail);
                isBeginningInning = false;
            }
            if (pitcher.pitchCount == 0){
                pitcher.matchRecord.startScoreDif = getScoreDif();
            }
            pitcher.pitchCount++;

            batter.matchRecord.dutyPitcher = pitcher.getCardToken();
            int r = getRandom100Integer();

//            Log.e("home batter : ",  batter.getBatterName()+"");
//            Log.e("away pitcher : ",  pitcher.getPitcherName()+"");
            checkRBISituation();
            battingLogic = new BattingLogic(batter,pitcher,r);
            resultOfBatting = battingLogic.getResultOfBatting();
            checkStealBaseSituation();
            checkBuntSituation();

            if (outCount < 3){
                setInningStatus(resultOfBatting);
            }
        };
        isBeginningInning = true;

    }

    private void checkRBISituation(){
        if (isSecondBon || isThirdBon){
            isRbiSituation = true;
        }else {
            isRbiSituation = false;
        }
    }

    private void playLastHalfInning(){
        isRbiSituation = false;
        isHomeTeamAttackInning = false;
        attackingTeam = getAttackingTeam();
        deffendingTeam = getDeffendingTeam();
        awayTeamInningScore.add(inning-1,0);
        int maxOutCount =getRPMaxOutCounter();

        if (inning > 8 && !isAlreadyUsedInHome(homePitcherRoaster.get(8))){

            if ((homeTeamScore-awayTeamScore)<4 && (homeTeamScore-awayTeamScore)>0){
                homeUsedPicherList.get(homeUsedPicherList.size()-1).matchRecord.endScoreDif = getScoreDif();

                pitcher = (Pitcher) homePitcherRoaster.get(8);
                pitcher.matchRecord.game++;
                homeUsedPicherList.add(homeUsedPicherList.size(),pitcher);
            }
        }

        while (outCount < 3) {
            isShowingBatterSkilll1 = false;
            isShowingBatterSkilll2 = false;
            isShowingPitcherSkill1 = false;
            isShowingPitcherSkill2 = false;
            batter = getBatterFromOrder(attackingTeam.equals(homeTeam));

            if (homeUsedPicherList.size() == 1){
                pitcher = homeStartPitcher;
                if (pitcher.getMaxPitchCount() < pitcher.pitchCount|| (pitcher.getPositionString().equals("SP") && pitcher.getMaxPitchOrder() == 0 && inning == 7)){
                    pitcher = getPitcherFromOrder(isHomeTeamAttackInning);
                }
            }else {
                pitcher = homeUsedPicherList.get(homeUsedPicherList.size()-1);
                if (pitcher.matchRecord.outcount > maxOutCount){
                    pitcher = getPitcherFromOrder(isHomeTeamAttackInning);
                }
            }
            if (isBeginningInning){
                matchDetail = new MatchDetail(homeTeamInningScore,awayTeamInningScore,homeStartPitcher,awayStartPitcher,null,inning,outCount,isHomeTeamAttackInning,homeTeamScore,homeTeamHit,homeTeamHR,homeTeamBB,homeTeamError,awayTeamScore,awayTeamHit,awayTeamHR,awayTeamBB,awayTeamError,isFirstBon,isSecondBon,isThirdBon,firstBasePlayer,secondBasePlayer,thirdBasePlayer,batter,pitcher,"beginInning",homeTeam,awayTeam,isShowingBatterSkilll1,isShowingBatterSkilll2,isShowingPitcherSkill1,isShowingPitcherSkill2);
                matchDetails.add(matchDetail);
                isBeginningInning = false;
            }
            if (pitcher.pitchCount == 0){
                pitcher.matchRecord.startScoreDif = getScoreDif();
            }
            pitcher.pitchCount++;
            batter.matchRecord.dutyPitcher = pitcher.getCardToken();

            int r = getRandom100Integer();
//            Log.e("home batter : ",  batter.getBatterName()+"");
//            Log.e("away pitcher : ",  pitcher.getPitcherName()+"");
            checkRBISituation();
            battingLogic = new BattingLogic(batter,pitcher,r);
            resultOfBatting = battingLogic.getResultOfBatting();
            checkStealBaseSituation();
            checkBuntSituation();
            if (outCount < 3){
                setInningStatus(resultOfBatting);
            }
        };
        isBeginningInning = true;
    }

    private int getRPMaxOutCounter(){
        int maxOutCount =5;
        if (inning < 5){
            maxOutCount = 8;
        }else if (inning > 9 && homeUsedPicherList.size() > 3){
            maxOutCount = 3000;
        }else if (inning > 9 && homeUsedPicherList.size() < 4){
            maxOutCount = 8;
        }
        return maxOutCount;
    }

    private void saveMatchRecordToSeason(){
        for (int i =0; i < homeBatterRoster.size();i++){
            homeTeam.getBatterSquad().get(i).getSeasonRecord().setGame(homeTeam.getBatterSquad().get(i).getSeasonRecord().getGame()+1);
            homeBatterRoster.get(i).saveMatchRecordToSeasonRecord(homeBatterRoster.get(i).matchRecord);
        }
        for (int i =0; i < homePitcherRoaster.size();i++){
            homeTeam.getPitcherSquad().get(i).saveMatchRecordToSeasonRecord(homePitcherRoaster.get(i).matchRecord);

        }
        for (int i =0; i < awayBatterRoaster.size();i++){
            awayTeam.getBatterSquad().get(i).getSeasonRecord().setGame(awayTeam.getBatterSquad().get(i).getSeasonRecord().getGame()+1);
            awayBatterRoaster.get(i).saveMatchRecordToSeasonRecord(awayBatterRoaster.get(i).matchRecord);
        }
        for (int i =0; i < awayPitcherRoaster.size();i++){
            awayPitcherRoaster.get(i).saveMatchRecordToSeasonRecord(awayPitcherRoaster.get(i).matchRecord);
        }

        winPoint = 10;
        losePoint = 0;

        if (homeTeamScore > awayTeamScore){
            homeTeam.setPvpWinNum(homeTeam.getPvpWinNum()+1);
            awayTeam.setPvpLoseNum(awayTeam.getPvpLoseNum()+1);
        }else if (homeTeamScore < awayTeamScore){
            homeTeam.setPvpLoseNum(homeTeam.getPvpLoseNum()+1);
            awayTeam.setPvpWinNum(awayTeam.getPvpWinNum()+1);
        }


//        Log.e("j13 ",""+homeTeam.getPvpWinNum() +" / "+ homeTeam.getPvpLoseNum());
//        Log.e("j13 ",""+awayTeam.getPvpWinNum() +" / "+ awayTeam.getPvpLoseNum());
    }





    private void setLastPitcherDif(){
//        awayUsedPicherList.get(awayUsedPicherList.size()-1).matchRecord.endScoreDif = awayTeamScore -homeTeamScore;
        homeUsedPicherList.get(homeUsedPicherList.size()-1).matchRecord.endScoreDif = homeTeamScore - awayTeamScore;
        awayUsedPicherList.get(awayUsedPicherList.size()-1).matchRecord.endScoreDif = awayTeamScore - homeTeamScore;

//        for (int i = 0 ; i < homeTeam.getPitcherSquard().size() ; i++){
//            Log.e("home score dif : " ,homeTeam.getPitcherSquard().get(i).getPitcherName() +" , "+homeTeam.getPitcherSquard().get(i).matchRecord.startScoreDif + " , "+homeTeam.getPitcherSquard().get(i).matchRecord.endScoreDif);
//        }
    }
    private void setHomePitcherWinLose(){
        int winIndex = 0;
        int loseIndex = 0;
        for (int i = 0 ; i < homeUsedPicherList.size() ; i++){

//            Log.e("score dif : ", homeUsedPicherList.get(i).getPitcherName()+" , "+ homeUsedPicherList.get(i).matchRecord.startScoreDif+" , " + homeUsedPicherList.get(i).matchRecord.endScoreDif);
//            Log.e("inning : ", homeUsedPicherList.get(i).getPitcherName()+" , "+ homeUsedPicherList.get(i).matchRecord.outcount);


            //Start Pitcher not complete game
            if (i == 0 && homeUsedPicherList.size() != 1){
                if ( (homeUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (homeUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }  // if complete game
            }else if (i == 0 && homeUsedPicherList.size() == 1){
                if ( (homeUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (homeUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }
            }

            //Reliever
            if (homeUsedPicherList.size() > 1 && i > 0 && i < homeUsedPicherList.size()-1 ){
                if ( (homeUsedPicherList.get(i).matchRecord.startScoreDif < 1) && (homeUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (homeUsedPicherList.get(i).matchRecord.startScoreDif >= 0) && (homeUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }else if ( (homeUsedPicherList.get(i).matchRecord.startScoreDif < 4) && (homeUsedPicherList.get(i).matchRecord.startScoreDif > 0) && (homeUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    homeUsedPicherList.get(i).matchRecord.hold++;
                }
            }

            //last pitcher
            // not starter p
            if (i == homeUsedPicherList.size()-1 && homeUsedPicherList.size() > 1){
                if ( (homeUsedPicherList.get(i).matchRecord.startScoreDif < 4) && (homeUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    homeUsedPicherList.get(i).matchRecord.save++;
                }else if ( (homeUsedPicherList.get(i).matchRecord.startScoreDif < 1) && (homeUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (homeUsedPicherList.get(i).matchRecord.startScoreDif > 0) && (homeUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }
            }
        }

        if (homeUsedPicherList.get(homeUsedPicherList.size()-1).matchRecord.endScoreDif > 0){
            homeUsedPicherList.get(winIndex).matchRecord.win++;
        }else if (homeUsedPicherList.get(homeUsedPicherList.size()-1).matchRecord.endScoreDif < 0){
            homeUsedPicherList.get(loseIndex).matchRecord.lose++;
        }
    }
    private void setAwayPitcherWinLose(){
        int winIndex = 0;
        int loseIndex = 0;
        for (int i = 0 ; i < awayUsedPicherList.size() ; i++){

//            Log.e("score dif : ", homeUsedPicherList.get(i).getPitcherName()+" , "+ homeUsedPicherList.get(i).matchRecord.startScoreDif+" , " + homeUsedPicherList.get(i).matchRecord.endScoreDif);
//            Log.e("inning : ", homeUsedPicherList.get(i).getPitcherName()+" , "+ homeUsedPicherList.get(i).matchRecord.outcount);


            //Start Pitcher not complete game
            if (i == 0 && awayUsedPicherList.size() != 1){
                if ( (awayUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (awayUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }  // if complete game
            }else if (i == 0 && awayUsedPicherList.size() == 1){
                if ( (awayUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (awayUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }
            }

            //Reliever
            if (awayUsedPicherList.size() > 1 && i > 0 && i < awayUsedPicherList.size()-1 ){
                if ( (awayUsedPicherList.get(i).matchRecord.startScoreDif < 1) && (awayUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (awayUsedPicherList.get(i).matchRecord.startScoreDif >= 0) && (awayUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }else if ( (awayUsedPicherList.get(i).matchRecord.startScoreDif < 4) && (awayUsedPicherList.get(i).matchRecord.startScoreDif > 0) && (awayUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    awayUsedPicherList.get(i).matchRecord.hold++;
                }
            }

            //last pitcher
            // not starter p
            if (i == awayUsedPicherList.size()-1 && awayUsedPicherList.size() > 1){
                if ( (awayUsedPicherList.get(i).matchRecord.startScoreDif < 4) && (awayUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    awayUsedPicherList.get(i).matchRecord.save++;
                }else if ( (awayUsedPicherList.get(i).matchRecord.startScoreDif < 1) && (awayUsedPicherList.get(i).matchRecord.endScoreDif > 0) ){
                    winIndex = i;
                }else if ( (awayUsedPicherList.get(i).matchRecord.startScoreDif > 0) && (awayUsedPicherList.get(i).matchRecord.endScoreDif < 0) ){
                    loseIndex = i;
                }
            }
        }

        if (awayUsedPicherList.get(awayUsedPicherList.size()-1).matchRecord.endScoreDif > 0){
            awayUsedPicherList.get(winIndex).matchRecord.win++;
        }else if (awayUsedPicherList.get(awayUsedPicherList.size()-1).matchRecord.endScoreDif < 0){
            awayUsedPicherList.get(loseIndex).matchRecord.lose++;
        }
    }


    private int getScoreDif(){
        if (isHomeTeamAttackInning){
            return awayTeamScore - homeTeamScore;
        }else {
            return homeTeamScore - awayTeamScore;
        }
    }
    private Pitcher getStartPitcherFromOrder(boolean isHomeTeamAttack){
        int pitcherOrder;
        Pitcher returnPitcher;

        if (isHomeTeamAttack){
            pitcherOrder = startingPicherIndex;
            returnPitcher = (Pitcher) awayPitcherRoaster.get(pitcherOrder);
        }else {
            pitcherOrder = startingPicherIndex;
            returnPitcher = (Pitcher) homePitcherRoaster.get(pitcherOrder);
        }
        return returnPitcher;
    }


    private Pitcher getPitcherWithName(String token){

        Pitcher returnPitcher=null;

        if (isHomeTeamAttackInning){
            for (int i = 0 ; i < awayPitcherRoaster.size();i++){
                if (awayPitcherRoaster.get(i).getCardToken().equals(token)){
                    returnPitcher = (Pitcher) awayPitcherRoaster.get(i);
                }
            }
        }else {
            for (int i = 0 ; i < homePitcherRoaster.size();i++){
                if (homePitcherRoaster.get(i).getCardToken().equals(token)){
                    returnPitcher = (Pitcher) homePitcherRoaster.get(i);
                }
            }
        }

        return returnPitcher;
    }

    private Pitcher getPitcherFromOrder(boolean isHomeTeamAttack){
        String teamOfPitcher;
        Pitcher returnPitcher;

        if (isHomeTeamAttack){
            teamOfPitcher = "away";
        }else {
            teamOfPitcher = "home";
        }
        returnPitcher = checkChangingPitcher(teamOfPitcher);

        returnPitcher.matchRecord.game++;
        pitcher.matchRecord.endScoreDif = getScoreDif();
        if (isHomeTeamAttack){
            awayUsedPicherList.add(awayUsedPicherList.size(),returnPitcher);
        }else {
            homeUsedPicherList.add(homeUsedPicherList.size(),returnPitcher);
        }

        return returnPitcher;
    }

    private Pitcher checkChangingPitcher(String teamOfPitcher){
        Pitcher relievePitcher = new Pitcher();

        if (teamOfPitcher.equals("away")){
            if ((awayTeamScore-homeTeamScore)<3 && (awayTeamScore-homeTeamScore)>-3){  //2,1,0,-1,-2
                relievePitcher = (Pitcher) awayPitcherRoaster.get(5);
                if (isAlreadyUsedInAway(relievePitcher)){
                    relievePitcher = awayPickOtherRP();
                }
            }else if ( ((awayTeamScore-homeTeamScore)>2 && (awayTeamScore-homeTeamScore)<6)  ||  ( ((awayTeamScore-homeTeamScore)>-6 && (awayTeamScore-homeTeamScore)>-2))  ){ //3,4,5,~ -3,-4,-5
                relievePitcher = (Pitcher) awayPitcherRoaster.get(6);
                if (isAlreadyUsedInAway(relievePitcher)){
                    relievePitcher = awayPickOtherRP();
                }
            }else {
                relievePitcher = (Pitcher) awayPitcherRoaster.get(7);
                if (isAlreadyUsedInAway(relievePitcher)){
                    relievePitcher = awayPickOtherRP();
                }
            }


        }else if (teamOfPitcher.equals("home")){
            if ((homeTeamScore-awayTeamScore)<3 && (homeTeamScore-awayTeamScore)>-3){  //2,1,0,-1,-2
                relievePitcher = (Pitcher) homePitcherRoaster.get(5);
                if (isAlreadyUsedInHome(relievePitcher)){
                    relievePitcher = homePickOtherRP();
                }
            }else if ( ((homeTeamScore-awayTeamScore)>2 && (homeTeamScore-awayTeamScore)<6)  ||  ( ((homeTeamScore-awayTeamScore)>-6 && (homeTeamScore-awayTeamScore)>-2))  ){ //3,4,5,~ -3,-4,-5
                relievePitcher = (Pitcher) homePitcherRoaster.get(6);
                if (isAlreadyUsedInHome(relievePitcher)){
                    relievePitcher = homePickOtherRP();
                }
            }else {
                relievePitcher = (Pitcher) homePitcherRoaster.get(7);
                if (isAlreadyUsedInHome(relievePitcher)){
                    relievePitcher = homePickOtherRP();
                }
            }
        }


        return relievePitcher;
    }
    private boolean isAlreadyUsedInHome(Pitcher rp){
        boolean flag=false;
        for (int i=0 ; i < homeUsedPicherList.size();i++){
            if (rp.getCardToken().equals(homeUsedPicherList.get(i).getCardToken())){
                flag = true;
            }
        }
        return flag;
    }

    private Pitcher homePickOtherRP(){
        for (int j=5 ; j <8 ;j++){
            boolean flag = true;
            for (int i=0 ; i < homeUsedPicherList.size();i++){
                if (homePitcherRoaster.get(j).getCardToken().equals(homeUsedPicherList.get(i).getCardToken())){
                    flag = false;
                }
            }
            if (flag){
                return homePitcherRoaster.get(j);
            }
        }
        return pitcher;
    }

    private boolean isAlreadyUsedInAway(Pitcher rp){
        boolean flag=false;
        for (int i=0 ; i < awayUsedPicherList.size();i++){
            if (rp.getCardToken().equals(awayUsedPicherList.get(i).getCardToken())){
                flag = true;
            }
        }
        return flag;
    }

    private Pitcher awayPickOtherRP(){
        for (int j=5 ; j <8 ;j++){
            boolean flag = true;
            for (int i=0 ; i < awayUsedPicherList.size();i++){
                if (awayPitcherRoaster.get(j).getCardToken().equals(awayUsedPicherList.get(i).getCardToken())){
                    flag = false;
                }
            }
            if (flag){
                return awayPitcherRoaster.get(j);
            }
        }

        return pitcher;
    }


    private void checkStealBaseSituation(){
        if ((isFirstBon && !isSecondBon && !isThirdBon) ||(isFirstBon && !isSecondBon && isThirdBon)) {

            int tryForStealDice = randomInt.nextInt(100)+1;
            int chanceForStealDice = randomInt.nextInt(100) + 1;

            Batter catcher;
            catcher = getBatterWithPosition(GlobalValue.catcher);
            float throwingStat = catcher.getTrueDefenseInhundred() * 100;
            float sbStat = firstBasePlayer.getTrueSpeed();
            float sbSuccessRate,trySBRate;


            if (sbStat <= 50) {
                trySBRate = GlobalValue.avgSBTryRate - (GlobalValue.avgSBTryRate - GlobalValue.minSBTryRate)*sbStat/100f;

            } else {
                trySBRate = GlobalValue.avgSBTryRate + (GlobalValue.maxSBTryRate - GlobalValue.avgSBTryRate) * (sbStat - throwingStat) / 100f;
            }

            if (throwingStat > sbStat) {
                sbSuccessRate = GlobalValue.avgSBSuccessRate - (GlobalValue.avgSBSuccessRate - GlobalValue.minSBSuccessRate) * (throwingStat - sbStat) / 100f;

            } else {
                sbSuccessRate = GlobalValue.avgSBSuccessRate + (GlobalValue.maxSBSuccessRate - GlobalValue.avgSBSuccessRate) * (sbStat - throwingStat) / 100f;
            }
            if (firstBasePlayer.getStealOrder() == 0){
                return;
            }else if (firstBasePlayer.getStealOrder() == 1){

            }else {
                trySBRate += 20;
            }

            if(firstBasePlayer.getBatterSkill1() != null &&firstBasePlayer.getBatterSkill1().getSkillName().equals("THIEF")){
                if (firstBasePlayer.getBatterSkill1().getSkillLevel() == 1){
                    sbSuccessRate +=3;
                }else if (firstBasePlayer.getBatterSkill1().getSkillLevel() == 2){
                    sbSuccessRate +=6;
                }else {
                    sbSuccessRate +=12;
                }
            }

            if(firstBasePlayer.getBatterSkill2() != null &&firstBasePlayer.getBatterSkill2().getSkillName().equals("THIEF")){
                if (firstBasePlayer.getBatterSkill2().getSkillLevel() == 1){
                    sbSuccessRate +=3;
                }else if (firstBasePlayer.getBatterSkill2().getSkillLevel() == 2){
                    sbSuccessRate +=6;
                }else {
                    sbSuccessRate +=12;
                }
            }



            if (tryForStealDice < trySBRate) {
                if (chanceForStealDice < sbSuccessRate) {

                    firstBasePlayer = getBatterFromList(firstBasePlayer);
                    firstBasePlayer.matchRecord.sb++;
                    setBatterStatus(firstBasePlayer);

                    catcher = getBatterFromList(catcher);
                    catcher.matchRecord.missSB++;
                    setBatterStatus(catcher);

                    isSecondBon = true;
                    secondBasePlayer = firstBasePlayer;
                    isFirstBon = false;
                    firstBasePlayer = null;
                    resultOfPAStr="STEAL BASE SUCCESS!";
                    drawPAStatus();
                } else {

                    firstBasePlayer = getBatterFromList(firstBasePlayer);
                    firstBasePlayer.matchRecord.cs++;
                    setBatterStatus(firstBasePlayer);

                    catcher = getBatterFromList(catcher);
                    catcher.matchRecord.gotSB++;
                    setBatterStatus(catcher);

                    isFirstBon = false;
                    firstBasePlayer = null;
                    outCount++;
                    addPitcherOutcount();
                    resultOfPAStr="STEAL BASE FAIL!";
                    drawPAStatus();
                }
            }
            checkRBISituation();

        }
    }

    private void addPitcherOutcount(){
        pitcher.matchRecord.outcount++;
        if (isRbiSituation){
         pitcher.matchRecord.rispOutcount++;
        }
    }
    private void addPitcherHit(){
        pitcher.matchRecord.hit++;
        if (isRbiSituation){
            pitcher.matchRecord.rispHit++;
        }
    }

    private void addBatterHit(){
        batter.matchRecord.hit++;
        if (isRbiSituation){
            batter.matchRecord.rispHit++;
        }
    }

    private void addBatterAb(){
        batter.matchRecord.ab++;
        if (isRbiSituation){
            batter.matchRecord.rispAb++;
        }
    }

    private void checkBuntSituation(){
        int successRateOfBunt = randomInt.nextInt(100);
        int tryRate = randomInt.nextInt(100);

        if(outCount == 0 || outCount == 1){
            if (isFirstBon && !isSecondBon && !isThirdBon){     //runner on 1b
                if (batter.getBuntOrder() == 0){
                    return;
                }else if (batter.getBuntOrder() == 1 && tryRate < 70){
                    return;
                }else if (batter.getBuntOrder() == 2 && tryRate < 30){
                    return;
                }
                if (successRateOfBunt < 90){
                    isFirstBon = false;
                    isSecondBon = true;
                    secondBasePlayer = firstBasePlayer;
                    resultOfPAStr = "BUNT SUCCESS!";
                }else {
                    isFirstBon = true;
                    firstBasePlayer = batter;
                    resultOfPAStr = "BUNT FAIL!";
                }
                batter = getBatterFromList(batter);
                batter.matchRecord.pa++;
                setBatterStatus(batter);
                outCount++;
                addPitcherOutcount();
                drawPAStatus();
                isShowingBatterSkilll1 = false;
                isShowingBatterSkilll2 = false;
                isShowingPitcherSkill1 = false;
                isShowingPitcherSkill2 = false;
                batter = getBatterFromOrder(attackingTeam.equals(homeTeam));
                batter.matchRecord.dutyPitcher = pitcher.getCardToken();

            }
        }
        checkRBISituation();
    }


    private boolean isPlayerError(int defPosition){
        float errorRate=0.0f;
        int diceForError = randomInt.nextInt(100);
        boolean errorFlag = false;

        if (defPosition != GlobalValue.pitcherInDefense){
            ArrayList<Batter> batterList = new ArrayList<Batter>();
            if (isHomeTeamAttackInning){
                batterList = awayTeam.getBatterSquad();
            }else {
                batterList = homeTeam.getBatterSquad();
            }
            for (int i=0 ; i < batterList.size() ; i++){
                if (batterList.get(i).getDefensePosition() == defPosition && batterList.get(i).getDefensePosition() != GlobalValue.DH){
                    defenseBatter = batterList.get(i);
                    if (batterList.get(i).getTrueDefenseInhundred() >= 0.5){
                        errorRate = (GlobalValue.positionAvgFieldingRate[defPosition] + (100.0f - GlobalValue.positionAvgFieldingRate[defPosition])*(defenseBatter.getTrueDefenseInhundred()) );  //0.1 ~ 5.0
                    }else {
                        errorRate = (GlobalValue.positionAvgFieldingRate[defPosition])-(GlobalValue.positionAvgFieldingRate[defPosition]-GlobalValue.positionMinFieldingRate[defPosition])*(1.0f - defenseBatter.getTrueDefenseInhundred());  //5.0~8.0
                    }
                }
            }

            if(defenseBatter.getBatterSkill1() != null &&defenseBatter.getBatterSkill1().getSkillName().equals("DEFENCE MASTER")){
                if (defenseBatter.getBatterSkill1().getSkillLevel() == 1){
                    diceForError -=2;
                }else if (defenseBatter.getBatterSkill1().getSkillLevel() == 2){
                    diceForError -=4;
                }else {
                    diceForError -=6;
                }
            }
            if(defenseBatter.getBatterSkill2() != null &&defenseBatter.getBatterSkill2().getSkillName().equals("DEFENCE MASTER")){
                if (defenseBatter.getBatterSkill2().getSkillLevel() == 1){
                    diceForError -=2;
                }else if (defenseBatter.getBatterSkill2().getSkillLevel() == 2){
                    diceForError -=4;
                }else {
                    diceForError -=6;
                }
            }
            if (diceForError >= errorRate){
                errorFlag = true;
                defenseBatter.matchRecord.error++;
            }else {
                defenseBatter.matchRecord.successDefense++;
            }
        }
        return errorFlag;
    }
    private boolean checkDoublePlay(){
        boolean isDoubleOut = false;
        int diceForPosition = randomInt.nextInt(10)+1;

        if (defenseBatter.getDefensePosition() == GlobalValue.firstBase || defenseBatter.getDefensePosition() == GlobalValue.secondBase || defenseBatter.getDefensePosition() == GlobalValue.thirdBase || defenseBatter.getDefensePosition() == GlobalValue.shortStop){
            if (diceForPosition <3 && outCount < 2){
                isDoubleOut = true;
            }
        }
        return isDoubleOut;
    }

    private boolean checkSacrficeFly(){
        boolean isFlyOut = false;
        int diceForPosition = randomInt.nextInt(10)+1;

        if (defenseBatter.getDefensePosition() == GlobalValue.leftField || defenseBatter.getDefensePosition() == GlobalValue.centerField || defenseBatter.getDefensePosition() == GlobalValue.rightField){
            if (diceForPosition < 11 && outCount < 2){
                isFlyOut = true;
            }
        }
        return isFlyOut;
    }



    private void initMatchRecord(){

        for (int i = 0 ; i < homeBatterRoster.size() ; i++){
            homeBatterRoster.get(i).matchRecord.resetRecord();
        }
        for (int i = 0 ; i < awayBatterRoaster.size() ; i++){
            awayBatterRoaster.get(i).matchRecord.resetRecord();
        }
        for (int i = 0 ; i < homePitcherRoaster.size() ; i++){
            homePitcherRoaster.get(i).matchRecord.resetRecord();
            homePitcherRoaster.get(i).pitchCount = 0;
        }
        for (int i = 0 ; i < awayPitcherRoaster.size() ; i++){
            awayPitcherRoaster.get(i).matchRecord.resetRecord();
            awayPitcherRoaster.get(i).pitchCount = 0;
        }

    }


    private Batter getBatterWithPosition(int defPosition){
        Batter returnCatcher=null;
        ArrayList<Batter> batterList = new ArrayList<Batter>();

        if (isHomeTeamAttackInning){
            batterList = awayTeam.getBatterSquad();
        }else {
            batterList = homeTeam.getBatterSquad();
        }

        for (int i=0 ; i < batterList.size() ; i++){
            if (batterList.get(i).getDefensePosition() == defPosition && batterList.get(i).getDefensePosition() != GlobalValue.DH){
                returnCatcher = batterList.get(i);
            }
        }
        return returnCatcher;
    }


    private Batter getBatterFromOrder(boolean isHomeTeamAttack){
        Batter returnBatter;
        if (homeBatterOrder == 10){
            homeBatterOrder = 1;
        }
        if (awayBatterOrder == 10){
            awayBatterOrder = 1;
        }
        if (isHomeTeamAttack){
            returnBatter = (Batter) homeBatterRoster.get(homeBatterOrder-1);
            homeBatterOrder++;
        }else {
            returnBatter = (Batter) awayBatterRoaster.get(awayBatterOrder-1);
            awayBatterOrder++;
        }

        return returnBatter;
    }


    private Batter getBatterFromList(Batter currentBatter){
        String batterName = currentBatter.getCardToken();
        ArrayList<Batter> batterList = new ArrayList<Batter>();

        if (isHomeTeamAttackInning){
            batterList = homeTeam.getBatterSquad();
        }else {
            batterList = awayTeam.getBatterSquad();
        }

        for (int i=0 ; i < batterList.size() ; i++){
            if (batterList.get(i).getCardToken().equals(batterName)){
                currentBatter = batterList.get(i);
            }
        }
        return currentBatter;
    }

    private void setPitcherStatus(){
        String pitcherName = pitcher.getCardToken();
        if (isHomeTeamAttackInning){
            for (int i=0 ; i < awayTeam.getPitcherSquad().size() ; i++){
                if (awayTeam.getPitcherSquad().get(i).getCardToken().equals(pitcherName)){
                    awayTeam.getPitcherSquad().set(i,pitcher);
                }
            }
        }else {
            for (int i=0 ; i < homeTeam.getPitcherSquad().size() ; i++){
                if (homeTeam.getPitcherSquad().get(i).getCardToken().equals(pitcherName)){
                    homeTeam.getPitcherSquad().set(i,pitcher);
                }
            }
        }
    }

    private Batter setBatterStatus(Batter currentBatter){
        String batterName = currentBatter.getCardToken();

        ArrayList<Batter> batterList = new ArrayList<Batter>();
        if (isHomeTeamAttackInning){
            batterList = homeTeam.getBatterSquad();
        }else {
            batterList = awayTeam.getBatterSquad();
        }

        for (int i=0 ; i < batterList.size() ; i++){
            if (batterList.get(i).getCardToken().equals(batterName)){
                batterList.set(i,currentBatter);
            }
        }
        return currentBatter;
    }




    private void addRBI(Batter currentBatter, int rbiNumber){
        Batter targetBatter;
        targetBatter = getBatterFromList(currentBatter);
        targetBatter.matchRecord.rbi += rbiNumber;
        setBatterStatus(targetBatter);
    }

    private void addRun(Batter currentBatter, int runNumber){
        Batter targetBatter;
        targetBatter = getBatterFromList(currentBatter);
        targetBatter.matchRecord.run += runNumber;
        setBatterStatus(targetBatter);
    }
    private void addError(Batter currentBatter){
        Batter targetBatter;
        targetBatter = getBatterFromList(currentBatter);
//        targetBatter.matchRecord.error++;
        setBatterStatus(targetBatter);
    }
    private void addSuccessDefense(Batter currentBatter){
        Batter targetBatter;
        targetBatter = getBatterFromList(currentBatter);
        targetBatter.matchRecord.successDefense++;
        setBatterStatus(targetBatter);
    }


    private void countDefenseSuccess(){
        int sumForFirst = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate);
        int sumForSecond = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate);
        int sumForThird = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate);
        int sumForShortstop = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate);
        int sumForLF = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate+GlobalValue.leftFieldFieldingRate);
        int sumForCF = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate+GlobalValue.leftFieldFieldingRate+GlobalValue.centerFieldFieldingRate);
        int sumForRF = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate+GlobalValue.leftFieldFieldingRate+GlobalValue.centerFieldFieldingRate+GlobalValue.rightFieldFieldingRate);


//        Batter defensePlayer;
        int diceForPosition = randomInt.nextInt(100)+1;

        if (diceForPosition <= GlobalValue.catcherFieldingRate){
            defenseBatter=getBatterWithPosition(GlobalValue.catcher);
            addSuccessDefense(defenseBatter);
        }else if (GlobalValue.catcherFieldingRate < diceForPosition && diceForPosition <= sumForFirst){
            defenseBatter=getBatterWithPosition(GlobalValue.firstBase);
            addSuccessDefense(defenseBatter);
        }else if (sumForFirst < diceForPosition && diceForPosition <= sumForSecond){
            defenseBatter=getBatterWithPosition(GlobalValue.secondBase);
            addSuccessDefense(defenseBatter);
        }else if (sumForSecond < diceForPosition && diceForPosition <= sumForThird){
            defenseBatter=getBatterWithPosition(GlobalValue.thirdBase);
            addSuccessDefense(defenseBatter);
        }else if (sumForThird < diceForPosition && diceForPosition <= sumForShortstop){
            defenseBatter=getBatterWithPosition(GlobalValue.shortStop);
            addSuccessDefense(defenseBatter);
        }else if (sumForShortstop < diceForPosition && diceForPosition <= sumForLF){
            defenseBatter=getBatterWithPosition(GlobalValue.leftField);
            addSuccessDefense(defenseBatter);
        }else if (sumForLF < diceForPosition && diceForPosition <= sumForCF){
            defenseBatter=getBatterWithPosition(GlobalValue.centerField);
            addSuccessDefense(defenseBatter);
        }else if (sumForCF < diceForPosition && diceForPosition <= sumForRF){
            defenseBatter=getBatterWithPosition(GlobalValue.rightField);
            addSuccessDefense(defenseBatter);
        }
    }

    private boolean checkFieldingError(){
        boolean isFeildingError = false;
        int sumForFirst = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate);
        int sumForSecond = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate);
        int sumForThird = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate);
        int sumForShortstop = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate);
        int sumForLF = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate+GlobalValue.leftFieldFieldingRate);
        int sumForCF = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate+GlobalValue.leftFieldFieldingRate+GlobalValue.centerFieldFieldingRate);
        int sumForRF = (GlobalValue.catcherFieldingRate+GlobalValue.firstBaseFieldingRate+GlobalValue.secondBaseFieldingRate+GlobalValue.thirdBaseFieldingRate+GlobalValue.shortStopFieldingRate+GlobalValue.leftFieldFieldingRate+GlobalValue.centerFieldFieldingRate+GlobalValue.rightFieldFieldingRate);


        int diceForPosition = randomInt.nextInt(100)+1;

        if (diceForPosition <= GlobalValue.catcherFieldingRate){
            if (isPlayerError(GlobalValue.catcher)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }else if (GlobalValue.catcherFieldingRate < diceForPosition && diceForPosition <= sumForFirst){
            if (isPlayerError(GlobalValue.firstBase)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }else if (sumForFirst < diceForPosition && diceForPosition <= sumForSecond){
            if (isPlayerError(GlobalValue.secondBase)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }else if (sumForSecond < diceForPosition && diceForPosition <= sumForThird){
            if (isPlayerError(GlobalValue.thirdBase)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }else if (sumForThird < diceForPosition && diceForPosition <= sumForShortstop){
            if (isPlayerError(GlobalValue.shortStop)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }else if (sumForShortstop < diceForPosition && diceForPosition <= sumForLF){
            if (isPlayerError(GlobalValue.leftField)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }else if (sumForLF < diceForPosition && diceForPosition <= sumForCF){
            if (isPlayerError(GlobalValue.centerField)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }else if (sumForCF < diceForPosition && diceForPosition <= sumForRF){
            if (isPlayerError(GlobalValue.rightField)){
                addError(defenseBatter);
                isFeildingError = true;
            }else {
                addSuccessDefense(defenseBatter);
            }
        }
        return isFeildingError;
    }


    private void increasePitcherRealER(int runScore,Batter batter){
        if (isTwoOutError){
            getPitcherWithName(batter.matchRecord.dutyPitcher).matchRecord.r +=runScore;
        }else {
            getPitcherWithName(batter.matchRecord.dutyPitcher).matchRecord.er +=runScore;
            getPitcherWithName(batter.matchRecord.dutyPitcher).matchRecord.r +=runScore;
        }
    }

    private void setInningStatus(int resultOfBatting){
        if (resultOfBatting == 1){
            resultOfPAStr = "SINGLE HIT!";
            int runScore = 0;
            countDefenseSuccess();
            if (!isFirstBon && !isSecondBon && !isThirdBon){
                //no runner
            }else if (isFirstBon && !isSecondBon && !isThirdBon){
                //runner on 1b
                int extraBaseChanceInt = randomInt.nextInt(9);

                if (outCount==2){
                    if(extraBaseChanceInt < 9){
                        isThirdBon = true;
                        thirdBasePlayer = firstBasePlayer;
                    }else {
                        isSecondBon = true;
                        secondBasePlayer = firstBasePlayer;
                    }
                }
                if (outCount < 2){
                    if(extraBaseChanceInt < 6){
                        isThirdBon = true;
                        thirdBasePlayer = firstBasePlayer;
                    }else {
                        isSecondBon = true;
                        secondBasePlayer = firstBasePlayer;
                    }
                }
            }else if (!isFirstBon && isSecondBon && !isThirdBon){
                //runner on 2b
                int extraBaseChanceInt = randomInt.nextInt(9);

                if (outCount==2){
                    if(extraBaseChanceInt < 9){
                        runScore = 1;
                        addRBI(batter,runScore);
                        addRun(secondBasePlayer,runScore);
                        increasePitcherRealER(runScore,secondBasePlayer);

                    }else {
                        isThirdBon = true;
                        thirdBasePlayer = secondBasePlayer;
                    }
                }
                if (outCount < 2){
                    if(extraBaseChanceInt < 7){
                        runScore = 1;
                        addRBI(batter,runScore);
                        addRun(secondBasePlayer,runScore);
                        increasePitcherRealER(runScore,secondBasePlayer);
                    }else {
                        isThirdBon = true;
                        thirdBasePlayer = secondBasePlayer;
                    }
                }
                isSecondBon = false;
                secondBasePlayer = null;

            }else if (!isFirstBon && !isSecondBon && isThirdBon){
                //runner on 3b
                runScore = 1;
                addRBI(batter,runScore);
                addRun(thirdBasePlayer,runScore);
                increasePitcherRealER(runScore,thirdBasePlayer);
                isThirdBon = false;
                thirdBasePlayer = null;
            }else if (isFirstBon && isSecondBon && !isThirdBon){
                //runner on 1b 2b
                runScore = 1;
                addRBI(batter,runScore);
                addRun(secondBasePlayer,runScore);
                increasePitcherRealER(runScore,secondBasePlayer);
                isSecondBon = false;
                secondBasePlayer = null;
                isThirdBon = true;
                thirdBasePlayer = firstBasePlayer;
                firstBasePlayer = null;
            }else if (!isFirstBon && isSecondBon && isThirdBon){
                //runner on 2b 3b
                runScore = 2;
                addRBI(batter,runScore);
                addRun(secondBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
                isSecondBon = false;
                secondBasePlayer = null;
                isThirdBon = false;
                thirdBasePlayer = null;
            }else if (isFirstBon && !isSecondBon && isThirdBon){
                //runner on 1b 3b
                runScore = 1;
                addRBI(batter,runScore);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(runScore,thirdBasePlayer);
                isThirdBon = false;
                thirdBasePlayer = null;
                isThirdBon = true;
                thirdBasePlayer = firstBasePlayer;

            }else if (isFirstBon && isSecondBon && isThirdBon){
                //runner on 1b 2b 3b
                runScore = 2;
                addRBI(batter,runScore);
                addRun(secondBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);

                isSecondBon = false;
                secondBasePlayer = null;
                isThirdBon = false;
                thirdBasePlayer = null;

                isSecondBon = false;
                isThirdBon = true;
                thirdBasePlayer = firstBasePlayer;
            }

            batter = getBatterFromList(batter);
            batter.matchRecord.singleHit++;
            addBatterHit();
            batter.matchRecord.pa++;
            addBatterAb();
            setBatterStatus(batter);
            addPitcherHit();
            getPitcherWithName(pitcher.getCardToken()).matchRecord.single++;


            isFirstBon =true;
            firstBasePlayer = batter;

            if (attackingTeam.equals(homeTeam)){
                homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+runScore);
                homeTeamScore += runScore;
                homeTeamHit++;
            }else {
                awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+runScore);
                awayTeamScore += runScore;
                awayTeamHit++;
            }
        }

        else if (resultOfBatting == 2){
            resultOfPAStr = "DOUBLE HIT!";
            int runScore = 0;
            countDefenseSuccess();
            if (!isFirstBon && !isSecondBon && !isThirdBon){
                //no runner

            }else if (isFirstBon && !isSecondBon && !isThirdBon){
                //runner on 1b
                isThirdBon = true;
                thirdBasePlayer = firstBasePlayer;
                firstBasePlayer = null;
            }else if (!isFirstBon && isSecondBon && !isThirdBon){
                //runner on 2b
                runScore = 1;
                addRun(secondBasePlayer,1);
                increasePitcherRealER(1,secondBasePlayer);
                addRBI(batter,1);
            }else if (!isFirstBon && !isSecondBon && isThirdBon){
                //runner on 3b
                runScore = 1;
                addRBI(batter,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,thirdBasePlayer);
                isThirdBon = false;
                thirdBasePlayer = null;
            }else if (isFirstBon && isSecondBon && !isThirdBon){
                //runner on 1b 2b
                runScore = 1;
                addRun(secondBasePlayer,1);
                addRBI(batter,1);
                increasePitcherRealER(runScore,secondBasePlayer);
                isThirdBon = true;
                thirdBasePlayer = firstBasePlayer;
            }else if (!isFirstBon && isSecondBon && isThirdBon){
                //runner on 2b 3b
                runScore = 2;
                addRBI(batter,2);
                addRun(secondBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
                isThirdBon = false;
                thirdBasePlayer = null;
            }else if (isFirstBon && !isSecondBon && isThirdBon){
                //runner on 1b 3b
                runScore = 1;
                addRBI(batter,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,thirdBasePlayer);
                isThirdBon = true;
                thirdBasePlayer = firstBasePlayer;
            }else if (isFirstBon && isSecondBon && isThirdBon){
                //runner on 1b 2b 3b
                runScore = 2;
                addRBI(batter,2);
                addRun(thirdBasePlayer,1);
                addRun(secondBasePlayer,1);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
                isThirdBon = true;
                thirdBasePlayer = firstBasePlayer;
            }
            isFirstBon =false;
            firstBasePlayer =null;
            isSecondBon = true;
            secondBasePlayer = batter;

            batter = getBatterFromList(batter);
            batter.matchRecord.doubleHit++;
            addBatterHit();
            batter.matchRecord.pa++;
            addBatterAb();
            setBatterStatus(batter);

            addPitcherHit();
            getPitcherWithName(pitcher.getCardToken()).matchRecord.doublehit++;

            if (attackingTeam.equals(homeTeam)){
                homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+runScore);
                homeTeamScore += runScore;
                homeTeamHit++;
            }else {
                awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+runScore);
                awayTeamScore += runScore;
                awayTeamHit++;
            }
        }else if (resultOfBatting == 3){
            resultOfPAStr = "TRIPPLE HIT!";
            int runScore = 0;
            countDefenseSuccess();
            if (!isFirstBon && !isSecondBon && !isThirdBon){
                //no runner

            }else if (isFirstBon && !isSecondBon && !isThirdBon){
                //runner on 1b
                runScore = 1;
                addRBI(batter,1);
                addRun(firstBasePlayer,1);
                increasePitcherRealER(1,firstBasePlayer);

            }else if (!isFirstBon && isSecondBon && !isThirdBon){
                //runner on 2b
                runScore = 1;
                addRBI(batter,1);
                addRun(secondBasePlayer,1);
                increasePitcherRealER(1,secondBasePlayer);
            }else if (!isFirstBon && !isSecondBon && isThirdBon){
                //runner on 3b
                runScore = 1;
                addRBI(batter,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,thirdBasePlayer);
            }else if (isFirstBon && isSecondBon && !isThirdBon){
                //runner on 1b 2b
                runScore = 2;
                addRBI(batter,2);
                addRun(firstBasePlayer,1);
                addRun(secondBasePlayer,1);
                increasePitcherRealER(1,firstBasePlayer);
                increasePitcherRealER(1,secondBasePlayer);

            }else if (!isFirstBon && isSecondBon && isThirdBon){
                //runner on 2b 3b
                runScore = 2;
                addRBI(batter,2);
                addRun(secondBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);

            }else if (isFirstBon && !isSecondBon && isThirdBon){
                //runner on 1b 3b
                runScore = 2;
                addRBI(batter,2);
                addRun(firstBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,firstBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
            }else if (isFirstBon && isSecondBon && isThirdBon){
                //runner on 1b 2b 3b
                runScore = 3;
                addRBI(batter,3);
                addRun(firstBasePlayer,1);
                addRun(secondBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,firstBasePlayer);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
            }

            isFirstBon = false;
            firstBasePlayer = null;
            isSecondBon = false;
            secondBasePlayer = null;
            isThirdBon = true;
            thirdBasePlayer = batter;

            batter = getBatterFromList(batter);
            batter.matchRecord.tripleHit++;
            addBatterHit();
            batter.matchRecord.pa++;
            addBatterAb();
            setBatterStatus(batter);

            addPitcherHit();
            getPitcherWithName(pitcher.getCardToken()).matchRecord.tripple++;

            if (attackingTeam.equals(homeTeam)){
                homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+runScore);
                homeTeamScore += runScore;
                homeTeamHit++;
            }else {
                awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+runScore);
                awayTeamScore += runScore;
                awayTeamHit++;
            }
        }else if (resultOfBatting == 4){
            resultOfPAStr = "HOMER!";
            int runScore = 0;
            if (!isFirstBon && !isSecondBon && !isThirdBon){
                //no runner
                runScore = 1;
                addRBI(batter,1);
                addRun(batter,1);
                increasePitcherRealER(1,batter);

            }else if (isFirstBon && !isSecondBon && !isThirdBon){
                //runner on 1b
                runScore = 2;
                addRBI(batter,2);
                addRun(batter,1);
                addRun(firstBasePlayer,1);
                increasePitcherRealER(1,batter);
                increasePitcherRealER(1,firstBasePlayer);

            }else if (!isFirstBon && isSecondBon && !isThirdBon){
                //runner on 2b
                runScore = 2;
                addRBI(batter,2);
                addRun(batter,1);
                addRun(secondBasePlayer,1);
                increasePitcherRealER(1,batter);
                increasePitcherRealER(1,secondBasePlayer);
            }else if (!isFirstBon && !isSecondBon && isThirdBon){
                //runner on 3b
                runScore = 2;
                addRBI(batter,2);
                addRun(batter,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,batter);
                increasePitcherRealER(1,thirdBasePlayer);
            }else if (isFirstBon && isSecondBon && !isThirdBon){
                //runner on 1b 2b
                runScore = 3;
                addRBI(batter,3);
                addRun(batter,1);
                addRun(firstBasePlayer,1);
                addRun(secondBasePlayer,1);
                increasePitcherRealER(1,batter);
                increasePitcherRealER(1,firstBasePlayer);
                increasePitcherRealER(1,secondBasePlayer);
            }else if (!isFirstBon && isSecondBon && isThirdBon){
                //runner on 2b 3b
                runScore = 3;
                addRBI(batter,3);
                addRun(batter,1);
                addRun(secondBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,batter);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
            }else if (isFirstBon && !isSecondBon && isThirdBon){
                //runner on 1b 3b
                runScore = 3;
                addRBI(batter,3);
                addRun(batter,1);
                addRun(firstBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,batter);
                increasePitcherRealER(1,firstBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
            }else if (isFirstBon && isSecondBon && isThirdBon){
                //runner on 1b 2b 3b
                runScore = 4;
                addRBI(batter,4);
                addRun(batter,1);
                addRun(firstBasePlayer,1);
                addRun(secondBasePlayer,1);
                addRun(thirdBasePlayer,1);
                increasePitcherRealER(1,batter);
                increasePitcherRealER(1,firstBasePlayer);
                increasePitcherRealER(1,secondBasePlayer);
                increasePitcherRealER(1,thirdBasePlayer);
            }

            isFirstBon = false;
            firstBasePlayer = null;
            isSecondBon = false;
            secondBasePlayer = null;
            isThirdBon = false;
            thirdBasePlayer = null;

            batter = getBatterFromList(batter);
            batter.matchRecord.homer++;
            addBatterHit();
            batter.matchRecord.pa++;
            addBatterAb();
            setBatterStatus(batter);
            pitcher.matchRecord.hr+=1;
            addPitcherHit();

            if (attackingTeam.equals(homeTeam)){
                homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+runScore);
                homeTeamScore += runScore;
                homeTeamHR++;
                homeTeamHit++;
            }else {
                awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+runScore);
                awayTeamScore += runScore;
                awayTeamHR++;
                awayTeamHit++;
            }
        }else if (resultOfBatting == 5){
            resultOfPAStr = "STRIKE OUT!";
            batter = getBatterFromList(batter);
            batter.matchRecord.strikeOut++;
            batter.matchRecord.pa++;
            addBatterAb();
            setBatterStatus(batter);
            outCount++;
            addPitcherOutcount();
            pitcher.matchRecord.so++;
        }else if (resultOfBatting == 6){
            resultOfPAStr = "BASE ON BALL!";
            if (!isFirstBon && !isSecondBon && !isThirdBon){
                //no runner
            }else if (isFirstBon && !isSecondBon && !isThirdBon){
                //runner on 1b
                isSecondBon = true;
                secondBasePlayer = firstBasePlayer;
            }else if (!isFirstBon && isSecondBon && !isThirdBon){
                //runner on 2b
            }else if (!isFirstBon && !isSecondBon && isThirdBon){
                //runner on 3b
            }else if (isFirstBon && isSecondBon && !isThirdBon){
                //runner on 1b 2b
                isThirdBon = true;
                thirdBasePlayer = secondBasePlayer;
                isSecondBon = true;
                secondBasePlayer = firstBasePlayer;
            }else if (!isFirstBon && isSecondBon && isThirdBon){
                //runner on 2b 3b
            }else if (isFirstBon && !isSecondBon && isThirdBon){
                //runner on 1b 3b
                isSecondBon = true;
                secondBasePlayer = firstBasePlayer;
            }else if (isFirstBon && isSecondBon && isThirdBon){
                //runner on 1b 2b 3b
                addRBI(batter,1);
                addRun(thirdBasePlayer,1);

                thirdBasePlayer = secondBasePlayer;
                secondBasePlayer =firstBasePlayer;

                if (attackingTeam.equals(homeTeam)){
                    homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+1);
                    homeTeamScore++;
                }else {
                    awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+1);
                    awayTeamScore++;
                }
            }
            isFirstBon = true;
            firstBasePlayer = batter;

            batter = getBatterFromList(batter);
            batter.matchRecord.baseOnball++;
            batter.matchRecord.pa++;
            setBatterStatus(batter);
            pitcher.matchRecord.bb++;
            if (attackingTeam.equals(homeTeam)){
                homeTeamBB++;
            }else {
                awayTeamBB++;
            }
        }else if (resultOfBatting == 7){

            boolean isError = checkFieldingError();
            boolean isDoublePlay = checkDoublePlay();
            boolean isSarificeFly = checkSacrficeFly();
            boolean isDoneSF = false;
            if (isError){
                resultOfPAStr = "FIELD ERROR!";
            }else {
                resultOfPAStr = "FIELD OUT!";
            }

            if (isError){
                if (attackingTeam.equals(homeTeam)){
                    awayTeamError++;
                }else {
                    homeTeamError++;
                }
                if (outCount == 2){
                    isTwoOutError = true;
                }
            }

            if (!isFirstBon && !isSecondBon && !isThirdBon){
                //no runner
                if (isError){
                }else {
                    outCount++;
                    addPitcherOutcount();
                }
            }else if (isFirstBon && !isSecondBon && !isThirdBon){
                //runner on 1b
                if (isError){
                    isSecondBon = true;
                    secondBasePlayer = firstBasePlayer;
                }else {
                    if (isDoublePlay){
                        resultOfPAStr = "DOUBLE PLAY!";
                        firstBasePlayer = null;
                        isFirstBon = false;
                        outCount ++;
                        addPitcherOutcount();
                    }
                    outCount++;
                    addPitcherOutcount();
                }
            }else if (!isFirstBon && isSecondBon && !isThirdBon){
                //runner on 2b
                if (isError){
                }else {
                    outCount++;
                    addPitcherOutcount();
                }
            }else if (!isFirstBon && !isSecondBon && isThirdBon){
                //runner on 3b
                if (isError){
                }else {
                    if (isSarificeFly){
                        resultOfPAStr = "SACRIFICE FLY!";
                        addRBI(batter,1);
                        addRun(thirdBasePlayer,1);
                        increasePitcherRealER(1,thirdBasePlayer);
                        isThirdBon = false;
                        thirdBasePlayer = null;
                        isDoneSF = true;
                        if (attackingTeam.equals(homeTeam)){
                            homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+1);
                            homeTeamScore++;
                        }else {
                            awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+1);
                            awayTeamScore++;
                        }
                    }
                    outCount++;
                    addPitcherOutcount();
                }
            }else if (isFirstBon && isSecondBon && !isThirdBon){
                //runner on 1b 2b
                if (isError){
                    isThirdBon =true;
                    thirdBasePlayer = secondBasePlayer;
                    isSecondBon = true;
                    secondBasePlayer = firstBasePlayer;
                }else {
                    if (isDoublePlay){
                        resultOfPAStr = "DOUBLE PLAY!";
                        isThirdBon =true;
                        thirdBasePlayer = secondBasePlayer;
                        isSecondBon = false;
                        secondBasePlayer=null;
                        isFirstBon = false;
                        firstBasePlayer = null;
                        outCount ++;
                        addPitcherOutcount();
                    }
                    outCount++;
                    addPitcherOutcount();
                }
            }else if (!isFirstBon && isSecondBon && isThirdBon){
                //runner on 2b 3b
                if (isError){
                }else {
                    if (isSarificeFly){
                        resultOfPAStr = "SACRIFICE FLY!";
                        addRBI(batter,1);
                        addRun(thirdBasePlayer,1);
                        increasePitcherRealER(1,thirdBasePlayer);
                        isThirdBon = false;
                        thirdBasePlayer = null;
                        isDoneSF = true;
                        if (attackingTeam.equals(homeTeam)){
                            homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+1);
                            homeTeamScore++;
                        }else {
                            awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+1);
                            awayTeamScore++;
                        }
                    }
                    outCount++;
                    addPitcherOutcount();
                }
            }else if (isFirstBon && !isSecondBon && isThirdBon){
                //runner on 1b 3b
                if (isError){
                    isSecondBon =true;
                    secondBasePlayer = firstBasePlayer;
                }else {
                    if (isDoublePlay){
                        resultOfPAStr = "DOUBLE PLAY!";
                        if (outCount==0){
                            addRun(thirdBasePlayer,1);
                            increasePitcherRealER(1,thirdBasePlayer);
                            addRBI(batter,1);
                            isThirdBon =false;
                            thirdBasePlayer = null;
                            isSecondBon = false;
                            secondBasePlayer=null;
                            isFirstBon = false;
                            firstBasePlayer = null;
                            outCount++;
                            addPitcherOutcount();
                            if (attackingTeam.equals(homeTeam)){
                                homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+1);
                                homeTeamScore++;
                            }else {
                                awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+1);
                                awayTeamScore++;
                            }
                        }else {
                            outCount++;
                            addPitcherOutcount();
                        }
                        if (isSarificeFly){
                            resultOfPAStr = "SACRIFICE FLY!";
                            addRBI(batter,1);
                            addRun(thirdBasePlayer,1);
                            increasePitcherRealER(1,thirdBasePlayer);
                            isThirdBon = false;
                            thirdBasePlayer = null;
                            isDoneSF = true;
                            if (attackingTeam.equals(homeTeam)){
                                homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+1);
                                homeTeamScore++;
                            }else {
                                awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+1);
                                awayTeamScore++;
                            }
                        }

                    }
                    outCount++;
                    addPitcherOutcount();
                }

            }else if (isFirstBon && isSecondBon && isThirdBon){
                //runner on 1b 2b 3b
                if (isError){
                    addRBI(batter,1);
                    addRun(thirdBasePlayer,1);
                    getPitcherWithName(thirdBasePlayer.matchRecord.dutyPitcher).matchRecord.r++;
                    thirdBasePlayer = secondBasePlayer;
                    secondBasePlayer = firstBasePlayer;

                    if (attackingTeam.equals(homeTeam)){
                        homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+1);
                        homeTeamScore++;
                    }else {
                        awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+1);
                        awayTeamScore++;
                    }
                }else {
                    if (isDoublePlay){
                        resultOfPAStr = "DOUBLE PLAY!";
                        if (outCount==0){
                            addRun(thirdBasePlayer,1);
                            increasePitcherRealER(1,thirdBasePlayer);
                            addRBI(batter,1);
                            isThirdBon =true;
                            thirdBasePlayer = secondBasePlayer;
                            isSecondBon = false;
                            secondBasePlayer=null;
                            isFirstBon = false;
                            firstBasePlayer = null;
                            outCount++;
                            addPitcherOutcount();
                        }else {
                            outCount++;
                            addPitcherOutcount();
                        }
                    }
                    if (isSarificeFly){
                        resultOfPAStr = "SACRIFICE FLY!";
                        addRBI(batter,1);
                        addRun(thirdBasePlayer,1);
                        increasePitcherRealER(1,thirdBasePlayer);
                        if (attackingTeam.equals(homeTeam)){
                            homeTeamInningScore.set(inning-1, homeTeamInningScore.get(inning-1)+1);
                            homeTeamScore++;
                        }else {
                            awayTeamInningScore.set(inning-1, awayTeamInningScore.get(inning-1)+1);
                            awayTeamScore++;
                        }
                        isThirdBon = false;
                        thirdBasePlayer = null;
                        isDoneSF = true;
                    }
                    outCount++;
                    addPitcherOutcount();
                }
            }
            if (isError){
                isFirstBon =true;
                firstBasePlayer = batter;
            }
            if (isDoneSF){
                batter = getBatterFromList(batter);
                batter.matchRecord.pa++;
                setBatterStatus(batter);
            }else {
                batter = getBatterFromList(batter);
                batter.matchRecord.pa++;
                addBatterAb();
                setBatterStatus(batter);
            }
        }
        drawPAStatus();
        resultOfPAStr="";
        defenseBatter = null;
    }

    private void appendMatchDetails(){
        matchDetail = new MatchDetail(homeTeamInningScore,awayTeamInningScore, homeStartPitcher,awayStartPitcher,defenseBatter,inning,outCount,isHomeTeamAttackInning,homeTeamScore,homeTeamHit,homeTeamHR,homeTeamBB,homeTeamError,awayTeamScore,awayTeamHit,awayTeamHR,awayTeamBB,awayTeamError,isFirstBon,isSecondBon,isThirdBon,firstBasePlayer,secondBasePlayer,thirdBasePlayer,batter,pitcher,resultOfPAStr,homeTeam,awayTeam,isShowingBatterSkilll1,isShowingBatterSkilll2,isShowingPitcherSkill1,isShowingPitcherSkill2);
        matchDetails.add(matchDetail);

    }
    private void drawScoreBoard(){
//
//        String inningNumber="";
//        String homeTeamscoreBoard = "";
//        String awayTeamscoreBoard = "";
//        for (int i = 1; i <= homeTeamInningScore.size();i++){
//            inningNumber += i + "  ";
//        }
//        for (int i = 0; i < homeTeamInningScore.size();i++){
//            homeTeamscoreBoard += homeTeamInningScore.get(i) + "  ";
//        }
//        for (int i = 0; i < awayTeamInningScore.size();i++){
//            awayTeamscoreBoard += awayTeamInningScore.get(i) + "  ";
//        }
//
//        inningNumber += "\tR  H  E  B  HR";
//        homeTeamscoreBoard += "\t"+homeTeamScore + "  " + homeTeamHit+ "  " + homeTeamError+ "  " + homeTeamBB + "  " + homeTeamHR;
//        awayTeamscoreBoard += "\t"+awayTeamScore + "  " + awayTeamHit+ "  " + awayTeamError+ "  " + awayTeamBB + "  " + awayTeamHR;
//
//        Log.d(" Inning    :",inningNumber);
//        Log.d(" Home      :",homeTeamscoreBoard);
//        Log.d(" AWAY      :",awayTeamscoreBoard);
//        Log.d("inning status : "," 3B : " + isThirdBon+",  2B : " + isSecondBon + ",  1B : " + isFirstBon);
////        for (int i = 0 ; i < homeBatterRoster.size() ; i++){
////            Log.d(homeBatterRoster.get(i).getBatterName()+ " : ", homeBatterRoster.get(i).matchRecord.toString());
////        }
////        Log.d(" Match    :","======================================================================================================Z");
////        for (int i = 0 ; i < homeUsedPicherList.size() ; i++){
////            Log.d(homeUsedPicherList.get(i).getPitcherName()+ " : ", homeUsedPicherList.get(i).matchRecord.toString() + "pitch count : " + homeUsedPicherList.get(i).pitchCount);
////        }
////
////        Log.d(" Team    :","======================================================================================================Z");
//////        for (int i = 0 ; i < awayBatterRoaster.size() ; i++){
//////            Log.d(awayBatterRoaster.get(i).getBatterName()+ " : ", awayBatterRoaster.get(i).matchRecord.toString());
//////        }
////        for (int i = 0 ; i < awayUsedPicherList.size() ; i++){
////            Log.d(awayUsedPicherList.get(i).getPitcherName()+ " : ", awayUsedPicherList.get(i).matchRecord.toString()+ "pitch count : " + awayUsedPicherList.get(i).pitchCount);
////        }
//        Log.d(" Season    :","======================================================================================================Z");
//        for (int i = 0 ; i < homePitcherRoaster.size() ; i++){
//            Log.d(homePitcherRoaster.get(i).getPitcherName()+ " : ", homePitcherRoaster.get(i).seasonRecord.toString()+((homePitcherRoaster.get(i).getPitcherSkill() == null) ? "" : homePitcherRoaster.get(i).getPitcherSkill().getSkillName())  +" ");
//        }
//        for (int i = 0 ; i < homeBatterRoster.size() ; i++){
//            Log.d(homeBatterRoster.get(i).getBatterName()+ " : ", homeBatterRoster.get(i).seasonRecord.toString()+((homeBatterRoster.get(i).getBatterSkill() == null) ? "" : homeBatterRoster.get(i).getBatterSkill().getSkillName()));
//        }
//
//        Log.d(" Team    :","======================================================================================================Z");
////        for (int i = 0 ; i < awayBatterRoaster.size() ; i++){
////            Log.d(awayBatterRoaster.get(i).getBatterName()+ " : ", awayBatterRoaster.get(i).matchRecord.toString());
////        }
//        for (int i = 0 ; i < awayPitcherRoaster.size() ; i++){
//            Log.d(awayPitcherRoaster.get(i).getPitcherName()+ " : ",awayPitcherRoaster.get(i).seasonRecord.toString()+((awayPitcherRoaster.get(i).getPitcherSkill() == null) ? "" : awayPitcherRoaster.get(i).getPitcherSkill().getSkillName()) +" ");
//        }
//        for (int i = 0 ; i < awayBatterRoaster.size() ; i++){
//            Log.d(awayBatterRoaster.get(i).getBatterName()+ " : ", awayBatterRoaster.get(i).seasonRecord.toString()+((awayBatterRoaster.get(i).getBatterSkill() == null) ? "" : awayBatterRoaster.get(i).getBatterSkill().getSkillName()));
//        }
    }

    private void seeSeasonRecord(){
        Log.d(" Season    :","======================================================================================================Z");
        for (int i = 0 ; i < homePitcherRoaster.size() ; i++){
            Log.d(homePitcherRoaster.get(i).getPitcherName()+ " : ", homePitcherRoaster.get(i).seasonRecord.toString());
        }
        for (int i = 0 ; i < homeBatterRoster.size() ; i++){
            Log.d(homeBatterRoster.get(i).getBatterName()+ " : ", homeBatterRoster.get(i).seasonRecord.toString());
        }

        Log.d(" Away Team    :","======================================================================================================Z");

        for (int i = 0 ; i < awayPitcherRoaster.size() ; i++){
            Log.d(awayPitcherRoaster.get(i).getPitcherName()+ " : ",awayPitcherRoaster.get(i).seasonRecord.toString());
        }
        for (int i = 0 ; i < awayBatterRoaster.size() ; i++){
            Log.d(awayBatterRoaster.get(i).getBatterName()+ " : ", awayBatterRoaster.get(i).seasonRecord.toString());
        }
    }
    private void drawPAStatus(){
        appendMatchDetails();

//        Log.d("Result : " , resultOfPAStr);
//        String inningNumber="";
//        String homeTeamscoreBoard = "";
//        String awayTeamscoreBoard = "";
//        for (int i = 1; i <= homeTeamInningScore.size();i++){
//            inningNumber += i + "  ";
//        }
//        for (int i = 0; i < homeTeamInningScore.size();i++){
//            homeTeamscoreBoard += homeTeamInningScore.get(i) + "  ";
//        }
//        for (int i = 0; i < awayTeamInningScore.size();i++){
//            awayTeamscoreBoard += awayTeamInningScore.get(i) + "  ";
//        }
//
//        inningNumber += "\tR  H  E  B  HR";
//        homeTeamscoreBoard += "\t"+homeTeamScore + "  " + homeTeamHit+ "  " + homeTeamError+ "  " + homeTeamBB + "  " + homeTeamHR;
//        awayTeamscoreBoard += "\t"+awayTeamScore + "  " + awayTeamHit+ "  " + awayTeamError+ "  " + awayTeamBB + "  " + awayTeamHR;
//
//        Log.d(" Inning    :",inningNumber);
//        Log.d(" Home      :",homeTeamscoreBoard);
//        Log.d(" AWAY      :",awayTeamscoreBoard);
//        Log.d("inning status : "," 3B : " + ((thirdBasePlayer == null) ? isThirdBon +"" : thirdBasePlayer.getBatterName())+",  2B : " + ((secondBasePlayer == null) ? isSecondBon +"" : secondBasePlayer.getBatterName()) + ",  1B : " + ((firstBasePlayer == null) ? isFirstBon +"" : firstBasePlayer.getBatterName()));

    }
    private int getRandom100Integer(){
        return randomInt.nextInt(100)+1;
    }

    public class BattingLogic {
        private Batter batter;
        private Pitcher pitcher;
        private Random randomPA;
        float gapRate,hrRate,thirdBRate,secondBRate;
        float soRate,bbRate,hitRate;
        float thirdBaseRate;
        private int randomInt;
        private boolean isSkill1=true;
        private final int firstBaseHit=1,secondBaseHit=2,thirdBaseHit=3,homer=4,strikeOut=5,baseOnBall=6,fieldOut=7;

        public BattingLogic(Batter batter, Pitcher pitcher,int randomInt) {
            this.batter = batter;
            this.pitcher = pitcher;
            this.randomInt = randomInt;
            randomPA = new Random();
            gapRate = 0f;
            hrRate = 0f;
            thirdBRate = 0f;
            secondBRate = 0f;
            thirdBaseRate = 0f;
            soRate = 0f;
            bbRate = 0f;
            hitRate = 0f;
        }

        public BattingLogic(){
            this.batter = new Batter();
            this.pitcher = new Pitcher();
            randomPA = new Random();
        }

        public int getResultOfBatting(){
            int result= 7;

            float soWeight,bbWeight,hitWeight;
            soWeight = 2.5f;
            bbWeight = 1.5f;
            hitWeight = 1.5f;

            if ((pitcher.getStrikeOutValue()-batter.getStrikeOutValue()) > 0){
                soRate = (GlobalValue.avgSORate +(GlobalValue.maxSORate-GlobalValue.avgSORate)*((pitcher.getStrikeOutValue()-batter.getStrikeOutValue())*(soWeight)/100f));
            }else {
                soRate = (GlobalValue.avgSORate -(GlobalValue.avgSORate -GlobalValue.minSORate)*(soWeight)*((batter.getStrikeOutValue()-pitcher.getStrikeOutValue())*(soWeight)/ (100f)) );
            }

            if ((pitcher.getBBValue()-batter.getBBValue()) > 0){
                bbRate = (GlobalValue.avgBBRate -(GlobalValue.avgBBRate -GlobalValue.minBBRate)*((pitcher.getBBValue()-batter.getBBValue())*bbWeight/ (100f)) );
            }else {
                bbRate = (GlobalValue.avgBBRate +(GlobalValue.maxBBRate-GlobalValue.avgBBRate)*((batter.getBBValue()-pitcher.getBBValue())*bbWeight/100f));
            }

            if ((pitcher.getHITValue()-batter.getHITValue()) > 0){
                hitRate = (GlobalValue.avgHITRate -(GlobalValue.avgHITRate -GlobalValue.minHITRate)*((pitcher.getHITValue()-batter.getHITValue())*(hitWeight)/(100f)));
            }else {
                hitRate = (GlobalValue.avgHITRate +(GlobalValue.maxHITRate-GlobalValue.avgHITRate)*(hitWeight)*((batter.getHITValue()-pitcher.getHITValue())*(hitWeight)/100f));
            }
            //associate skill============================================================================
            isShowingBatterSkilll1 = false;
            isShowingBatterSkilll2 = false;
            isShowingPitcherSkill1 = false;
            isShowingPitcherSkill2 = false;
            isSkill1 = true;
            associatePitcherSkill(pitcher.getPitcherSkill1());
            isSkill1 = false;
            associatePitcherSkill(pitcher.getPitcherSkill2());
            isSkill1 = true;
            associateBatterSkill(batter.getBatterSkill1());
            isSkill1 = false;
            associateBatterSkill(batter.getBatterSkill2());

            if (isRbiSituation){
                int batterRbiBonus = batter.getClutchWithItem() - pitcher.getMentalWithItem();
                hitRate += (batterRbiBonus*GlobalValue.clutchGravity);
            }


            int resultOfHit = randomPA.nextInt(100);  // 1 ~ 100

            if (resultOfHit <= hitRate){
                String typeOfHit = getTypeOfHit(batter,pitcher);

                if (typeOfHit.equals("Homerun")){
                    result = homer;
                }else if (typeOfHit.equals("1B")){
                    result = firstBaseHit;
                }else if (typeOfHit.equals("2B")){
                    result = secondBaseHit;
                }else if (typeOfHit.equals("3B")){
                    result = thirdBaseHit;
                }else {
                    Log.e("----","something wrong!!!!!!!!!!!!!");
                }
            }else if (hitRate < resultOfHit && resultOfHit <= (soRate+hitRate)){
                result = strikeOut;
            }else if ((soRate+hitRate) < resultOfHit && resultOfHit <= (soRate +bbRate + hitRate)){
                result = baseOnBall;
            }else{
                result = fieldOut;
            }
            return result;
        }

        private String getTypeOfHit(Batter batter, Pitcher pitcher){
            String result="";

            float hrWeight,gapWeight;
            hrWeight = 2.5f;
            gapWeight = 2.0f;

            if ((pitcher.getHRValue()-batter.getHRValue()) > 0){
                hrRate = (GlobalValue.avgHRRate -(GlobalValue.avgHRRate -GlobalValue.minHRRate)*(pitcher.getHRValue()-batter.getHRValue())* hrWeight/ (100f ) );
            }else {
                hrRate = (GlobalValue.avgHRRate +(GlobalValue.maxHRRate-GlobalValue.avgHRRate)*(batter.getHRValue()-pitcher.getHRValue())* hrWeight / 100f);
            }

            if ((pitcher.getGapValue()-batter.getGapValue()) > 0){
                gapRate = (GlobalValue.avgGapRate -(GlobalValue.avgGapRate -GlobalValue.minGapRate)*(pitcher.getGapValue()-batter.getGapValue())* gapWeight/(100f ));
            }else {
                gapRate = (GlobalValue.avgGapRate + (GlobalValue.maxGapRate - GlobalValue.avgGapRate)*(batter.getGapValue() - pitcher.getGapValue())* gapWeight / 100f);
            }

            thirdBRate = getThirdBRate(batter);
            //associate gapRate & hrRate skill======================================
            associatePitcherGapSkill(pitcher.getPitcherSkill1());
            associatePitcherGapSkill(pitcher.getPitcherSkill2());
            associateBatterGapSkill(batter.getBatterSkill1());
            associateBatterGapSkill(batter.getBatterSkill2());
            secondBRate = gapRate - thirdBRate;


            int resultOfHit = randomInt;
            float resultOfHitFloat = (float) resultOfHit;

            if (0<= resultOfHitFloat && resultOfHitFloat < hrRate){
                result = "Homerun";
            }else if (hrRate <= resultOfHitFloat && resultOfHitFloat < (hrRate+secondBRate)){
                result = "2B";
            }else if ((hrRate+secondBRate) <= resultOfHitFloat && resultOfHitFloat < (hrRate+secondBRate + thirdBRate)){
                result = "3B";
            }else{
                result = "1B";
            }
            return result;
        }

        private float getThirdBRate(Batter batter){
            thirdBaseRate = 0f;
            float thirdWeight;
            thirdWeight = 1.5f;
            if ((batter.getTrueSpeed()-batter.getTrueLeagueEveSpeed()) >= 0){
                thirdBaseRate= (GlobalValue.avgThirdBaseRate +(GlobalValue.maxThirdBaseRate-GlobalValue.avgThirdBaseRate)*(batter.getTrueSpeed()-batter.getTrueLeagueEveSpeed())*thirdWeight/100f);
            }else {
                thirdBaseRate =  (GlobalValue.avgThirdBaseRate -(GlobalValue.avgThirdBaseRate -GlobalValue.minThirdBaseRate)*(batter.getTrueLeagueEveSpeed()-batter.getTrueSpeed())/ (100f *thirdWeight) );
            }
            return thirdBaseRate;
        }

        private void associatePitcherSkill(PitcherSkill skill){
            if (skill == null){
                return;
            }

            int skillType =  skill.getSkillType();
            int rateAmount3 = skill.getSkillLevel()+5;
            int rateAmount2 = skill.getSkillLevel()+2;
            int rateAmount1 = skill.getSkillLevel()+1;
            switch (skillType){
                case GlobalValue.P_GOLIATH: //when pitcher is stronger than batter
                    if (pitcher.getAverageStat() >= batter.getAverageStat()){
                        printPitcherPA();
                        hitRate -= rateAmount2;
                        soRate += rateAmount2;
                        bbRate -= rateAmount1;
                        printPitcherPA();
                    }
                    break;
                case GlobalValue.P_DAVID: //when pitcher is weaker than batter
                    if (pitcher.getAverageStat() < batter.getAverageStat()){
                        printPitcherPA();
                        hitRate -= rateAmount2;
                        soRate += rateAmount2;
                        bbRate -= rateAmount1;
                        printPitcherPA();
                    }
                    break;
                case GlobalValue.FINAL_BOSS:
                    if (inning == 9 ){
                        printPitcherPA();
                        hitRate -= rateAmount2;
                        soRate += rateAmount2;
                        bbRate -= rateAmount1;
                        printPitcherPA();
                    }
                    break;
                case GlobalValue.PERFECTIONIST:
                    if (inning < 7 ){
                        printPitcherPA();
                        hitRate -= rateAmount2;
                        soRate += rateAmount2;
                        bbRate -= rateAmount1;
                        printPitcherPA();
                    }
                    break;
                case GlobalValue.HOLDING_MAN:
                    if (inning == 8 ){
                        printPitcherPA();
                        hitRate -= rateAmount2;
                        soRate += rateAmount2;
                        bbRate -= rateAmount1;
                        printPitcherPA();
                    }
                    break;
                case GlobalValue.PINPOINT_MASTER:
                    printPitcherPA();
                    hitRate -= rateAmount1;
                    soRate += rateAmount1;
                    bbRate -= rateAmount2;
                    printPitcherPA();
                    break;
                case GlobalValue.WARRIOR:
                    if (isFirstBon || isSecondBon || isThirdBon ){
                        printPitcherPA();
                        hitRate -= rateAmount2;
                        soRate += rateAmount2;
                        bbRate -= rateAmount1;
                        printPitcherPA();
                    }
                    break;
                case GlobalValue.BBMASTER:
                    printPitcherPA();
                    hitRate -= rateAmount3;
                    soRate += rateAmount1;
                    printPitcherPA();
                    break;
                case GlobalValue.FIRE_BALLER:
                    printPitcherPA();
                    soRate += rateAmount3+5;
                    bbRate += rateAmount1;
                    printPitcherPA();
                    break;
                case GlobalValue.HEAVY_STUFF:
                    printPitcherPA();
                    break;
            }
        }

        private void associatePitcherGapSkill(PitcherSkill skill){
            if (skill ==null){
                return;
            }

            int type =  skill.getSkillType();
            int rateAmount3 = skill.getSkillLevel()+5;
            int rateAmount2 = skill.getSkillLevel()+2;
            int rateAmount1 = skill.getSkillLevel()+1;

            switch (type){
                case GlobalValue.HEAVY_STUFF:
                    printPitcherGap();
                    hrRate -= rateAmount2;
                    gapRate -= rateAmount2;
                    printPitcherGap();
                    break;

            }

        }
        // CLEANUP_MEMBER,GUESS_HITTER,CUTTING_MACHINE,THIEF,PULL_HITTER,SPRAY_HITTER,PISTOL,DEFENCE_MASTER
        private void associateBatterSkill(BatterSkill skill){
            if (skill ==null){
                return;
            }
            int type =  skill.getSkillType();
            int rateAmount3 = skill.getSkillLevel()+5;
            int rateAmount2 = skill.getSkillLevel()+2;
            int rateAmount1 = skill.getSkillLevel()+1;
            switch (type){
                case GlobalValue.B_GOLIATH:
                    if (pitcher.getAverageStat() <= batter.getAverageStat()){
                        printBatterPA();
                        hitRate += rateAmount2;
                        soRate -= rateAmount2;
                        bbRate += rateAmount2;
                        printBatterPA();
                    }
                    break;
                case GlobalValue.B_DAVID: //when pitcher is weaker than batter
                    if (pitcher.getAverageStat() > batter.getAverageStat()){
                        printBatterPA();
                        hitRate += rateAmount2;
                        soRate -= rateAmount2;
                        bbRate += rateAmount2;
                        printBatterPA();
                    }
                    break;
                case GlobalValue.RBI_GUY:
                    if (isSecondBon || isThirdBon){
                        printBatterPA();
                        hitRate += rateAmount3;
                        soRate -= rateAmount3;
                        bbRate += rateAmount3;
                        printBatterPA();
                    }
                    break;
                case GlobalValue.CUTTING_MACHINE:
                    printBatterPA();
                    soRate -= rateAmount2;
                    bbRate += rateAmount2;
                    printBatterPA();
                    break;
                case GlobalValue.SPRAY_HITTER:
                    printBatterPA();
                    hitRate += rateAmount2;
                    printBatterPA();
                    break;
                case GlobalValue.PISTOL:
                    printBatterPA();
                    hitRate += rateAmount3;
                    printBatterPA();
                    break;
                case GlobalValue.GUESS_HITTER:
                    printBatterPA();
                    break;
                case GlobalValue.PULL_HITTER:
                    printBatterPA();
                    break;
                case GlobalValue.CLEANUP_MEMBER:
                    if (getBatterOrder()>2 && getBatterOrder() <6){
                        printBatterPA();
                        hitRate += rateAmount2;
                        printBatterPA();
                    }
                    break;
            }

        }
        private void associateBatterGapSkill(BatterSkill skill){
            if (skill ==null){
                return;
            }
            int type =  skill.getSkillType();
            int rateAmount3 = skill.getSkillLevel()+5;
            int rateAmount2 = skill.getSkillLevel()+2;
            int rateAmount1 = skill.getSkillLevel()+1;
            switch (type){
                case GlobalValue.GUESS_HITTER:
                    printBatterGap();
                    gapRate += rateAmount3;
                    printBatterGap();
                    break;
                case GlobalValue.PULL_HITTER:
                    printBatterGap();
                    hrRate += rateAmount3;
                    printBatterGap();
                    break;
                case GlobalValue.PISTOL:
                    printBatterGap();
                    hrRate -= rateAmount1;
                    gapRate -= rateAmount1;
                    printBatterGap();
                    break;
                case GlobalValue.CLEANUP_MEMBER:
                    if (getBatterOrder()>2 && getBatterOrder() <6){
                        printBatterGap();
                        hrRate += rateAmount1;
                        gapRate += rateAmount1;
                        printBatterGap();
                    }
                    break;
            }
        }
        private void printBatterPA(){
            if (isSkill1){
                isShowingBatterSkilll1 = true;
            }else {
                isShowingBatterSkilll2 = true;
            }
        }
        private void printBatterGap(){
            if (isSkill1){
                isShowingBatterSkilll1 = true;
            }else {
                isShowingBatterSkilll2 = true;
            }
        }
        private void printPitcherPA(){
            if (isSkill1){
                isShowingPitcherSkill1 = true;
            }else {
                isShowingPitcherSkill2 = true;
            }
        }
        private void printPitcherGap(){
            if (isSkill1){
                isShowingPitcherSkill1 = true;
            }else {
                isShowingPitcherSkill2 = true;
            }
        }

        private int getBatterOrder(){
            int returnInt=0;
            if (isHomeTeamAttackInning){
                returnInt= homeBatterOrder;
            }else {
                returnInt= awayBatterOrder;
            }
            returnInt -=1;
            return returnInt;
        }
    }
}

