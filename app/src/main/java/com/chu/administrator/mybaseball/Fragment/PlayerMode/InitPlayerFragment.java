package com.chu.administrator.mybaseball.Fragment.PlayerMode;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.chu.administrator.mybaseball.Fragment.BaseFragment;
import com.chu.administrator.mybaseball.Helper.HashMapManager;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.BatterSkill;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.Item;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.PitcherSkill;
import com.chu.administrator.mybaseball.Model.PlayerUserModel;
import com.chu.administrator.mybaseball.Model.TitleItem;
import com.chu.administrator.mybaseball.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018-06-21.
 */

public class InitPlayerFragment extends BaseFragment {

    Button createPitcherBtn;
    Spinner spinnerBatter,spinnerPitcher,spinnerBPosition;
    List<String> batterTypeList = new ArrayList<>();
    List<String> batterPositionList = new ArrayList<>();
    List<String> pitcherTypeList = new ArrayList<>();
    ArrayAdapter<String> spinnerBatterAdapter,spinnerPitcherAdapter,spinnerPositionAdapter;
    int batterType=0,pitcherType=0,batterPosition=0;
    EditText bNameET,pNameET;
    Batter selectedBatter;
    Pitcher selectedPitcher;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = true;
        return inflater.inflate(R.layout.init_player_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        createPitcherBtn = (Button) view.findViewById(R.id.createPitcherBtn);
        bNameET = (EditText) view.findViewById(R.id.bNameET);
        pNameET = (EditText) view.findViewById(R.id.pNameET);

        //batter spinner

        spinnerBatter =(Spinner) view.findViewById(R.id.spinnerBatter);
        spinnerBatterAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerBatterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBatter.setAdapter(spinnerBatterAdapter);


        spinnerBatter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int selectedPosition= position;
                batterType = position;
                selectedBatter = bInstance.createBatter("",batterPosition,batterType);
                Toast.makeText(getContext(),""+selectedBatter.toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        batterTypeList.add(GlobalValue.getPlayerTypeString(0));
        batterTypeList.add(GlobalValue.getPlayerTypeString(1));
        batterTypeList.add(GlobalValue.getPlayerTypeString(2));
        batterTypeList.add(GlobalValue.getPlayerTypeString(3));
        batterTypeList.add(GlobalValue.getPlayerTypeString(4));
        batterTypeList.add(GlobalValue.getPlayerTypeString(5));
        batterTypeList.add(GlobalValue.getPlayerTypeString(6));


        spinnerBatterAdapter.addAll(batterTypeList);
        spinnerBatterAdapter.notifyDataSetChanged();

        //batter position spinner

        spinnerBPosition =(Spinner) view.findViewById(R.id.spinnerBPosition);
        spinnerPositionAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerPositionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBPosition.setAdapter(spinnerPositionAdapter);


        spinnerBPosition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                batterPosition = position;
                selectedBatter = bInstance.createBatter("",batterPosition,batterType);
                Toast.makeText(getContext(),""+selectedBatter.toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        batterPositionList.add(GlobalValue.getPositionString(0));
        batterPositionList.add(GlobalValue.getPositionString(1));
        batterPositionList.add(GlobalValue.getPositionString(2));
        batterPositionList.add(GlobalValue.getPositionString(3));
        batterPositionList.add(GlobalValue.getPositionString(4));
        batterPositionList.add(GlobalValue.getPositionString(5));
        batterPositionList.add(GlobalValue.getPositionString(6));
        batterPositionList.add(GlobalValue.getPositionString(7));

        spinnerPositionAdapter.addAll(batterPositionList);
        spinnerPositionAdapter.notifyDataSetChanged();

        //pitcher spinner

        spinnerPitcher =(Spinner) view.findViewById(R.id.spinnerPitcher);
        spinnerPitcherAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerPitcherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPitcher.setAdapter(spinnerPitcherAdapter);


        spinnerPitcher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int selectedPosition= position;
                pitcherType = position+10;
                selectedPitcher = bInstance.createPitcher("",pitcherType);
                Toast.makeText(getContext(),""+selectedPitcher.toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        pitcherTypeList.add(GlobalValue.getPlayerTypeString(10));
        pitcherTypeList.add(GlobalValue.getPlayerTypeString(11));
        pitcherTypeList.add(GlobalValue.getPlayerTypeString(12));
        pitcherTypeList.add(GlobalValue.getPlayerTypeString(13));
        pitcherTypeList.add(GlobalValue.getPlayerTypeString(14));
        pitcherTypeList.add(GlobalValue.getPlayerTypeString(15));
        pitcherTypeList.add(GlobalValue.getPlayerTypeString(16));


        spinnerPitcherAdapter.addAll(pitcherTypeList);
        spinnerPitcherAdapter.notifyDataSetChanged();


        createPitcherBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                createPitcherBtn.setEnabled(false);
                createPlayer();

            }
        });

        selectedBatter = getDemoBatter();
        selectedPitcher = getDemoPitcher();

        Toast.makeText(getContext(),selectedPitcher.toString()+selectedBatter.toString(),Toast.LENGTH_SHORT).show();


        super.onViewCreated(view, savedInstanceState);
    }
    private void createPlayer(){

        if (bInstance.isLengthOk(bNameET.getText().toString()) && bInstance.isLengthOk(pNameET.getText().toString()) && !(pNameET.getText().toString().equals(bNameET.getText().toString())) ){
            createBatter();
            createPitcher();
        }else {
            bInstance.popupGeneralWindow("Not valid player name..");
            createPitcherBtn.setEnabled(true);
            return;
        }

    }

    //TODO check player name duplicate

    private Batter getDemoBatter(){
        return bInstance.createBatter("",0,GlobalValue.GENERAL_BATTER);
    }
    private Pitcher getDemoPitcher(){
        return bInstance.createPitcher("",GlobalValue.GENERAL_PITCHER);
    }

    private void createBatter(){
        Batter batter = bInstance.createBatter(bNameET.getText().toString(),batterPosition,batterType);
        selectedBatter = batter;
        bInstance.getmPlayerUserModel().setMainBatter(batter);
        bInstance.getmPlayerUserModel().getSubBatters().add(batter);

    }
    private void createPitcher(){
        Pitcher pitcher = bInstance.createPitcher(pNameET.getText().toString(),pitcherType);
        selectedPitcher = pitcher;
        bInstance.getmPlayerUserModel().setMainPitcher(pitcher);
        bInstance.getmPlayerUserModel().getSubPitchers().add(pitcher);
        bInstance.getmPlayerUserModel().setCash(20);
        bInstance.getmPlayerUserModel().setGp(50000);
        bInstance.getmPlayerUserModel().setTeamUid("none");
        checkPlayerNameUsed();
    }

    public void storePlayerUserModel(){
        bInstance.showProgressBar();
        HashMapManager hManager = bInstance.gethManager();

        DocumentReference modeRef = bInstance.getDb().collection("userModel").document(bInstance.getUID());

        PlayerUserModel playerUserModel = bInstance.getmPlayerUserModel();

        Map<String, Object> modeInfo = new HashMap<>();

        modeInfo = hManager.createPlayerModeModelMap(modeInfo,playerUserModel);

        modeRef.set(modeInfo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                bInstance.hideProgressBar();
                bInstance.addAsRootFragment(new PlayerMainFragment());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
                createPitcherBtn.setEnabled(true);
                bInstance.popupGeneralWindow("server error.");
            }
        });  //        modeRef.set(modeInfo, SetOptions.merge()); j13

    }

    private void checkPlayerNameUsed(){
        String batterName =  selectedBatter.getBatterName();
        final String pitcherName = selectedPitcher.getPitcherName();


        bInstance.showProgressBar();
        final CollectionReference docRef = bInstance.getDb().collection("playerName");
        docRef.document(batterName).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
                        bInstance.popupGeneralWindow("Please use other name..");
                        bInstance.hideProgressBar();
                        createPitcherBtn.setEnabled(true);
                    } else {
                        docRef.document(pitcherName).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot != null && documentSnapshot.exists()) {
                                    bInstance.popupGeneralWindow("Please use other name..");
                                    bInstance.hideProgressBar();
                                    createPitcherBtn.setEnabled(true);
                                }else {
                                    storePlayerName();
                                }
                            }
                        });
                    }
                } else {
                    bInstance.popupGeneralWindow("Error in checking player name..");
                    createPitcherBtn.setEnabled(true);
                    bInstance.hideProgressBar();
                }
            }
        });
    }

    private void storePlayerName(){
        bInstance.showProgressBar();

        String batterName =  selectedBatter.getBatterName();
        String pitcherName = selectedPitcher.getPitcherName();

        DocumentReference docRefP = bInstance.getDb().collection("playerName").document(pitcherName);

        Map<String, Object> nameInfo = new HashMap<>();
        nameInfo.put("name", pitcherName);

        docRefP.set(nameInfo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("j13 ","store pitcher name.");
            }
        });

        DocumentReference docRef = bInstance.getDb().collection("playerName").document(batterName);

        nameInfo = new HashMap<>();
        nameInfo.put("name", batterName);

        docRef.set(nameInfo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("j13 ","store batter name.");
                storePlayerUserModel();
            }
        });

    }

}


























