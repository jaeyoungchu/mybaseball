package com.chu.administrator.mybaseball.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chu.administrator.mybaseball.Fragment.BaseFragment;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.MarketRVItem;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018-07-09.
 */

public class MarketFragment extends BaseFragment {

    private ArrayList<MarketRVItem> marketRVItems;
    private RecyclerView marketRV;
    private RecyclerView.Adapter marketAdapter;
    private Button registerBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = true;
        return inflater.inflate(R.layout.market_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        marketRVItems = new ArrayList<>();
        marketRV = (RecyclerView) view.findViewById(R.id.marketRV);
        registerBtn = (Button) view.findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerPlayers();
            }
        });

        registerBtn.setVisibility(View.INVISIBLE);
        super.onViewCreated(view, savedInstanceState);
    }

    private void registerPlayers(){
        bInstance.showProgressBar();

        Map<String, Object> playersInfo = new HashMap<>();

        Batter batter = bInstance.getmPlayerUserModel().getMainBatter();
        Pitcher pitcher = bInstance.getmPlayerUserModel().getMainPitcher();
        int totalStat = batter.getSumStat() + pitcher.getSumStat();

        playersInfo = bInstance.gethManager().createMarketModelMap(playersInfo,new MarketRVItem(batter,pitcher,totalStat,bInstance.getUID()));

        DocumentReference marketRef = bInstance.getDb().collection("market").document(bInstance.getUID());

        marketRef.set(playersInfo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                bInstance.hideProgressBar();
                Log.e("j13 "," registered..");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
                Log.e("j13 "," registered fail..");
            }
        });

    }

    private void initFragment(){

        if (bInstance.isManagerMode()){
            registerBtn.setVisibility(View.INVISIBLE);
        }else {
            registerBtn.setVisibility(View.VISIBLE);
        }
        marketAdapter = new MarketItemAdapter(getContext());
        marketRV.setAdapter(marketAdapter);
        marketRV.setLayoutManager(new GridLayoutManager(getActivity(),1));

    }

    private void getMarketItems(){
        bInstance.showProgressBar();
        bInstance.getDb().collection("market").orderBy("totalStat", Query.Direction.DESCENDING).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {

                MarketRVItem marketRVItem;
                for (DocumentSnapshot document : documentSnapshots) {

                    if (document.exists()) {
                        marketRVItem = bInstance.gethManager().getMarketRVItem(document);
                        marketRVItems.add(marketRVItem);
                    }
                }
                bInstance.hideProgressBar();
                initFragment();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
                bInstance.popupGeneralWindow("Server Error.");
            }
        });


    }

    @Override
    public void onResume() {
        getMarketItems();
        super.onResume();
    }




    public class MarketItemAdapter extends RecyclerView.Adapter<MarketItemAdapter.CustomViewHolder> {
        private Context context;
        private ArrayList<MarketRVItem> mItems;

        public MarketItemAdapter(Context mContext) {
            mItems = marketRVItems;
            context = mContext;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.market_rv_item,null);
            CustomViewHolder holder = new CustomViewHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {


            Batter batter = mItems.get(position).getBatter();
            Pitcher pitcher = mItems.get(position).getPitcher();

            holder.pNameTV.setText(pitcher.getPitcherName());
            holder.pNameTV.setBackgroundResource(pitcher.getPlayerBackgroundResource());

            holder.bNameTV.setText(batter.getBatterName());
            holder.bNameTV.setBackgroundResource(batter.getPlayerBackgroundResource());

            if (pitcher.getPitcherSkill1() != null){
                holder.pSkill1TV.setText(pitcher.getPitcherSkill1().getSkillName());
                holder.pSkill1TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill1().getSkillLevel()));
            }else {
                holder.pSkill1TV.setText("");
                holder.pSkill1TV.setBackgroundResource(pitcher.getPlayerSkillBG(0));
            }

            if (pitcher.getPitcherSkill2() != null){
                holder.pSkill2TV.setText(pitcher.getPitcherSkill2().getSkillName());
                holder.pSkill2TV.setBackgroundResource(pitcher.getPlayerSkillBG(pitcher.getPitcherSkill2().getSkillLevel()));
            }else {
                holder.pSkill2TV.setText("");
                holder.pSkill2TV.setBackgroundResource(pitcher.getPlayerSkillBG(0));
            }

            if (batter.getBatterSkill1() != null){
                holder.bSkill1TV.setText(batter.getBatterSkill1().getSkillName());
                holder.bSkill1TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill1().getSkillLevel()));
            }else {
                holder.bSkill1TV.setText("");
                holder.bSkill1TV.setBackgroundResource(batter.getPlayerSkillBG(0));
            }

            if (batter.getBatterSkill2() != null){
                holder.bSkill2TV.setText(batter.getBatterSkill2().getSkillName());
                holder.bSkill2TV.setBackgroundResource(batter.getPlayerSkillBG(batter.getBatterSkill2().getSkillLevel()));
            }else {
                holder.bSkill2TV.setText("");
                holder.bSkill2TV.setBackgroundResource(batter.getPlayerSkillBG(0));
            }


            bInstance.setTextViewWithColor(holder.p1TV,pitcher.getMovementWithItem());
            bInstance.setTextViewWithColor(holder.p2TV,pitcher.getLocationWithItem());
            bInstance.setTextViewWithColor(holder.p3TV,pitcher.getStuffWithItem());
            bInstance.setTextViewWithColor(holder.p4TV,pitcher.getVelocityWithItem());
            bInstance.setTextViewWithColor(holder.p5TV,pitcher.getMentalWithItem());
            bInstance.setTextViewWithColor(holder.p6TV,pitcher.getHealthWithItem());

            bInstance.setTextViewWithColor(holder.b1TV,batter.getContactWithItem());
            bInstance.setTextViewWithColor(holder.b2TV,batter.getPowerWithItem());
            bInstance.setTextViewWithColor(holder.b3TV,batter.getEyeWithItem());
            bInstance.setTextViewWithColor(holder.b4TV,batter.getClutchWithItem());
            bInstance.setTextViewWithColor(holder.b5TV,batter.getSpeedWithItem());
            bInstance.setTextViewWithColor(holder.b6TV,batter.getDefenseWithItem());
            holder.positionTV.setText(GlobalValue.getPositionString(batter.getPrimaryPosition()));

            holder.pNameTV.getLayoutParams().height = 100;
            holder.pSkill1TV.getLayoutParams().height = 100;
            holder.pSkill2TV.getLayoutParams().height = 100;
            holder.bNameTV.getLayoutParams().height = 100;
            holder.bSkill1TV.getLayoutParams().height = 100;
            holder.bSkill2TV.getLayoutParams().height = 100;
        }



        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public TextView pNameTV;
            public TextView p1TV;
            public TextView p2TV;
            public TextView p3TV;
            public TextView p4TV;
            public TextView p5TV;
            public TextView p6TV;
            public TextView pSkill1TV;
            public TextView pSkill2TV;

            public TextView bNameTV;
            public TextView b1TV;
            public TextView b2TV;
            public TextView b3TV;
            public TextView b4TV;
            public TextView b5TV;
            public TextView b6TV;
            public TextView positionTV;
            public TextView bSkill1TV;
            public TextView bSkill2TV;

            public LinearLayout itemMainLo;

            public CustomViewHolder(View view) {

                super(view);
                pNameTV = (TextView) view.findViewById(R.id.pNameTV);
                p1TV = (TextView) view.findViewById(R.id.p1TV);
                p2TV = (TextView) view.findViewById(R.id.p2TV);
                p3TV = (TextView) view.findViewById(R.id.p3TV);
                p4TV = (TextView) view.findViewById(R.id.p4TV);
                p5TV = (TextView) view.findViewById(R.id.p5TV);
                p6TV = (TextView) view.findViewById(R.id.p6TV);
                pSkill1TV = (TextView) view.findViewById(R.id.pSkill1TV);
                pSkill2TV = (TextView) view.findViewById(R.id.pSkill2TV);

                bNameTV = (TextView) view.findViewById(R.id.bNameTV);
                b1TV = (TextView) view.findViewById(R.id.b1TV);
                b2TV = (TextView) view.findViewById(R.id.b2TV);
                b3TV = (TextView) view.findViewById(R.id.b3TV);
                b4TV = (TextView) view.findViewById(R.id.b4TV);
                b5TV = (TextView) view.findViewById(R.id.b5TV);
                b6TV = (TextView) view.findViewById(R.id.b6TV);
                positionTV = (TextView) view.findViewById(R.id.positionTV);
                bSkill1TV = (TextView) view.findViewById(R.id.bSkill1TV);
                bSkill2TV = (TextView) view.findViewById(R.id.bSkill2TV);
                itemMainLo = (LinearLayout) view.findViewById(R.id.itemMainLo);

                itemMainLo.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {

                if (view.getId() == R.id.itemMainLo){
                    bInstance.addFragment(new PlayerDetailFragment(marketRVItems.get(getLayoutPosition())));
                }

            }
        }
    }
}
