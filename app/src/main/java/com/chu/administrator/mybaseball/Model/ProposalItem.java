package com.chu.administrator.mybaseball.Model;

/**
 * Created by Administrator on 2018-07-16.
 */

public class ProposalItem {
    private String deviceId,message,teamName,teamUid;
    private boolean isNew;

    public ProposalItem(String deviceId, String message, String teamName, String teamUid, boolean isNew) {
        this.deviceId = deviceId;
        this.message = message;
        this.teamName = teamName;
        this.teamUid = teamUid;
        this.isNew = isNew;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getMessage() {
        return message;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getTeamUid() {
        return teamUid;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setTeamUid(String teamUid) {
        this.teamUid = teamUid;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    @Override
    public String toString() {
        return "ProposalItem{" +
                "deviceId='" + deviceId + '\'' +
                ", message='" + message + '\'' +
                ", teamName='" + teamName + '\'' +
                ", teamUid='" + teamUid + '\'' +
                ", isNew=" + isNew +
                '}';
    }
}
