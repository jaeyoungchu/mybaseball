package com.chu.administrator.mybaseball.Model;

/**
 * Created by Administrator on 2018-06-20.
 */

public class PitcherSkill {
    private int skillType;
    private String skillName;
    private int skillLevel;
    private String skillDescription;

    public PitcherSkill() {
    }

    public PitcherSkill(int type, int skillLevel) {
        this.skillType = type;
        this.skillName = GlobalValue.getSkillName(type);
        this.skillDescription = GlobalValue.getSkillDescription(type);
        this.skillLevel = skillLevel;
    }

    public int getSkillType() {
        return skillType;
    }

    public String getSkillDescription() {
        return skillDescription;
    }

    public String getSkillName() {
        return skillName;
    }

    public int getSkillLevel() {
        return skillLevel;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public void setSkillLevel(int skillLevel) {
        this.skillLevel = skillLevel;
    }

    @Override
    public String toString() {
        return "PitcherSkill{" +
                "skillName='" + skillName + '\'' +
                "skillType='" + skillType + '\'' +
                ", skillLevel=" + skillLevel +
                ", skillDescription='" + skillDescription + '\'' +
                '}';
    }
}