package com.chu.administrator.mybaseball.Model;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by Administrator on 2018-06-20.
 */

public class BatterSkill {
    private int skillType;
    private int skillLevel;
    private String skillName;
    private String skillDescription;


    public BatterSkill() {
        this.skillType = -1;
        this.skillDescription = "";
        this.skillLevel = -1;
        this.skillName = "";
    }

    public BatterSkill(int type, int skillLevel) {

        this.skillType = type;
        this.skillName = GlobalValue.getSkillName(type);
        this.skillDescription = GlobalValue.getSkillDescription(type);
        this.skillLevel = skillLevel;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public String getSkillDescription() {
        return skillDescription;
    }

    public void setSkillDescription(String skillDescription) {
        this.skillDescription = skillDescription;
    }

    public int getSkillType() {
        return skillType;
    }

    public int getSkillLevel() {
        return skillLevel;
    }

    public void setSkillType(int skillType) {
        this.skillType = skillType;
    }

    public void setSkillLevel(int skillLevel) {
        this.skillLevel = skillLevel;
    }

    @Override
    public String toString() {
        return "BatterSkill{" +
                "skillName='" + skillName + '\'' +
                "skillType='" + skillType + '\'' +
                ", skillLevel=" + skillLevel +
                ", skillDescription='" + skillDescription + '\'' +
                '}';
    }
}
