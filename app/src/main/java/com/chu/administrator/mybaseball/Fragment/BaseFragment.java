package com.chu.administrator.mybaseball.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chu.administrator.mybaseball.BaseCoreActivity;

/**
 * Created by Administrator on 2018-06-11.
 */

public class BaseFragment extends Fragment {
    protected boolean isHiding = false;
    protected BaseCoreActivity bInstance;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bInstance = BaseCoreActivity.getInstance();
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        BaseCoreActivity.getInstance().hideAppBar(isHiding);
    }

}
