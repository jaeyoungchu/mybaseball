package com.chu.administrator.mybaseball.Model;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-06-21.
 */

public class PlayerUserModel {

    private String uid,deviceId,teamUid;
    private int cash,gp,modeType;
    private int historyCap; // capacity of containing exp max.

    private Batter mainBatter;
    private Pitcher mainPitcher;

    private ArrayList<Item> subItems;
    private ArrayList<Batter> subBatters;
    private ArrayList<Pitcher> subPitchers;

    public PlayerUserModel() {
    }

    public PlayerUserModel(String deviceId, String uid, Batter mainBatter, Pitcher mainPitcher, int cash, int gp, int modeType, ArrayList<Item> itemArrayList, ArrayList<Batter> subBatters, ArrayList<Pitcher> subPitchers) {
        this.uid = uid;
        this.deviceId = deviceId;
        this.teamUid = "none";
        this.historyCap = 100;
        this.mainBatter = mainBatter;
        this.mainPitcher = mainPitcher;
        this.cash = cash;
        this.gp = gp;
        this.modeType = modeType;
        this.subItems = itemArrayList;
        this.subBatters = subBatters;
        this.subPitchers = subPitchers;
    }

    public PlayerUserModel(String deviceId,String uid, int modeType) {
        this.deviceId = deviceId;
        this.uid = uid;
        this.teamUid = "none";
        this.historyCap = 100;
        this.modeType = modeType;
        subItems = new ArrayList<>();
        subBatters = new ArrayList<>();
        subPitchers = new ArrayList<>();
        this.cash = 0;
        this.gp = 0;
    }

    public String getTeamUid() {
        return teamUid;
    }

    public void setTeamUid(String teamUid) {
        this.teamUid = teamUid;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUid() {
        return uid;
    }

    public int getHistoryCap() {
        return historyCap;
    }

    public void setHistoryCap(int historyCap) {
        this.historyCap = historyCap;
    }

    public Batter getMainBatter() {
        return mainBatter;
    }

    public Pitcher getMainPitcher() {
        return mainPitcher;
    }

    public int getCash() {
        return cash;
    }

    public int getGp() {
        return gp;
    }

    public int getModeType() {
        return modeType;
    }

    public ArrayList<Item> getSubItems() {
        return subItems;
    }

    public ArrayList<Batter> getSubBatters() {
        return subBatters;
    }

    public ArrayList<Pitcher> getSubPitchers() {
        return subPitchers;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setMainBatter(Batter mainBatter) {
        this.mainBatter = mainBatter;
    }

    public void setMainPitcher(Pitcher mainPitcher) {
        this.mainPitcher = mainPitcher;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public void setGp(int gp) {
        this.gp = gp;
    }

    public void setModeType(int modeType) {
        this.modeType = modeType;
    }

    public void setSubItems(ArrayList<Item> subItems) {
        this.subItems = subItems;
    }

    public void setSubBatters(ArrayList<Batter> subBatters) {
        this.subBatters = subBatters;
    }

    public void setSubPitchers(ArrayList<Pitcher> subPitchers) {
        this.subPitchers = subPitchers;
    }

    @Override
    public String toString() {
        String val ="j13 player user model toString \n" ;

        val += "uid " + uid +" \n"
                +"mode type " + modeType +" \n" +
                "cash " + cash +" \n" +
                "gp " + gp +" \n" +
                "batter " + mainBatter.toString() +" \n" +
                "pitcher " + mainPitcher.toString() +" \n" +
                "sub batter size " + subBatters.size() +" \n" +
                "sub pitcher size " + subPitchers.size() +" \n" +
                "sub items size " + subItems.size() +" \n";;

        return val;
    }
}
