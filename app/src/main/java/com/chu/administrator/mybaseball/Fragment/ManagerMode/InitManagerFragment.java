package com.chu.administrator.mybaseball.Fragment.ManagerMode;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.chu.administrator.mybaseball.Fragment.BaseFragment;
import com.chu.administrator.mybaseball.Helper.HashMapManager;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.BatterSkill;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.Item;
import com.chu.administrator.mybaseball.Model.ManagerUserModel;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.PitcherSkill;
import com.chu.administrator.mybaseball.Model.Team;
import com.chu.administrator.mybaseball.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018-06-25.
 */

public class InitManagerFragment extends BaseFragment {

    Button createManagerBtn;
    ManagerUserModel managerUserModel;
    EditText teamNameET;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = true;
        return inflater.inflate(R.layout.init_manager_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        createManagerBtn = (Button) view.findViewById(R.id.createManagerBtn);
        teamNameET = (EditText)view.findViewById(R.id.teamNameET);
        createManagerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkTeamNameLength(teamNameET.getText().toString())){
                    bInstance.popupGeneralWindow("Not Valid team name.");
                    return;
                }

                enableUI(false);
                createManagerModel1(teamNameET.getText().toString());
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    private boolean checkTeamNameLength(String inputString){
        return bInstance.isLengthOk(inputString);
    }

    private void createManagerModel1(String name){
        String uid = bInstance.getUID();
        String deviceId = bInstance.getDeviceID();
        String teamName = name;
        int cash = 20;
        int gp = 200000;
        int modeType = GlobalValue.MANAGERMODE;

        managerUserModel = new ManagerUserModel(uid,deviceId,cash,gp,modeType);
        managerUserModel.setTeamName(teamName);
        bInstance.setmManagerUserModel(managerUserModel);
        checkTeamNameUsed(teamName);
    }

    private void createManagerModel2(String teamName){
        managerUserModel.setSubBatterSquad(new ArrayList<Batter>());

        managerUserModel.setSubPitcherSquad(new ArrayList<Pitcher>());
        managerUserModel.setManager(bInstance.createRandomManager());

//        //j13
//        Batter batter = new Batter(bInstance.getUID(),bInstance.createPlayerToken(),"dummyBatter", GlobalValue.catcher,60,50,50,50,50,50,GlobalValue.DEFENCE_BATTER);
//        batter.setBatterSkill1(new BatterSkill(1,2));
//        batter.setBatterSkill2(new BatterSkill(2,2));
//        batter.setBatterSkill3(new BatterSkill(3,3));
//        batter.setBatterSkill4(new BatterSkill(4,2));
//        batter.setBat(new Item(0,2,0,0,0,0,0));
//        batter.setGlasses(new Item(GlobalValue.typeGlasses,2,0,0,0,0,0));
//        managerUserModel.getSubBatterSquad().add(batter);
//
//        bInstance.setmManagerUserModel(managerUserModel);
//
//        Pitcher pitcher = new Pitcher(bInstance.getUID(),bInstance.createPlayerToken(),"dummyPitcher",50,60,50,50,50,50,GlobalValue.LOCATION_PITCHER);
//        pitcher.setShoes(new Item(GlobalValue.typeShoes,2,0,0,0,0,0));
//        pitcher.setCap(new Item(GlobalValue.typeCap,2,0,0,0,0,0));
//        pitcher.setPitcherSkill1(new PitcherSkill(21,2));
//        pitcher.setPitcherSkill2(new PitcherSkill(22,2));
//        pitcher.setPitcherSkill3(new PitcherSkill(23,2));
//        pitcher.setPitcherSkill4(new PitcherSkill(24,2));
//
//        managerUserModel.getSubPitcherSquad().add(pitcher);

        //j13

        Team team = new Team();
        team.setTeamName(teamName);
        team.setBatterSquad(createBatterSquad(teamName));
        team.setPitcherSquad(createPitcherSuqad(teamName));
        managerUserModel.setTeam(team);

        storeManagerModel();

    }



    private void storeManagerModel(){

        bInstance.showProgressBar();
        HashMapManager hManager = bInstance.gethManager();

        DocumentReference modeRef = bInstance.getDb().collection("userModel").document(bInstance.getUID());

        ManagerUserModel managerUserModel = bInstance.getmManagerUserModel();
        Map<String, Object> modeInfo = new HashMap<>();

        modeInfo = hManager.createManagerModeModelMap(modeInfo,managerUserModel);

        modeRef.set(modeInfo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                bInstance.hideProgressBar();
                bInstance.addAsRootFragment(new ManagerMainFragment());
                enableUI(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
                bInstance.popupGeneralWindow("server error.");
                enableUI(true);
            }
        });  //        modeRef.set(modeInfo, SetOptions.merge()); j13

    }

    private ArrayList<Pitcher> createPitcherSuqad(String teamName){
        ArrayList<Pitcher> pitcherArrayList = new ArrayList<>();

        Pitcher SP1 = bInstance.createPitcher(0);

        Pitcher SP2 = bInstance.createPitcher(1);
        Pitcher SP3 = bInstance.createPitcher(2);
        Pitcher SP4 = bInstance.createPitcher(3);
        Pitcher SP5 = bInstance.createPitcher(4);
        Pitcher RP1 = bInstance.createPitcher(5);
        Pitcher RP2 = bInstance.createPitcher(6);
        Pitcher RP3 = bInstance.createPitcher(7);
        Pitcher RP4 = bInstance.createPitcher(8);

        pitcherArrayList.add(SP1);
        pitcherArrayList.add(SP2);
        pitcherArrayList.add(SP3);
        pitcherArrayList.add(SP4);
        pitcherArrayList.add(SP5);
        pitcherArrayList.add(RP1);
        pitcherArrayList.add(RP2);
        pitcherArrayList.add(RP3);
        pitcherArrayList.add(RP4);

        return pitcherArrayList;
    }


    private ArrayList<Batter> createBatterSquad(String teamName){
        ArrayList<Batter> batterArrayList = new ArrayList<>();
        Batter C = bInstance.createBatter(0);
        C.setBattingOrder(0);
        Batter B1 = bInstance.createBatter(1);
        B1.setBattingOrder(1);
        Batter B2 = bInstance.createBatter(2);
        B1.setBattingOrder(2);

        Batter B3 = bInstance.createBatter(3);
        B1.setBattingOrder(3);

        Batter SS = bInstance.createBatter(4);
        B1.setBattingOrder(4);

        Batter LF = bInstance.createBatter(5);
        B1.setBattingOrder(5);

        Batter CF = bInstance.createBatter(6);
        B1.setBattingOrder(6);

        Batter RF = bInstance.createBatter(7);
        B1.setBattingOrder(7);

        Batter DH = bInstance.createBatter(8);
        B1.setBattingOrder(8);

        batterArrayList.add(C);
        batterArrayList.add(B1);
        batterArrayList.add(B2);
        batterArrayList.add(B3);
        batterArrayList.add(SS);
        batterArrayList.add(LF);
        batterArrayList.add(CF);
        batterArrayList.add(RF);
        batterArrayList.add(DH);

        return batterArrayList;
    }
    private void enableUI(boolean flag){
        createManagerBtn.setEnabled(flag);
    }

    private void checkTeamNameUsed(final String teamName){
        bInstance.showProgressBar();
        CollectionReference userModelsRef = bInstance.getDb().collection("userModel");

        Query query = userModelsRef.whereEqualTo("mode",GlobalValue.MANAGERMODE).whereEqualTo("teamName", teamName);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                QuerySnapshot document = task.getResult();

                if (task.isSuccessful()) {

                    if (document != null && !document.isEmpty()) {
                        bInstance.popupGeneralWindow("Already used team name..");
                        bInstance.hideProgressBar();
                        enableUI(true);
                        return;
                    } else {
                        bInstance.hideProgressBar();
                        createManagerModel2(teamName);
                    }
                } else {
                    bInstance.popupGeneralWindow("Server Connection Error");
                    bInstance.hideProgressBar();
                    enableUI(true);
                }
            }
        });
    }
}
























