package com.chu.administrator.mybaseball.Model;

/**
 * Created by Administrator on 2018-06-17.
 */

public class PithcerMatchRecord {
    public int game,outcount,er,r,so,hit,bb,hr,outNum,sf,single,doublehit,tripple,rispOutcount,rispHit;
    public int win,lose,hold,save;
    public int startScoreDif,endScoreDif;
//    public float era,whip,so9,bb9,hr9;


    public PithcerMatchRecord(int game, int outcount, int er, int r, int so, int hit, int bb, int hr, int outNum, int sf, int single, int doublehit, int tripple, int win, int lose, int hold, int save, int rispOutcount, int rispHit) {
        this.game = game;
        this.outcount = outcount;
        this.er = er;
        this.r = r;
        this.so = so;
        this.hit = hit;
        this.bb = bb;
        this.hr = hr;
        this.outNum = outNum;
        this.sf = sf;
        this.single = single;
        this.doublehit = doublehit;
        this.tripple = tripple;
        this.win = win;
        this.lose = lose;
        this.hold = hold;
        this.save = save;
        this.rispOutcount = rispOutcount;
        this.rispHit = rispHit;
    }

    public PithcerMatchRecord() {
        this.game = 0;
        this.outcount = 0;
        this.er = 0;
        this.r = 0;
        this.so = 0;
        this.hit = 0;
        this.bb = 0;
        this.sf = 0;
        this.single = 0;
        this.doublehit = 0;
        this.tripple = 0;
        this.hr = 0;
        this.win = 0;
        this.lose = 0;
        this.hold = 0;
        this.save = 0;
        this.outNum = 0;
        this.startScoreDif = 0;
        this.endScoreDif = 0;
        this.rispOutcount = 0;
        this.rispHit = 0;
    }

    public void resetRecord(){
        this.game = 0;
        this.outcount = 0;
        this.er = 0;
        this.r = 0;
        this.so = 0;
        this.hit = 0;
        this.bb = 0;
        this.sf = 0;
        this.single = 0;
        this.doublehit = 0;
        this.tripple = 0;
        this.hr = 0;
        this.win = 0;
        this.lose = 0;
        this.hold = 0;
        this.save = 0;
        this.outNum = 0;
        this.startScoreDif = 0;
        this.endScoreDif = 0;
        this.rispOutcount = 0;
        this.rispHit = 0;
    }

    public String getERA(){
        if (outcount > 0){
            float era = (float) er*9/(float)((float)outcount/(float)3);
            String str = String.format("%.2f",era);
            return str;
        }else {
            return "0.00";
        }
    }
    public float getRERA(){
        if (outcount > 0){
            float era = (float) er*9/(float)((float)outcount/(float)3);

            return era;
        }else {
            return 0;
        }
    }

    public String getWinPerGame(){
        float avg;
        if (game != 0 ){
            avg = (float)win/(float)game;
            String str = String.format("%.2f",avg);
            if (avg == 0){
                str = "0.00";
            }
            return str;
        }else {
            return "0.00";
        }
    }

    public String getInningPerGame(){
        float avg;
        if (game != 0 ){
            avg = (float)((float)outcount/(float)3)/(float)game;
            String str = String.format("%.1f",avg);
            if (avg == 0){
                str = "0.0";
            }
            return str;
        }else {
            return "0.0";
        }
    }
    public String getInning(){
        if (outcount > 0){
            int i = outcount/3;
            int n = outcount%3;
            return i+"."+n;
        }else {
            return "0.0";
        }
    }

    public String getOrisp(){
        if (rispOutcount >0){
            float oavg = (float) rispHit/(float) (rispOutcount + rispHit);

            String str = String.format("%.3f",oavg);
            return  str;
        }else {
            return ".000";
        }
    }

    public String getOavg(){
        if (outcount >0){
            float oavg = (float) hit/(float) (outcount + hit);

            String str = String.format("%.3f",oavg);
            return  str;
        }else {
            return ".000";
        }
    }
    public String getOslg(){
        if (outcount >0){
            float oslg = (float) (single + (doublehit*2) + (tripple*3) + (hr*4))/(float) (outcount+hit);
//            Log.e("oslg : ", " s " + single+" s " + doublehit+" s " + tripple+" s " + hr+" s " + outcount);

            String str = String.format("%.3f",oslg);
            return  str;
        }else {
            return ".000";
        }
    }
    public String getWhip(){
        if (outcount >0){
            float whip = (float) (bb + hit)/(float) ((float)outcount/(float)3);
            if ((bb + hit) == 0){
                return "0.000";
            }
            String str = String.format("%.3f",whip);
            return  str;
        }else {
            return "0.000";
        }
    }

    public String getSo9(){
        if (outcount >0){
            float so9 = (float) (so)*9/(float) ((float)outcount/(float)3);
            String str = String.format("%.1f",so9);
            return  str;
        }else {
            return "0.0";
        }
    }
    public String getBb9(){
        if (outcount >0){
            float bb9 = (float) (bb)*9/(float) ((float)outcount/(float)3);
            String str = String.format("%.1f",bb9);
            return  str;
        }else {
            return "0.0";
        }
    }
    public String getHr9(){
        if (outcount >0){
            float hr9 = (float) (hr)*9/(float) ((float)outcount/(float)3);
            String str = String.format("%.1f",hr9);
            return  str;
        }else {
            return "0.0";
        }
    }

    public void saveMatchRecord(PithcerMatchRecord matchRecord){
        this.game += matchRecord.game;
        this.outcount += matchRecord.outcount;
        this.er += matchRecord.er;
        this.r += matchRecord.r;
        this.so += matchRecord.so;
        this.hit += matchRecord.hit;
        this.bb += matchRecord.bb;
        this.hr += matchRecord.hr;
        this.win += matchRecord.win;
        this.lose += matchRecord.lose;
        this.hold += matchRecord.hold;
        this.save += matchRecord.save;

        this.sf += matchRecord.sf;
        this.single += matchRecord.single;
        this.doublehit += matchRecord.doublehit;
        this.tripple += matchRecord.tripple;
        this.rispHit += matchRecord.rispHit;
        this.rispOutcount += matchRecord.rispOutcount;
    }

    @Override
    public String toString() {
        return "PithcerMatchRecord{\t\t" +
                "\tgame=" + game +
                ", getInning=" + getInning() +
                ", getERA=" + getERA() +
                ", getBb9=" + getBb9() +
                ", getSo9=" + getSo9() +
                ", getOavg=" + getOavg() +
                ", getOslg=" + getOslg() +
                ", getWhip=" + getWhip() +
                ", getHr9=" + getHr9() +
                ", win=" + win +
                ", lose=" + lose +
                ", hold=" + hold +
                ", save=" + save +
                ", rispHit=" + rispHit +
                ", rispOutcount=" + rispOutcount +
                ", ORISP=" + getOrisp() +
                '}';
    }
}
