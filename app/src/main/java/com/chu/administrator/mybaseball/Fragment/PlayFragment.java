package com.chu.administrator.mybaseball.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chu.administrator.mybaseball.BaseCoreActivity;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.MatchDetail;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.R;


import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Administrator on 2018-07-06.
 */

public class PlayFragment extends BaseFragment {

    private ArrayList<MatchDetail> matchDetails;
    private int i = 0;
    private TextView homeScoreTV,inningTV,awayScoreTV,homeTeamNameTV,awayTeamNameTV;
    private ImageView b1,b2,b3,s1,s2,o1,o2,up,down;
    private Random randomInt;
    private int diceForball,diceForStrike;
    private TextView batterSkillTV,pitcherSkillTV,batterNameTV,pitcherNameTV;
    private LinearLayout pitcherSkillLayout,batterSkillLayout;
    private TextView b1RunnerNameTV,b2RunnerNameTV,b3RunnerNameTV;
    private TextView b1RunnerSpeedStatTV,b2RunnerSpeedStatTV,b3RunnerSpeedStatTV;
    private LinearLayout b1RunnerLayout,b2RunnerLayout,b3RunnerLayout;
    private LinearLayout batterLayout,pitcherLayout;
    private int delayTime = 1000;
    private ImageView frontRIV;
    private ImageView afterIV,imageCIV,out1BIV,out2BIV,out3BIV,imageRFIV,imageCFIV,imageLFIV,image2BIV,imageSSIV,imageTripeRightIV,imageTripeLeftIV,imageHomerRightIV,imageHomerCenterIV,imageHomerLeftIV;
    private Thread t;
    private int previousScoreHome,previousScoreAway;
    private Animation anim;
    Runnable runnable;
    Handler handler;
    Random random;
    private int zoomSpeedFast,zoomSpeedSlow;
    LinearLayout h1l,h2l,h3l,h4l,h5l,h6l,h7l,h8l,h9l,hPl;
    LinearLayout a1l,a2l,a3l,a4l,a5l,a6l,a7l,a8l,a9l,aPl;
    TextView h1NTV,h2NTV,h3NTV,h4NTV,h5NTV,h6NTV,h7NTV,h8NTV,h9NTV,hPNTV;
    TextView a1NTV,a2NTV,a3NTV,a4NTV,a5NTV,a6NTV,a7NTV,a8NTV,a9NTV,aPNTV;
    TextView h1TV,h2TV,h3TV,h4TV,h5TV,h6TV,h7TV,h8TV,h9TV;
    TextView a1TV,a2TV,a3TV,a4TV,a5TV,a6TV,a7TV,a8TV,a9TV;
    TextView batterPositionTV,pitcherPositionTV,batterTotalStatTV,pitcherTotalStatTV;
    ArrayList<TextView> homeNameList,homeResultList,awayNameList,awayResultList;
    ArrayList<LinearLayout> homeBackgroundList, awayBackgroundList;
    LinearLayout pitcherBackgroud,batterBackground;
    Button skipBtn;
    int delayLong,delayShort;
    Button x2Btn;
    MatchDetail firstM;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        handler = new Handler();

        randomInt = new Random();
        delayShort=300;
        delayLong=500;
//        for (final MatchDetail m : matchDetails){
//            Log.e("j13 " , "isFirst  "+(m.isFristHalf));
//            Log.e("j13 " , "outCount "+(m.outCount));
//            Log.e("j13 " , "result  "+(m.result));
//            Log.e("j13 ", "-----------------------------------");
//        }

        return inflater.inflate(R.layout.play_frag, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        random = new Random();
        initUI(view);
        firstM = matchDetails.get(0);
        homeTeamNameTV.setText(matchDetails.get(0).homeTeam.getTeamName());
        homeTeamNameTV.setTextColor(getResources().getColor(R.color.white));
        awayTeamNameTV.setText(matchDetails.get(0).awayTeam.getTeamName());
        awayTeamNameTV.setTextColor(getResources().getColor(R.color.white));
        initOrderLayout(matchDetails.get(0));
        if (speedFlag){
            x2Btn.setText("X1");
            delayLong = 600;
            delayShort = 1000;
        }else {
            x2Btn.setText("X2");
            delayLong = 300;
            delayShort = 500;
        }

        t = new Thread() {
            @Override
            public void run() {
                try {
                    for (final MatchDetail m : matchDetails) {
//                        Log.e("j13 ", m.toString());
                        if (m.result.equals("beginInning")) {
                            delayTime = delayShort;
                        } else {
                            delayTime = delayLong;
                        }

                        if (t == null){
                            break;
                        }
                        bInstance.runOnUiThread(runnable = new Runnable() {
                            @Override
                            public void run() {
                                standPA(m);
                            }
                        });
                        Thread.sleep(delayTime);
                        bInstance.runOnUiThread(runnable = new Runnable() {
                            @Override
                            public void run() {
                                firstResult(m);
                            }
                        });
                        Thread.sleep(delayTime);
                        bInstance.runOnUiThread(runnable = new Runnable() {
                            @Override
                            public void run() {
                                showResult(m);
                            }
                        });
                        Thread.sleep(delayTime);
                        bInstance.runOnUiThread(runnable = new Runnable() {
                            @Override
                            public void run() {
                                afterResult(m);
                            }
                        });
                        Thread.sleep(delayTime);
                        if (m.isFristHalf){
                            previousScoreHome = m.homeTeamScore;
                        }else {
                            previousScoreAway = m.awayTeamScore;
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

    }

    private void standPA(MatchDetail m){
        if (getContext() == null){
            return;
        }
        frontRIV.setVisibility(View.INVISIBLE);
        if (m.result.equals("beginInning")){

            if (m.isFristHalf){
                if (m.inning == 1){
                    afterIV.setBackgroundResource(R.drawable.startgame);

                }else {
                    afterIV.setBackgroundResource(R.drawable.inningchange);
                }

            }else {
                afterIV.setBackgroundResource(R.drawable.inningchange);
            }
            setLeftToRightAnim(afterIV,View.VISIBLE);
            batterLayout.setVisibility(View.INVISIBLE);
        }else {
            batterLayout.setVisibility(View.VISIBLE);
        }

        clearCount(m);
        showBatterPitcher(m);

    }
    //STEAL BASE SUCCESS! , STEAL BASE FAIL! , BUNT SUCCESS! , BUNT FAIL! , SINGLE HIT! , DOUBLE HIT! ,
    // TRIPPLE HIT! , HOMER! ,STRIKE OUT! , BASE ON BALL! , FIELD ERROR! , FIELD OUT! , DOUBLE PLAY! , SACRIFICE FLY!
    private void firstResult(MatchDetail m){
        if (getContext() == null){
            return;
        }
        if (m.result.equals("beginInning")){
            return;
        }
        showCount(m);

        if (m.result.equals("SINGLE HIT!")){
            setZoomFast(getSingleHitIV(),View.VISIBLE,zoomSpeedFast);
        }else if (m.result.equals("DOUBLE HIT!")){
            setZoomFast(getDoubleHitIV(),View.VISIBLE,zoomSpeedFast);
        }else if (m.result.equals("TRIPPLE HIT!")){
            setZoomFast(getTripleHitIV(),View.VISIBLE,zoomSpeedFast);
        }else if (m.result.equals("HOMER!")){
            setZoomFast(getHomerunIV(),View.VISIBLE,zoomSpeedFast);
        }else if (m.result.equals("FIELD ERROR!")){
            getDutyDefender(m).setBackgroundResource(R.drawable.error);
            setFadeOutUp(getDutyDefender(m),View.VISIBLE);
        }else if (m.result.equals("FIELD OUT!")){
            if (isGroundballOut(m)){
                getDutyDefender(m).setBackgroundResource(R.drawable.success);
            }else {
                getDutyDefender(m).setBackgroundResource(R.drawable.out);
            }
            getDutyDefender(m).setVisibility(View.VISIBLE);
//            setFadeOutUp(getDutyDefender(m),View.VISIBLE);

        }else if (m.result.equals("DOUBLE PLAY!")){
            getDutyDefender(m).setBackgroundResource(R.drawable.success);
            getDutyDefender(m).setVisibility(View.VISIBLE);
        }else if (m.result.equals("SACRIFICE FLY!")){
            getDutyDefender(m).setBackgroundResource(R.drawable.sacrifice_fly);
            getDutyDefender(m).setVisibility(View.VISIBLE);
        }else if (m.result.equals("BUNT SUCCESS!") || m.result.equals("BUNT FAIL!")){
            hideBatter();
            frontRIV.setBackgroundResource(R.drawable.bunt);
            setZoomFast(frontRIV,View.VISIBLE,zoomSpeedFast);
        }else if (m.result.equals("STEAL BASE SUCCESS!") || m.result.equals("STEAL BASE FAIL!")){

            b1RunnerLayout.setVisibility(View.INVISIBLE);
            out1BIV.setBackgroundResource(R.drawable.steal);
            out1BIV.setVisibility(View.VISIBLE);

        }else if (m.result.equals("STRIKE OUT!")){
            setTwoStrike();
//            frontRIV.setBackgroundResource(R.drawable.swung);
//            setZoomFast(frontRIV,View.VISIBLE,zoomSpeedFast);
        }else if (m.result.equals("BASE ON BALL!")){
            setThreeBall();
            hideBatter();
            frontRIV.setBackgroundResource(R.drawable.base_on_ball);
            setZoomFast(frontRIV,View.VISIBLE,zoomSpeedFast);
        }

    }

    private void showResult(MatchDetail m){
        if (m.result.equals("beginInning")){
            return;
        }
        setZoomIn(frontRIV,View.INVISIBLE,zoomSpeedSlow);
        hideView();
        if (m.result.equals("BUNT SUCCESS!")){
            hideRunner();
            batterLayout.setVisibility(View.INVISIBLE);
            out1BIV.setBackgroundResource(R.drawable.out);
            out1BIV.setVisibility(View.VISIBLE);
//            setZoomIn(out1BIV,View.VISIBLE,zoomSpeedSlow);
//            showRunner(m);
        }else if (m.result.equals("BUNT FAIL!")) {
            hideRunner();
            batterLayout.setVisibility(View.INVISIBLE);
            out2BIV.setBackgroundResource(R.drawable.out);
            out2BIV.setVisibility(View.VISIBLE);
//            setZoomIn(out2BIV,View.VISIBLE,zoomSpeedSlow);
//            showRunner(m);
        }else if (m.result.equals("STEAL BASE SUCCESS!")) {
            out2BIV.setBackgroundResource(R.drawable.success);
            out2BIV.setVisibility(View.VISIBLE);
//            showRunner(m);
        }else if (m.result.equals("STEAL BASE FAIL!")) {
            out2BIV.setBackgroundResource(R.drawable.fail);
            out2BIV.setVisibility(View.VISIBLE);
            showRunner(m);
        }else if (m.result.equals("FIELD ERROR!")){
            hideRunner();
            batterLayout.setVisibility(View.INVISIBLE);
        }else if (m.result.equals("FIELD OUT!")){
            hideRunner();
            batterLayout.setVisibility(View.INVISIBLE);

            if (isGroundballOut(m)){
                out1BIV.setBackgroundResource(R.drawable.out);
                out1BIV.setVisibility(View.VISIBLE);
            }

        }else if (m.result.equals("SACRIFICE FLY!")){
//            setFadeOutUp(getDutyDefender(m),View.VISIBLE);
            imageCIV.setBackgroundResource(R.drawable.safe);
            imageCIV.setVisibility(View.VISIBLE);
        }else if (m.result.equals("STRIKE OUT!")){
            hideBatter();
            frontRIV.setBackgroundResource(R.drawable.strike_out);
            setZoomFast(frontRIV,View.VISIBLE,zoomSpeedFast);
        }else if (m.result.equals("BASE ON BALL!")){
            hideBatter();
            showRunner(m);
//            setZoomIn(afterIV,View.VISIBLE,zoomSpeedSlow);
//            afterIV.setBackgroundResource(R.drawable.base_on_ball);
        }else if (m.result.equals("SINGLE HIT!")){
            hideBatter();
            hideRunner();
        }else if (m.result.equals("DOUBLE HIT!")){
            hideBatter();
            hideRunner();
            out2BIV.setBackgroundResource(R.drawable.safe);
            out2BIV.setVisibility(View.VISIBLE);
        }else if (m.result.equals("TRIPPLE HIT!")){
            hideBatter();
            hideRunner();
        }else if (m.result.equals("HOMER!")){
            hideBatter();
            hideRunner();
            setBlink(afterIV,View.VISIBLE);
            afterIV.setBackgroundResource(R.drawable.homerun);
        }else if (m.result.equals("DOUBLE PLAY!")){
            batterLayout.setVisibility(View.INVISIBLE);

//            setZoomOut(afterIV,View.VISIBLE);
//            afterIV.setBackgroundResource(R.drawable.doubleplay);

            showRunner(m);
        }
    }
    private void afterResult(MatchDetail m){
        if (getContext() == null){
            return;
        }

        setZoomIn(afterIV,View.INVISIBLE,zoomSpeedSlow);
        if (m.result.equals("beginInning")){
            return;
        }
        hideView();
        drawScore(m);
        drawOrderLayout(m);
        if (m.result.equals("BUNT SUCCESS!")){
            showRunner(m);
        }else if (m.result.equals("BUNT FAIL!")) {
            showRunner(m);
//            b1RunnerLayout.setVisibility(View.VISIBLE);
        }else if (m.result.equals("DOUBLE PLAY!")){
            showRunner(m);
            b1RunnerLayout.setVisibility(View.INVISIBLE);
        }else if (m.result.equals("HOMER!")){
//            delayTime = 1000;
        }else if (m.result.equals("STRIKE OUT!")){
            frontRIV.setVisibility(View.INVISIBLE);
        }else if (m.result.equals("FIELD ERROR!") ||m.result.equals("FIELD OUT!") ||m.result.equals("SACRIFICE FLY!")){
            setFadeOutUp(getDutyDefender(m),View.INVISIBLE);
        }


        if (m.isFristHalf){
            if (previousScoreHome != m.homeTeamScore){
                if ((m.homeTeamScore -previousScoreHome) == 1){
                    frontRIV.setBackgroundResource(R.drawable.plus1);
                }else if ((m.homeTeamScore -previousScoreHome) == 2){
                    frontRIV.setBackgroundResource(R.drawable.plus2);
                }else if ((m.homeTeamScore -previousScoreHome) == 3){
                    frontRIV.setBackgroundResource(R.drawable.plus3);
                }else if ((m.homeTeamScore -previousScoreHome) == 4){
                    frontRIV.setBackgroundResource(R.drawable.plus4);
                }
                setFadeOutUp(frontRIV,View.VISIBLE);
            }
        }else {
            if (previousScoreAway != m.awayTeamScore){
                if ((m.awayTeamScore -previousScoreAway) == 1){
                    frontRIV.setBackgroundResource(R.drawable.plus1);
                }else if ((m.awayTeamScore -previousScoreAway) == 2){
                    frontRIV.setBackgroundResource(R.drawable.plus2);
                }else if ((m.awayTeamScore -previousScoreAway) == 3){
                    frontRIV.setBackgroundResource(R.drawable.plus3);
                }else if ((m.awayTeamScore -previousScoreAway) == 4){
                    frontRIV.setBackgroundResource(R.drawable.plus4);
                }
                setFadeOutUp(frontRIV,View.VISIBLE);
            }
        }

        if (m.outCount >2){
            clearInning(m);
        }else {
            setOutCount(m);
            showRunner(m);
            if (!m.result.equals("STEAL BASE FAIL!") && !m.result.equals("STEAL BASE SUCCESS!")){
                batterLayout.setVisibility(View.INVISIBLE);
            }

        }
    }

    private void initOrderLayout(MatchDetail m){
        if (getContext() == null){
            return;
        }
        for (int i = 0 ; i < homeNameList.size() ; i++){
            if (i == (homeNameList.size()-1)){
                homeNameList.get(i).setText(m.homeStartPitcher.getPitcherName());
                homeBackgroundList.get(i).setBackgroundResource(m.homeStartPitcher.getPlayerBackgroundResource());

            }else {
                homeNameList.get(i).setText(m.homeTeam.getBatterSquad().get(i).getBatterName());
                homeBackgroundList.get(i).setBackgroundResource(m.homeTeam.getBatterSquad().get(i).getPlayerBackgroundResource());

            }
        }
        for (int i = 0 ; i < awayNameList.size() ; i++){
            if (i == (awayNameList.size()-1)){
                awayNameList.get(i).setText(m.awayStartPitcher.getPitcherName());
                awayBackgroundList.get(i).setBackgroundResource(m.awayStartPitcher.getPlayerBackgroundResource());

            }else {
                awayNameList.get(i).setText(m.awayTeam.getBatterSquad().get(i).getBatterName());
                awayBackgroundList.get(i).setBackgroundResource(m.awayTeam.getBatterSquad().get(i).getPlayerBackgroundResource());

            }
        }
    }
    private void drawOrderLayout(MatchDetail m){
        if (getContext() == null){
            return;
        }
        if (m.result.equals("STEAL BASE SUCCESS!") ||m.result.equals("STEAL BASE FAIL!")) {
            return;
        }
        int color = ContextCompat.getColor(getContext(), R.color.colorPrimaryDark);

        if (m.isFristHalf){
            int i = m.homeBatterRoster.indexOf(m.currentBatter);
            homeResultList.get(i).setVisibility(View.VISIBLE);

            if (m.result.equals("BUNT SUCCESS!") || m.result.equals("BUNT FAIL!")){
                homeResultList.get(i).setText("BO");
                color = ContextCompat.getColor(getContext(), R.color.sacrifyColor);
            }else if (m.result.equals("FIELD ERROR!")){
                homeResultList.get(i).setText("EB");
                color = ContextCompat.getColor(getContext(), R.color.errorColor);
            }else if (m.result.equals("FIELD OUT!")){
                homeResultList.get(i).setText("OUT");
                color = ContextCompat.getColor(getContext(), R.color.outColor);
            }else if (m.result.equals("SACRIFICE FLY!")){
                homeResultList.get(i).setText("SF");
                color = ContextCompat.getColor(getContext(), R.color.sacrifyColor);
            }else if (m.result.equals("STRIKE OUT!")){
                homeResultList.get(i).setText("SO");
                color = ContextCompat.getColor(getContext(), R.color.outColor);
            }else if (m.result.equals("BASE ON BALL!")){
                homeResultList.get(i).setText("BB");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("SINGLE HIT!")){
                homeResultList.get(i).setText("1B");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("DOUBLE HIT!")){
                homeResultList.get(i).setText("2B");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("TRIPPLE HIT!")){
                homeResultList.get(i).setText("3B");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("HOMER!")){
                homeResultList.get(i).setText("HR");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("DOUBLE PLAY!")){
                homeResultList.get(i).setText("DO");
                color = ContextCompat.getColor(getContext(), R.color.outColor);
            }
            homeResultList.get(i).setBackgroundColor(color);
            // draw current pitcher
            Pitcher pitcher = m.currentPitcher;
            awayNameList.get(awayNameList.size()-1).setText(pitcher.getPitcherName());

            awayBackgroundList.get(awayBackgroundList.size()-1).setBackgroundResource(pitcher.getPlayerBackgroundResource());

        }else {
            int i = m.awayBatterRoaster.indexOf(m.currentBatter);
            awayResultList.get(i).setVisibility(View.VISIBLE);

            if (m.result.equals("BUNT SUCCESS!") || m.result.equals("BUNT FAIL!")){
                awayResultList.get(i).setText("BO");
                color = ContextCompat.getColor(getContext(), R.color.sacrifyColor);
            }else if (m.result.equals("FIELD ERROR!")){
                awayResultList.get(i).setText("EB");
                color = ContextCompat.getColor(getContext(), R.color.errorColor);
            }else if (m.result.equals("FIELD OUT!")){
                awayResultList.get(i).setText("OUT");
                color = ContextCompat.getColor(getContext(), R.color.outColor);
            }else if (m.result.equals("SACRIFICE FLY!")){
                awayResultList.get(i).setText("SF");
                color = ContextCompat.getColor(getContext(), R.color.sacrifyColor);
            }else if (m.result.equals("STRIKE OUT!")){
                awayResultList.get(i).setText("SO");
                color = ContextCompat.getColor(getContext(), R.color.outColor);
            }else if (m.result.equals("BASE ON BALL!")){
                awayResultList.get(i).setText("BB");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("SINGLE HIT!")){
                awayResultList.get(i).setText("1B");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("DOUBLE HIT!")){
                awayResultList.get(i).setText("2B");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("TRIPPLE HIT!")){
                awayResultList.get(i).setText("3B");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("HOMER!")){
                awayResultList.get(i).setText("HR");
                color = ContextCompat.getColor(getContext(), R.color.hitColor);
            }else if (m.result.equals("DOUBLE PLAY!")){
                awayResultList.get(i).setText("DO");
                color = ContextCompat.getColor(getContext(), R.color.outColor);
            }
            awayResultList.get(i).setBackgroundColor(color);

            // draw current pitcher
            Pitcher pitcher = m.currentPitcher;
            homeNameList.get(homeNameList.size()-1).setText(pitcher.getPitcherName());

            homeBackgroundList.get(homeBackgroundList.size()-1).setBackgroundResource(pitcher.getPlayerBackgroundResource());

        }

    }
    private void setLastResult(ImageView imageView, int willVisibile){
        if (getContext() == null){
            return;
        }
        imageView.setVisibility(willVisibile);
        anim = AnimationUtils.loadAnimation(getContext(),R.anim.zoom_out_last);
        if (imageView.getVisibility() == View.VISIBLE){
            imageView.startAnimation(anim);
        }else {
            imageView.clearAnimation();
        }
    }
    private void setFadeOutUp(ImageView imageView, int willVisibile){
        if (getContext() == null){
            return;
        }
        imageView.setVisibility(willVisibile);
        anim = AnimationUtils.loadAnimation(getContext(),R.anim.fadeout_up);
        if (imageView.getVisibility() == View.VISIBLE){
            imageView.startAnimation(anim);
        }else {
            imageView.clearAnimation();
        }
    }
    private void setBlink(ImageView imageView, int willVisibile){
        if (getContext() == null){
            return;
        }
        imageView.setVisibility(willVisibile);
        anim = AnimationUtils.loadAnimation(getContext(),R.anim.blink);
        if (imageView.getVisibility() == View.VISIBLE){
            imageView.startAnimation(anim);
        }else {
            imageView.clearAnimation();
        }
    }
    private void setBlinkSlow(TextView imageView, int willVisibile){
        if (getContext() == null){
            return;
        }

        anim = AnimationUtils.loadAnimation(getContext(),R.anim.alph);

        if (willVisibile == View.VISIBLE){
//            Log.e("j13","visible");
            imageView.setVisibility(View.VISIBLE);
            imageView.startAnimation(anim);

        }else {
//            Log.e("j13","invisible");
            imageView.setVisibility(View.INVISIBLE);
            imageView.clearAnimation();
        }
    }
    private void setLeftToRightAnim(ImageView imageView, int willVisibile){
        if (getContext() == null){
            return;
        }
        imageView.setVisibility(willVisibile);
        anim = AnimationUtils.loadAnimation(getContext(),R.anim.bounce);
        if (imageView.getVisibility() == View.VISIBLE){
            imageView.startAnimation(anim);
        }else {
            imageView.clearAnimation();
        }
    }
    private void setZoomIn(ImageView imageView, int willVisibile,int speed){
        if (getContext() == null){
            return;
        }
        imageView.setVisibility(willVisibile);
        anim = AnimationUtils.loadAnimation(getContext(),R.anim.zoom_in);
        anim.setDuration(speed);
        if (imageView.getVisibility() == View.VISIBLE){
            imageView.startAnimation(anim);
        }else {
            imageView.clearAnimation();
        }
    }
    private void setZoomFast(ImageView imageView, int willVisibile,int speed){
        if (getContext() == null){
            return;
        }
        imageView.setVisibility(willVisibile);
        anim = AnimationUtils.loadAnimation(getContext(),R.anim.zomm_in_fast);
        anim.setDuration(speed);
        if (imageView.getVisibility() == View.VISIBLE){
            imageView.startAnimation(anim);
        }else {
            imageView.clearAnimation();
        }
    }
    private void setZoomOut(ImageView imageView, int willVisibile){
        if (getContext() == null){
            return;
        }
        imageView.setVisibility(willVisibile);
        anim = AnimationUtils.loadAnimation(getContext(),R.anim.zoom_out);
        if (imageView.getVisibility() == View.VISIBLE){
            imageView.startAnimation(anim);
        }else {
            imageView.clearAnimation();
        }
    }

    private boolean isGroundballOut(MatchDetail m){
        if (m.defenceBatter.getDefensePosition() < 5 && !m.isFirstBon && m.defenceBatter.getDefensePosition() != 1){
            return true;
        }else {
            return false;
        }
    }

    private ImageView getDutyDefender(MatchDetail m){
        ImageView imageView;
//        Log.e("j13 ", "defense position " + ""+ m.defenceBatter.defensePosition+"");
//        Log.e("j13 ", "defense position " + ""+ m.toString());
        int defPosition = m.defenceBatter.getDefensePosition();
        switch (defPosition){
            case 0 :
                imageView = imageCIV;
                break;
            case 1 :
                imageView = out1BIV;
                break;
            case 2 :
                imageView = image2BIV;
                break;
            case 3 :
                imageView = out3BIV;
                break;
            case GlobalValue.shortStop :
                imageView = imageSSIV;
                break;
            case GlobalValue.leftField :
                imageView = imageLFIV;
                break;
            case 6 :
                imageView = imageCFIV;
                break;
            case 7 :
                imageView = imageRFIV;
                break;
            default:
                imageView = null;
//                    Log.e("j13","error in get defender!!!!!!!!!!!!!!!!!");
                break;
        }
        return imageView;
    }

    private ImageView getHomerunIV(){
        ImageView imageView;

        int randomInt = random.nextInt(3);

        switch (randomInt){
            case 0 :
                imageView = imageHomerRightIV;
                imageView.setBackgroundResource(R.drawable.homerun);
                break;
            case 1 :
                imageView = imageHomerCenterIV;
                imageView.setBackgroundResource(R.drawable.homerun);
                break;
            case 2 :
                imageView = imageHomerLeftIV;
                imageView.setBackgroundResource(R.drawable.homerun);
                break;
            default:
                imageView = null;
                break;
        }
        return imageView;
    }

    private ImageView getTripleHitIV(){
        ImageView imageView;

        int randomInt = random.nextInt(2);

        switch (randomInt){
            case 0 :
                imageView = imageTripeRightIV;
                imageView.setBackgroundResource(R.drawable.tripple_hit);
                break;
            case 1 :
                imageView = imageTripeLeftIV;
                imageView.setBackgroundResource(R.drawable.tripple_hit);
                break;
            default:
                imageView = null;
                break;
        }
        return imageView;
    }

    private ImageView getDoubleHitIV(){
        ImageView imageView;

        int randomInt = random.nextInt(3);

        switch (randomInt){
            case 0 :
                imageView = imageRFIV;
                imageView.setBackgroundResource(R.drawable.double_hit);
                break;
            case 1 :
                imageView = imageCFIV;
                imageView.setBackgroundResource(R.drawable.double_hit);
                break;
            case 2 :
                imageView = imageLFIV;
                imageView.setBackgroundResource(R.drawable.double_hit);
                break;
            default:
                imageView = null;
                break;
        }
        return imageView;
    }

    private ImageView getSingleHitIV(){
        ImageView imageView;

        int randomInt = random.nextInt(5);

        switch (randomInt){
            case 0 :
                imageView = image2BIV;
                imageView.setBackgroundResource(R.drawable.single_hit);
                break;
            case 1 :
                imageView = imageSSIV;
                imageView.setBackgroundResource(R.drawable.single_hit);
                break;
            case 2 :
                imageView = imageRFIV;
                imageView.setBackgroundResource(R.drawable.single_hit);
                break;
            case 3 :
                imageView = imageCFIV;
                imageView.setBackgroundResource(R.drawable.single_hit);
                break;
            case 4 :
                imageView = imageLFIV;
                imageView.setBackgroundResource(R.drawable.single_hit);
                break;
            default:
                imageView = null;
                break;
        }
        return imageView;
    }
    private void drawScore(MatchDetail m){
        if (getContext() == null){
            return;
        }
        homeScoreTV.setText(m.homeTeamScore+"");
        inningTV.setText(m.inning+"");
        awayScoreTV.setText(m.awayTeamScore+"");
        if (m.isFristHalf){
            up.setBackgroundResource(R.drawable.white_up);
            down.setBackgroundResource(R.drawable.black_down);
        }else {
            up.setBackgroundResource(R.drawable.black_up);
            down.setBackgroundResource(R.drawable.white_down);
        }
    }

    private void showBatterPitcher(MatchDetail m){
        if (getContext() == null){
            return;
        }
        batterNameTV.setText(m.currentBatter.getBatterName());
        pitcherNameTV.setText(m.currentPitcher.getPitcherName());


        batterPositionTV.setText(GlobalValue.getPositionString(m.currentBatter.getDefensePosition()));
        pitcherPositionTV.setText(m.currentPitcher.getPositionString());


        if (m.currentBatter.getAverageStat() > 109){
            batterTotalStatTV.setText(""+(m.currentBatter.getAverageStat()-100));
            batterTotalStatTV.setTextColor(getResources().getColor(R.color.hitColor));
        }else if (m.currentBatter.getAverageStat() > 99){
            batterTotalStatTV.setText("0"+(m.currentBatter.getAverageStat()-100));
            batterTotalStatTV.setTextColor(getResources().getColor(R.color.hitColor));
        }else {
            batterTotalStatTV.setText(""+m.currentBatter.getAverageStat());
            batterTotalStatTV.setTextColor(getResources().getColor(R.color.colorAwayTeam));
        }
        if (m.currentPitcher.getAverageStat() > 109){
            pitcherTotalStatTV.setText(""+ (m.currentPitcher.getAverageStat()-100));
            pitcherTotalStatTV.setTextColor(getResources().getColor(R.color.hitColor));
        }else if (m.currentPitcher.getAverageStat() > 99){
            pitcherTotalStatTV.setText("0"+ (m.currentPitcher.getAverageStat()-100));
            pitcherTotalStatTV.setTextColor(getResources().getColor(R.color.hitColor));
        }else {
            pitcherTotalStatTV.setText(""+ m.currentPitcher.getAverageStat());
            pitcherTotalStatTV.setTextColor(getResources().getColor(R.color.colorAwayTeam));
        }

        pitcherBackgroud.setBackgroundResource(m.currentPitcher.getPlayerBackgroundResource());

        batterBackground.setBackgroundResource(m.currentBatter.getPlayerBackgroundResource());



        if (m.isFristHalf){
            batterNameTV.setTextColor(ContextCompat.getColor(getContext(), R.color.colorHomeTeam));
            pitcherNameTV.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAwayTeam));
        }else {
            batterNameTV.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAwayTeam));
            pitcherNameTV.setTextColor(ContextCompat.getColor(getContext(), R.color.colorHomeTeam));
        }

        //TODO j13 add skill 2
        if (m.isShowingPitcherSkill1){
            setBlinkSlow(pitcherSkillTV,View.VISIBLE);
            pitcherSkillLayout.setVisibility(View.VISIBLE);
            if (m.currentPitcher.getPitcherSkill1().getSkillLevel() == 1){
                pitcherSkillTV.setBackgroundResource(R.drawable.skilllevel1);
            }else if (m.currentPitcher.getPitcherSkill1().getSkillLevel() == 2){
                pitcherSkillTV.setBackgroundResource(R.drawable.skilllevel2);
            }else if (m.currentPitcher.getPitcherSkill1().getSkillLevel() == 3){
                pitcherSkillTV.setBackgroundResource(R.drawable.skilllevel3);
            }
            pitcherSkillTV.setText(m.currentPitcher.getPitcherSkill1().getSkillName());
        }else {
            setBlinkSlow(pitcherSkillTV,View.INVISIBLE);
            pitcherSkillLayout.setVisibility(View.INVISIBLE);
        }
        if (m.isShowingBatterSkill1){
            batterSkillLayout.setVisibility(View.VISIBLE);
            setBlinkSlow(batterSkillTV,View.VISIBLE);

            if (m.currentBatter.getBatterSkill1().getSkillLevel() == 1){
                batterSkillTV.setBackgroundResource(R.drawable.skilllevel1);
            }else if (m.currentBatter.getBatterSkill1().getSkillLevel() == 2){
                batterSkillTV.setBackgroundResource(R.drawable.skilllevel2);
            }else if (m.currentBatter.getBatterSkill1().getSkillLevel() == 3){
                batterSkillTV.setBackgroundResource(R.drawable.skilllevel3);
            }

            batterSkillTV.setText(m.currentBatter.getBatterSkill1().getSkillName());
        }else {
            setBlinkSlow(batterSkillTV,View.INVISIBLE);
            batterSkillLayout.setVisibility(View.INVISIBLE);
        }
    }


    private void showRunner(MatchDetail m){
        if (getContext() == null){
            return;
        }
        b1RunnerLayout.setVisibility(View.INVISIBLE);
        b2RunnerLayout.setVisibility(View.INVISIBLE);
        b3RunnerLayout.setVisibility(View.INVISIBLE);
        int colorInt;
        if (m.isFristHalf){
            colorInt = ContextCompat.getColor(getContext(), R.color.colorHomeTeam);
        }else {
            colorInt = ContextCompat.getColor(getContext(), R.color.colorAwayTeam);
        }

        if (m.isFirstBon){
            b1RunnerLayout.setVisibility(View.VISIBLE);
            b1RunnerNameTV.setTextColor(colorInt);
            b1RunnerNameTV.setText(m.firstBasePlayer.getBatterName());

            if (m.firstBasePlayer.getSpeedWithItem() > 109){
                b1RunnerSpeedStatTV.setText((m.firstBasePlayer.getSpeedWithItem()-100)+"");
                b1RunnerSpeedStatTV.setTextColor(getResources().getColor(R.color.hitColor));
            }else if (m.firstBasePlayer.getSpeedWithItem() > 99){
                b1RunnerSpeedStatTV.setText("0"+(m.firstBasePlayer.getSpeedWithItem()-100));
                b1RunnerSpeedStatTV.setTextColor(getResources().getColor(R.color.hitColor));
            }else {
                b1RunnerSpeedStatTV.setText(m.firstBasePlayer.getSpeedWithItem()+"");
                b1RunnerSpeedStatTV.setTextColor(Color.WHITE);
            }
        }
        if (m.isSecondBon){
            b2RunnerLayout.setVisibility(View.VISIBLE);
            b2RunnerNameTV.setTextColor(colorInt);
            b2RunnerNameTV.setText(m.secondBasePlayer.getBatterName());
            b2RunnerSpeedStatTV.setText(m.secondBasePlayer.getSpeedWithItem()+"");

            if (m.secondBasePlayer.getSpeedWithItem() > 109){
                b2RunnerSpeedStatTV.setText((m.secondBasePlayer.getSpeedWithItem()-100)+"");
                b2RunnerSpeedStatTV.setTextColor(getResources().getColor(R.color.hitColor));
            }else if (m.secondBasePlayer.getSpeedWithItem() > 99){
                b2RunnerSpeedStatTV.setText("0"+(m.secondBasePlayer.getSpeedWithItem()-100));
                b2RunnerSpeedStatTV.setTextColor(getResources().getColor(R.color.hitColor));
            }else {
                b2RunnerSpeedStatTV.setText(m.secondBasePlayer.getSpeedWithItem()+"");
                b2RunnerSpeedStatTV.setTextColor(Color.WHITE);
            }
        }
        if (m.isThirdBon){
            b3RunnerLayout.setVisibility(View.VISIBLE);
            b3RunnerNameTV.setTextColor(colorInt);
            b3RunnerNameTV.setText(m.thirdBasePlayer.getBatterName());

            if (m.thirdBasePlayer.getSpeedWithItem() > 109){
                b3RunnerSpeedStatTV.setText((m.thirdBasePlayer.getSpeedWithItem()-100)+"");
                b3RunnerSpeedStatTV.setTextColor(getResources().getColor(R.color.hitColor));
            }else if (m.thirdBasePlayer.getSpeedWithItem() > 99){
                b3RunnerSpeedStatTV.setText("0"+(m.thirdBasePlayer.getSpeedWithItem()-100));
                b3RunnerSpeedStatTV.setTextColor(getResources().getColor(R.color.hitColor));
            }else {
                b3RunnerSpeedStatTV.setText(m.thirdBasePlayer.getSpeedWithItem()+"");
                b3RunnerSpeedStatTV.setTextColor(Color.WHITE);
            }
        }
    }



    private Batter getBatterWithPosition(int defPosition, MatchDetail m){
        Batter returnCatcher=null;
        ArrayList<Batter> batterList = new ArrayList<Batter>();

        if (m.isFristHalf){
            batterList = m.awayTeam.getBatterSquad();
        }else {
            batterList = m.homeTeam.getBatterSquad();
        }

        for (int i=0 ; i < batterList.size() ; i++){
            if (batterList.get(i).getDefensePosition() == defPosition && batterList.get(i).getDefensePosition() != GlobalValue.DH){
                returnCatcher = batterList.get(i);
            }
        }
        return returnCatcher;
    }
    private void clearCount(MatchDetail m){
        if (getContext() == null){
            return;
        }
        b1.setBackgroundResource(R.drawable.black_light);
        b2.setBackgroundResource(R.drawable.black_light);
        b3.setBackgroundResource(R.drawable.black_light);
        s1.setBackgroundResource(R.drawable.black_light);
        s2.setBackgroundResource(R.drawable.black_light);
    }
    private void setTwoStrike(){
        if (getContext() == null){
            return;
        }
        diceForball = randomInt.nextInt(4);

        if (diceForball == 0){
            b1.setBackgroundResource(R.drawable.black_light);
            b2.setBackgroundResource(R.drawable.black_light);
            b3.setBackgroundResource(R.drawable.black_light);
        }else if (diceForball == 1){
            b1.setBackgroundResource(R.drawable.green_light);
            b2.setBackgroundResource(R.drawable.black_light);
            b3.setBackgroundResource(R.drawable.black_light);
        }else if (diceForball == 2){
            b1.setBackgroundResource(R.drawable.green_light);
            b2.setBackgroundResource(R.drawable.green_light);
            b3.setBackgroundResource(R.drawable.black_light);
        }else {
            b1.setBackgroundResource(R.drawable.green_light);
            b2.setBackgroundResource(R.drawable.green_light);
            b3.setBackgroundResource(R.drawable.green_light);
        }
        s1.setBackgroundResource(R.drawable.yellow_light);
        s2.setBackgroundResource(R.drawable.yellow_light);
    }
    private void setThreeBall(){
        if (getContext() == null){
            return;
        }
        diceForStrike = randomInt.nextInt(3);


        b1.setBackgroundResource(R.drawable.green_light);
        b2.setBackgroundResource(R.drawable.green_light);
        b3.setBackgroundResource(R.drawable.green_light);

        if (diceForStrike == 0){
            s1.setBackgroundResource(R.drawable.black_light);
            s2.setBackgroundResource(R.drawable.black_light);
        }else if (diceForStrike == 1){
            s1.setBackgroundResource(R.drawable.yellow_light);
            s2.setBackgroundResource(R.drawable.black_light);
        }else {
            s1.setBackgroundResource(R.drawable.yellow_light);
            s2.setBackgroundResource(R.drawable.yellow_light);
        }
    }
    private void setOutCount(MatchDetail m){
        if (getContext() == null){
            return;
        }
        clearCount(m);
        if (m.outCount == 0){
            o1.setBackgroundResource(R.drawable.black_light);
            o2.setBackgroundResource(R.drawable.black_light);
        }else if (m.outCount ==1){
            o1.setBackgroundResource(R.drawable.red_light);
            o2.setBackgroundResource(R.drawable.black_light);
        }else if (m.outCount ==2){
            o1.setBackgroundResource(R.drawable.red_light);
            o2.setBackgroundResource(R.drawable.red_light);
        }
    }
    private void clearInning(MatchDetail m){
        if (getContext() == null){
            return;
        }
        b1.setBackgroundResource(R.drawable.black_light);
        b2.setBackgroundResource(R.drawable.black_light);
        b3.setBackgroundResource(R.drawable.black_light);
        s1.setBackgroundResource(R.drawable.black_light);
        s2.setBackgroundResource(R.drawable.black_light);
        o1.setBackgroundResource(R.drawable.black_light);
        o2.setBackgroundResource(R.drawable.black_light);

        if (m.isFristHalf){
            up.setBackgroundResource(R.drawable.white_up);
            down.setBackgroundResource(R.drawable.black_down);
            for (TextView textView : homeResultList){
                textView.setVisibility(View.INVISIBLE);
            }
        }else {
            up.setBackgroundResource(R.drawable.black_up);
            down.setBackgroundResource(R.drawable.white_down);
            for (TextView textView : awayResultList){
                textView.setVisibility(View.INVISIBLE);
            }
        }
        batterLayout.setVisibility(View.INVISIBLE);
        b1RunnerLayout.setVisibility(View.INVISIBLE);
        b2RunnerLayout.setVisibility(View.INVISIBLE);
        b3RunnerLayout.setVisibility(View.INVISIBLE);

    }
    private void hideRunner(){
        b1RunnerLayout.setVisibility(View.INVISIBLE);
        b2RunnerLayout.setVisibility(View.INVISIBLE);
        b3RunnerLayout.setVisibility(View.INVISIBLE);
    }
    private void hideBatter(){
        batterLayout.setVisibility(View.INVISIBLE);
    }
    private void showBatter(){
        batterLayout.setVisibility(View.VISIBLE);
    }
    private void showCount(MatchDetail m){
        if (getContext() == null){
            return;
        }
        diceForball = randomInt.nextInt(4);
        diceForStrike = randomInt.nextInt(3);

        if (diceForball == 0){
            b1.setBackgroundResource(R.drawable.black_light);
            b2.setBackgroundResource(R.drawable.black_light);
            b3.setBackgroundResource(R.drawable.black_light);
        }else if (diceForball == 1){
            b1.setBackgroundResource(R.drawable.green_light);
            b2.setBackgroundResource(R.drawable.black_light);
            b3.setBackgroundResource(R.drawable.black_light);
        }else if (diceForball == 2){
            b1.setBackgroundResource(R.drawable.green_light);
            b2.setBackgroundResource(R.drawable.green_light);
            b3.setBackgroundResource(R.drawable.black_light);
        }else {
            b1.setBackgroundResource(R.drawable.green_light);
            b2.setBackgroundResource(R.drawable.green_light);
            b3.setBackgroundResource(R.drawable.green_light);
        }

        if (diceForStrike == 0){
            s1.setBackgroundResource(R.drawable.black_light);
            s2.setBackgroundResource(R.drawable.black_light);
        }else if (diceForStrike == 1){
            s1.setBackgroundResource(R.drawable.yellow_light);
            s2.setBackgroundResource(R.drawable.black_light);
        }else {
            s1.setBackgroundResource(R.drawable.yellow_light);
            s2.setBackgroundResource(R.drawable.yellow_light);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (t != null){
            t = null;
            handler.removeCallbacks(runnable);

        }
    }
    boolean speedFlag = true;

    private void hideView(){
        frontRIV.setVisibility(View.INVISIBLE);
        afterIV.setVisibility(View.INVISIBLE);
        imageCIV.setVisibility(View.INVISIBLE);
        out1BIV.setVisibility(View.INVISIBLE);
        out2BIV.setVisibility(View.INVISIBLE);
        out3BIV.setVisibility(View.INVISIBLE);
        imageRFIV.setVisibility(View.INVISIBLE);
        imageCFIV.setVisibility(View.INVISIBLE);
        imageLFIV.setVisibility(View.INVISIBLE);
        image2BIV.setVisibility(View.INVISIBLE);
        imageSSIV.setVisibility(View.INVISIBLE);
        imageTripeRightIV.setVisibility(View.INVISIBLE);
        imageTripeLeftIV.setVisibility(View.INVISIBLE);
        imageHomerRightIV.setVisibility(View.INVISIBLE);
        imageHomerCenterIV.setVisibility(View.INVISIBLE);
        imageHomerLeftIV.setVisibility(View.INVISIBLE);
    }
    private void initUI(View view){
//        bInstance.setIsFromPlayScreen(true);
        matchDetails = BaseCoreActivity.getInstance().getMatchDetails();
        isHiding = true;
        zoomSpeedFast=100;
        zoomSpeedSlow = 1500;

        previousScoreHome = 0;
        previousScoreAway = 0;
        frontRIV = (ImageView) view.findViewById(R.id.frontResultIV);

        afterIV = (ImageView) view.findViewById(R.id.afterRIV);
        imageCIV = (ImageView) view.findViewById(R.id.imageCIV);
        out1BIV = (ImageView) view.findViewById(R.id.out1BIV);
        out2BIV = (ImageView) view.findViewById(R.id.out2BIV);
        out3BIV = (ImageView) view.findViewById(R.id.out3BIV);
        imageRFIV = (ImageView) view.findViewById(R.id.imageRFIV);
        imageCFIV = (ImageView) view.findViewById(R.id.imageCFIV);
        imageLFIV = (ImageView) view.findViewById(R.id.imageLFIV);
        image2BIV = (ImageView) view.findViewById(R.id.image2BIV);
        imageSSIV = (ImageView) view.findViewById(R.id.imageSSIV);
        imageTripeRightIV = (ImageView) view.findViewById(R.id.imageTripeRightIV);
        imageTripeLeftIV = (ImageView) view.findViewById(R.id.imageTripeLeftIV);
        imageHomerRightIV = (ImageView) view.findViewById(R.id.imageHomerRightIV);
        imageHomerCenterIV = (ImageView) view.findViewById(R.id.imageHomerCenterIV);
        imageHomerLeftIV = (ImageView) view.findViewById(R.id.imageHomerLeftIV);

        hideView();

        b1RunnerNameTV = (TextView) view.findViewById(R.id.b1RunnerNameTV);
        b2RunnerNameTV = (TextView) view.findViewById(R.id.b2RunnerNameTV);
        b3RunnerNameTV = (TextView) view.findViewById(R.id.b3RunnerNameTV);

        b1RunnerSpeedStatTV = (TextView) view.findViewById(R.id.b1RunnerSpeedStatTV);
        b2RunnerSpeedStatTV = (TextView) view.findViewById(R.id.b2RunnerSpeedStatTV);
        b3RunnerSpeedStatTV = (TextView) view.findViewById(R.id.b3RunnerSpeedStatTV);

        b1RunnerLayout = (LinearLayout) view.findViewById(R.id.b1RunnerLayout);
        b2RunnerLayout = (LinearLayout) view.findViewById(R.id.b2RunnerLayout);
        b3RunnerLayout = (LinearLayout) view.findViewById(R.id.b3RunnerLayout);

        batterLayout = (LinearLayout) view.findViewById(R.id.batterFrame);
        pitcherLayout = (LinearLayout) view.findViewById(R.id.pitcherFrame);

        homeScoreTV = (TextView) view.findViewById(R.id.homeScoreTV);
        inningTV = (TextView) view.findViewById(R.id.inningTV);
        awayScoreTV = (TextView) view.findViewById(R.id.awayScoreTV);
        homeTeamNameTV = (TextView) view.findViewById(R.id.homeTeamNameTV);
        awayTeamNameTV = (TextView) view.findViewById(R.id.awayTeamNameTV);
        batterSkillTV = (TextView) view.findViewById(R.id.batterSkillTV);
        pitcherSkillTV = (TextView) view.findViewById(R.id.pitcherSkillTV);
        batterNameTV = (TextView) view.findViewById(R.id.batterNameTV);
        pitcherNameTV = (TextView) view.findViewById(R.id.pitcherNameTV);
        pitcherSkillLayout = (LinearLayout) view.findViewById(R.id.pitcherSkillLayout);
        batterSkillLayout = (LinearLayout) view.findViewById(R.id.batterSkillLayout);

        b1 = (ImageView)view.findViewById(R.id.ballLight1);
        b2 = (ImageView)view.findViewById(R.id.ballLight2);
        b3 = (ImageView)view.findViewById(R.id.ballLight3);
        s1 = (ImageView)view.findViewById(R.id.strikeLight1);
        s2 = (ImageView)view.findViewById(R.id.strikeLight2);
        o1 = (ImageView)view.findViewById(R.id.outLight1);
        o2 = (ImageView)view.findViewById(R.id.outLight2);
        up = (ImageView)view.findViewById(R.id.upArrow);
        down = (ImageView)view.findViewById(R.id.downArrow);

        h1l = (LinearLayout) view.findViewById(R.id.h1L);
        h2l = (LinearLayout) view.findViewById(R.id.h2L);
        h3l = (LinearLayout) view.findViewById(R.id.h3L);
        h4l = (LinearLayout) view.findViewById(R.id.h4L);
        h5l = (LinearLayout) view.findViewById(R.id.h5L);
        h6l = (LinearLayout) view.findViewById(R.id.h6L);
        h7l = (LinearLayout) view.findViewById(R.id.h7L);
        h8l = (LinearLayout) view.findViewById(R.id.h8L);
        h9l = (LinearLayout) view.findViewById(R.id.h9L);
        hPl = (LinearLayout) view.findViewById(R.id.hPL);

        a1l = (LinearLayout) view.findViewById(R.id.a1L);
        a2l = (LinearLayout) view.findViewById(R.id.a2L);
        a3l = (LinearLayout) view.findViewById(R.id.a3L);
        a4l = (LinearLayout) view.findViewById(R.id.a4L);
        a5l = (LinearLayout) view.findViewById(R.id.a5L);
        a6l = (LinearLayout) view.findViewById(R.id.a6L);
        a7l = (LinearLayout) view.findViewById(R.id.a7L);
        a8l = (LinearLayout) view.findViewById(R.id.a8L);
        a9l = (LinearLayout) view.findViewById(R.id.a9L);
        aPl = (LinearLayout) view.findViewById(R.id.aPL);

        h1NTV = (TextView) view.findViewById(R.id.h1NTV);
        h2NTV = (TextView) view.findViewById(R.id.h2NTV);
        h3NTV = (TextView) view.findViewById(R.id.h3NTV);
        h4NTV = (TextView) view.findViewById(R.id.h4NTV);
        h5NTV = (TextView) view.findViewById(R.id.h5NTV);
        h6NTV = (TextView) view.findViewById(R.id.h6NTV);
        h7NTV = (TextView) view.findViewById(R.id.h7NTV);
        h8NTV = (TextView) view.findViewById(R.id.h8NTV);
        h9NTV = (TextView) view.findViewById(R.id.h9NTV);
        hPNTV = (TextView) view.findViewById(R.id.hPNTV);

        a1NTV = (TextView) view.findViewById(R.id.a1NTV);
        a2NTV = (TextView) view.findViewById(R.id.a2NTV);
        a3NTV = (TextView) view.findViewById(R.id.a3NTV);
        a4NTV = (TextView) view.findViewById(R.id.a4NTV);
        a5NTV = (TextView) view.findViewById(R.id.a5NTV);
        a6NTV = (TextView) view.findViewById(R.id.a6NTV);
        a7NTV = (TextView) view.findViewById(R.id.a7NTV);
        a8NTV = (TextView) view.findViewById(R.id.a8NTV);
        a9NTV = (TextView) view.findViewById(R.id.a9NTV);
        aPNTV = (TextView) view.findViewById(R.id.aPNTV);

        h1TV = (TextView) view.findViewById(R.id.h1TV);
        h2TV = (TextView) view.findViewById(R.id.h2TV);
        h3TV = (TextView) view.findViewById(R.id.h3TV);
        h4TV = (TextView) view.findViewById(R.id.h4TV);
        h5TV = (TextView) view.findViewById(R.id.h5TV);
        h6TV = (TextView) view.findViewById(R.id.h6TV);
        h7TV = (TextView) view.findViewById(R.id.h7TV);
        h8TV = (TextView) view.findViewById(R.id.h8TV);
        h9TV = (TextView) view.findViewById(R.id.h9TV);

        a1TV = (TextView) view.findViewById(R.id.a1TV);
        a2TV = (TextView) view.findViewById(R.id.a2TV);
        a3TV = (TextView) view.findViewById(R.id.a3TV);
        a4TV = (TextView) view.findViewById(R.id.a4TV);
        a5TV = (TextView) view.findViewById(R.id.a5TV);
        a6TV = (TextView) view.findViewById(R.id.a6TV);
        a7TV = (TextView) view.findViewById(R.id.a7TV);
        a8TV = (TextView) view.findViewById(R.id.a8TV);
        a9TV = (TextView) view.findViewById(R.id.a9TV);


        batterPositionTV  = (TextView) view.findViewById(R.id.batterPositionTV);
        pitcherPositionTV  = (TextView) view.findViewById(R.id.pitcherPositionTV);

        batterTotalStatTV = (TextView) view.findViewById(R.id.batterTotalStatTV);
        pitcherTotalStatTV = (TextView) view.findViewById(R.id.pitcherTotalStatTV);


        homeBackgroundList = new ArrayList<>();
        homeBackgroundList.add(h1l);
        homeBackgroundList.add(h2l);
        homeBackgroundList.add(h3l);
        homeBackgroundList.add(h4l);
        homeBackgroundList.add(h5l);
        homeBackgroundList.add(h6l);
        homeBackgroundList.add(h7l);
        homeBackgroundList.add(h8l);
        homeBackgroundList.add(h9l);
        homeBackgroundList.add(hPl);

        awayBackgroundList = new ArrayList<>();
        awayBackgroundList.add(a1l);
        awayBackgroundList.add(a2l);
        awayBackgroundList.add(a3l);
        awayBackgroundList.add(a4l);
        awayBackgroundList.add(a5l);
        awayBackgroundList.add(a6l);
        awayBackgroundList.add(a7l);
        awayBackgroundList.add(a8l);
        awayBackgroundList.add(a9l);
        awayBackgroundList.add(aPl);

        homeNameList = new ArrayList<>();
        homeNameList.add(h1NTV);
        homeNameList.add(h2NTV);
        homeNameList.add(h3NTV);
        homeNameList.add(h4NTV);
        homeNameList.add(h5NTV);
        homeNameList.add(h6NTV);
        homeNameList.add(h7NTV);
        homeNameList.add(h8NTV);
        homeNameList.add(h9NTV);
        homeNameList.add(hPNTV);

        awayNameList = new ArrayList<>();
        awayNameList.add(a1NTV);
        awayNameList.add(a2NTV);
        awayNameList.add(a3NTV);
        awayNameList.add(a4NTV);
        awayNameList.add(a5NTV);
        awayNameList.add(a6NTV);
        awayNameList.add(a7NTV);
        awayNameList.add(a8NTV);
        awayNameList.add(a9NTV);
        awayNameList.add(aPNTV);

        homeResultList = new ArrayList<>();
        homeResultList.add(h1TV);
        homeResultList.add(h2TV);
        homeResultList.add(h3TV);
        homeResultList.add(h4TV);
        homeResultList.add(h5TV);
        homeResultList.add(h6TV);
        homeResultList.add(h7TV);
        homeResultList.add(h8TV);
        homeResultList.add(h9TV);

        awayResultList = new ArrayList<>();
        awayResultList.add(a1TV);
        awayResultList.add(a2TV);
        awayResultList.add(a3TV);
        awayResultList.add(a4TV);
        awayResultList.add(a5TV);
        awayResultList.add(a6TV);
        awayResultList.add(a7TV);
        awayResultList.add(a8TV);
        awayResultList.add(a9TV);

        batterBackground = (LinearLayout) view.findViewById(R.id.batterBackgroud);
        pitcherBackgroud =(LinearLayout) view.findViewById(R.id.pitcherBackground);
        skipBtn = (Button) view.findViewById(R.id.skipButton);
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                bInstance.addAsRootFragment(new1 ResultFragment());
            }
        });
        x2Btn = (Button) view.findViewById(R.id.x2Btn);
        x2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speedFlag = !speedFlag;
                if (speedFlag){
                    x2Btn.setText("X1");
                    delayLong = 600;
                    delayShort = 1000;
                }else {
                    x2Btn.setText("X2");
                    delayLong = 300;
                    delayShort = 500;
                }


            }
        });
    }

}