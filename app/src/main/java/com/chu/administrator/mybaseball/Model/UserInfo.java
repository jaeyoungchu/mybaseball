package com.chu.administrator.mybaseball.Model;

import io.realm.RealmObject;

/**
 * Created by Administrator on 2018-06-12.
 */

public class UserInfo extends RealmObject{

    String userId,mode;

    public UserInfo() {
        this.userId = "";
        this.mode = "";
    }

    public UserInfo(String userId,String mode) {
        this.userId = userId;
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
