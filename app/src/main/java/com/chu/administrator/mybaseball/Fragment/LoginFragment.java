package com.chu.administrator.mybaseball.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.chu.administrator.mybaseball.Fragment.ManagerMode.ManagerMainFragment;
import com.chu.administrator.mybaseball.Fragment.PlayerMode.PlayerMainFragment;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.PlayerUserModel;
import com.chu.administrator.mybaseball.R;


import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018-06-11.
 */

public class LoginFragment extends BaseFragment {
    FirebaseAuth mAuth;

    SignInButton googleLoginBtn;
    Button guestLoginBtn;
    final String TAG="Login Fragment";
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = true;
        return inflater.inflate(R.layout.login_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        googleLoginBtn = (SignInButton) view.findViewById(R.id.googleLoginBtn);
        guestLoginBtn = (Button) view.findViewById(R.id.guestLoginBtn);

        mAuth = FirebaseAuth.getInstance();

        googleLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableUI(false);
                bInstance.signInGoogle();
            }
        });

        guestLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableUI(false);
                bInstance.signInGuest();
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    public void signInResult(boolean isOk, FirebaseUser user) {
        if (isOk){
            bInstance.saveUserId(user.getUid());
            uploadLoginInfo(user);
        }else {
            bInstance.popupGeneralWindow("sign in failed");
            enableUI(true);
        }
    }

    private void uploadLoginInfo(final FirebaseUser user){
        bInstance.showProgressBar();

        Map<String, Object> userInfo = new HashMap<>();

        String collectionPath;

        if (TextUtils.isEmpty(user.getEmail())){
            userInfo.put("uid", user.getUid());
            userInfo.put("version", GlobalValue.version+"");
            userInfo.put("deviceId", bInstance.getDeviceID());
            userInfo.put("timestamp", FieldValue.serverTimestamp());
            collectionPath = user.getUid();
        }else {
            userInfo.put("email", user.getEmail());
            userInfo.put("name", user.getDisplayName());
            userInfo.put("uid", user.getUid());
            userInfo.put("version", GlobalValue.version+"");
            userInfo.put("deviceId", bInstance.getDeviceID());
            userInfo.put("timestamp", FieldValue.serverTimestamp());
            collectionPath = user.getEmail();
        }

        bInstance.getDb().collection("loginInfo").document(bInstance.getLocalDate()).collection(collectionPath).add(userInfo).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                checkIsBanned(user.getUid());
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                bInstance.hideProgressBar();
                enableUI(true);
                bInstance.popupGeneralWindow("Error in Sign up..");
            }
        });
    }
    private void enableUI(boolean enable){
        googleLoginBtn.setEnabled(enable);
        guestLoginBtn.setEnabled(enable);
    }

    private void checkVersion(){
        bInstance.showProgressBar();
        DocumentReference docRef = bInstance.getDb().collection("version").document(GlobalValue.version+"");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
//                        Log.d("j13", "version matched");
                        checkIsFirstLogin(bInstance.getUID());
                    } else {
                        bInstance.popupGeneralWindow("Please update new1 version from play store.");
                        bInstance.hideProgressBar();
                        enableUI(true);
                        //TODO implement update playstore uri
//                        Intent intent = new1 Intent(Intent.ACTION_VIEW);
//                        intent.setData(Uri.parse("market://details?id=com.chu.administrator.baseball9"));
//                        startActivity(intent);
                    }
                } else {
                    bInstance.popupGeneralWindow("Error in Sign up..");
                    bInstance.hideProgressBar();
                    enableUI(true);
                }
            }
        });
    }

    private void checkIsBanned(final String uID){
        bInstance.showProgressBar();
        DocumentReference docRef = bInstance.getDb().collection("banlist").document(uID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        bInstance.popupGeneralWindow("You are banned for illegal activity. \n email : java.jaeyoungchu@gmail.com");
                        bInstance.hideProgressBar();
                        enableUI(true);
                    } else {
                        checkVersion();
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                    bInstance.popupGeneralWindow("Error in Sign up..");
                    bInstance.hideProgressBar();
                    enableUI(true);
                }
            }
        });
    }

    private void checkIsFirstLogin(final String uID){
        bInstance.showProgressBar();
        DocumentReference docRef = bInstance.getDb().collection("userModel").document(uID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
//                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        int mode = Integer.parseInt(document.get("mode").toString());

                        bInstance.setUserModelFromServer(document);



                        if (mode == GlobalValue.PLAYERMODE){
                            bInstance.addAsRootFragment(new PlayerMainFragment());
                        }else {
                            bInstance.addAsRootFragment(new ManagerMainFragment());
                        }

                        bInstance.hideProgressBar();
                        enableUI(true);

                    } else {
                        bInstance.hideProgressBar();
                        enableUI(true);
                        bInstance.addAsRootFragment(new InitModeFragment());
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                    bInstance.popupGeneralWindow("Error in Sign up..");
                    bInstance.hideProgressBar();
                    enableUI(true);
                }
            }
        });
    }


}
