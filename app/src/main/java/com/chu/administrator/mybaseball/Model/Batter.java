package com.chu.administrator.mybaseball.Model;


import com.chu.administrator.mybaseball.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-06-17.
 */

public class Batter{

    private String teamName;
    private String batterName,uID,cardToken;
    private ArrayList<TitleItem> titleItemArrayList;
    private int defensePosition,primaryPosition,batterType;
    private int playerExp,playerLevel;
    private int stealOrder,buntOrder,battingOrder;
    public int contact,power,eye,clutch,speed,defense;

    private ArrayList<DefencePosition> positionLVList;
    public BatterMatchRecord matchRecord, seasonRecord;
    private Item bat,glasses,glove;
    private BatterSkill batterSkill1,batterSkill2,batterSkill3,batterSkill4;

    int item;

    public Batter() {
        matchRecord = new BatterMatchRecord();
        seasonRecord = new BatterMatchRecord();
        positionLVList = new ArrayList<>();
        titleItemArrayList = new ArrayList<>();
    }

    public Batter(String uid,String cardToken,String name,int primaryPosition, int contact, int power, int eye,int clutch, int speed, int defense,int batterType) {
        this.teamName = "";
        matchRecord = new BatterMatchRecord();
        seasonRecord = new BatterMatchRecord();
        positionLVList = new ArrayList<>();
        this.playerLevel = 1;
        this.playerExp = 0;
        this.battingOrder = -1;
        this.uID = uid;
        this.cardToken = cardToken;

        this.stealOrder = 1;
        this.buntOrder = 1;
        this.primaryPosition = primaryPosition;
        this.defensePosition = -1;
        this.batterName = name;
        this.contact = contact;
        this.power = power;
        this.eye = eye;
        this.clutch = clutch;
        this.speed = speed;
        this.defense = defense;
        this.batterType = batterType;
        positionLVList = new ArrayList<>();
        titleItemArrayList = new ArrayList<>();
        initDefencePositions();
        setPrimaryPosition(primaryPosition);
    }



    public Batter(int primaryPosition, int batterType, int playerExp, int playerLevel, ArrayList<DefencePosition> positionLVList, String batterName, String uID, String cardToken, int contact, int power, int eye, int clutch, int speed, int defense, Item bat, Item glasses, Item glove, BatterSkill batterSkill1, BatterSkill batterSkill2, BatterSkill batterSkill3, BatterSkill batterSkill4) {
        this.defensePosition = -1;
        this.primaryPosition = primaryPosition;
        this.batterType = batterType;
        this.playerExp = playerExp;
        this.playerLevel = playerLevel;
        this.positionLVList = positionLVList;
        this.battingOrder = -1;
        this.teamName = "";
        this.stealOrder = 1;
        this.buntOrder = 1;
        this.batterName = batterName;
        this.uID = uID;
        this.cardToken = cardToken;
        this.contact = contact;
        this.power = power;
        this.eye = eye;
        this.clutch = clutch;
        this.speed = speed;
        this.defense = defense;

        this.matchRecord = new BatterMatchRecord();
        this.seasonRecord = new BatterMatchRecord();
        this.bat = bat;
        this.glasses = glasses;
        this.glove = glove;
        this.batterSkill1 = batterSkill1;
        this.batterSkill2 = batterSkill2;
        this.batterSkill3 = batterSkill3;
        this.batterSkill4 = batterSkill4;
    }

    public ArrayList<TitleItem> getTitleItemArrayList() {
        return titleItemArrayList;
    }

    public void addTitle(TitleItem titleItem){
        titleItemArrayList.add(0,titleItem);
    }

    public void setTitleItemArrayList(ArrayList<TitleItem> titleItemArrayList) {
        this.titleItemArrayList = titleItemArrayList;
    }

    public int getBattingOrder() {
        return battingOrder;
    }

    public void setBattingOrder(int battingOrder) {
        this.battingOrder = battingOrder;
    }

    public int getPrimaryPosition() {
        return primaryPosition;
    }

    public int getBatterType() {
        return batterType;
    }

    public void setBatterType(int batterType) {
        this.batterType = batterType;
    }

    public void setPositionLVList(ArrayList<DefencePosition> positionLVList) {
        this.positionLVList = positionLVList;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public ArrayList<DefencePosition> getPositionLVList() {
        return positionLVList;
    }

    public BatterMatchRecord getMatchRecord() {
        return matchRecord;
    }

    public int getItem() {
        return item;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public BatterSkill getBatterSkill1() {
        return batterSkill1;
    }

    public BatterSkill getBatterSkill2() {
        return batterSkill2;
    }

    public BatterSkill getBatterSkill3() {
        return batterSkill3;
    }

    public BatterSkill getBatterSkill4() {
        return batterSkill4;
    }

    public void setBatterSkill1(BatterSkill batterSkill1) {
        this.batterSkill1 = batterSkill1;
    }

    public void setBatterSkill2(BatterSkill batterSkill2) {
        this.batterSkill2 = batterSkill2;
    }

    public void setBatterSkill3(BatterSkill batterSkill3) {
        this.batterSkill3 = batterSkill3;
    }

    public void setBatterSkill4(BatterSkill batterSkill4) {
        this.batterSkill4 = batterSkill4;
    }

    public void setBat(Item bat) {
        this.bat = bat;
    }

    public void setGlasses(Item glasses) {
        this.glasses = glasses;
    }

    public void setGlove(Item glove) {
        this.glove = glove;
    }

    public Item getBat() {
        return bat;
    }

    public Item getGlasses() {
        return glasses;
    }

    public Item getGlove() {
        return glove;
    }

    public int getPlayerLevel() {
        return playerLevel;
    }



    public void setPlayerLevel(int playerCurrentLevel) {
        this.playerLevel = playerCurrentLevel;
    }

    public void addPlayerExp(int exp ){
        setPlayerExp(getPlayerExp()+exp);
    }
    public int getPlayerExp() {
        return playerExp;
    }

    public void setPlayerExp(int playerExp) {
        this.playerExp = playerExp;
    }


    public String getuID() {
        return uID;
    }


    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }


    public void saveMatchRecordToSeasonRecord(BatterMatchRecord newMatchRecord){
        this.seasonRecord.saveMatchRecord(newMatchRecord);
    }

    public ArrayList<Integer> getLevelExpArray(){
        ArrayList<Integer> expArray = new ArrayList<>();
        int expGravity;
        for (int i = 1 ; i < 101 ; i++){
            if (i<50){
                expGravity = i/10 +1; //1,2,3,4,5....
            }else {
                expGravity = i/10 +10;//15,16,17,18,19
            }

            expArray.add(expGravity*100);
        }

        return expArray;
    }

    public int getCurrentPlayerLevelByExp(){
        int level = 0;
        int sum= 0;
        ArrayList<Integer> arry = getLevelExpArray();
        for (int i = 0 ; i < arry.size() ; i++){
            sum += arry.get(i);
            if (sum >= playerExp){
                level = i+1;
                break;
            }
        }
        return level;
    }

    public int getNotUsedExp(){
        int notUsedExp = 0;
        notUsedExp = playerExp-getUsedExp();
        return notUsedExp;
    }
    private int getUsedExp(){
        int usedExp = 0;
        ArrayList<Integer> arry = getLevelExpArray();
        for (int i = 0 ; i < arry.size() ; i++){
            if (i < playerLevel-1){
                usedExp += arry.get(i);
            }else {
                break;
            }
        }
        return usedExp;
    }


    public int getCurrentLevelExp(){

        ArrayList<Integer> arry = getLevelExpArray();

        return arry.get(playerLevel-1);
    }

    public int getUpgradePoint(){
        return getCurrentPlayerLevelByExp() - playerLevel;
    }

    public float getTrueDefenseInhundred(){
        // min 0.01f  && max 0.9f
        float returnfloat = (float)((float)getDefenceWithPosition()-(float)GlobalValue.minDef)/(float)((float)GlobalValue.maxDef-(float)GlobalValue.minDef)*GlobalValue.defenseGravity;
        if (returnfloat > 0.8f){
            return 0.8f;
        }else {
            return returnfloat;
        }
    }

    public float getErrorRate(){
        float val=0f;

        float gap = 100f - GlobalValue.positionMinFieldingRate[defensePosition]; // ex 18

        float plus = gap * getTrueDefenseInhundred(); // ex 0.2 ~ 18

        val = GlobalValue.positionMinFieldingRate[defensePosition] + plus;

        return val;
    }

    public BatterMatchRecord getSeasonRecord(){
        return seasonRecord;
    }
    public float getTrueSpeed(){
        return (float)(speed- GlobalValue.minSpd)/(GlobalValue.maxSpd-GlobalValue.minSpd)*100*GlobalValue.batterGravityForSpeed;
    }

    public int getDefensePosition() {
        return defensePosition;
    }

    public float getTrueLeagueEveSpeed(){
        return (float)(GlobalValue.avgLeagueSpd - GlobalValue.minSpd)/(GlobalValue.maxSpd-GlobalValue.minSpd)*100*GlobalValue.batterGravityForSpeed;
    }

    public float getStrikeOutValue(){
        float strikeOutValue;
        strikeOutValue = (getContactWithItem()*0.3f + getEyeWithItem()*0.7f);
        return strikeOutValue;
    }

    public float getHRValue(){
        return (getPowerWithItem()*1.0f);
    }

    public float getGapValue(){
        return (getPowerWithItem()*1.0f);
    }

    public float getBBValue(){
        return (getEyeWithItem()*1.0f);
    }
    public float getHITValue(){
        return (getContactWithItem()*0.9f+ getEyeWithItem()*0.05f+ getSpeedWithItem()*0.05f)*GlobalValue.batterGravityForHit;
    }

    public int getSumStat(){
        int r = getContactWithItem() + getPowerWithItem() + getClutchWithItem() + getEyeWithItem() + getSpeedWithItem() + getDefenseWithItem();
        return r;
    }

    public int getAverageStat(){
        int r = getContactWithItem() + getPowerWithItem() + getClutchWithItem() + getEyeWithItem() + getSpeedWithItem() + getDefenseWithItem();
        return (int) r/6;
    }
    public int getDefenceWithPosition(){
        float val = defense * getPositionInfoByInt(defensePosition).getPositionPercent();

        return (int)val;
    }

    public int getClutchWithItem() {
        item = 0;
        if (bat != null){
            item += bat.getClutch();
        }
        if (glasses != null){
            item += glasses.getClutch();
        }
        if (glove != null){
            item += glove.getClutch();
        }
        return clutch+item;
    }


    public int getPowerWithItem(){
        item = 0;
        if (bat != null){
            item += bat.getPower();
        }
        if (glasses != null){
            item += glasses.getPower();
        }
        if (glove != null){
            item += glove.getPower();
        }
        return power+item;
    }


    public int getContactWithItem() {
        item = 0;
        if (bat != null){
            item += bat.getContact();
        }
        if (glasses != null){
            item += glasses.getContact();
        }
        if (glove != null){
            item += glove.getContact();
        }
        return contact+item;
    }


    public int getEyeWithItem() {
        item = 0;
        if (bat != null){
            item += bat.getEye();
        }
        if (glasses != null){
            item += glasses.getEye();
        }
        if (glove != null){
            item += glove.getEye();
        }
        return eye+item;
    }

    public int getSpeedWithItem() {
        item = 0;
        if (bat != null){
            item += bat.getSpeed();
        }
        if (glasses != null){
            item += glasses.getSpeed();
        }
        if (glove != null){
            item += glove.getSpeed();
        }
        return speed+item;
    }

    // in player Mode
    public int getDefenseWithItem() {
        item = 0;
        if (bat != null){
            item += bat.getDefence();
        }
        if (glasses != null){
            item += glasses.getDefence();
        }
        if (glove != null){
            item += glove.getDefence();
        }
        return defense+item;
    }

    // in Manager Mode
    public int getDefenceInPositionWithItem(){
        item = 0;
        if (bat != null){
            item += bat.getDefence();
        }
        if (glasses != null){
            item += glasses.getDefence();
        }
        if (glove != null){
            item += glove.getDefence();
        }

        return getDefenceWithPosition()+item;
    }

    public void setStealOrder(int stealOrder) {
        this.stealOrder = stealOrder;
    }

    public void setBuntOrder(int buntOrder) {
        this.buntOrder = buntOrder;
    }

    public int getStealOrder() {
        return stealOrder;
    }

    public int getBuntOrder() {
        return buntOrder;
    }

    public String getBatterName() {
        return batterName;
    }



    public void setBatterName(String batterName) {
        this.batterName = batterName;
    }

    public boolean upgradePlayer(int contact,int power,int eye,int clutch,int speed,int defense){
        if (getUpgradePoint() == 0){
            return false;
        }
        if (isMaxPlayerLevel()){
            return false;
        }else {
            setPlayerLevel(playerLevel+1);
            addContact(contact);
            addPower(power);
            addEye(eye);
            addClutch(clutch);
            addSpeed(speed);
            addDefence(defense);
            return true;
        }
    }

    public void addContact(int add){
        this.contact +=add;
    }
    public void addPower(int add){
        this.power +=add;
    }
    public void addEye(int add){
        this.eye +=add;
    }
    public void addClutch(int add){
        this.clutch +=add;
    }
    public void addSpeed(int add){
        this.speed +=add;
    }
    public void addDefence(int add){
        this.defense +=add;
    }
    public boolean isMaxPlayerLevel(){
        if (playerLevel >= 100){
            return true;
        }else {
            return false;
        }
    }
    public void setContact(int contact) {
        this.contact = contact;
    }

    public void setClutch(int clutch) {
        this.clutch = clutch;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setEye(int eye) {
        this.eye = eye;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }


    public void setDefensePosition(int defensePosition) {
        this.defensePosition = defensePosition;
    }

    public void setMatchRecord(BatterMatchRecord matchRecord) {
        this.matchRecord = matchRecord;
    }

    public void setSeasonRecord(BatterMatchRecord seasonRecord) {
        this.seasonRecord = seasonRecord;
    }
    private void initDefencePositions(){
        for (int i = 0 ; i < 8 ; i++){
            positionLVList.add(new DefencePosition(i,0));
        }
    }
    public void setPrimaryPosition(int positionInt){
        getPositionInfoByInt(positionInt).setPositionExp(5000);
    }
    public void putExpToPosition(int positionInt,int exp){
        getPositionInfoByInt(positionInt).putExp(exp);
    }

    private DefencePosition getPositionInfoByInt(int position){
        DefencePosition retrunVal = new DefencePosition();
        for (DefencePosition p : positionLVList){
            if (p.getPositionInt() == position){
                retrunVal = p;
                break;
            }
        }
        return retrunVal;
    }

    public class DefencePosition{
        private int positionInt;
        private int positionExp;  //C,B,A,S  //85,90,95,100
        public float[] expPerLevel = {500f,2000f,5000f};

        public DefencePosition() {
        }

        public DefencePosition(int positionInt, int positionExp) {
            this.positionInt = positionInt;
            this.positionExp = positionExp;
        }
        public float getPositionPercent(){
            float returnVal;
            if (positionExp< expPerLevel[0]){
                returnVal = 0.70f;
            }else if (positionExp<expPerLevel[1]){
                returnVal = 0.80f;
            }else if (positionExp<expPerLevel[2]){
                returnVal = 0.90f;
            }else {
                returnVal = 1.00f;
            }
            return returnVal;
        }

        public void putExp(int exp){
            setPositionExp(getPositionExp()+exp);
        }

        public int getPositionInt() {
            return positionInt;
        }

        public int getPositionExp() {
            return positionExp;
        }

        public void setPositionExp(int positionExp) {
            this.positionExp = positionExp;
        }
        public boolean isMaxExp(){
            if (positionExp >= expPerLevel[2]){
                return true;
            }else {
                return false;
            }
        }

        public String getPositionLVString(){
            String returnVal;
            if (positionExp<expPerLevel[0]){
                returnVal = "C";
            }else if (positionExp<expPerLevel[1]){
                returnVal = "B";
            }else if (positionExp<expPerLevel[2]){
                returnVal = "A";
            }else {
                returnVal = "S";
            }
            return returnVal;
        }


        public int getPositionLvImage(){
             int returnVal;
            if (positionExp<expPerLevel[0]){
                returnVal = R.drawable.position_c;
            }else if (positionExp<expPerLevel[1]){
                returnVal = R.drawable.position_b;
            }else if (positionExp<expPerLevel[2]){
                returnVal = R.drawable.position_a;
            }else {
                returnVal = R.drawable.position_s;
            }
            return returnVal;
        }
    }

    public int getPlayerBackgroundResource(){
        int val;
        if (playerLevel <30){
            val = R.drawable.playerlevel1;
        }else if (playerLevel <60){
            val = R.drawable.playerlevel2;
        }else{
            val = R.drawable.playerlevel3;
        }
        return val;
    }

    public int getPlayerSkillBG(int level){
        int val;
        if (level == 0){
            val = R.drawable.skilllevel0;
        }else if (level == 1){
            val = R.drawable.skilllevel1;
        }else if (level == 2){
            val = R.drawable.skilllevel2;
        }else{
            val = R.drawable.skilllevel3;
        }
        return val;
    }

    @Override
    public String toString() {
        String reStr = "";

        reStr += "Batter{" +
                "batterName='" + batterName + '\'' +
                "uid='" + uID + '\'' +
                "playerLevel='" + playerLevel + '\'' +
                "getUpgradePoint='" + getUpgradePoint() + '\'' +
                "playerExp='" + playerExp + '\'' +
                "getNotUsedExp='" + getNotUsedExp() + '\'' +
                "bat= " + (bat != null) + '\'' +
                "glasses= " + (glasses != null) + '\'' +
                "glove= " + (glove != null) + '\'' +
                "batterSkill1= " + (batterSkill1 != null) + '\'' +
                "batterSkill2= " + (batterSkill2 != null) + '\'' +
                "batterSkill3= " + (batterSkill3 != null) + '\'' +
                "batterSkill4= " + (batterSkill4 != null) + '\'' +
                "getCurrentLevelExp='" + getCurrentLevelExp() + '\'' +
                ", contact=" + contact +
                ", power=" + power +
                ", eye=" + eye +
                ", clutch=" + clutch +
                ", speed=" + speed +
                ", defense=" + defense +
                ", getCurrentPlayerLevelByExp='" + getCurrentPlayerLevelByExp() + '\'' +
                ", seasonRecord='" + seasonRecord.toString() + '\'' +
                '}';
        String pStr = "\n";
        for (DefencePosition d : positionLVList){
            pStr += d.getPositionInt() +" / " + d.getPositionExp()+"\n";
        }

        return reStr+pStr;
    }
}
