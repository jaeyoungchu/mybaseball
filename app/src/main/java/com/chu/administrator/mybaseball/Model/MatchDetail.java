package com.chu.administrator.mybaseball.Model;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-06-28.
 */

public class MatchDetail {
    public int homeTeamScore, homeTeamHit, homeTeamHR,homeTeamBB, homeTeamError;
    public int awayTeamScore, awayTeamHit, awayTeamHR,awayTeamBB, awayTeamError;
    public boolean isFirstBon,isSecondBon,isThirdBon;
    public Batter firstBasePlayer,secondBasePlayer,thirdBasePlayer;
    public Batter currentBatter;
    public Pitcher currentPitcher,homeStartPitcher,awayStartPitcher;
    public ArrayList<Batter> homeBatterRoster, awayBatterRoaster;
    public String result ="";
    public Team homeTeam,awayTeam;
    public ArrayList<Integer> homeTeamInningScore,awayTeamInningScore;
    public boolean isShowingBatterSkill1,isShowingBatterSkill2,isShowingPitcherSkill1,isShowingPitcherSkill2;
    public int inning,outCount;
    public boolean isFristHalf;
    public Batter defenceBatter;

    public MatchDetail(ArrayList<Integer> homeTeamInningScore,ArrayList<Integer> awayTeamInningScore, Pitcher homeStartPitcher,Pitcher awayStartPitcher,Batter defenceBatter,int inning,int outCount, boolean isFristHalf,int homeTeamScore, int homeTeamHit, int homeTeamHR, int homeTeamBB, int homeTeamError, int awayTeamScore, int awayTeamHit, int awayTeamHR, int awayTeamBB, int awayTeamError, boolean isFirstBon, boolean isSecondBon, boolean isThirdBon, Batter firstBasePlayer, Batter secondBasePlayer, Batter thirdBasePlayer, Batter currentBatter, Pitcher currentPitcher, String result, Team homeTeam, Team awayTeam,boolean isShowingBatterSkill1,boolean isShowingBatterSkill2,boolean isShowingPitcherSkill1,boolean isShowingPitcherSkill2) {
        this.homeTeamInningScore = homeTeamInningScore;
        this.awayTeamInningScore = awayTeamInningScore;
        this.homeStartPitcher = homeStartPitcher;
        this.awayStartPitcher = awayStartPitcher;
        this.defenceBatter = defenceBatter;
        this.inning = inning;
        this.outCount = outCount;
        this.isFristHalf = isFristHalf;
        this.homeTeamScore = homeTeamScore;
        this.homeTeamHit = homeTeamHit;
        this.homeTeamHR = homeTeamHR;
        this.homeTeamBB = homeTeamBB;
        this.homeTeamError = homeTeamError;
        this.awayTeamScore = awayTeamScore;
        this.awayTeamHit = awayTeamHit;
        this.awayTeamHR = awayTeamHR;
        this.awayTeamBB = awayTeamBB;
        this.awayTeamError = awayTeamError;
        this.isFirstBon = isFirstBon;
        this.isSecondBon = isSecondBon;
        this.isThirdBon = isThirdBon;
        this.firstBasePlayer = firstBasePlayer;
        this.secondBasePlayer = secondBasePlayer;
        this.thirdBasePlayer = thirdBasePlayer;
        this.currentBatter = currentBatter;
        this.currentPitcher = currentPitcher;
        this.homeBatterRoster = homeTeam.getBatterSquad();
        this.awayBatterRoaster = awayTeam.getBatterSquad();
        this.result = result;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.isShowingBatterSkill1 = isShowingBatterSkill1;
        this.isShowingBatterSkill2 = isShowingBatterSkill2;
        this.isShowingPitcherSkill1 = isShowingPitcherSkill1;
        this.isShowingPitcherSkill2 = isShowingPitcherSkill2;
    }

    @Override
    public String toString() {
        return "inning " + inning +
                " Out " + outCount +
                (isFristHalf ? "  first half" : " second half")+ '\n' +
                "  home=" + homeTeamScore +
                ", away=" + awayTeamScore + '\n' +

                ", isFirstBon=" + (isFirstBon ? firstBasePlayer.getBatterName() : "null") +
                ", isSecondBon=" + (isSecondBon ? secondBasePlayer.getBatterName() : "null") +
                ", isThirdBon=" + (isThirdBon ? thirdBasePlayer.getBatterName() : "null") + '\n' +

                ", result='" + result + '\'' +'\n' +
                ", currentBatter=" + currentBatter.getBatterName() +
                ", Skill 1 =" + (isShowingBatterSkill1 ? currentBatter.getBatterSkill1().getSkillName() : " ") +'\n' +
                ", Skill 2 =" + (isShowingBatterSkill2 ? currentBatter.getBatterSkill2().getSkillName() : " ") +'\n' +
                ", currentPitcher=" + currentPitcher.getPitcherName() +
                ", Skill 1 =" + (isShowingPitcherSkill1 ? currentPitcher.getPitcherSkill1().getSkillName() : " ") +'\n' +
                ", Skill 2 =" + (isShowingPitcherSkill2 ? currentPitcher.getPitcherSkill2().getSkillName() : " ") +'\n' +
                ", defence player= " + (defenceBatter != null ? GlobalValue.getPositionString(defenceBatter.getDefensePosition()) : " ")
                ;
    }
}
