package com.chu.administrator.mybaseball.Fragment.ManagerMode;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.chu.administrator.mybaseball.Fragment.BaseFragment;
import com.chu.administrator.mybaseball.Fragment.MarketFragment;
import com.chu.administrator.mybaseball.Fragment.PlayFragment;
import com.chu.administrator.mybaseball.Helper.MatchLogic;
import com.chu.administrator.mybaseball.Model.Batter;
import com.chu.administrator.mybaseball.Model.GlobalValue;
import com.chu.administrator.mybaseball.Model.Pitcher;
import com.chu.administrator.mybaseball.Model.Team;
import com.chu.administrator.mybaseball.R;


import java.util.ArrayList;

/**
 * Created by Administrator on 2018-06-14.
 */

public class ManagerMainFragment extends BaseFragment {
    Button signOutBtn,playBtn,marketBtn,myTeamBtn;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isHiding = false;
        return inflater.inflate(R.layout.main_manager_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        marketBtn = (Button) view.findViewById(R.id.marketMBtn);
        signOutBtn = (Button) view.findViewById(R.id.signOutBtn);
        playBtn = (Button) view.findViewById(R.id.playBtn);
        myTeamBtn = (Button) view.findViewById(R.id.myTeamBtn);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playGame();
            }
        });
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                playGame();
                bInstance.signOutGoogle();
//                Log.e("j13 ",""+bInstance.getmManagerUserModel().getManager().toString());
            }
        });
        marketBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bInstance.addFragment(new MarketFragment());
            }
        });
        myTeamBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bInstance.addFragment(new MyTeamFragment());
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    private void playGame(){
        Team a = new Team("mine",createBatterSquad("mine"),createPitcherSuqad("mine"));
        Team b = new Team("yours",createBatterSquad("yours"),createPitcherSuqad("yours"));

        MatchLogic m;
        for (int i = 0 ; i < 1 ; i++){
            m = new MatchLogic(a,b);
            bInstance.setMatchDetails(m.playGame());
        }
        Log.e("j13 ","size "+bInstance.getMatchDetails().size());
        bInstance.addAsRootFragment(new PlayFragment());
    }

    private ArrayList<Pitcher> createPitcherSuqad(String teamName){
        ArrayList<Pitcher> pitcherArrayList = new ArrayList<>();

        Pitcher SP1 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1sp1Sub"+teamName,50,50,50,50,70,50, GlobalValue.STARTER_PITCHER);
        SP1.setPositionInt(0);
        Pitcher SP2 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1sp2Sub"+teamName,50,50,50,50,50,50,GlobalValue.STARTER_PITCHER);
        SP2.setPositionInt(1);
        Pitcher SP3 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1sp3Sub"+teamName,50,50,50,50,50,50,GlobalValue.STARTER_PITCHER);
        SP3.setPositionInt(2);
        Pitcher SP4 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1sp4Sub"+teamName,50,50,50,50,50,50,GlobalValue.STARTER_PITCHER);
        SP4.setPositionInt(3);
        Pitcher SP5 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1sp5Sub"+teamName,50,50,50,50,50,50,GlobalValue.STARTER_PITCHER);
        SP5.setPositionInt(4);
        Pitcher RP1 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1rp1Sub"+teamName,50,50,50,50,50,50,GlobalValue.RELIEF_PITCHER);
        RP1.setPositionInt(5);
        Pitcher RP2 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1rp2Sub"+teamName,50,50,50,50,50,50,GlobalValue.RELIEF_PITCHER);
        RP2.setPositionInt(6);
        Pitcher RP3 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1rp3Sub"+teamName,50,50,50,50,50,50,GlobalValue.RELIEF_PITCHER);
        RP3.setPositionInt(7);
        Pitcher RP4 = new Pitcher(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"1cl4Sub"+teamName,50,50,50,50,50,50,GlobalValue.RELIEF_PITCHER);
        RP4.setPositionInt(8);

        pitcherArrayList.add(SP1);
        pitcherArrayList.add(SP2);
        pitcherArrayList.add(SP3);
        pitcherArrayList.add(SP4);
        pitcherArrayList.add(SP5);
        pitcherArrayList.add(RP1);
        pitcherArrayList.add(RP2);
        pitcherArrayList.add(RP3);
        pitcherArrayList.add(RP4);

        for (Pitcher p : pitcherArrayList){
            Log.e("j13 ","token "+p.getCardToken());
        }

        return pitcherArrayList;
    }


    private ArrayList<Batter> createBatterSquad(String teamName){
        ArrayList<Batter> batterArrayList = new ArrayList<>();
        Batter C = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"subCatcher"+teamName,GlobalValue.catcher,50,50,50,70,50,50,GlobalValue.GENERAL_BATTER);        C.setBattingOrder(0);
        C.setDefensePosition(GlobalValue.catcher);

        Batter B1 = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"sub1B"+teamName,GlobalValue.firstBase,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        B1.setBattingOrder(1);
        B1.setDefensePosition(GlobalValue.firstBase);

        Batter B2 = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"sub2B"+teamName,GlobalValue.secondBase,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        B2.setBattingOrder(2);
        B2.setDefensePosition(GlobalValue.secondBase);

        Batter B3 = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"sub3B"+teamName,GlobalValue.thirdBase,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        B3.setBattingOrder(3);
        B3.setDefensePosition(GlobalValue.thirdBase);

        Batter SS = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"subSS"+teamName,GlobalValue.shortStop,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        SS.setBattingOrder(4);
        SS.setDefensePosition(GlobalValue.shortStop);

        Batter LF = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"subLF"+teamName,GlobalValue.leftField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        LF.setBattingOrder(5);
        LF.setDefensePosition(GlobalValue.leftField);

        Batter CF = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"subCF"+teamName,GlobalValue.centerField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        CF.setBattingOrder(6);
        CF.setDefensePosition(GlobalValue.centerField);

        Batter RF = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"subRF"+teamName,GlobalValue.rightField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        RF.setBattingOrder(7);
        RF.setDefensePosition(GlobalValue.rightField);

        Batter DH = new Batter(bInstance.getUID(),"ai"+bInstance.createPlayerToken(),"subDH"+teamName,GlobalValue.centerField,50,50,50,50,50,50,GlobalValue.GENERAL_BATTER);
        DH.setBattingOrder(8);
        DH.setDefensePosition(GlobalValue.DH);

        batterArrayList.add(C);
        batterArrayList.add(B1);
        batterArrayList.add(B2);
        batterArrayList.add(B3);
        batterArrayList.add(SS);
        batterArrayList.add(LF);
        batterArrayList.add(CF);
        batterArrayList.add(RF);
        batterArrayList.add(DH);

        return batterArrayList;
    }

    @Override
    public void onResume() {
        bInstance.saveModeToRealm("manager");
        super.onResume();
    }
}
