package com.chu.administrator.mybaseball.Model;

import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-06-25.
 */

public class ManagerUserModel {
    private String uid,devideId,teamName;
    private int cash,gp,modeType;
    private Manager manager;
    private ArrayList<Batter> subBatterSquad;
    private ArrayList<Pitcher> subPitcherSquad;
    private Team team;

    public ManagerUserModel(String uid, String devideId, int cash, int gp, int modeType) {
        this.uid = uid;
        this.devideId = devideId;
        this.cash = cash;
        this.gp = gp;
        this.modeType = modeType;
    }

    public ManagerUserModel(String uid, String devideId, int cash, int gp, int modeType, Manager manager, ArrayList<Batter> batterSquad, ArrayList<Batter> subBatterSquad, ArrayList<Pitcher> pitcherSquad, ArrayList<Pitcher> subPitcherSquad) {
        this.uid = uid;
        this.devideId = devideId;
        this.cash = cash;
        this.gp = gp;
        this.modeType = modeType;
        this.manager = manager;
        this.subBatterSquad = subBatterSquad;
        this.subPitcherSquad = subPitcherSquad;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getUid() {
        return uid;
    }

    public String getDevideId() {
        return devideId;
    }

    public int getCash() {
        return cash;
    }

    public int getGp() {
        return gp;
    }

    public int getModeType() {
        return modeType;
    }

    public Manager getManager() {
        return manager;
    }

    public Pitcher getPitcherByPosition(int position){
        Pitcher pitcher = null;

        for (Pitcher p : team.getPitcherSquad()){
            if (p.getPositionInt() == position){
                pitcher =p;
                break;
            }
        }
        return pitcher;
    }

    public Batter getBatterByPosition(int position){
        Batter batter = null;
        for (Batter b : team.getBatterSquad()){
            if (b.getDefensePosition() == position){
                batter =b;
                break;
            }
        }
        return batter;
    }

    public void setBatter(Batter batter){
        for(int i = 0 ; i < team.getBatterSquad().size(); i++){
            if (team.getBatterSquad().get(i).getCardToken().equals(batter.getCardToken())){
                team.getBatterSquad().set(i,batter);
                break;
            }
        }
    }

    public void setPitcher(Pitcher pitcher){
        for(int i = 0 ; i < team.getPitcherSquad().size(); i++){
            if (team.getPitcherSquad().get(i).getCardToken().equals(pitcher.getCardToken())){
                team.getPitcherSquad().set(i,pitcher);
                break;
            }
        }
    }

    public ArrayList<Batter> getSubBatterSquad() {
        return subBatterSquad;
    }


    public ArrayList<Pitcher> getSubPitcherSquad() {
        return subPitcherSquad;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setDevideId(String devideId) {
        this.devideId = devideId;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public void setGp(int gp) {
        this.gp = gp;
    }

    public void setModeType(int modeType) {
        this.modeType = modeType;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }



    public void setSubBatterSquad(ArrayList<Batter> subBatterSquad) {
        this.subBatterSquad = subBatterSquad;
    }

    public void setSubPitcherSquad(ArrayList<Pitcher> subPitcherSquad) {
        this.subPitcherSquad = subPitcherSquad;
    }
}
