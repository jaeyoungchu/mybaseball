package com.chu.administrator.mybaseball.Model;

import com.chu.administrator.mybaseball.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018-06-17.
 */

public class Pitcher {
    private String teamName;
    private String pitcherName,uID,cardToken;
    private int positionInt,playerExp;
    private int playerLevel,pitcherType;
    private int MaxPitchOrder;
    public int movement,location,stuff,velocity,mental,health;
    public int pitchCount;

    private ArrayList<TitleItem> titleItemArrayList;
    public PithcerMatchRecord matchRecord,seasonRecord;
    private Item cap,uniform,shoes;
    private PitcherSkill pitcherSkill1,pitcherSkill2,pitcherSkill3,pitcherSkill4;

    int item;

    public Pitcher() {
        matchRecord = new PithcerMatchRecord();
        seasonRecord = new PithcerMatchRecord();
        titleItemArrayList = new ArrayList<>();
    }

    public Pitcher(String uID, String cardToken,String name,int movement, int location, int velocity, int stuff,int mental, int health,int pitcherType) {
        this.teamName = "";
        this.playerLevel = 1;
        matchRecord = new PithcerMatchRecord();
        seasonRecord = new PithcerMatchRecord();
        this.positionInt = -1;
        this.MaxPitchOrder = 1;
        this.cardToken = cardToken;
        this.uID = uID;
        this.pitcherName = name;
        this.movement = movement;
        this.location = location;
        this.stuff = stuff;
        this.mental = mental;
        this.velocity = velocity;
        this.health = health;
        this.pitcherType= pitcherType;
        titleItemArrayList = new ArrayList<>();
    }

    public ArrayList<TitleItem> getTitleItemArrayList() {
        return titleItemArrayList;
    }

    public void addTitle(TitleItem titleItem){
        titleItemArrayList.add(0,titleItem);
    }

    public void setTitleItemArrayList(ArrayList<TitleItem> titleItemArrayList) {
        this.titleItemArrayList = titleItemArrayList;
    }


    public int getPitcherType() {
        return pitcherType;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public PitcherSkill getPitcherSkill1() {
        return pitcherSkill1;
    }

    public PitcherSkill getPitcherSkill2() {
        return pitcherSkill2;
    }

    public PitcherSkill getPitcherSkill3() {
        return pitcherSkill3;
    }

    public PitcherSkill getPitcherSkill4() {
        return pitcherSkill4;
    }

    public void setPitcherSkill1(PitcherSkill pitcherSkill1) {
        this.pitcherSkill1 = pitcherSkill1;
    }

    public void setPitcherSkill2(PitcherSkill pitcherSkill2) {
        this.pitcherSkill2 = pitcherSkill2;
    }

    public void setPitcherSkill3(PitcherSkill pitcherSkill3) {
        this.pitcherSkill3 = pitcherSkill3;
    }

    public void setPitcherSkill4(PitcherSkill pitcherSkill4) {
        this.pitcherSkill4 = pitcherSkill4;
    }

    public void setSeasonRecord(PithcerMatchRecord seasonRecord) {
        this.seasonRecord = seasonRecord;
    }

    public void setCap(Item cap) {
        this.cap = cap;
    }

    public void setUniform(Item uniform) {
        this.uniform = uniform;
    }

    public void setShoes(Item shoes) {
        this.shoes = shoes;
    }

    public Item getCap() {
        return cap;
    }

    public Item getUniform() {
        return uniform;
    }

    public Item getShoes() {
        return shoes;
    }

    public int getPlayerLevel() {
        return playerLevel;
    }

    public void setPlayerLevel(int playerCurrentLevel) {
        this.playerLevel = playerCurrentLevel;
    }

    public String getPositionString(){
        String val;
        if (positionInt >-1 && positionInt <5){
            val = "SP";
        }else {
            val = "RP";
        }
        return val;
    }

    public ArrayList<Integer> createLevelExpArray(){
        ArrayList<Integer> expArray = new ArrayList<>();
        int expGravity;
        for (int i = 1 ; i < 101 ; i++){
            if (i<30){
                expGravity = i/10 +1; //100,200,300,400,500....
            }else if (i<60){
                expGravity = i/10 +5; //500,600,700,800,900....
            }else if (i<90){
                expGravity = i/10 +10; //1000,2000,3000,4000,5000....
            }else {
                expGravity = i/10 +20;//2000,2100,2200,2300,2400
            }
            expArray.add(expGravity*100);
        }

        return expArray;
    }

    public int getCurrentPlayerLevelByExp(){
        int level = 0;
        int sum= 0;
        ArrayList<Integer> arry = createLevelExpArray();
        for (int i = 0 ; i < arry.size() ; i++){
            sum += arry.get(i);
            if (sum >= playerExp){
                level = i+1;
                break;
            }
        }
        return level;
    }

//    public int getCurrentExpOnLevel(){
//        int remainExp = 0;
//        int sum= 0;
//        ArrayList<Integer> arry = createLevelExpArray();
//        for (int i = 0 ; i < arry.size() ; i++){
//            sum += arry.get(i);
//            if (sum >= playerExp){
//                remainExp = playerExp - (sum-arry.get(i));
//                break;
//            }
//        }
//        return remainExp;
//    }

    public int getCurrentLevelExp(){

        ArrayList<Integer> arry = createLevelExpArray();

        return arry.get(playerLevel-1);
    }

    public int getNotUsedExp(){
        int notUsedExp = 0;
        notUsedExp = playerExp-getUsedExp();
        return notUsedExp;
    }
    private int getUsedExp(){
        int usedExp = 0;
        ArrayList<Integer> arry = createLevelExpArray();
        for (int i = 0 ; i < arry.size() ; i++){
            if (i < playerLevel-1){
                usedExp += arry.get(i);
            }else {
                break;
            }
        }
        return usedExp;
    }

    public int getPositionInt() {
        return positionInt;
    }

    public void setPositionInt(int positionInt) {
        this.positionInt = positionInt;
    }

    public String getuID() {
        return uID;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }




    public boolean upgradePlayer(int movement,int location,int stuff,int velocity,int mental,int health){
        if (getUpgradePoint() == 0){
            return false;
        }
        if (isMaxPlayerLevel()){
            return false;
        }else {
            setPlayerLevel(playerLevel+1);
            addMovement(movement);
            addLocation(location);
            addStuff(stuff);
            addVelocity(velocity);
            addMental(mental);
            addHealth(health);
            return true;
        }
    }

    public void addMovement(int add){
        this.movement +=add;
    }
    public void addLocation(int add){
        this.location +=add;
    }
    public void addStuff(int add){
        this.stuff +=add;
    }
    public void addMental(int add){
        this.mental +=add;
    }
    public void addVelocity(int add){
        this.velocity +=add;
    }
    public void addHealth(int add){
        this.health +=add;
    }
    public boolean isMaxPlayerLevel(){
        if (playerLevel >= 100){
            return true;
        }else {
            return false;
        }
    }

    public void setStuff(int stuff) {
        this.stuff = stuff;
    }

    public void setMental(int mental) {
        this.mental = mental;
    }


    public void saveMatchRecordToSeasonRecord(PithcerMatchRecord newMatchRecord){
        this.seasonRecord.saveMatchRecord(newMatchRecord);
    }

    public void setPitcherName(String pitcherName) {
        this.pitcherName = pitcherName;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public void setLocation(int location) {
        this.location = location;
    }


    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }



    public PithcerMatchRecord getSeasonRecord() {
        return seasonRecord;
    }

    public void setMatchRecord(PithcerMatchRecord matchRecord) {
        this.matchRecord = matchRecord;
    }

    public PithcerMatchRecord getMatchRecord() {
        return matchRecord;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxPitchCount(){
        int returnInt = 0;
        if (getMaxPitchOrder()==0){
            returnInt = getHealthWithItem()/5;
        }else if (getMaxPitchOrder() == 1){
            returnInt = getHealthWithItem()/3;
        }else {
            returnInt = (getHealthWithItem()/3)+3;
        }
        return returnInt;
    }

    public int getSumStat(){
        int r = getMovementWithItem() + getLocationWithItem() + getStuffWithItem() + getMentalWithItem()+ getVelocityWithItem()+ getHealthWithItem();
        return r;
    }

    public int getAverageStat(){
        int r = getMovementWithItem() + getLocationWithItem() + getStuffWithItem() + getMentalWithItem()+ getVelocityWithItem()+ getHealthWithItem();
        return (int) r/6;
    }
    public float getStrikeOutValue(){
        float strikeOutValue;
        strikeOutValue = (getMovementWithItem()*0.05f + getLocationWithItem()*0.1f + getStuffWithItem()*0.5f + getVelocityWithItem()*0.35f);
        return strikeOutValue;
    }
    public float getBBValue(){
        return (getMovementWithItem()*0.1f+ getLocationWithItem()*0.9f);
    }
    public float getHITValue(){
        return (getMovementWithItem()*0.4f+ getLocationWithItem()*0.3f+ getStuffWithItem()*0.15f+ getVelocityWithItem()*0.15f);
    }
    public float getHRValue(){
        return getStuffWithItem()*1.0f;
    }
    public float getGapValue(){
        return getStuffWithItem()*1.0f;
    }

    public int getHealthWithItem() {
        item = 0;
        if (cap != null){
            item += cap.getHealth();
        }
        if (uniform != null){
            item += uniform.getHealth();
        }
        if (shoes != null){
            item += shoes.getHealth();
        }
        return health+item;
    }

    public int getMovementWithItem() {
        item = 0;
        if (cap != null){
            item += cap.getMovement();
        }
        if (uniform != null){
            item += uniform.getMovement();
        }
        if (shoes != null){
            item += shoes.getMovement();
        }
        return movement+item;
    }

    public int getLocationWithItem() {
        item = 0;
        if (cap != null){
            item += cap.getLocation();
        }
        if (uniform != null){
            item += uniform.getLocation();
        }
        if (shoes != null){
            item += shoes.getLocation();
        }
        return location+item;
    }

    public int getStuffWithItem() {
        item = 0;
        if (cap != null){
            item += cap.getStuff();
        }
        if (uniform != null){
            item += uniform.getStuff();
        }
        if (shoes != null){
            item += shoes.getStuff();
        }
        return stuff+item;
    }

    public int getVelocityWithItem() {
        item = 0;
        if (cap != null){
            item += cap.getVelocity();
        }
        if (uniform != null){
            item += uniform.getVelocity();
        }
        if (shoes != null){
            item += shoes.getVelocity();
        }
        return velocity+item;
    }
    public int getMentalWithItem() {
        item = 0;
        if (cap != null){
            item += cap.getMental();
        }
        if (uniform != null){
            item += uniform.getMental();
        }
        if (shoes != null){
            item += shoes.getMental();
        }
        return mental+item;
    }
    public int getUpgradePoint(){
        return getCurrentPlayerLevelByExp() - playerLevel;
    }

    public int getPlayerExp() {
        return playerExp;
    }

    public void setPlayerExp(int playerExp) {
        this.playerExp = playerExp;
    }

    public void addPlayerExp(int exp ){
        setPlayerExp(getPlayerExp()+exp);
    }
    public void setMaxPitchOrder(int maxPitchOrder) {
        this.MaxPitchOrder = maxPitchOrder;
    }

    public int getMaxPitchOrder() {
        return MaxPitchOrder;
    }

    public String getPitcherName() {
        return pitcherName;
    }
    public int getPlayerBackgroundResource(){
        int val;
        if (playerLevel <30){
            val = R.drawable.playerlevel1;
        }else if (playerLevel <60){
            val = R.drawable.playerlevel2;
        }else{
            val = R.drawable.playerlevel3;
        }
        return val;
    }

    public int getPlayerSkillBG(int level){
        int val;
        if (level == 0){
            val = R.drawable.skilllevel0;
        }else if (level == 1){
            val = R.drawable.skilllevel1;
        }else if (level == 2){
            val = R.drawable.skilllevel2;
        }else{
            val = R.drawable.skilllevel3;
        }
        return val;
    }

    @Override
    public String toString() {
        String reStr = "";

        reStr += "Pitcher{" +
                "pitcherName='" + pitcherName + '\'' +
                "uid='" + uID + '\'' +
                "playerLevel='" + playerLevel + '\'' +
                "getUpgradePoint='" + getUpgradePoint() + '\'' +
                "playerExp='" + playerExp + '\'' +
                "getNotUsedExp='" + getNotUsedExp() + '\'' +
                "getCurrentLevelExp='" + getCurrentLevelExp() + '\'' +
                "cap= " + (cap != null) + '\'' +
                "uniform= " + (uniform != null) + '\'' +
                "shoes= " + (shoes != null) + '\'' +
                "pitcherSkill1= " + (pitcherSkill1 != null) + '\'' +
                "pitcherSkill2= " + (pitcherSkill2 != null) + '\'' +
                "pitcherSkill3= " + (pitcherSkill3 != null) + '\'' +
                "pitcherSkill4= " + (pitcherSkill4 != null) + '\'' +
                ", movement=" + movement +
                ", location=" + location +
                ", stuff=" + stuff +
                ", velocity=" + velocity +
                ", mental=" + mental +
                ", health=" + health +
                ", seasonRecord='" + seasonRecord.toString() + '\'' +
                ", getCurrentPlayerLevelByExp='" + getCurrentPlayerLevelByExp() + '\'' +
                '}';


        return reStr;
    }
}